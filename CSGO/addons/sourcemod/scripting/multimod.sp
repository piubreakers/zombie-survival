#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <main>
#include <cstrike>

#pragma semicolon 1


#define PLUGIN_VERSION	"1.0"
public Plugin myinfo =
{
	name = "MULTIMOD",
	author = "PIU-BREAKERS",
	description = "Multimod",
	version = PLUGIN_VERSION,
	url = ""
};


#define GG_PREMIO 500

bool deathmatch = false;
bool gungame = false;

int levels[32];
int points[32];


char weapons_order[][] = {
	"weapon_mac10",
	"weapon_mp7",
	"weapon_bizon",
	"weapon_ump45",
	"weapon_p90",
	"weapon_nova",
	"weapon_mag7",
	"weapon_xm1014",
	"weapon_sawedoff",
	"weapon_galilar",
	"weapon_famas",
	"weapon_ak47",
	"weapon_m4a1",
	"weapon_sg556",
	"weapon_aug",
	"weapon_awp",
	"weapon_m249",
	"weapon_negev",
	"weapon_glock",
	"weapon_usp_silencer",
	"weapon_tec9",
	"weapon_p250",
	"weapon_deagle",
	"weapon_fiveseven",
	"weapon_elite",
	"weapon_knife"
};

public void OnPluginStart()
{
	if(GetEngineVersion() != Engine_CSGO)
		SetFailState("This plugin is for the game CSGO only.");
		
	HookEvent("round_start", EventPostRoundStart, EventHookMode_Post);
	HookEvent("player_death", EventPlayerDeath, EventHookMode_Post);
	HookEvent("weapon_fire", EventWeaponFire, EventHookMode_Post);
	
	RegConsoleCmd("discord", infoDiscord);
	RegConsoleCmd("zp", serverInfoCmd);
	RegConsoleCmd("server", serverInfoCmd);
	RegConsoleCmd("dm", activateDeathmatch, "Activar/desactivar deathmatch", ADMFLAG_ROOT);
	RegConsoleCmd("gg", activateGungame, "Activar/desactivar gungame", ADMFLAG_ROOT);
//	RegConsoleCmd("cheat", maxlvl, "", ADMFLAG_ROOT);
	RegConsoleCmd("isdm", isDeathmatch);
	RegConsoleCmd("isgg", isGungame);
	RegConsoleCmd("isdm", isDeathmatch,);
		
	CreateTimer(120.0, TimerAdvisor, INVALID_HANDLE, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}

//public Action maxlvl(int client, int args){
//	levels[client]= 24;
//	return Plugin_Handled;
//}

public Action infoDiscord(int client, int args){
	PrintToChat(client, "%s Unite a nuestro discord oficial: https://discord.gg/ndKfkfD", SERVERSTRING);
	return Plugin_Handled;
}

public Action EventPostRoundStart(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	PrintToChatAll("%s Bienvenidos a Piu-Breakers!", SERVERSTRING);
	PrintToChatAll("%s Unite a nuestro discord oficial: https://discord.gg/ndKfkfD", SERVERSTRING);
	if(gungame){
	for(int i = 1; i <= 32; i++)
		if(IsPlayerExist(i, true)){
			giveWeapons(i);
		}
	}
}

public Action TimerAdvisor(Handle hTime, any data){
	PrintToChatAll("%s Bienvenido a PIU, esperamos que lo disfrutes!", SERVERSTRING);
	PrintToChatAll("%s \x04Para visitar nuestro servidor de zombie, la ip es : \x0945.235.98.242:27035\x01.", SERVERSTRING);
	
}

public Action serverInfoCmd(int client, int args){
	PrintToChatAll("%s \x04Para visitar nuestro servidor de zombie, la ip es : \x0945.235.98.242:27035\x01.", SERVERSTRING);
	return Plugin_Handled;
}

public Action OnClientSayCommand(int client, const char[] command, const char[] sArgs){
	int flags = GetUserFlagBits(client);
	
	if(flags&(ADMFLAG_RESERVATION|ADMFLAG_GENERIC|ADMFLAG_ROOT)){
		char msg[128];
		strcopy(msg, sizeof(msg), sArgs);
		if(msg[0] == '@' && msg[1] == '@'){
			ReplaceStringEx(msg, sizeof(msg), "@", "");
			ReplaceStringEx(msg, sizeof(msg), "@", "");
			printVipSay(client, msg, VIP_SAY_MAXLEN);
			return Plugin_Handled;
		}
	}
	
	return Plugin_Continue;
}

public Action printVipSay(int client, char[] text, int maxlen){
	char name[64];
	GetClientName(client, name, sizeof(name));
	
	bool staff = view_as<bool>(GetUserFlagBits(client)&ADMFLAG_ROOT);
	bool admin = view_as<bool>(GetUserFlagBits(client)&ADMFLAG_CHANGEMAP);
	bool mod = view_as<bool>(GetUserFlagBits(client)  &ADMFLAG_KICK);
	
	char msg[VIP_SAY_MAXLEN];
	if (staff)
		FormatEx(msg, maxlen, "[STAFF] %s: %s", name, text);
	else if(admin)
		FormatEx(msg, maxlen, "[ADMIN] %s: %s", name, text);
	else if(mod)
		FormatEx(msg, maxlen, "[MOD] %s: %s", name, text);
	else
		FormatEx(msg, maxlen, "[VIP] %s: %s", name, text);
	
	Handle hRotativeSynchronizer = CreateHudSynchronizer();
	
	// Show hud message
	SetHudTextParams(0.03, GetRandomFloat(0.35, 0.7), VIP_SAY_DURATION, GetRandomInt(80, 255), GetRandomInt(80, 255), GetRandomInt(80, 255), 255, 1, 3.0, 0.4, 0.4);
	for (int i = 1; i <= MaxClients; i++){
		
		if (!IsPlayerExist(i))
			continue;

		ShowSyncHudText(i, hRotativeSynchronizer, msg);
		PrintToConsole(i,"[VIP SAY] %s", msg);
	}
	//CloseHandle(hRotativeSynchronizer);
	delete hRotativeSynchronizer;
	return Plugin_Handled;
}

public Action activateDeathmatch(int client, int args){
	if(GetUserFlagBits(client)&ADMFLAG_ROOT){
		deathmatch = !deathmatch;
		isDeathmatch(client, args);
		for(int i = 1; i<MaxClients; i++){
			if(!IsPlayerExist(i))
				continue;
			if(!IsPlayerAlive(i)){
				CS_RespawnPlayer(i);
			}
		}
	}
	
	return Plugin_Handled;
}

public Action activateGungame(int client, int args){
	if(GetUserFlagBits(client)&ADMFLAG_ROOT){
		switchGungame();
	}
	
	return Plugin_Handled;
}

public void switchGungame(){
	gungame = !gungame;
	deathmatch = gungame;
	isGungame(0, 0);
	CS_TerminateRound(0.1, CSRoundEnd_GameStart);
	for(int i = 0; i <= sizeof(levels); i++){
		levels[i] = 0;
	}
	
	if(gungame){
		hookWeaponCanUse();
	}else{
		unhookWeaponCanUse();
	}
}

public Action isDeathmatch(int client, int args){
	PrintToChatAll("%s Modo deathmatch \x09%s\x01.",SERVERSTRING, deathmatch ? "Activado":"Desactivado");
	return Plugin_Handled;
}

public Action isGungame(int client, int args){
	PrintToChatAll("%s Modo gungame \x09%s\x01.",SERVERSTRING, gungame ? "Activado":"Desactivado");
	return Plugin_Handled;
public Action isDeathmatch(int client, int args){
		PrintToChatAll("%s Modo deathmatch %s", deathmatch? "Activado":"Desactivado");
		return Plugin_Handled;
}

public Action EventPlayerDeath(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	int victim = GetClientOfUserId(gEventHook.GetInt("userid"));
	int attacker = GetClientOfUserId(gEventHook.GetInt("attacker"));
	
	if(deathmatch){
		CreateTimer(2.0, respawnPlayer, victim);
	}
	
	if(gungame){
		checkLevels(victim, attacker);
	}
	
	
}

public void checkLevels(int victim, int attacker){
	char weapon[32];
	bool levelUp = false;
	int weap = GetEntPropEnt(attacker, Prop_Data, "m_hActiveWeapon");
	if(IsValidEdict(weap)){
    	GetEntPropString(weap, Prop_Data, "m_iClassname", weapon, sizeof(weapon));
	}
	if(StrEqual(weapon, "weapon_knife") && levels[attacker]>=25){
		char name[32];
		GetClientName(attacker, name, sizeof(name));
		PrintToChatAll("%s El ganador es \x09%s\x01, se lleva %d creditos!", SERVERSTRING, name, GG_PREMIO);
		CS_TerminateRound(0.1, CSRoundEnd_GameStart);
		ServerCommand("sm_givecredits %s 500", GG_PREMIO);
		switchGungame();
	}
	else if(StrEqual(weapon, "weapon_knife")){
		levels[attacker]++;
		levelUp = true;
		points[attacker] = 0;
		if(levels[victim] >0){
			levels[victim]--;
		}
		points[victim]=0;
	}else{
		points[attacker]++;
		if(points[attacker]>= 2){
			levelUp = true;
			levels[attacker]++;
			points[attacker]=0;
		}
	}
	
	if(levelUp){
		giveWeapons(attacker);
	}

}

public Action respawnPlayer(Handle timer, int client){

	if (!IsPlayerExist(client))
		return Plugin_Stop;
	CS_RespawnPlayer(client);
	
	if(gungame){
		giveWeapons(client);
	}
	
	return Plugin_Stop;
}

public void OnClientPostAdminCheck(int client){
	EmitSoundToClient(client, WELCOME_SOUND, SOUND_FROM_PLAYER, SNDCHAN_STATIC, SNDLEVEL_NORMAL,_,_,_,_,_,_,_,5.0);
}


public Action WeaponsOnCanUse(int client, int weaponIndex){
	if(!IsValidEdict(weaponIndex) || !IsPlayerExist(client)){
		return Plugin_Handled;
	}
	
	char weap[32];
	GetEdictClassname(weaponIndex, weap, 32);
	
	
	if(gungame){
		if(StrEqual(weap,weapons_order[levels[client]])) return Plugin_Continue;
		if(StrEqual(weap,"weapon_knife")) return Plugin_Continue;
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action EventWeaponFire(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	int client = GetClientOfUserId(gEventHook.GetInt("userId"));
	
	if(!IsPlayerExist(client))
		return Plugin_Handled;
	
	int weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
	
	char classname[64];
	GetEdictClassname(weapon, classname, 64);
	if (StrContains(classname, "_projectile") != -1 || StrContains(classname, "grenade") != -1 || StrContains(classname, "flashbang") != -1){
		return Plugin_Continue;
	}
	
	int ammo = GetEntProp(weapon, Prop_Data, "m_iClip1");
	bool adm = view_as<bool>(GetUserFlagBits(client)&ADMFLAG_ROOT);
	
	if(adm){
		SetEntProp(weapon, Prop_Send, "m_iClip1", ammo+1);
	}
	if(ammo <= 1 && adm){
		SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 200);
	}
	
	return Plugin_Handled;
}


public void removeWeapons(int client){
	
	int weapon1;
	int weapon2;
	int weapon3;
	
	weapon1 = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
	
	if(weapon1 != -1 && IsValidEdict(weapon1)){
		RemovePlayerItem(client, weapon1);
		RemoveEdict(weapon1);
	}
	
	weapon2 = GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY);
	if(weapon2 != -1 && IsValidEdict(weapon2)){
		RemovePlayerItem(client, weapon2);
		RemoveEdict(weapon2);
	}
	
	for (int i; i <= 4; i++){
		weapon3 = GetPlayerWeaponSlot(client, CS_SLOT_GRENADE);
		if(weapon3 != -1 && IsValidEdict(weapon3)){
			RemovePlayerItem(client, weapon3);
			RemoveEdict(weapon3);
		}
	}
}
    
public void giveWeapons(int client){
	removeWeapons(client);
	if(levels[client]<25){
		GivePlayerItem(client, weapons_order[levels[client]]);
	}
}

public void hookWeaponCanUse(){
	for(int i = 1; i <= 32; i++){
		if(IsPlayerExist(i)){
			SDKHook(i, SDKHook_WeaponCanUse, WeaponsOnCanUse);
		}
	}
}
public void unhookWeaponCanUse(){
	for(int i = 1; i <= 32; i++){
		if(IsPlayerExist(i)){
			SDKUnhook(i, SDKHook_WeaponCanUse, WeaponsOnCanUse);
		}
	}
}