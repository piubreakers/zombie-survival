#define PLUGIN_NAME           "Ported Weapons Simulator"
#define PLUGIN_AUTHOR         "Desktop/Deco & Codes"
#define PLUGIN_DESCRIPTION    "Custom weapons menu"
#define PLUGIN_VERSION        "0.1"
#define PLUGIN_URL            ""

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <resources>

#pragma semicolon 1
#define DEBUG

#define PLUGIN_CFG_ROOT "addons/sourcemod/configs/pweapons"

#define NOT_PORTED	-1

#define WEAPONS_MAX_PROPERTIES	10

enum{
	WEAPONS_DATA_NAME = 0,
	WEAPONS_DATA_ENTITY,
	WEAPONS_DATA_SLOT,
	WEAPONS_DATA_VMODEL,
	WEAPONS_DATA_VMODELINDEX,
	WEAPONS_DATA_WMODEL,
	WEAPONS_DATA_WMODELINDEX,
	WEAPONS_DATA_SOUND,
	WEAPONS_DATA_INDEX,
	WEAPONS_DATA_GROUP
};

// Initialize vars
ArrayList WeaponsData[WEAPONS_MAX_PROPERTIES];
int iWeaponIndex[MAXPLAYERS+1][4];
int iPredictedViewModelIndex[MAXPLAYERS+1];

public Plugin myinfo = {
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

public void OnPluginStart(){
	
	// We're supporting CSGO for now
	if(GetEngineVersion() != Engine_CSGO)
		SetFailState("This plugin is for the game CSGO only.");
	
	// Hook temp entity
	AddTempEntHook("Shotgun Shot", WeaponFireBullets);
		
	// Hook player spawn to get correct predicted-viewmodel-index
	HookEvent("player_spawn", EventPostPlayerSpawn, EventHookMode_Post);
	
	// Auto precache & downloads
	for (int i = 0; i < WEAPONS_MAX_PROPERTIES; i++){
		WeaponsData[i] = CreateArray(ByteCountToCells(256));
	}
	
	RegConsoleCmd("kv", GroupsMenu, "Opens ported weapons menu");
	RegConsoleCmd("mainmenu", GroupsMenu, "Opens ported weapons menu");
	RegConsoleCmd("portedweapons", GroupsMenu, "Opens ported weapons menu");
	RegConsoleCmd("reload", LoadWeapons, "Reloads ported weapons menu");
	
	// Load our ported weapons, yay!
	LoadWeapons(0, 0);
	SetUpPluginConfigs();
}

public OnMapStart(){
	
	GetExclusionList();
	
	for(int x = 0; x < sizeof(g_cRootDirectoryName); x++){
		RegisterResources(x, view_as<DirectoryListing>(INVALID_HANDLE), g_cRootDirectoryName[x]);
	}
	
	for(int y = 0; y < sizeof(g_cRootDirectoryName); y++){
		delete g_FileExclusionList[y];
	}
}

// When the player joins the server
public OnClientPutInServer(int client){
	for (int i; i <= CS_SLOT_GRENADE; i++)
		iWeaponIndex[client][i] = NOT_PORTED;
}

public void OnClientPostAdminCheck(int client){
	SDKHooksClientInit(client);
}

void SDKHooksClientInit(int client){
	
	//SDKHook(client, SDKHook_WeaponEquip, WeaponsOnEquip);
	SDKHook(client, SDKHook_WeaponDropPost, WeaponsOnDropPost);
	SDKHook(client, SDKHook_WeaponSwitchPost, OnClientWeaponSwitchPost);
	//SDKHook(client, SDKHook_PostThinkPost, WeaponsOnAnimationFix);
}

// Weapons menu
public Action showWeaponsMenu(int client, const char[] group){

	Menu menu = new Menu(HandleWeaponsMenu, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Select your desired weapon");
	
	// Get array size
	int iSize = WeaponsData[WEAPONS_DATA_NAME].Length;
	
	PrintToChat(client, "size: %d", iSize);
	
	for(int i; i < iSize; i++) {
		char sWeap[32], sGroup[32], id[8];
		GetArrayString(WeaponsData[WEAPONS_DATA_NAME], i, sWeap, sizeof(sWeap));
		GetArrayString(WeaponsData[WEAPONS_DATA_GROUP], i, sGroup, sizeof(sGroup));
		
		//PrintToChat(client, "Item read: name %s | group %s | menugroup %s", sWeap, sGroup, group);
		IntToString(i, id, sizeof(id));
		if(StrEqual(group, sGroup, false)){
			//PrintToChat(client, "Item added: name %s | group %s", sWeap, sGroup);
			menu.AddItem(id, sWeap);
		}
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int HandleWeaponsMenu(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			char sWeapName[32], sWeapEnt[32], id[8];
			menu.GetItem(selection, id, sizeof(id));
			selection = StringToInt(id);
			GetArrayString(WeaponsData[WEAPONS_DATA_NAME], selection, sWeapName, sizeof(sWeapName));
			GetArrayString(WeaponsData[WEAPONS_DATA_ENTITY], selection, sWeapEnt, sizeof(sWeapEnt));
			PrintToChat(client, "\x01 \x09[PWS]\x01 You've selected \x09%s (index: %d | ent: %s)\x01 as your weapon", sWeapName, selection, sWeapEnt);
			
			int slot = WeaponsData[WEAPONS_DATA_SLOT].Get(selection);
			
			iWeaponIndex[client][slot] = selection;
			
			PrintToChat(client, "\x01 \x09[PWS]\x01 Your weapon index in slot \x09%d01 is \x09%d\x01 now.", slot, iWeaponIndex[client][slot]);
			
			//PrintToChat(client, "weapon: %s, wmodelindex: %d, vmodelindex: %d", sWeapName, WeaponsData[WEAPONS_DATA_WMODELINDEX].Get(selection), WeaponsData[WEAPONS_DATA_VMODELINDEX].Get(selection));
			
			if (slot != CS_SLOT_GRENADE){
			
				int weapon;
				weapon = CSGO_ReplaceWeapon(client, slot, sWeapEnt);
				
				
				// SEGUIR
				// Initialize variables
				char siName[32];
				char sTemp[16];
				switch (slot){
					case CS_SLOT_PRIMARY: sTemp = "primary";
					case CS_SLOT_SECONDARY: sTemp = "secondary";
					case CS_SLOT_KNIFE: sTemp = "knife";
				}
				
				// Store to primary weapon id
				FormatEx(siName, sizeof(siName), "%s_%d", sTemp, iWeaponIndex[client][slot]);
				SetEntPropString(weapon, Prop_Data, "m_iName", siName);
				
				EquipPlayerWeapon(client, weapon);
			}
			else{
				int weapon;
				weapon = GivePlayerItem(client, sWeapEnt);
				EquipPlayerWeapon(client, weapon);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

/*
public Action WeaponsOnEquip(int client, int weapon){
	ZPlayer player = ZPlayer(client);
	
	char sWeapon[32];
	GetEntPropString(weapon, Prop_Data, "m_iName", sWeapon, sizeof(sWeapon));
	
	if (StrContains(sWeapon, "primary") != -1){
		player.iWeaponPrimary = StringToInt(sWeapon[8]);
		//PrintToChat(player.id, "%s PWeapon: %d", SERVERSTRING, StringToInt(sWeapon[8]));
	}
	else if (StrContains(sWeapon, "secondary") != -1){
		player.iWeaponSecondary = StringToInt(sWeapon[10]);
		//PrintToChat(player.id, "%s SWeapon: %d", SERVERSTRING, StringToInt(sWeapon[10]));
	}
	
	return Plugin_Continue;
}*/
public void OnClientWeaponSwitchPost(int client, int weapon){
	
	if(!IsValidPlayer(client) || !IsValidEntity(weapon))
		return;
	
	int playerview = iPredictedViewModelIndex[client];
	if (playerview == INVALID_ENT_REFERENCE){
		iPredictedViewModelIndex[client] = Weapon_GetViewModelIndex(client, -1);
		playerview = iPredictedViewModelIndex[client];
		
		if (playerview == INVALID_ENT_REFERENCE)
			return;
	}
	
	// Store player's weapon index
	int iWeaponid;
	
	for (int i; i <= CS_SLOT_GRENADE; i++){
		if (GetPlayerWeaponSlot(client, i) == weapon){
			iWeaponid = iWeaponIndex[client][i];
			break;
		}
	}
		
	//////////////////////////////////////////////////////////////
	//					IMPORTANT BUGFIX
	//////////////////////////////////////////////////////////////
	
	// Get primary weapon id
	int primary = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);

	// Get secondary weapon id
	int secondary = GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY);
	
	int knife = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
	
	if (primary == weapon){
		iWeaponid = iWeaponIndex[client][CS_SLOT_PRIMARY];
	}
	else if (secondary == weapon){
		iWeaponid = iWeaponIndex[client][CS_SLOT_SECONDARY];
	}
	else if (knife == weapon){
		iWeaponid = iWeaponIndex[client][CS_SLOT_KNIFE];
	}
	else return;
	
	// If weapon is signed as not ported, our job is done here
	if (iWeaponid == NOT_PORTED)
		return;
	
	// READ IF ITS PRIMARY OR SECONDARY, TO APPLY THE CORRECT MODEL
	/*char sWeaponiName[32];
	GetEntPropString(weapon, Prop_Data, "m_iName", sWeaponiName, sizeof(sWeaponiName));
	
	int iWeaponid;
	if (StrContains(sWeaponiName, "primary") != -1){
		iWeaponid = StringToInt(sWeaponiName[8]);
	}
	else if (StrContains(sWeaponiName, "secondary") != -1){
		iWeaponid = StringToInt(sWeaponiName[10]);
	}
	else if (StrContains(sWeaponiName, "knife") != -1){
		iWeaponid = StringToInt(sWeaponiName[6]);
	}
	else return;*/
	// NOW THAT THE PLUGIN KNOWS WHERE TO READ THE DATA FROM, WE SHALL CONTINUE
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	
	// Get weapons classname
	char sWpn[64];
	GetEdictClassname(weapon, sWpn, sizeof(sWpn));
	
	// Get ported weapon classname
	char sWeapEntName[32];
	WeaponsData[WEAPONS_DATA_ENTITY].GetString(iWeaponid, sWeapEntName, sizeof(sWeapEntName));
	
	// If the weapon classname doesn't match ported one's, our job is done here
	if (StrEqual(sWpn, sWeapEntName) || (StrEqual(sWpn, "weapon_m4a1") && StrContains(sWeapEntName, "weapon_m4a1") != -1) || (StrEqual(sWpn, "weapon_mp7") && StrContains(sWeapEntName, "weapon_mp5sd") != -1)){
		
		// Get vmodel data
		char sWeapModel[256];
		WeaponsData[WEAPONS_DATA_VMODEL].GetString(iWeaponid, sWeapModel, sizeof(sWeapModel));
		if (StrEqual(sWeapModel, "empty")){
			return;
		}
		
		// Apply it
		SetViewModel(weapon, playerview, WeaponsData[WEAPONS_DATA_VMODELINDEX].Get(iWeaponid));
		
		// Get wmodel data
		char sWeapWorldModel[256];
		WeaponsData[WEAPONS_DATA_WMODEL].GetString(iWeaponid, sWeapWorldModel, sizeof(sWeapWorldModel));
		if (!StrEqual(sWeapWorldModel, "empty"))										// In case we have a modelindex cached
			SetWorldModel(weapon, WeaponsData[WEAPONS_DATA_WMODELINDEX].Get(iWeaponid)); // Apply it
	}
}

// Apply selected weapon dropped skin when users drop them
public void WeaponsOnDropPost(int client, int weapon){
	if (!IsValidEdict(weapon) || !IsValidPlayer(client))
		return;
	
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	
	char sDroppedModel[256], sWeapon[32];
	GetEntPropString(weapon, Prop_Data, "m_iName", sWeapon, sizeof(sWeapon));
	
	if (StrContains(sWeapon, "primary") != -1){
		int iWeapon = StringToInt(sWeapon[8]);
		//PrintToChat(player.id, "%s PWeapon: %d", SERVERSTRING, StringToInt(sWeapon[8]));
		WeaponsData[WEAPONS_DATA_WMODEL].GetString(iWeapon, sDroppedModel, sizeof(sDroppedModel));
		iWeaponIndex[client][0] = NOT_PORTED;
	}
	else if (StrContains(sWeapon, "secondary") != -1){
		int iWeapon = StringToInt(sWeapon[10]);
		//PrintToChat(player.id, "%s SWeapon: %d", SERVERSTRING, StringToInt(sWeapon[10]));
		WeaponsData[WEAPONS_DATA_WMODEL].GetString(iWeapon, sDroppedModel, sizeof(sDroppedModel));
		iWeaponIndex[client][1] = NOT_PORTED;
	}/*
	else if (StrContains(sWeaponiName, "knife") != -1){
		int iWeapon = StringToInt(sWeaponiName[6]);
		//PrintToChat(player.id, "%s SWeapon: %d", SERVERSTRING, StringToInt(sWeapon[10]));
		WeaponsData[WEAPONS_DATA_WMODEL].GetString(iWeapon, sDroppedModel, sizeof(sDroppedModel));
	}*/
	else return;
	
	if (!StrEqual(sDroppedModel, "empty") && !StrEqual(sDroppedModel, "")){
	
		// Send data to the next frame
		DataPack hPack = new DataPack();
		hPack.WriteCell(weapon);
		hPack.WriteString(sDroppedModel);
		
		RequestFrame(view_as<RequestFrameCallback>(SetDroppedModel), hPack);
	}
	
}
public void SetDroppedModel(any hPack){
	// Resets the position in the datapack
	ResetPack(hPack);
	
	// Get the weapon index from the datapack
	int weapon = ReadPackCell(hPack);
	
	if (IsValidEdict(weapon)){
		// Get the world model from the datapack
		static char sDroppedModel[256];
		ReadPackString(hPack, sDroppedModel, sizeof(sDroppedModel));
		SetEntityModel(weapon, sDroppedModel);
	}
	
	delete view_as<DataPack>(hPack);
}

// Hook player spawn post
public Action EventPostPlayerSpawn(Handle event, const char[] name, bool dontBroadcast){
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (!IsValidPlayer(client)){
		return Plugin_Handled;
	}
	
	iPredictedViewModelIndex[client] = Weapon_GetViewModelIndex(client, -1);
	
	return Plugin_Continue;
}

// Found these in gubka's code, so i think i have to thank him
stock int Weapon_GetViewModelIndex(int client, int sIndex){  
	while ((sIndex = FindEntityByClassname2(sIndex, "predicted_viewmodel")) != -1){  
		int Owner = GetEntPropEnt(sIndex, Prop_Send, "m_hOwner");
          
		if (Owner != client)
			continue;
	          
		return sIndex;  
	} 
	return -1;  
}
stock int FindEntityByClassname2(int sStartEnt, char[] szClassname){
	while (sStartEnt > -1 && !IsValidEntity(sStartEnt)) sStartEnt--;
	return FindEntityByClassname(sStartEnt, szClassname);
}
stock void SetWorldModel(int weaponIndex, int modelIndex){
	int worldIndex = GetEntPropEnt(weaponIndex, Prop_Send, "m_hWeaponWorldModel");
	
	if(IsValidEdict(worldIndex))
		SetEntProp(worldIndex, Prop_Send, "m_nModelIndex", modelIndex);
}
stock void SetViewModel(int weaponId, int viewId, int modelId){
	SetEntProp(weaponId, Prop_Send, "m_nModelIndex", 0);
	SetEntProp(viewId, Prop_Send, "m_nModelIndex", modelId);
}

// Thanks SMLIB for this func
stock int CSGO_ReplaceWeapon(int client, int slot, const char[] weapon_string){

	int old_weapon = GetPlayerWeaponSlot(client, slot);

	if (IsValidEntity(old_weapon)){
		if (GetEntPropEnt(old_weapon, Prop_Send, "m_hOwnerEntity") != client){
			SetEntPropEnt(old_weapon, Prop_Send, "m_hOwnerEntity", client);
		}

		CS_DropWeapon(client, old_weapon, false, true);
		AcceptEntityInput(old_weapon, "Kill");
	}

	int new_weapon = GivePlayerItem(client, weapon_string);

	if (IsValidEntity(new_weapon)){
		EquipPlayerWeapon(client, new_weapon);
	}
	
	return new_weapon;
}

// Is player index valid?
stock bool IsValidPlayer(int client){

	return (0 < client < MAXPLAYERS+1 && IsClientInGame(client));
}

// Is the registered weapon's entity valid?
stock bool IsEntWeaponValid(const char[] sWeapEnt, int maxSize){

	char sTemp[32];
	strcopy(sTemp, maxSize, sWeapEnt);
	
	ReplaceString(sTemp, maxSize, "weapon_", "");

	// Convert weapon alias to ID
	CSWeaponID weaponID = CS_AliasToWeaponID(sTemp);

	// If weapon alias invalid, then remove, log, and stop
	if(!CS_IsValidWeaponID(weaponID))
		return false;
	
	return true;
}

// Dynamic arrays storing func
stock int CreatePortedWeapon(const char[] group, const char[] name, const char[] entityName, int slot, const char[] modelName, const char[] worldModelName, const char[] fireSound){
	
	// If entity is not valid, don't push data
	if (!IsEntWeaponValid(entityName, 32)){
		PrintToServer("[PWS] Not valid entity name %s, weapon not added.", entityName);
		return NOT_PORTED;
	}
	
	// Debugging purposes
	int weap = WeaponsData[WEAPONS_DATA_NAME].Length;
	PrintToServer("[PWS] ArrayLength: %d", weap);
	
	// Basic data
	WeaponsData[WEAPONS_DATA_GROUP].PushString(group); // Store group
	WeaponsData[WEAPONS_DATA_NAME].PushString(name); // Store name
	WeaponsData[WEAPONS_DATA_ENTITY].PushString(entityName); // Store entityname or classname
	WeaponsData[WEAPONS_DATA_SLOT].Push(slot); // Store slot (CS_SLOT_XX | PRIMARY, SECONDARY, KNIFE, GRENADE)
	
	// Viewmodel
	WeaponsData[WEAPONS_DATA_VMODEL].PushString(modelName); // Store viewmodel
	WeaponsData[WEAPONS_DATA_VMODELINDEX].Push(PrecacheModel(modelName)); // in case it's not empty, precache and store modelindex
	// Worldmodel
	WeaponsData[WEAPONS_DATA_WMODEL].PushString(worldModelName); // Store worldmodel
	WeaponsData[WEAPONS_DATA_WMODELINDEX].Push(PrecacheModel(worldModelName)); // in case it's not empty, precache and store modelindex
	// Fire sound
	WeaponsData[WEAPONS_DATA_SOUND].PushString(fireSound); // Store fire sound
	
	// Debugging purposes
	weap = WeaponsData[WEAPONS_DATA_NAME].Length;
	PrintToServer("[PWS] ArrayLength after: %d", weap);
	
	return weap;
}

// Hook weapons fire
public Action WeaponFireBullets(const char[] sTEName, const int[] iPlayers, int numClients, float flDelay){
	
    // Get all required event info
	int client = TE_ReadNum("m_iPlayer") + 1;
	
	// Initialize weapon index
	int weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
	
	// Initialize weapon type
	char sWeaponType[32];
	GetEntPropString(weapon, Prop_Data, "m_iName", sWeaponType, sizeof(sWeaponType));
	
	// Read fire sound
	char sFireSound[256];
	if (StrContains(sWeaponType, "primary") != -1){
		int iWeapon = StringToInt(sWeaponType[8]);
		WeaponsData[WEAPONS_DATA_SOUND].GetString(iWeapon, sFireSound, sizeof(sFireSound));
	}
	else if (StrContains(sWeaponType, "secondary") != -1){
		int iWeapon = StringToInt(sWeaponType[10]);
		WeaponsData[WEAPONS_DATA_SOUND].GetString(iWeapon, sFireSound, sizeof(sFireSound));
	}
	else
		return;
	
	if (StrEqual(sFireSound, "empty"))
		return;
	
	// Play sound
	EmitSoundToAll(sFireSound, client, SNDCHAN_WEAPON, SNDLEVEL_ROCKET);
	EmitSoundToAll(sFireSound, client, SNDCHAN_STATIC, SNDLEVEL_NORMAL);
}

// Func to load our desired weapons
public Action LoadWeapons(int client, int args){
	// Usage: CreatePortedWeapon("WeaponName", "Classname", int SLOT (be sure to use the correct slot), "v_model path", "w_model path", "fire sound path");
	// Not ported example: CreatePortedWeapon("AK47", "weapon_ak47", CS_SLOT_PRIMARY, "empty", "empty", "empty"); // Those 'empty' strings are for not ported properties
	// Note: If you don't add Viewmodel, the plugin WON'T apply Worldmodel!
	/*CreatePortedWeapon("CARLITOS", "weapon_ak47", CS_SLOT_PRIMARY, "models/weapons/ak117/v_rif_ak117.mdl", "models/weapons/ak117/w_rif_ak117.mdl", "empty");
	CreatePortedWeapon("MABEL", "weapon_awp", CS_SLOT_PRIMARY, "models/weapons/eminem/dsr_50/v_dsr_50.mdl", "models/weapons/eminem/dsr_50/w_dsr_50.mdl", "empty");
	CreatePortedWeapon("RIKRDO", "weapon_glock", CS_SLOT_SECONDARY, "empty", "empty", "empty");
	CreatePortedWeapon("FRAG", "weapon_hegrenade", CS_SLOT_GRENADE, "empty", "empty", "empty");
	CreatePortedWeapon("AURA", "weapon_smokegrenade", CS_SLOT_GRENADE, "models/weapons/eminem/shield_grenade/v_shield_grenade.mdl", "models/weapons/eminem/shield_grenade/w_shield_grenade.mdl", "empty");
	*/
	for(int i; i < MAXPLAYERS+1; i++){
		for(int j; j<4; j++){
			iWeaponIndex[i][j] = NOT_PORTED;
		}
	}
	
	for(int i; i< WEAPONS_MAX_PROPERTIES; i++){
		WeaponsData[i].Clear();
	}
	
	char weapons[255];
	
	FormatEx(weapons, sizeof(weapons), "%s/weapons.txt", PLUGIN_CFG_ROOT);
	
	
	KeyValues kv = new KeyValues("Weapons");
	kv.ImportFromFile(weapons);
	kv.GotoFirstSubKey();
	
	char group[32], name[32], ent[32], slot[32], vModel[256], wModel[256], sound[256];
	do{
		kv.GetString("group", group, sizeof(group));
		kv.GetString("name", name, sizeof(name));
		kv.GetString("weapon_ent", ent, sizeof(ent));
		kv.GetString("slot", slot, sizeof(slot));
		kv.GetString("v_model", vModel, sizeof(vModel));
		kv.GetString("w_model", wModel, sizeof(wModel));
		kv.GetString("shoot_sound", sound, sizeof(sound));
		
		int iSlot = MapSlotToInt(slot);
		
		CreatePortedWeapon(group, name, ent, iSlot, vModel, wModel, sound);
		
	} while(kv.GotoNextKey());
	
	delete kv;
	return Plugin_Handled;
}

public int MapSlotToInt(const char[] slot){
	int ret = 0;
	if(StrEqual(slot, "primary")){
		ret = CS_SLOT_PRIMARY;	
	} else if(StrEqual(slot, "secondary")){
		ret = CS_SLOT_SECONDARY;	
	} else if(StrEqual(slot, "knife")){
		ret = CS_SLOT_KNIFE;	
	} else if(StrEqual(slot, "grenade")){
		ret = CS_SLOT_GRENADE;	
	} else{
		ret = -1;
	}
	return ret;
}

public Action GroupsMenu(int client, int args){
	char groups[255];
	
	FormatEx(groups, sizeof(groups), "%s/groups.txt", PLUGIN_CFG_ROOT);
	
	
	KeyValues kv = new KeyValues("Groups");
	kv.ImportFromFile(groups);
	kv.GotoFirstSubKey();
	Menu menu = new Menu(HandleGroupsMenu, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Weapons Groups");
	
	char buff[256], name[64];
	do{
		kv.GetSectionName(buff, sizeof(buff));
		kv.GetString("name", name, sizeof(name));
		PrintToServer("%s: %s", buff, name);
		menu.AddItem(buff, name);
	} while(kv.GotoNextKey());
	
	
	delete kv;
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int HandleGroupsMenu(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			char buff[32];
			menu.GetItem(selection, buff, sizeof(buff));
			PrintToServer(buff);
			showWeaponsMenu(client, buff);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}
/*
public Action WeaponsMenu(int client, const char[] group){
	char weapons[255];
	
	FormatEx(weapons, sizeof(weapons), "%s/weapons.txt", CFG_ROOT);
	
	
	KeyValues kv = new KeyValues("Weapons");
	kv.ImportFromFile(weapons);
	kv.GotoFirstSubKey();
	Menu menu = new Menu(HandleWeapons1Menu, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Weapons Groups");
	
	char buff[32], Group[32], name[32];
	do{
		kv.GetSectionName(buff, sizeof(buff));
		kv.GetString("name", name, sizeof(name));
		kv.GetString("group", Group, sizeof(Group));
		PrintToServer("%s: %s | %s", buff, group, name);
		if(StrEqual(group, Group)){
			menu.AddItem(buff, name);
		}
	} while(kv.GotoNextKey());
	
	
	delete kv;
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int HandleWeapons1Menu(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
		
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}
*/

stock void SetUpPluginConfigs(){
	char groups[255], weapons[255];
	
	FormatEx(weapons, sizeof(weapons), "%s/weapons.txt", PLUGIN_CFG_ROOT);
	FormatEx(groups, sizeof(groups), "%s/groups.txt", PLUGIN_CFG_ROOT);

	if(!DirExists(PLUGIN_CFG_ROOT)){
		PrintToServer("Creating directory.. (%s)", CreateDirectory(PLUGIN_CFG_ROOT, 511) ? "Success" : "Failed");
	}
	if(!FileExists(groups)){
		PrintToServer("Creating groups file...");
		File f = OpenFile(groups, "w");
		delete f;
	}
	if(!FileExists(weapons)){
		PrintToServer("Creating weapons file...");
		File f = OpenFile(weapons, "w");
		delete f;
	}
}

#define	WeaponsValidateKnife(%0) (%0 == CSWeapon_KNIFE || %0 == CSWeapon_KNIFE_GG)
public Action WeaponsOnAnimationFix(int client){

	// Validate client
	if(!IsValidPlayer(client)){
		return;
	}

	// Convert weapon index to ID
	CSWeaponID weaponID = WeaponsGetID(GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon"));
	
	// If weapon isn't valid, then stop
	if(!CS_IsValidWeaponID(weaponID)){
		return;
	}
	
	// Get view index
	int viewIndex = GetEntPropEnt(client, Prop_Send, "m_hViewModel");
	
	// If weapon isn't valid, then stop
	if(IsValidEdict(viewIndex)){
	
		// Initialize variable
		static int nOldSequence[MAXPLAYERS+1]; static float flOldCycle[MAXPLAYERS+1];
		
		// Get the sequence number and it's playing time
		int nSequence = GetEntProp(viewIndex, Prop_Send, "m_nSequence");
		float flCycle = GetEntPropFloat(viewIndex, Prop_Data, "m_flCycle");
		
		// Validate a knife
		if(nSequence == nOldSequence[client] && flCycle < flOldCycle[client]){
			
			// Validate animation delay
			if (WeaponsValidateKnife(weaponID)){
				switch (nSequence){
					case 3 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 4);
					case 4 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 3);
					case 5 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 6);
					case 6 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 5);
					case 7 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 8);
					case 8 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 7);
					case 9 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 10);
					case 10: SetEntProp(viewIndex, Prop_Send, "m_nSequence", 11); 
					case 11: SetEntProp(viewIndex, Prop_Send, "m_nSequence", 10);
				}
			}
			else if (weaponID == CSWeapon_DEAGLE){
				switch (nSequence)
				{
					case 3: SetEntProp(viewIndex, Prop_Send, "m_nSequence", 2);
					case 2: SetEntProp(viewIndex, Prop_Send, "m_nSequence", 1);
					case 1: SetEntProp(viewIndex, Prop_Send, "m_nSequence", 3);	
				}
			}
		}
		else{
			// Initialize variable
			static int nPrevSequence[MAXPLAYERS+1]; static float flDelay[MAXPLAYERS+1];

			// Returns the game time based on the game tick
			float flCurrentTime = GetEngineTime();
			
			// Play previous animation
			if(nPrevSequence[client] != 0 && flDelay[client] < flCurrentTime){
			
				SetEntProp(viewIndex, Prop_Send, "m_nSequence", nPrevSequence[client]);
				nPrevSequence[client] = 0;
			}
			
			// Validate animation delay
			if(flCycle < flOldCycle[client]){
			
				// Validate animation
				if(nSequence == nOldSequence[client]){
					SetEntProp(viewIndex, Prop_Send, "m_nSequence", 0);
					nPrevSequence[client] = nSequence;
					flDelay[client] = flCurrentTime + 0.03;
				}
			}
		}
		
		// Update the animation interval delay
		nOldSequence[client] = nSequence;
		flOldCycle[client] = flCycle;
	}
}
stock CSWeaponID WeaponsGetID(int weaponIndex){

	// If weapon isn't valid, then stop
	if(!IsValidEdict(weaponIndex)){
		return CSWeapon_NONE;
	}
	
	// Initialize char
	static char sClassname[16];
	
	// Get weapon classname and convert it to alias
	GetEntityClassname(weaponIndex, sClassname, sizeof(sClassname));
	ReplaceString(sClassname, sizeof(sClassname), "weapon_", "");
	
	// Convert weapon alias to ID
	return CS_AliasToWeaponID(sClassname);
}