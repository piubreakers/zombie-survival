/**
 * =============================================================================
 * Zombie Plague 6.4 Copyright (C) 2015-2016 Nikita Ushakov (Ireland, Dublin).
 * =============================================================================
 *
 * This file is part of the Zombie Plague Core.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * As a special exception, AlliedModders LLC gives you permission to link the
 * code of this program (as well as its derivative works) to "Half-Life 2," the
 * "Source Engine," the "SourcePawn JIT," and any Game MODs that run on software
 * by the Valve Corporation.  You must obey the GNU General Public License in
 * all respects for all other code used.  Additionally, AlliedModders LLC grants
 * this exception to all derivative works.  AlliedModders LLC defines further
 * exceptions, found in LICENSE.txt (as of this writing, version JULY-31-2007),
 * or <http://www.sourcemod.net/license.php>.
 **/

#if defined _zombieplaguemod_included
 #endinput
#endif
#define _zombieplaguemod_included


/**
 * Lenth of usual strings.
 **/
#define SMALL_LINE_LENGTH 	32
#define NORMAL_LINE_LENGTH 	64
#define BIG_LINE_LENGTH 	128
#define HUGE_LINE_LENGTH	512

/**
 * Number of game modes.
 **/
enum
{
	MODE_NONE = -1,			/**< Used as return value when an mode didn't start */
	
	MODE_INFECTION,			/**< Normal infection */
	MODE_MULTI,				/**< Multi infection */
	MODE_SWARM,				/**< Swarm mode */
	MODE_NEMESIS,			/**< Nemesis round */
	MODE_SURVIVOR,			/**< Survivor round */
	MODE_ARMAGEDDON			/**< Armageddon round */
};

/**
 * Number of valid player classes.
 **/
enum ZPClassType
{
	TYPE_ZOMBIE,			/**< Make a zombie */
	TYPE_NEMESIS,			/**< Make a nemesis */
	TYPE_SURVIVOR,			/**< Make a survivor */
	TYPE_HUMAN				/**< Make a human */
};

/**
 * Number of valid states.
 **/
enum ZPStateType
{
	STATE_MODE_STARTED,		/**< Zombie round started ? */
	STATE_ROUND_NUM,		/**< Number of round */
	STATE_ROUND_MODE,		/**< Mode of round */
	STATE_NEW_ROUND,		/**< Round not started */
	STATE_END_ROUND			/**< Round ended ? */
};

/**
 * Number of valid player teams.
 **/
enum ZPClassTeam
{
	ZP_TEAM_HUMAN,			/**< Human team */
	ZP_TEAM_ZOMBIE,			/**< Zombie team */
	ZP_TEAM_ALL				/**< All teams */
};

/**
 * Number of options.
 **/
enum ZPSelectOption
{
	NO,				
	YES
}

/**
 * Number of valid player slots.
 **/
enum
{ 
	WEAPON_SLOT_INVALID = -1,
	
	WEAPON_SLOT_PRIMARY, 
	WEAPON_SLOT_SECONDARY, 
	WEAPON_SLOT_MELEE, 
	WEAPON_SLOT_EQUEPMENT
};


//*********************************************************************
//*           		   ZOMBIE PLAGUE FORWARDS            		  	  *
//*********************************************************************


/**
 * Called when a client became a zombie.
 * 
 * @param clientIndex		The client index.
 * @param attackerIndex		The attacker index.
 *
 * @noreturn
 **/
forward void ZP_OnClientInfected(int clientIndex, int attackerIndex);

/**
 * Called when a client became a survivor.
 * 
 * @param clientIndex		The client index.
 *
 * @noreturn
 **/
forward void ZP_OnClientHeroed(int clientIndex);

/**
 * Called when a client take a fake damage.
 * 
 * @param clientIndex		The client index.
 * @param attackerIndex		The attacker index.
 *
 * @noreturn
 **/
forward void ZP_OnClientDamaged(int clientIndex, int attackerIndex);

/**
 * Called after a zombie round is started.
 * 
 * @param modeIndex			The round mode.
 *
 * @noreturn
 **/
forward void ZP_OnZombieModStarted(int modeIndex);

/**
 * Called after select an extraitem in the equipment menu.
 * 
 * @param clientIndex		The client index.
 * @param extraitemIndex	The index of extraitem from ZP_RegisterExtraItem() native.
 *
 * @return					Plugin_Handled or Plugin_Stop to block purhase. Anything else
 *                          	(like Plugin_Continue) to allow purhase and taking ammopacks.
 **/
forward Action ZP_OnExtraBuyCommand(int clientIndex, int extraitemIndex);

/**
 * Called when a client use a zombie skill.
 * 
 * @param clientIndex		The client index.
 *
 * @return					Plugin_Handled or Plugin_Stop to block using skill. Anything else
 *                          	(like Plugin_Continue) to allow use.
 *
 **/
forward Action ZP_OnSkillUsed(int clientIndex);

/**
 * Called when a zombie skill duration is over.
 * 
 * @param clientIndex		The client index.
 *
 **/
forward void ZP_OnSkillOver(int clientIndex);


//*********************************************************************
//*           		     ZOMBIE PLAGUE NATIVES            		  	  *
//*********************************************************************


/**
 * Registers a custom class which will be added to the zombie classes menu of ZP.
 *
 * 	Note: The returned zombie class ID can be later used to identify
 * 			the class when calling the ZP_GetClientZombieClass() natives.
 *
 * @param name				Caption to display on the menu.
 * @param model				Player model to be used.
 * @param claw_model		Knife model to be used.
 * @param health			Initial health points.
 * @param speed				Maximum speed.
 * @param gravity			Gravity multiplier.
 * @param knockback			Knockback multiplier.
 * @param female			Will have a female sounds.
 * @param access			Will available only for VIP players.
 * @param duration			Duration of skill, for your custom addons. (0-off)
 * @param countdown			Countdown of skill, for your custom addons. (0-off)
 * @param regenhealth		Regenerating health. (0-off)
 * @param regeninterval		Regenerating health's interval. (0.0-off)
 *
 * 	Note: The duration and countdown must be turn off together,
 *	 		for disabling skill usage.
 *
 * @return					An internal zombie class ID, or -1 on failure.
 **/
native int ZP_RegisterZombieClass(char[] name, char[] model, char[] claw_model, int health, float speed, float gravity, float knockback, ZPSelectOption female = NO, ZPSelectOption access = NO, int duration = 0, int countdown = 0, int regenhealth = 0, float regeninterval = 0.0);

/**
 * Registers a custom class which will be added to the human classes menu of ZP.
 *
 * 	Note: The returned human class ID can be later used to identify
 * 			the class when calling the ZP_GetClientHumanClass() natives.
 *
 * @param name				Caption to display on the menu.
 * @param model				Player model to be used.
 * @param arm_model			Arm model to be used.
 * @param health			Initial health points.
 * @param speed				Maximum speed.
 * @param gravity			Gravity multiplier.
 * @param armor				Armor on the spawn. (100-maximum)
 * @param access			Will available only for VIP players.
 *
 * 	Note: The duration and countdown must be turn off together,
 *	 		for disabling skill usage.
 *
 * @return					An internal human class ID, or -1 on failure.
 **/
native int ZP_RegisterHumanClass(char[] name, char[] model, char[] arm_model, int health, float speed, float gravity, int armor, ZPSelectOption access = NO);

/**
* Registers a custom extra item which will be added to the equipment menu of ZP.
 *
 * 	Note: The returned extra item ID can be later used to identify
 * 			the class when calling the ZP_OnExtraBuyCommand() forward.
 *
 * @param name				Caption to display on the menu.
 * @param cost				Price of this item. (0-off any messages and ammo cost from menu)
 * @param team				Which team allowed to use the item.
 * @param level				The level of the player, which allow to buy item. (0-off)
 * @param online			The number of players, which allowed to buy item. (0-off)
 * @param limit				The number of purchases, which allowed to buy item. (0-off)
 *							
 *
 * @return					An internal extra item ID, or -1 on failure.
 **/
native int ZP_RegisterExtraItem(char[] name, int cost, ZPClassTeam team, int level = 0, int online = 0, int limit = 0);

/**
 * Returns true if the player is a VIP, false if not.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        True or false.
 **/
native bool ZP_IsPlayerVIP(int clientIndex);

/**
 * Returns true if the player is a zombie, false if not. 
 *
 * @param clientIndex		The client index.
 *  
 * @return			        True or false.
 **/
native bool ZP_IsPlayerZombie(int clientIndex);

/**
 * Returns true if the player is a human, false if not.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        True or false.
 **/
native bool ZP_IsPlayerHuman(int clientIndex);

/**
 * Returns true if the player is a nemesis, false if not. (Nemesis always have ZP_IsPlayerZombie() native)
 *
 * @param clientIndex		The client index.
 *  
 * @return			        True or false.
 **/
native bool ZP_IsPlayerNemesis(int clientIndex);

/**
 * Returns true if the player is a survivor, false if not.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        True or false.
 **/
native bool ZP_IsPlayerSurvivor(int clientIndex);

/**
 * Returns true if the player use a zombie skill, false if not. 
 *
 * @param clientIndex		The client index.
 *  
 * @return			        True or false.
 **/
native bool ZP_IsPlayerUseZombieSkill(int clientIndex);

/**
 * Force to switch a player's class.
 *
 * @param typeIndex			The class type. See enum ZPClassType.
 * @param clientIndex		The client index.
 *  
 * @noreturn
 **/
native void ZP_SwitchClass(ZPClassType typeIndex, int clientIndex);

/**
 * Gets the player's amount of ammopacks.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        The number of ammopacks.
 **/
native int ZP_GetClientAmmoPack(int clientIndex);

/**
 * Sets the player's amount of ammopacks.
 *
 * @param clientIndex		The client index.
 * @param amountAmmo		The number of ammopacks.
 *  
 * @noreturn
 **/
native void ZP_SetClientAmmoPack(int clientIndex, int amountAmmo);

/**
 * Gets the player's level.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        The number of level.
 **/
native int ZP_GetClientLevel(int clientIndex);

/**
 * Sets the player's level.
 *
 * @param clientIndex		The client index.
 * @param amountLevel		The number of level.
 *  
 * @noreturn
 **/
native int ZP_SetClientLevel(int clientIndex, int amountLevel);

/**
 * Gets the player's zombie class.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        The number of class.
 **/
native int ZP_GetClientZombieClass(int clientIndex);

/**
 * Gets the player's human class.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        The number of class.
 **/
native int ZP_GetClientHumanClass(int clientIndex);

/**
 * Gets the player's view model.
 *
 * @param clientIndex		The client index.
 *  
 * @return			        The view model index. See stock SetViewModel.
 **/
native int ZP_GetClientViewModel(int clientIndex);

/**
 * Gets the round's state.
 *
 * @param typeIndex         The mode of state. See enum ZPStateType.
 *  
 * @return			        The integer of type state.
 **/
native int ZP_GetRoundState(ZPStateType typeIndex);


//*********************************************************************
//*           			OTHER USEFUL STOCKS            				  *
//*********************************************************************


/**
 * Sets the view weapon's model.
 *
 * @param weaponIndex		The weapon index.
 * @param viewIndex			The view model index.
 * @param modelIndex		The model index. (Must be precached)
 *
 * @noreturn
 **/
stock void SetViewModel(int weaponIndex, int viewIndex, int modelIndex)
{
	// Delete default model index
	SetEntProp(weaponIndex, Prop_Send, "m_nModelIndex", 0);
	
	// Set a new view model index for the weapon
	SetEntProp(viewIndex, Prop_Send, "m_nModelIndex", modelIndex);
}

/**
 * Sets the attached world weapon's model.
 *
 * @param weaponIndex		The weapon index.
 * @param modelIndex		The model index. (Must be precached)
 *
 * @noreturn
 **/
stock void SetWorldModel(int weaponIndex, int modelIndex)
{
	// Get world index
	int worldIndex = GetEntPropEnt(weaponIndex, Prop_Send, "m_hWeaponWorldModel");
	
	// Verify that the entity is valid
	if(IsValidEdict(worldIndex))
	{
		// Set model for the entity
		SetEntProp(worldIndex, Prop_Send, "m_nModelIndex", modelIndex);
	}
}

/**
 * Returns true if the player is connected and alive, false if not.
 *
 * @param clientIndex		The client index.
 * @param aliveness			(Optional) Set to true to validate that the client is alive, false to ignore.
 *  
 * @return					True or false.
 **/
stock bool IsPlayerExist(int clientIndex, bool aliveness = true)
{
	// If client isn't valid
	if (clientIndex <= 0 || clientIndex > MaxClients)
	{
		return false;
	}
	
	// If client isn't connected
	if (!IsClientConnected(clientIndex))
	{
		return false;
	}

	// If client isn't in game
	if (!IsClientInGame(clientIndex))
	{
		return false;
	}

	// If client isn't alive
	if(aliveness && !IsPlayerAlive(clientIndex))
	{
		return false;
	}
	
	// If client exist
	return true;
}

/**
 * Returns true if the player has a current weapon, false if not.
 *
 * @param clientIndex		The client index.
 * @param weaponName		The weapon name.
 *
 * @return					True or false.
 **/
stock bool IsPlayerHasWeapon(int clientIndex, char[] weaponName)
{
	// Initialize char
	char sClassname[SMALL_LINE_LENGTH];

	// Get number of all client weapons
	int iSize = GetEntPropArraySize(clientIndex, Prop_Send, "m_hMyWeapons");
	
	// i = weapon number
	for (int i = 0; i < iSize; i++)
	{
		// Get weapon index
		int weaponIndex = GetEntPropEnt(clientIndex, Prop_Send, "m_hMyWeapons", i);
		
		// If entity isn't valid, then skip
		if (IsValidEdict(weaponIndex))
		{
			// Get weapon classname
			GetEdictClassname(weaponIndex, sClassname, sizeof(sClassname));
			
			// If weapon find, then return
			if (StrEqual(sClassname, weaponName, false))
			{
				return true;
			}
		}
		
		// Go to next weapon
		continue;
	}

	// If wasn't found
	return false;
}  

/**
 * Checks if a timer is currently running.
 * 
 * @param hTimer     		The timer handle.
 **/
stock bool IsTimerRunning(Handle hTimer)
{
    // Return true if the handle isn't empty
    return (hTimer != INVALID_HANDLE);
}

/**
 * Wrapper functions for KilLTimer.
 * Ends a timer if running, and resets its timer handle variable.
 * 
 * @param hTimer     		The timer handle.
 *
 **/
stock void EndTimer(Handle &hTimer)
{
    // If the timer is running, then kill it
    if (IsTimerRunning(hTimer))
    {
        // Kill
        KillTimer(hTimer);
        
        // Reset variable
        hTimer = INVALID_HANDLE;
    }
}

/**
 * Precache models and also adding them into the downloading table.
 * 
 * @param modelPath			The model path.
 *
 * @return					The model index.
 **/
stock int FakePrecacheModel(const char[] modelPath)
{
	// Precache main model
	int modelIndex = PrecacheModel(modelPath);
	
	// Adding main model to the download list
	AddFileToDownloadsTable(modelPath);

	// Initialize path char
	char fullPath[PLATFORM_MAX_PATH];
	char typePath[3][SMALL_LINE_LENGTH] = { ".dx90", ".phy", ".vvd" };
	
	// Get number of the all types
	int iSize = sizeof(typePath);
	
	// i = type index
	for(int i = 0; i < iSize; i++)
	{
		// Adding other parts to download list
		Format(fullPath, sizeof(fullPath), "%s", modelPath);
		ReplaceString(fullPath, sizeof(fullPath), ".mdl", typePath[i]);
		AddFileToDownloadsTable(fullPath);
	}
	
	// Return model index
	return modelIndex;
}

/**
 * Precache sounds and also adding them into the downloading table.
 * 
 * @param soundPath			The sound path. .
 **/
stock void FakePrecacheSound(const char[] soundPath)
{
	// Initialize path char
	char fullPath[PLATFORM_MAX_PATH];
	
	// Adding sound to the download list
	Format(fullPath, sizeof(fullPath), "sound/%s", soundPath);
	AddFileToDownloadsTable(fullPath);
	
	// Precache sound
	Format(fullPath, sizeof(fullPath), "*/%s", soundPath);
	AddToStringTable(FindStringTable("soundprecache"), fullPath);
}