#pragma semicolon 1

#define DEBUG

#include <sourcemod>

#define CTARMS "models/weapons/ct_arms.mdl"
#define TTARMS "models/weapons/t_arms.mdl"

public Plugin:myinfo = 
{
	name = "",
	author = "",
	description = "",
	version = "0.0",
	url = ""
};


public void OnPluginStart()
{
    HookEvent("player_spawn", PlayerSpawn);
}

public OnMapStart()
{
    PrecacheModel(CTARMS, true);
    PrecacheModel(TTARMS, true);
}

public Action PlayerSpawn(Handle event, const char[] name, bool dbc)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    
    if(client)
    {
        switch(GetClientTeam(client))
        {
            case 2: SetEntPropString(client, Prop_Send, "m_szArmsModel", TTARMS);
            case 3: SetEntPropString(client, Prop_Send, "m_szArmsModel", CTARMS);
        }
    }
}  