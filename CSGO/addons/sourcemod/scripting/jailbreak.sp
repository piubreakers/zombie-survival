#pragma semicolon 1

#define DEBUG

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <jailbreak>
#include <resources>

ArrayList users;
ArrayList passwords;
ArrayList characterNames;

// HUD handle
Handle hHudSynchronizer = null;
//Handle hAnnouncerSynchronizer = null;

// Countdown
//int iCounter = ROUND_START_COUNTDOWN;
//Handle hCounter = null;
/*
char ttModels[][]{
	"models/player/custom_player/kuristaja/jailbreak/prisoner1/prisoner1.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner2/prisoner2.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner3/prisoner3.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner4/prisoner4.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner5/prisoner5.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner6/prisoner6.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner7/prisoner7.mdl"	
}

char ttArms[][]{
	"models/player/custom_player/kuristaja/jailbreak/prisoner1/prisoner1_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner2/prisoner2_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner3/prisoner3_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner4/prisoner4_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner5/prisoner5_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner6/prisoner6_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/prisoner7/prisoner7_arms.mdl"
}

char ctModels[][]{
	"models/player/custom_player/kuristaja/jailbreak/guard1/guard1.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard2/guard2.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard3/guard3.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard4/guard4.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard5/guard5.mdl",
	

}
char ctArms[][]{
	"models/player/custom_player/kuristaja/jailbreak/guard1/guard1_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard2/guard2_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard2/guard3_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard2/guard4_arms.mdl",
	"models/player/custom_player/kuristaja/jailbreak/guard2/guard5_arms.mdl",
	
}
*/

enum RazonesUsuario{
	DAR_HABLA,
	ACTIVAR_DIBUJO,
	DUELO_PISTOLAS,
	DUELO_SCOUTS,
	DUELO_AWP,
	DUELO_KNIFE
}

bool gOnEvent;
bool gBox;

float min = 300.0;
ArrayList gDoorlist;

int iSimon = -1;

enum{
	JB_REASON_NEW_SIMON = 0,
	JB_REASON_SIMON_DEAD,
	JB_REASON_LAST_WILL_SELECTED,
	JB_REASON_ROUND_MODE,
	JB_REASON_REASON
}

char PrisonReasons[][] = { "Narcotraficante", "Violador", "Pedófilo", "Asesino", "Portación de armas", "Bigote de leche", "Corrupción", "Crear cheats para PUBG" };

public Plugin myinfo = 
{
	name = "",
	author = "",
	description = "",
	version = "0.0",
	url = ""
};

public OnPluginStart()
{
	users = CreateArray(8, MAXPLAYERS+1);
	passwords = CreateArray(8, MAXPLAYERS+1);
	characterNames = CreateArray(8, MAXPLAYERS+1);
	
	gDoorlist = CreateArray(1);
	// Create synchronizer for HUD channel
	hHudSynchronizer = CreateHudSynchronizer();
	//hAnnouncerSynchronizer = CreateHudSynchronizer();
	
	RegConsoleCmd("gime", gime);
	
	ServerCommand("exec gamemode_casual.cfg");
	//HookEvent("player_changename", EventPrePlayerChangeName, EventHookMode_Pre);
	LoadPresos();
	
	RegConsoleCmd("mainmenu", showMainMenu);
	RegConsoleCmd("voluntad", showMenuVoluntad);
	RegConsoleCmd("cambiar", cambiarTeam);
	RegConsoleCmd("simon", serSimon);
	RegConsoleCmd("reglas", showReglas);
	
	HookEvent("round_start", EventPostRoundStart, EventHookMode_Post);
	HookEvent("round_end", EventRoundEnd, EventHookMode_Pre);
	
	HookEvent("player_death", EventPlayerDeath, EventHookMode_Post);
	HookEvent("player_spawn", EventPostPlayerSpawn, EventHookMode_Post);
	
	HookEvent("weapon_fire", EventWeaponFire, EventHookMode_Pre);
}

public Action cambiarTeam(int client, int args){
	JPlayer p = JPlayer(client);
	if(p.iTeamNum == CS_TEAM_CT) p.iTeamNum = CS_TEAM_T;
	else if(p.iTeamNum == CS_TEAM_T) p.iTeamNum = CS_TEAM_CT;
	
	return Plugin_Handled;
}
public Action showReglas(int client, int args){
	PrintToChat(client, "%s Reglas del server: ", SERVERSTRING);
	PrintToChat(client, "%s 1) Solo pueden ser policias los usuarios CON microfono, de lo contrario seran kickeados.", SERVERSTRING);
	PrintToChat(client, "%s 2) Los policias no pueden entrar a las celdas de los prisioneros. (Ni a celdas principales).", SERVERSTRING);
	PrintToChat(client, "%s 3) Los policias pueden matar a los prisioneros que tengan armas(Solo si los vieron con el arma encima).", SERVERSTRING);
	PrintToChat(client, "%s 4) Los policias pueden matar a los prisioneros que esten en ductos o en armas", SERVERSTRING);
	PrintToChat(client, "%s 5) Las ordenes de simon cancelan las ordenes anteriores.", SERVERSTRING);
	PrintToChat(client, "%s 6) Los policias no pueden matar al prisionero con ultima voluntad sin razon.", SERVERSTRING);
	return Plugin_Handled;
}
public Action gime(int client, int args){
	JPlayer p = JPlayer(client);
	p.iCigarros += 100;
	return Plugin_Handled;
}

public Action serSimon(int client, int args){
	bool haySimon = false;
	JPlayer player = JPlayer(client);
	if(player.iTeamNum != CS_TEAM_CT) {
		PrintToChat(client, "%s No eres policia.", SERVERSTRING);
		return Plugin_Handled;
	}
	for(int i = 1; i < MAXPLAYERS+1; i++){
		if(IsPlayerExist(i, true)){
			JPlayer p = JPlayer(i);
			if(p.bSimon){
				haySimon = true;
				break;
			}
		}
	}
	
	if(!haySimon){
		hacerSimon(client);
	}
	return Plugin_Handled;
}

public OnMapStart(){
	GetExclusionList();
	CacheDoors();
	
	for(int x = 0; x < sizeof(g_cRootDirectoryName); x++){
		RegisterResources(x, view_as<DirectoryListing>(INVALID_HANDLE), g_cRootDirectoryName[x]);
	}
	for(int y = 0; y < sizeof(g_cRootDirectoryName); y++){
		delete g_FileExclusionList[y];
	}
	
	//AddToStringTable(FindStringTable("soundprecache"), DUEL_SOUND);
	
	gOnEvent = false;
	//hCounter = null;
	//iCounter = ROUND_START_COUNTDOWN;
}
public OnMapEnd(){
	for(int i = 1; i < MAXPLAYERS; i++){
		users.SetString(i, "");
		passwords.SetString(i, "");
		characterNames.SetString(i, "");
		if(IsPlayerExist(i)){
			saveCharacter(i);
		}
	}
	ClearArray(gDoorlist);
}

public Action printVipSay(int client, char text[128]){
	JPlayer player = JPlayer(client);
	
	if (client != 100){
		if (!player.bAdmin && !player.bStaff)
			return Plugin_Handled;
	}
	
	char name[64];
	if (client != 100)
		GetClientName(player.id, name, sizeof(name));
	else name = "INFO";
	
	char msg[192];
	FormatEx(msg, sizeof(msg), "%s: %s", name, text);
	
	Handle hRotativeSynchronizer = CreateHudSynchronizer();
	
	// Show hud message
	SetHudTextParams(0.03, GetRandomFloat(0.35, 0.7), VIP_SAY_DURATION, GetRandomInt(55, 255), GetRandomInt(55, 255), GetRandomInt(55, 255), 255, 1, 3.0, 0.4, 0.4);
	for (int i = 1; i <= MaxClients; i++){
		JPlayer puto = JPlayer(i);
		
		if (!IsPlayerExist(puto.id))
			continue;

		ShowSyncHudText(puto.id, hRotativeSynchronizer, msg);
		PrintToConsole(puto.id, msg);
	}
	//CloseHandle(hRotativeSynchronizer);
	delete hRotativeSynchronizer;
	return Plugin_Handled;
}


public OnClientPutInServer(int client){
	users.SetString(client, "");
	passwords.SetString(client, "");
	characterNames.SetString(client, "");
	if(IsPlayerExist(client)){
		CreateTimer(0.5, PrintMsg, client, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
	JPlayer p = JPlayer(client);
	p.bLogged = false;
	p.bInGame = false;
	
	SDKHook(client, SDKHook_TraceAttack, OnTraceAttack);
	SDKHook(client, SDKHook_WeaponSwitchPost, OnWeaponSwitchPost);
}

public OnClientDisconnect(int client){
	users.SetString(client, "");
	passwords.SetString(client, "");
	characterNames.SetString(client, "");
	saveCharacter(client);
	JPlayer p = JPlayer(client);
	if(p.bSimon) {
		if(fnGetAliveInTeam(CS_TEAM_CT) < 1) CS_TerminateRound(5.0, CSRoundEnd_TerroristWin);
		else PrintToChatAll("%s \x0BSimon\x01 se tomó el palo. Los policias pueden ser simon poniendo \x05!simon\x01.");
		
		iSimon = -1;
	}
	SDKUnhook(client, SDKHook_TraceAttack, OnTraceAttack);
	SDKUnhook(client, SDKHook_WeaponSwitchPost, OnWeaponSwitchPost);
}

public void OnWeaponSwitchPost(int client, int weapon){
	JPlayer player = JPlayer(client);
	
	if(!IsPlayerExist(player.id) || !IsValidEntity(weapon))
		return;
		
	char sWpn[32]; 
	GetEdictClassname(weapon, sWpn, sizeof(sWpn));
	
	if(StrEqual(sWpn, "weapon_knife")){
		if (player.iTeamNum == CS_TEAM_T){
			if (player.bMartillo){
				SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(MODEL_MARTILLO));
				SetWorldModel(weapon, GetModelIndex(WMODEL_MARTILLO));
			}
			else{
				SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(VMODEL_FISTS));
				hideWorldModel(weapon);
			}
		}
		else if (player.iTeamNum == CS_TEAM_CT){
			SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(VMODEL_BATON));
			SetWorldModel(weapon, GetModelIndex(WMODEL_BATON));
		}
	}
}

public void OnClientPostAdminCheck(int client){
	EmitSoundToClient(client, WELCOME_SOUND, SOUND_FROM_PLAYER, SNDCHAN_STATIC, SNDLEVEL_NORMAL,_,_,_,_,_,_,_,5.0);
}
public Action OnClientCommand(int client, int args)
{
	JPlayer player = JPlayer(client);
	char cmd[16];

	if (!IsPlayerExist(player.id))
		return Plugin_Continue;

	GetCmdArg(0, cmd, sizeof(cmd));
	if ( StrEqual(cmd, "jointeam") || StrEqual(cmd, "chooseteam")){
		if(player.iTeamNum == CS_TEAM_NONE || player.iTeamNum == CS_TEAM_SPECTATOR || !player.bLogged){
			showLoginMenu(player.id);
		}
		return Plugin_Handled;
	}
	if(StrEqual(cmd, "buy")){
		PrintHintText(player.id,"You cant buy");
		return Plugin_Handled;
	}
	if(StrEqual(cmd, "name")) return Plugin_Handled;
	if(StrEqual(cmd, "kill")) return Plugin_Handled;
	
	
	return Plugin_Continue;
}
public Action OnClientSayCommand(int client, const char[] command, const char[] sArgs){
	JPlayer player = JPlayer(client);
	if(player.bInUser){
		char name[64];
		strcopy(name, 64, sArgs);
		switch(isEntryValidToSQL(name, DATA_NAME)){
			case INPUT_SHORT:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email muy corto.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_LARGE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email muy largo.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_INVALID_SIMBOL:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email contiene simbolos.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_INVALID_SPACE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email contiene espacios.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_OK: {//makeValidToSQL(name, namev, DATA_NAME);
				users.SetString(client, name);
				PrintHintText(client, "El email ingresado es: %s", name);
				showIndicacionesRegister(client, true);
			}
		}
		return Plugin_Handled;
	}
	if(player.bInPassword) {
		char pass[32];
		strcopy(pass, 32, sArgs);
		switch(isEntryValidToSQL(pass, DATA_PASSWORD)){
			case INPUT_SHORT:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña muy corto.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_LARGE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña muy largo.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_INVALID_SIMBOL:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña contiene simbolos.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_INVALID_SPACE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña contiene espacios.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_OK: {
				passwords.SetString(client, pass);
				PrintHintText(client, "La contraseña ingresado es: %s", pass);
				showIndicacionesPasswordRegister(client, true);
			}
		}
		return Plugin_Handled;
	}
	if(player.bInCreatingCharacter){
		char name[32];
		strcopy(name, 32, sArgs);
		switch(isEntryValidToSQL(name, DATA_NAME)){
			case INPUT_SHORT:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre muy corto.");
				showCrearCharacter(client, false);
			}
			case INPUT_LARGE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre muy largo.");
				showCrearCharacter(client, false);
			}
			case INPUT_INVALID_SIMBOL:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre contiene simbolos.");
				showCrearCharacter(client, false);
			}
			case INPUT_INVALID_SPACE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre contiene espacios.");
				showCrearCharacter(client, false);
			}
			case INPUT_OK: {
				characterNames.SetString(client, name);
				PrintHintText(client, "El nombre ingresado es: %s", name);
				showCrearCharacter(client, true);
			}
		}
		return Plugin_Handled;
	}
	if(player.bStaff || player.bAdmin){
		char msg[128];
		strcopy(msg, sizeof(msg), sArgs);
		if(msg[0] == '@' && msg[1] == '@'){
			ReplaceStringEx(msg, sizeof(msg), "@", "");
			ReplaceStringEx(msg, sizeof(msg), "@", "");
			printVipSay(client, msg);
			return Plugin_Handled;
		}
	}
	return Plugin_Continue;
}

public Action EventPrePlayerChangeName(Handle event, const char[] name, bool dontBroadcast){
	if(!dontBroadcast){
		Handle new_event = CreateEvent("player_changename", true);
		
		char oldname[32];
		char newname[32];
		
		GetEventString(event, "oldname", oldname, sizeof(oldname));
		GetEventString(event, "newname", newname, sizeof(newname));
		
		SetEventInt(new_event, "userid", GetEventInt(event, "userid"));
		SetEventString(new_event, "oldname", oldname);
		SetEventString(new_event, "newname", newname);
		
		FireEvent(new_event, true);
		
		return Plugin_Handled;
	}
	return Plugin_Handled; 
}
public Action EventPlayerDeath(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	JPlayer victim = JPlayer(GetClientOfUserId(gEventHook.GetInt("userid")));
	JPlayer attacker = JPlayer(GetClientOfUserId(gEventHook.GetInt("attacker")));
	JPreso atType = JPreso(attacker.iTipoPresoSelected);
	
	if(gOnEvent && attacker.iAdversario == victim.id){
		gOnEvent = false;
		attacker.iAdversario = 0;
		victim.iAdversario = 0;
	}
	if(attacker.iTeamNum == CS_TEAM_T){
		int gain = RoundToFloor(GetRandomInt(1, 3) * atType.flCigarros);
		attacker.iCigarros += gain;
	}
	if(victim.bSimon){
		victim.bSimon = false;
		PrintToChatAll("%s Se cagaron en \x0Bsimon\x01!.");
	}
	if(fnGetAliveInTeam(CS_TEAM_T) == 1 && fnGetAliveInTeam(CS_TEAM_CT) >= 1){
		for(int i=1; i < MAXPLAYERS; i++){
			JPlayer p = JPlayer(i);
			if(IsPlayerAlive(p.id) && p.iTeamNum == CS_TEAM_T){
				showMenuVoluntad(i, 0);
				break;
			}
		}
	}
	
	if(fnGetAliveInTeam(CS_TEAM_CT) > 0 && fnGetAliveInTeam(CS_TEAM_T) == 0){
		CS_TerminateRound(12.5, CSRoundEnd_CTWin);
		return Plugin_Continue;
	}
	else if(fnGetAliveInTeam(CS_TEAM_CT) == 0 && fnGetAliveInTeam(CS_TEAM_T) > 0){
		CS_TerminateRound(12.5, CSRoundEnd_TerroristWin);
		return Plugin_Continue;
	}
	
	return Plugin_Continue;
}

public Action EventWeaponFire(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	JPlayer player = JPlayer(GetClientOfUserId(gEventHook.GetInt("userid")));
	
	if(gOnEvent){
		int weapon = GetPlayerWeaponSlot(player.id, CS_SLOT_PRIMARY);
		bool isKnife = false;
		if(weapon == -1) weapon = GetPlayerWeaponSlot(player.id, CS_SLOT_SECONDARY);
		if(weapon == -1) {
			weapon = GetPlayerWeaponSlot(player.id, CS_SLOT_KNIFE);
			isKnife=true;
		}
		if(player.iAdversario != 0 && !isKnife){
			SetEntProp(weapon, Prop_Send, "m_iClip1", 1);
			SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 1);
		}
	}
	return Plugin_Continue;
}

public Action EventPostPlayerSpawn(Handle event, const char[] name, bool dontBroadcast){
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	JPlayer player = JPlayer(client);
	
	if(player.iTipoPresoSelected != player.iNextPreso) player.iTipoPresoSelected = player.iNextPreso;
	// He doesnt exist, doesn't matter
	if (!IsPlayerExist(player.id))
		return Plugin_Handled;
	
	player.removeWeapons();
	int modelNum;
	char model[128];
	char armModel[128];
	if(player.iTeamNum == CS_TEAM_T){
		modelNum = GetRandomInt(1, 7);
		FormatEx(model, sizeof(model), "models/player/custom_player/kuristaja/jailbreak/prisoner%d/prisoner%d.mdl", modelNum, modelNum);
		FormatEx(armModel, sizeof(armModel), "models/player/custom_player/kuristaja/jailbreak/prisoner%d/prisoner%d_arms.mdl", modelNum, modelNum);
		player.setAttribs();
		
		SendHudAnnouncement(player.id, JB_REASON_REASON/*, true*/);
	}
	else if(player.iTeamNum == CS_TEAM_CT){
		modelNum = GetRandomInt(1, 5);
		FormatEx(model, sizeof(model), "models/player/custom_player/kuristaja/jailbreak/guard%d/guard%d.mdl", modelNum, modelNum);
		FormatEx(armModel, sizeof(armModel), "models/player/custom_player/kuristaja/jailbreak/guard%d/guard%d_arms.mdl", modelNum, modelNum);
	}
	player.setModel(model, armModel);
	player.bMuted = true;
	//int msgDay = GetRandomInt(0, 2);
	//PrintToChat(client, "%s %s", SERVERSTRING,);
	
	if(player.bStaff) player.bMuted = false;
	
	// Cache important variables
	player.iPredictedViewModelIndex = Weapon_GetViewModelIndex(player.id, -1);
	
	player.bMartillo = false;
	player.bLockpick = false;
	player.iFOV(100);
	
	FakeClientCommand(client, "use weapon_knife");

	return Plugin_Continue;
}

public Action EventPostRoundStart(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	
	// Print some info
	SendAnnouncement();
	gOnEvent = false;
	gBox = false;
	
	/*
	for (int i = 1; i < MaxClients; i++){
		JPlayer player = JPlayer(i);
		
		if (player.iTeamNum != CS_TEAM_T)
			continue;
		
		SendHudAnnouncement(i, JB_REASON_REASON, true);
	}*/
	
	/*if (hCounter != null){
		delete hCounter;
		hCounter = null;
		iCounter = ROUND_START_COUNTDOWN;
	}
	hCounter = CreateTimer(1.0, TimerShowCountdown, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);*/
}
public Action EventRoundEnd(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	
	// If the counter is still alive, kill it
	/*if(hCounter != null){
		delete hCounter;
		hCounter = null;
		iCounter = ROUND_START_COUNTDOWN;
	}*/
	for(int i = 1; i < MAXPLAYERS+1; i++){
		JPlayer p = JPlayer(i);
		if(IsPlayerExist(i)){
			p.bSimon = false;
			saveCharacter(i);
		}
	}
	
	int reason = gEventHook.GetInt("reason");
	PrintToChatAll("Reason: %d", reason);
	
	if(reason == 9) EmitSoundToAll(TT_WIN_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, 0.75);
	else if(reason == 8) EmitSoundToAll(CT_WIN_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, 0.75);
	
	balanceTeams();
	return Plugin_Continue;
}

public Action OnTraceAttack(victim, &attacker, &inflictor, &Float:damage, &damagetype, &ammotype, hitbox, hitgroup)
{
	JPlayer Attacker = JPlayer(attacker);	
	JPlayer Victim = JPlayer(victim);
	if(Victim.iTeamNum == CS_TEAM_CT && Attacker.iTeamNum == CS_TEAM_CT) return Plugin_Handled;
	
	if(!gBox && Victim.iTeamNum == CS_TEAM_T && Attacker.iTeamNum == CS_TEAM_T) return Plugin_Handled;
	if(gOnEvent){
		if(Attacker.iAdversario != Victim.id) return Plugin_Handled;
	}
	
	
	return Plugin_Continue;
}

public Action showMainMenu(int client, int args){
	JPlayer p = JPlayer(client);
	if(!p.bLogged){
		showLoginMenu(client);
		return Plugin_Handled;
	}
	Menu menu = new Menu(MainMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("[JB] Menu Principal");
	menu.AddItem("0", p.iTeamNum == CS_TEAM_CT ? "Menu de Simon" : "Menu de Ultima Voluntad");
	menu.AddItem("1", "Mercado negro");
	menu.AddItem("2", "Tipo de preso");
	menu.AddItem("3", "Pasarse de equipo");
	menu.AddItem("4", "Ajustes");
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MainMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			//player.bInUser = true;
			switch(selection){
				case 0:{
					if(player.iTeamNum == CS_TEAM_CT){
						if(player.bSimon) showMenuSimon(client, 0); 
						else PrintToChat(client, "%s No eres simon.", SERVERSTRING);
					} else{
						showMenuVoluntad(client, 0);
					}
				}
				case 1: {
					if(player.iTeamNum == CS_TEAM_T) showMenuItemsExtra(client);
					else PrintToChat(client, "%s Solo los presos pueden acceder al mercado negro.", SERVERSTRING);
				}
				case 2: showMenuPreso(client, 0);
				case 3: checkChangeTeam(client, player.iTeamNum);
				case 4: PrintToChat(client, "%s En Construccion.", SERVERSTRING);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showMenuUsuarios(int client, RazonesUsuario razon){
	char buff[16], raz[4], id[4], name[32];
	IntToString(view_as<int>(razon), raz, sizeof(raz));
	Menu menu = new Menu(UsuariosMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("[JB] Selecciona un jugador");
	for(int i = 1; i < MAXPLAYERS+1; i++){
		JPlayer p = JPlayer(i);
		if(IsPlayerExist(i, true) && p.iTeamNum == CS_TEAM_CT){
			IntToString(i, id, sizeof(id));
			FormatEx(buff, sizeof(buff), "%s|||||%s", raz, id);
			GetClientName(i,name, sizeof(name));
			menu.AddItem(buff, name);
		}
	}
	
	
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int UsuariosMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			char buf[16], string[2][4];
			
			menu.GetItem(selection, buf, sizeof(buf));
			
			PrintToServer(buf);
			
			ExplodeString(buf, "|||||", string, 2, sizeof(buf));
			
			PrintToServer("%s | %s", string[0], string[1]);
			
			int razon = StringToInt(string[0]);
			int id = StringToInt(string[1]);
			JPlayer selected = JPlayer(id);
			
			switch(razon){
				case DAR_HABLA:{
					char Buff[32];
					GetClientName(id, Buff, sizeof(Buff));
					PrintToChatAll("%s Simon le dio habla a %s", SERVERSTRING, Buff);
					selected.bMuted = !selected.bMuted;
				}
				case DUELO_PISTOLAS:{
					if(fnGetAliveInTeam(CS_TEAM_T) == 1){
						EmitSoundToAll(DUEL_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, 0.75);
					
						selected.removeWeapons();
						player.removeWeapons();
						
						player.iHealth = 100;
						selected.iHealth = 100;
						
						int weap1 = GivePlayerItem(player.id, "weapon_deagle");
						int weap2 = GivePlayerItem(selected.id, "weapon_deagle");
						
						SetEntProp(weap1, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
						SetEntProp(weap2, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
						
						SetEntProp(weap1, Prop_Send, "m_iClip1", 1);
						SetEntProp(weap2, Prop_Send, "m_iClip1", 1);
						
						gOnEvent = true;
						player.iAdversario = selected.id;
						selected.iAdversario = player.id;
						
						player.iArmor = 100;
						selected.iArmor = 100;
					} 
				}
				case DUELO_AWP:{
					if(fnGetAliveInTeam(CS_TEAM_T) == 1){
						EmitSoundToAll(DUEL_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, 0.5);
						
						selected.removeWeapons();
						player.removeWeapons();
						
						player.iHealth = 100;
						selected.iHealth = 100;
						
						int weap1 = GivePlayerItem(player.id, "weapon_awp");
						int weap2 = GivePlayerItem(selected.id, "weapon_awp");
						
						SetEntProp(weap1, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
						SetEntProp(weap2, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
						
						SetEntProp(weap1, Prop_Send, "m_iClip1", 1);
						SetEntProp(weap2, Prop_Send, "m_iClip1", 1);
						
						gOnEvent = true;
						player.iAdversario = selected.id;
						selected.iAdversario = player.id;
						
						player.iArmor = 100;
						selected.iArmor = 100;
					} 
				}
				case DUELO_SCOUTS:{
					if(fnGetAliveInTeam(CS_TEAM_T) == 1){
						EmitSoundToAll(DUEL_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, 0.5);
						
						selected.removeWeapons();
						player.removeWeapons();
						
						player.iHealth = 100;
						selected.iHealth = 100;
						
						int weap1 = GivePlayerItem(player.id, "weapon_ssg08");
						int weap2 = GivePlayerItem(selected.id, "weapon_ssg08");
						
						SetEntProp(weap1, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
						SetEntProp(weap2, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
						
						SetEntProp(weap1, Prop_Send, "m_iClip1", 1);
						SetEntProp(weap2, Prop_Send, "m_iClip1", 1);
						
						gOnEvent = true;
						player.iAdversario = selected.id;
						selected.iAdversario = player.id;
						
						player.iArmor = 100;
						selected.iArmor = 100;
					} 
				}
				case DUELO_KNIFE:{
					if(fnGetAliveInTeam(CS_TEAM_T) == 1){
						EmitSoundToAll(DUEL_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, 0.5);
						
						player.iHealth = 100;
						selected.iHealth = 100;
						
						selected.removeWeapons();
						player.removeWeapons();
						
						gOnEvent = true;
						player.iAdversario = selected.id;
						selected.iAdversario = player.id;
						
						player.iArmor = 100;
						selected.iArmor = 100;
					} 
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showMenuPreso(int client, int args){
	Menu menu = new Menu(PresoMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("[JB] Tipos de preso");
	JPlayer p = JPlayer(client);
	char buff[128];
	char nam[32];
	for(int i = 0; i < gPNombre.Length; i++){
		JPreso preso = JPreso(i);
		gPNombre.GetString(i,nam, sizeof(nam));
		Format(buff, sizeof(buff), "%s (%d%% HP, %d%% ARM, %4.2f%% DMG, %4.2f%% VEL, %4.2f%% GRAV, %4.2f%% CIG)", nam, preso.iHealth, preso.iArmor, preso.flDamage, preso.flVelocidad, preso.flGravedad, preso.flCigarros);
		menu.AddItem("", buff, i==p.iTipoPresoSelected ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int PresoMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			player.iNextPreso = selection;
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showMenuSimon(int client, int args){
	JPlayer p = JPlayer(client);
	if(!p.bSimon) return Plugin_Handled;
	Menu menu = new Menu(SimonMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("[JB] Menu de Simon");
	menu.AddItem("0", "Abrir celdas");
	menu.AddItem("1", "Activar Box");
	menu.AddItem("2", "Activar dibujo");
	menu.AddItem("3", "Dar habla a compañero");
	menu.AddItem("4", "Empezar modo");
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int SimonMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	if(!player.bSimon) return 0;
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0: {
					if(player.bSimon) openCells(client);
					else PrintToChat(client, "%s No eres simon.", SERVERSTRING);
				}
				case 1: {
					gBox = !gBox;
					PrintToChatAll("%s Box %s", SERVERSTRING, gBox ? "Activado" : "Desactivado");
				}
				case 2: PrintToChat(client, "%s En Construccion.", SERVERSTRING);
				case 3: showMenuUsuarios(client, DAR_HABLA);
				case 4: PrintToChat(client, "%s En Construccion.", SERVERSTRING);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showMenuItemsExtra(int client){
	Menu menu = new Menu(ItemsExtraMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	
	char deagle[64], lockpick[64], merk[64], jeringa[64], gas[64], chaleco[64], bate[64], disfraz[64];
	
	Format(deagle, sizeof(deagle), "Deagle con 2 bala (%d Cigarrillos)", DEAGLE_PRICE);
	Format(lockpick, sizeof(lockpick), "Lockpick (%d Cigarrillos)", LOCKPICK_PRICE);
	Format(merk, sizeof(merk), "Mer-K (%d Cigarrillos)", MERCA_PRICE);
	Format(jeringa, sizeof(jeringa), "Jeringa (%d Cigarrillos)", JERINGA_PRICE);
	Format(gas, sizeof(gas), "Gas Pimienta (%d Cigarrillos)", PEPPERSPRAY_PRICE);
	Format(chaleco, sizeof(chaleco), "100 Chaleco (%d Cigarrillos)", ARMOR_PRICE);
	Format(bate, sizeof(bate), "Martillo (%d Cigarrillos)", BATE_PRICE);
	Format(disfraz, sizeof(disfraz), "Disfraz de policia (%d Cigarrillos)", POLICE_PRICE);
	
	menu.SetTitle("[JB] Mercado Negro");
	menu.AddItem("0", deagle);
	menu.AddItem("1", lockpick);
	menu.AddItem("2", merk);
	menu.AddItem("3", jeringa);
	menu.AddItem("4", gas);
	menu.AddItem("5", chaleco);
	menu.AddItem("6", bate);
	menu.AddItem("7", disfraz);
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int ItemsExtraMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			//player.bInUser = true;
			PrintToChat(client, "%d, %d, %b", selection, player.iTeamNum, IsPlayerExist(client, true));
			if(player.iTeamNum == CS_TEAM_T && IsPlayerExist(client, true)){
				switch(selection){
					case 0: {
						if(player.iCigarros >= DEAGLE_PRICE){
							player.iCigarros-= DEAGLE_PRICE;
							int weap = GivePlayerItem(player.id, "weapon_deagle");
							SetEntProp(weap, Prop_Send, "m_iClip1", 7);
							SetEntProp(weap, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
							PrintToChat(player.id, "%s Compraste \x09Deagle\x01.", SERVERSTRING);
						} else{
							PrintToChat(player.id, "%s No tienes suficientes cigarrillos.", SERVERSTRING);
						}
					}
					case 1: {
						if(player.iCigarros >= LOCKPICK_PRICE){
							player.iCigarros-= LOCKPICK_PRICE;
							player.bLockpick = true;
							PrintToChat(player.id, "%s Compraste \x09lockpick\x01.", SERVERSTRING);
						}
						else{
							PrintToChat(player.id, "%s No tienes suficientes cigarrillos.", SERVERSTRING);
						}
					}
					case 2: {
						if(player.iCigarros >= MERCA_PRICE){
							player.iCigarros-= MERCA_PRICE;
							player.iFOV(150);
							player.flSpeed = player.flSpeed+0.12;
							PrintToChat(player.id, "%s Compraste \x09Merca\x01.", SERVERSTRING);
						}
						else{
							PrintToChat(player.id, "%s No tienes suficientes cigarrillos.", SERVERSTRING);
						}
					}
					case 3: PrintToChat(client, "%s En Construccion.%d", SERVERSTRING, selection);
					case 4: PrintToChat(client, "%s En Construccion.%d", SERVERSTRING, selection);
					case 5: {
						if(player.iCigarros >= ARMOR_PRICE){
							player.iCigarros-= ARMOR_PRICE;
							player.iArmor += 100;
							PrintToChat(player.id, "%s Compraste\x09 100 de chaleco\x01.", SERVERSTRING);
						} 
						else{
							PrintToChat(player.id, "%s No tienes suficientes cigarrillos.", SERVERSTRING);
						}
					}
					case 6: {
						if(player.iCigarros >= BATE_PRICE){
							player.iCigarros-= BATE_PRICE;
							player.bMartillo = true;
							
							CSGO_ReplaceWeapon(player.id, CS_SLOT_KNIFE, "weapon_knife");
							
							FakeClientCommandEx(player.id, "use weapon_knife");
							
							char weapon[32];
							GetClientWeapon(player.id, weapon, 32);
							
							PrintToChat(player.id, "%s Compraste\x09 Martillo\x01.", SERVERSTRING);
						} 
						else{
							PrintToChat(player.id, "%s No tienes suficientes cigarrillos.", SERVERSTRING);
						}
					}
					case 7: {
						if(player.iCigarros >= POLICE_PRICE){
							player.iCigarros-= POLICE_PRICE;
							int random = GetRandomInt(1, 5);
							char buff[128], buff2[128];
							Format(buff, sizeof(buff), "models/player/custom_player/kuristaja/jailbreak/guard%d/guard%d.mdl", random, random);
							Format(buff2, sizeof(buff2), "models/player/custom_player/kuristaja/jailbreak/guard%d/guard%d_arms.mdl", random, random);
							player.setModel(buff, buff2);
							PrintToChat(player.id, "%s Comprado\x09 traje de policia\x01.", SERVERSTRING);
						}
						else{
							PrintToChat(player.id, "%s No tienes suficientes cigarrillos.", SERVERSTRING);
						}
					}
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showMenuVoluntad(int client, int args){
	JPlayer p = JPlayer(client);
	if(!gOnEvent &&IsPlayerAlive(client) && p.iTeamNum == CS_TEAM_T && fnGetAliveInTeam(CS_TEAM_T) == 1){
		Menu menu = new Menu(VoluntadMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
		menu.SetTitle("[JB] Ultima Voluntad");
		menu.AddItem("0", "Dia libre proximo dia");
		menu.AddItem("1", "Duelo de pistolas");
		menu.AddItem("2", "Duelo de Scouts");
		menu.AddItem("3", "Duelo de AWP");
		menu.AddItem("4", "Duelo de Knife");
		menu.AddItem("5", "Free Kill");
		menu.AddItem("6", "Elegir evento proximo dia");
		menu.ExitButton = true;
		
		menu.Display(client, MENU_TIME_FOREVER);
	}
	
	
	return Plugin_Handled;
}
public int VoluntadMenuHandler(Menu menu, MenuAction action, int client, int selection){
	//JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			//player.bInUser = true;
			if(fnGetAliveInTeam(CS_TEAM_T) == 1 && IsPlayerAlive(client)){
				switch(selection){
					case 0: {
						PrintToChat(client, "%s En Construccion.", SERVERSTRING);
						showMenuVoluntad(client,0);
					}
					case 1: showMenuUsuarios(client, DUELO_PISTOLAS);
					case 2: showMenuUsuarios(client, DUELO_SCOUTS);
					case 3: showMenuUsuarios(client, DUELO_AWP);
					case 4: showMenuUsuarios(client, DUELO_KNIFE);
					case 5: {
						PrintToChat(client, "%s En Construccion.", SERVERSTRING);
						showMenuVoluntad(client,0);
					}
					case 6: {
						PrintToChat(client, "%s En Construccion.", SERVERSTRING);
						showMenuVoluntad(client,0);
					}
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showLoginMenu(int client){
	Menu menu = new Menu(LoginMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("[JB] Menu de registro");
	menu.AddItem("0", "Iniciar Sesion");
	menu.AddItem("1", "Registrarse");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int LoginMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			//player.bInUser = true;
			switch(selection){
				case 0: loginPlayer(player.id, true);//showIndicacionesLogin(client);
				case 1: {
					player.bInUser = true;
					showIndicacionesRegister(client, false);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showIndicacionesLogin(int client, bool canAccept){
	Menu menu = new Menu(IndLoginMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Para iniciar sesion, debes escribir en\nel chat tu email sin espacios.\nLuego pulsa siguiente..");
	menu.AddItem("0", "Siguiente...", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndLoginMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0: {
					player.bInUser = false;
					player.bInPassword = true;
					showIndicacionesPassword(client);
				}
				case 1: {
					player.bInUser = false;
					showLoginMenu(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showIndicacionesPassword(int client){
	Menu menu = new Menu(IndLoginPasswordMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("..Ahora debes, de la misma manera,\ningresar tu contraseña.\nLuego pulsa aceptar.");
	menu.AddItem("0", "Aceptar");
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndLoginPasswordMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0:{
					player.bInPassword = false;
					loginPlayer(client, false);
				}
				
				case 1:{
					player.bInPassword = false;
					showLoginMenu(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showIndicacionesRegister(int client, bool canAccept){
	Menu menu = new Menu(IndRegisterMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Para registrarte, debes escribir en\nel chat tu email sin espacios.\nLuego pulsa siguiente..");
	menu.AddItem("0", "Siguiente...", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndRegisterMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					player.bInUser = false;
					player.bInPassword = true;
					showIndicacionesPasswordRegister(client, false);
				}
				case 1: showLoginMenu(client);
				
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showIndicacionesPasswordRegister(int client, bool canAccept){
	Menu menu = new Menu(IndRegisterPassMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("..Ahora debes, de la misma manera,\ningresar tu contraseña.\nLuego pulsa aceptar.");
	menu.AddItem("0", "Aceptar", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndRegisterPassMenuHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					player.bInPassword = false;
					registerPlayer(client);
				}
				case 1:{
					player.bInPassword = false;
					showLoginMenu(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action showCrearCharacter(int client, bool canAccept){
	Menu menu = new Menu(crearCharacterHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Debes ingresar por chat el nombre\nque deseas para tu personaje.");
	menu.AddItem("0", "Aceptar", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int crearCharacterHandler(Menu menu, MenuAction action, int client, int selection){
	JPlayer player = JPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					player.bInCreatingCharacter = false;
					createCharacter(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action registerPlayer(int client){
	int steam_id = GetSteamAccountID(client);
	char mail[32];
	char pass[32];
	users.GetString(client, mail, 32);
	passwords.GetString(client, pass, 32);
		
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	
	if (!isEntryValidToSQL(mail, DATA_MAIL))
	{
		PrintToServer("Email invalido, vuelve a intentar...");
		showLoginMenu(client);
		return Plugin_Handled;
	}
	
	/* Check if we haven't already created the statement */
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "INSERT INTO Players(email, contrasenia, steamid) VALUES(?, ?, ?);", error2, sizeof(error2));
		if (hUserStmt == null)
		{
			PrintToServer(error2);
			return Plugin_Handled;
		}
	}
	
	SQL_BindParamString(hUserStmt, 0, mail, false);
	SQL_BindParamString(hUserStmt, 1, pass, false);
	SQL_BindParamInt(hUserStmt, 2, steam_id, false);
	if (!SQL_Execute(hUserStmt)){
		return Plugin_Handled;
	}
	
	PrintHintText(client, "Registrado correctamente como\nMail: %s\nContraseña: %s ", mail, pass);
	showLoginMenu(client);
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public Action loginPlayer(int client, bool autolog){
	int steam_id = GetSteamAccountID(client);
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (hUserStmt == null)
	{
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT P.steamid FROM Players P WHERE P.steamid=?", error2, sizeof(error2));
		if (hUserStmt == null)
		{
			PrintToServer(error2);
			return Plugin_Handled;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, steam_id, false);
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Error en loginPlayer al ejecutar consulta");
		return Plugin_Handled;
	}
	GetValues(client, hUserStmt);	
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public Action GetValues(int client, Handle query){
	JPlayer player = JPlayer(client);
	int rows = SQL_GetRowCount(query);
	if(rows == 0 ){
		PrintToServer("0 filas devueltas de loginPlayer");
		showLoginMenu(client);
		return Plugin_Handled;
	}
	player.bLogged = true;
	loadCharacter(client);

	
	return Plugin_Handled;
}
public Action loadCharacter(int client){
	int steam_id = GetSteamAccountID(client);
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT C.nombre, C.cigarros, C.vip, C.id FROM Players p RIGHT JOIN JBCharacter C ON (p.id = C.idPlayer) WHERE p.steamid = ?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			return Plugin_Handled;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, steam_id, false);
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Error en loadCharacter.");
		return Plugin_Handled;
	}
	
	
	JPlayer player = JPlayer(client);
	char name[32];

	int rows = SQL_GetRowCount(hUserStmt);
	
	// Si no encontro nada return;
	if(rows == 0 ){
		player.bInCreatingCharacter = true;
		showCrearCharacter(client, false);
		return Plugin_Handled;
	}
	//Si se encontro, cargar los datos del personaje.
	else{
		//Moverse a la fila que se encontro.
		if(SQL_FetchRow(hUserStmt)){
			//Carga de datos
			SQL_FetchString(hUserStmt, 0, name, 32);
			player.iCigarros = SQL_FetchInt(hUserStmt, 1);
			player.iVipLevel = SQL_FetchInt(hUserStmt, 2);
			player.iPjSelected = SQL_FetchInt(hUserStmt, 3);
		}
	}
	CheckQuantityPlaying();
	StopSound(player.id, SNDCHAN_STATIC,"*/MassiveInfection/bienvenidoapiubreakers.mp3");
	
	// Change name
	player.bCanChangeName = true;
	SetClientInfo(client, "name", name);
	//CS_SetClientClanTag(client, "");
	bool staff = (StrEqual(name, "Deecode") || StrEqual(name, "Codes"));
	
	// Augmented gain?
	if(player.iVipLevel > 0 && !staff){
		player.bAdmin = true;
		CS_SetClientClanTag(client, "VIP |");
		PrintToChatAll("%s Bienvenido \x0FVIP %s\x01.", SERVERSTRING, name);
	}
	if(staff){
		CS_SetClientClanTag(client, "☣ STAFF ☣ |");
		PrintToChatAll("%s Bienvenido \x09STAFF %s\x01.", SERVERSTRING, name);
	}
	
	JoinPlayer(player.id);
	player.bInGame = true;
	player.bMuted = true;
	CheckQuantityPlaying();
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}

public Action saveCharacter(int client){	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	JPlayer p = JPlayer(client);
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "UPDATE JBCharacter SET cigarros=? WHERE id=?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			return Plugin_Handled;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, p.iCigarros, false);
	//No mover JAMAS
	SQL_BindParamInt(hUserStmt, 1, p.iPjSelected, false);
	
	
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Error en saveCharacter.");
		return Plugin_Handled;
	}
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}

public Action createCharacter(int client){
	int steamid = GetSteamAccountID(client, true);
	JPlayer player = JPlayer(client);
	char name[32];
	characterNames.GetString(client, name, 32);

	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	/* Check if we haven't already created the statement */
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "CALL createCharacterJB(?, ?)", error2, sizeof(error2));
		if (hUserStmt == null)
		{
			PrintToServer(error2);
			return Plugin_Handled;
		}
	}
	
	SQL_BindParamInt(hUserStmt, 0, steamid, false);
	SQL_BindParamString(hUserStmt, 1, name, false);
	
	if (!SQL_Execute(hUserStmt)) {
		char err[128];
		SQL_GetError(hUserStmt, err, sizeof(err));
		PrintToServer(err);
		player.bInCreatingCharacter = true;
		hUserStmt = null;
		showCrearCharacter(client, false);
		return Plugin_Handled;
	}
	
	PrintHintText(client, "Personaje %s creado con exito!", name);
	PrintToChat(player.id, "%s Bienvenido a \x09Massive Infection\x01! Para conocer los binds y la información básica escribe \x0B!ayuda\x01 en el chat.", SERVERSTRING);
	PrintToChat(player.id, "%s \x09RECOMENDACIÓN\x01: Escribe !reglas para \x09evitar penalizaciones\x01!", SERVERSTRING);
	
	loadCharacter(client);
	delete db;
	return Plugin_Handled;
}

public void checkChangeTeam(int client, int team){
	JPlayer player = JPlayer(client);
	if(team == CS_TEAM_CT){
		if(fnGetInTeam(CS_TEAM_CT)-1 > 0){
			player.moveToTeam(CS_TEAM_T);
			PrintToChat(client, "%s Te metieron preso por corrupto.", SERVERSTRING);
		}
		else PrintToChat(client, "%s La carcel no puede quedarse sin policias!", SERVERSTRING);
	} else{
		if(CanJoinCt()){
			player.moveToTeam(CS_TEAM_CT);
			PrintToChat(client, "%s Te unieron a policia por chupamedias.", SERVERSTRING);
		}
		else PrintToChat(client, "%s Ya hay demasiadas gorras en esta carcel.", SERVERSTRING);
		
	}
}

void CheckQuantityPlaying(){
	if (fnGetAlive() < 2){
		CS_TerminateRound(0.2, CSRoundEnd_CTWin, false);
	}
}

stock bool CanJoinCt(){
	return fnGetInTeam(CS_TEAM_CT) == 0 || (fnGetInTeam(CS_TEAM_CT) == fnGetInTeam(CS_TEAM_T) / TT_QUANTITY_REQUIRED);
}
stock bool JoinPlayer(int client){
	
	JPlayer player = JPlayer(client);
	
	if (CanJoinCt())
		player.iTeamNum = CS_TEAM_CT;
	else
		player.iTeamNum = (fnGetInTeam(CS_TEAM_CT) < fnGetInTeam(CS_TEAM_T) / TT_QUANTITY_REQUIRED) ? CS_TEAM_CT : CS_TEAM_T;

	
	PrintToChat(player.id, "%s CTs: %d, TTs: %d, Res: %d", SERVERSTRING, fnGetInTeam(CS_TEAM_CT), fnGetInTeam(CS_TEAM_T), player.iTeamNum);
	
	return true;
}
public void hacerSimon(int client){
	JPlayer p = JPlayer(client);
	
	char sName[48];
	GetClientName(client, sName, sizeof(sName));
	PrintToChatAll("%s \x09%s\x01 es \x09Simón\x01 en esta ronda.", SERVERSTRING, sName);
	p.bSimon = true;
	p.bMuted = false;
	
	iSimon = p.id;
}
stock void balanceTeams(){
	if (fnGetPlaying() < 2)
		return;
	
	int iCT, iTT;
	iCT = fnGetInTeam(CS_TEAM_CT);
	iTT = fnGetInTeam(CS_TEAM_T);
	
	int rand;
	rand = fnGetRandomPlaying(GetRandomInt(1, fnGetPlaying()));
	JPlayer random = JPlayer(rand);
	if (iCT < iTT / TT_QUANTITY_REQUIRED){
		if (random.iTeamNum != CS_TEAM_T){
			balanceTeams();
			return;
		}
		
		random.moveToTeam(CS_TEAM_CT);
	}
}

/*
stock int getRandomClient(int team){
	int rand;
	rand = fnGetRandomPlaying(GetRandomInt(1, fnGetPlaying()));
	
	if (!rand)
		return -1;
	
	JPlayer random = JPlayer(rand);
	if (random.iTeamNum != team){
		getRandomClient(team);
		return -1;
	}
	
	return random.id;
}
*/

public void LoadPresos(){
	gPNombre = CreateArray(10);
	gPHealth = CreateArray(5);
	gPArmor = CreateArray(5);
	gPCigarros = CreateArray(5);
	gPVelocidad = CreateArray(5);
	gPGravedad = CreateArray(5);
	gPDamage = CreateArray(5);
	
	/*menu.AddItem("1", "Preso Resistente(-20% DMG, +25% HP, +50% ARM)");
	menu.AddItem("2", "Preso Asesinó (+25% DMG, +10% VEL)");
	menu.AddItem("3", "Preso Ladrón (-12% DMG, +12% VEL, +25% Cigarrillos)");
	menu.AddItem("4", "Preso VIP Plata (+15% ARM, +5% VEL y GRAV, +50% Cigarrillos)");
	menu.AddItem("5", "Preso VIP Gold (+30% ARM, +10% VEL y GRAV, +100% Cigarrillos)");
	*/
	CreateJPreso("Agil", 		100, 0, 1.0, 1.25, 0.8, 0.9);
	CreateJPreso("Resistente", 	125, 50, 1.0, 1.0, 1.0, 0.8);
	CreateJPreso("Asesino", 	100, 0, 1.0, 1.1, 1.0, 1.25);
	CreateJPreso("Ladron", 		100, 0, 1.25, 1.12, 1.0, 0.88);
	CreateJPreso("VIP Plata", 	100, 15, 1.5, 1.05, 0.95, 1.0);
	CreateJPreso("VIP Gold", 	100, 30, 2.0, 1.1, 0.9, 1.0);
}

/*public Action TimerShowCountdown(Handle timer){
	// When counter finishes
	if(iCounter <= 0){
		if(ActualMode.id == view_as<int>(NO_MODE)) {
			StartMode(GetRandomInt(view_as<int>(MODE_INFECTION), gModeName.Length-1));
			bForcedMode = false;
		}
		
		iCounter = ROUND_START_COUNTDOWN;
		hCounter = null;
		return Plugin_Stop;
	}
	
	iCounter--;
	return Plugin_Handled;
}*/

// Server announcements
stock void SendAnnouncement(){
	// Initialize randomization
	int number = GetRandomInt(0, 3);
	
	// Initialize info buffer
	char sBuffer[128];
	
	switch (number){
		//case 0: FormatEx(sBuffer, sizeof(sBuffer), "%s Bievenidos al servidor de \x09Jailbreak\x01 de \x09PIU\x01.", SERVERSTRING);
		case 0: FormatEx(sBuffer, sizeof(sBuffer), "%s Servidor en estado de \x09BETA\x01.", SERVERSTRING);
		case 1: FormatEx(sBuffer, sizeof(sBuffer), "%s Conoces nuestro servidor \x09Zombie Plague + Levels\x01?.\nIp: cs.piu-games.com:27015", SERVERSTRING);
		case 2: FormatEx(sBuffer, sizeof(sBuffer), "%s Conoces nuestro servidor \x09Casual\x01?.\nIP: cs.piu-games.com:27017", SERVERSTRING);
		case 3: FormatEx(sBuffer, sizeof(sBuffer), "%s Pone \x09!reglas\x01 para conocer las reglas del servidor.", SERVERSTRING);
	}
	
	// Print the announcement
	PrintToChatAll(sBuffer);
}

// HUD functions
/*
public void hideHUD(int client){
	if(IsPlayerExist(client))
		SetEntProp(client, Prop_Send, "m_iHideHUD", HIDEHUD_RADARANDTIMER);
}*/
public void CacheDoors(){

	int ent;
	int door;
	
	while((ent = FindEntityByClassname(ent, "info_player_terrorist")) != -1){
		float coor[3];
		GetEntPropVector(ent, Prop_Data, "m_vecOrigin", coor);
		
		while((door = FindEntityByClassname(door, "func_door")) != -1){
			float posi[3];
			GetEntPropVector(door, Prop_Data, "m_vecOrigin", posi);
			
			if(GetVectorDistance(posi, coor) < min){
				min = GetVectorDistance(posi, coor);
			}
		}
	}
	
	ent = -1;
	door = -1;
	
	min += 80.0;
	
	while((ent = FindEntityByClassname(ent, "info_player_terrorist")) != -1){
		float coor[3];
		GetEntPropVector(ent, Prop_Data, "m_vecOrigin", coor);
		
		while((door = FindEntityByClassname(door, "func_door")) != -1){
			float posi[3];
			GetEntPropVector(door, Prop_Data, "m_vecOrigin", posi);
			
			if(GetVectorDistance(posi, coor) <= min){
				PushArrayCell(gDoorlist, door);
			}
		}
	}
}

public Action PrintMsg(Handle timer, int client){
	JPlayer player = JPlayer(client);
	if(!IsPlayerExist(client) || IsFakeClient(client))
		return Plugin_Handled;
		
	if(!player.bInGame)
		return Plugin_Handled;

	// Hide CSGO HUD
	hideHUD(player.id);
	
	// Is our player spectating someone?
	int iSpecMode = GetEntProp(player.id, Prop_Send, "m_iObserverMode");
	
	// Initialize message string
	char msg[640];
	
	// Parse data in both cases
	if (player.bAlive){
		// Format the HUD
		formatHUD(player.id, msg);
	}
	else if (iSpecMode == SPECMODE_FIRSTPERSON || iSpecMode == SPECMODE_3RDPERSON){
		int iTarget = GetEntPropEnt(player.id, Prop_Send, "m_hObserverTarget");
		
		if (!IsPlayerExist(iTarget, true))
			return Plugin_Handled;
		
		JPlayer target = JPlayer(iTarget);
		
		formatHUD(target.id, msg, true);
	}
	
	// Show hud message
	SetHudTextParams(0.02, 0.02, 0.6, 102, 178, 255, 255, 1, 0.2, 0.8, 0.4);
	ShowSyncHudText(player.id, hHudSynchronizer, msg);
	return Plugin_Continue;
}
void formatHUD(int client, char buffer[640], bool bSpec = false){
	JPlayer player = JPlayer(client);
	
	if (player.iTeamNum != CS_TEAM_T && player.iTeamNum != CS_TEAM_CT)
		return;
	
	// Create array
	char sType[16];
	char sClass[16];
	char sCigs[32];
	AddPoints(player.iCigarros, sCigs, sizeof(sCigs));
	if (player.iTeamNum == CS_TEAM_T){
		sType = "Prisionero";
		gPNombre.GetString(player.iTipoPresoSelected, sClass, sizeof(sClass));
	}
	else{
		sType = "Oficial";
		
		sClass = (player.bSimon) ? "Simón" : "Guardia";
	}
	FormatEx(buffer, sizeof(buffer), "%sVida: %d\nArmor: %d\nCigarrillos: %s\nTipo: %s\nClase: %s", bSpec ? "OBSERVANDO\n" : "", player.iHealth, player.iArmor, sCigs, sType, sClass);
}
public void hideHUD(int client){
	if(IsPlayerExist(client))
		SetEntProp(client, Prop_Send, "m_iHideHUD", HIDEHUD_RADARANDTIMER);
}

public Action openCells(int client)
{
	for(int i = 0; i < GetArraySize(gDoorlist); i++)
	{
		int temp = GetArrayCell(gDoorlist, i);
		AcceptEntityInput(temp, "Use");
	}
	
	char name[32];
	GetClientName(client, name, sizeof(name));
	PrintToChatAll("%s Simon ha abierto las celdas.", SERVERSTRING);
}

public void OnClientSpeakingEx(int client)
{
	JPlayer p = JPlayer(client);
	if (p.bMuted) return;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon){

	JPlayer player = JPlayer(client);
	if (!IsPlayerExist(player.id, true))
		return Plugin_Changed;
	
	if (buttons & IN_USE){
		if (player.iTeamNum != CS_TEAM_T)
			return Plugin_Continue;
		
		if (IsOnCooldown(player.id, 3.0))
			return Plugin_Continue;
			
		if (!player.bLockpick){
			PrintToChat(player.id, "%s No tienes \x09lockpick\x01!", SERVERSTRING);
			return Plugin_Continue;
		}
			
		int targetIndex = GetClientAimTarget(player.id, false);
		
		char sEntity[16]; 
		GetEntityClassname(targetIndex, sEntity, sizeof(sEntity));
		
		if (!StrEqual(sEntity, "func_door")){
			PrintToChat(player.id, "%s Esto \x09no es una puerta\x01!", SERVERSTRING);
			return Plugin_Continue;
		}
		
		PrintToChat(player.id, "%s \x09Celda abierta\x01!", SERVERSTRING);
		AcceptEntityInput(targetIndex, "Use");
		player.bLockpick = false;
	}
	
	return Plugin_Continue;
}

stock void SendHudAnnouncement(int client, int reason, bool sided = false){
	
	if (!IsPlayerExist(client) || IsFakeClient(client))
		return;
	
	char sData[128];
	
	switch (reason){
		case JB_REASON_NEW_SIMON:{
			char sName[48];
			
			if (!iSimon)
				return;
			
			GetClientName(iSimon, sName, sizeof(sName));
			FormatEx(sData, sizeof(sData), "%s es el nuevo Simon", sName);
		}
		case JB_REASON_SIMON_DEAD:{
			FormatEx(sData, sizeof(sData), "Simon ha muerto");
		}
		case JB_REASON_LAST_WILL_SELECTED:{
			FormatEx(sData, sizeof(sData), "La última voluntad es: ACÁ DEBERÍA IR LA ÚLTIMA VOLUNTAD, PUTO DE MIERDA");
		}
		case JB_REASON_ROUND_MODE:{
			FormatEx(sData, sizeof(sData), "Día PORCENTAJEESE");
		}
		case JB_REASON_REASON:{
			FormatEx(sData, sizeof(sData), "Estás preso por: %s", PrisonReasons[GetRandomInt(0, sizeof(PrisonReasons))]);
		}
	}
	
	Handle hAnnouncerSynchronizer = CreateHudSynchronizer();
	
	// Show hud message
	if (sided)
		SetHudTextParams(0.03, GetRandomFloat(0.35, 0.7), 5.0, GetRandomInt(55, 255), GetRandomInt(55, 255), GetRandomInt(55, 255), 255, 1, 3.0, 0.4, 0.4);
	else
		SetHudTextParams(0.42, 0.20, 5.0, GetRandomInt(55, 255), GetRandomInt(55, 255), GetRandomInt(55, 255), 255, 1, 3.0, 0.4, 0.4);
	
	ShowSyncHudText(client, hAnnouncerSynchronizer, sData);
	
	PrintToChat(client, sData);
	
	delete hAnnouncerSynchronizer;
}