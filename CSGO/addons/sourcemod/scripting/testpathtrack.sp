// Tested in CSS de_dust2
#include <sourcemod>
#include <sdktools>

// This model exists both CSS and CS:GO
#define MAGIC_BRUSH_MODEL       "models/props/cs_office/vending_machine.mdl"

#define PROP_MODEL              "models/combine_helicopter.mdl"


#define MOVE_SND                "npc/attack_helicopter/aheli_rotor_loop1.wav"


// de_dust2 tracks, from A site to long doors.
float g_vecTracks[][] =
{
    { 1422.0, 2176.0, 136.0 },
    { 1423.0, 1230.0, 388.0 },
    { 1214.0, 914.0, 365.0 },
    { 698.0, 669.0, 597.0 }
};


public void OnPluginStart()
{
    RegConsoleCmd( "heli", Cmd_Heli );
}

public void OnMapStart()
{
    PrecacheModel( MAGIC_BRUSH_MODEL );
    PrecacheModel( PROP_MODEL );
    
    PrecacheSound( MOVE_SND );
}

public Action Cmd_Heli( int client, int args )
{
    CreateAll();
    
    return Plugin_Handled;
}

stock void CreateAll()
{
    // Create all paths and link them together.
    // This has to be done in reverse since target linking is done on entity activation.
    char trackname[32];
    char prevtrackname[32];
    
    for ( int i = sizeof( g_vecTracks ) - 1; i >= 0; i-- )
    {
        FormatEx( trackname, sizeof( trackname ), "_track%i", i );
        
        CreatePath( trackname, g_vecTracks[i], prevtrackname );
        
        strcopy( prevtrackname, sizeof( prevtrackname ), trackname );
    }
    
    
    // Create func_tracktrain
    int tracktrain = CreateTrackTrain( "_train", "_track0", "200" );
    
    
    // Create our "helicopter"
    int prop = CreateTrainProp( "_prop" );
    
    
    // Parent it to func_tracktrain
    ParentToEntity( prop, tracktrain );
}

stock int CreateTrackTrain( const char[] name, const char[] firstpath, const char[] spd )
{
    int ent = CreateEntityByName( "func_tracktrain" );
    
    if ( ent < 1 )
    {
        LogError( "Couldn't create func_tracktrain!" );
        return -1;
    }
    
#define SF_NOUSERCONTROL    2
#define SF_PASSABLE         8
//#define SF_UNBLOCKABLE      512

    char spawnflags[12];
    FormatEx( spawnflags, sizeof( spawnflags ), "%i", SF_NOUSERCONTROL | SF_PASSABLE );
    
    
    DispatchKeyValue( ent, "targetname", name );
    DispatchKeyValue( ent, "target", firstpath );
    DispatchKeyValue( ent, "model", MAGIC_BRUSH_MODEL );
    DispatchKeyValue( ent, "startspeed", spd );
    DispatchKeyValue( ent, "speed", spd );
    
    DispatchKeyValue( ent, "MoveSound", MOVE_SND );
    
    // Make turning smoother
    DispatchKeyValue( ent, "wheels", "256" );
    DispatchKeyValue( ent, "bank", "20" );
    
    DispatchKeyValue( ent, "orientationtype", "2" ); // Linear blend, adds some smoothness
    
    DispatchKeyValue( ent, "spawnflags", spawnflags );
    DispatchSpawn( ent );
    
    
    // Brush model specific stuff
    SetEntProp( ent, Prop_Send, "m_fEffects", 32 );
    
    
    return ent;
}

stock int CreateTrainProp( const char[] name )
{
    int ent = CreateEntityByName( "prop_dynamic_override" );
    
    if ( ent < 1 )
    {
        LogError( "Couldn't create prop_dynamic_override!" );
        return -1;
    }
    
    DispatchKeyValue( ent, "targetname", name );
    DispatchKeyValue( ent, "solid", "0" );
    DispatchKeyValue( ent, "model", PROP_MODEL );
    DispatchSpawn( ent );
    
    return ent;
}

stock int CreatePath( const char[] name, const float pos[3], const char[] nexttarget )
{
    int ent = CreateEntityByName( "path_track" );
    
    if ( ent < 1 )
    {
        LogError( "Couldn't create path_track!" );
        return -1;
    }
    
    
    DispatchKeyValue( ent, "targetname", name );
    DispatchKeyValue( ent, "target", nexttarget );
    DispatchSpawn( ent );
    
    // path_tracks have to be activated to assign targets.
    ActivateEntity( ent );
    
    TeleportEntity( ent, pos, NULL_VECTOR, NULL_VECTOR );
    
    return ent;
}

stock bool ParentToEntity( int ent, target )
{
    SetVariantEntity( target );
    
    return AcceptEntityInput( ent, "SetParent" );
}