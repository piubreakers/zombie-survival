
#define DEBUG

#define PLUGIN_NAME           "autitovich"
#define PLUGIN_AUTHOR         ""
#define PLUGIN_DESCRIPTION    ""
#define PLUGIN_VERSION        "1.0"
#define PLUGIN_URL            ""

#include <sourcemod>
#include <sdktools>

#pragma semicolon 1


public Plugin:myinfo =
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

public OnPluginStart()
{
	RegConsoleCmd("AUTOVICH", CreateAutovich);
}

public Action CreateAutovich(int client, int args){
	
	return Plugin_Handled;
}
