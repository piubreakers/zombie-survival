#pragma semicolon 1

#define DEBUG

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <entity>
#include <global>
#include <resources>
#include <mapchooser>

// ASDASSDADSADSADSASDDSDAS
int decalSmoke;
int decalBloodDecal;
bool eventmode;

// Disable gain when players < PLAYERS_TO_GAIN
#define PLAYERS_TO_GAIN	5

// Map selector
#define ROUNDS_TO_END_MAP	15

// Not yet organized stuff
int iFireAwp;
int iFreezeAwp;

int iVipClass[4];

// Grenades
float NULL_VELOCITY[3] = {0.0, 0.0, 0.0};

// Allow gain
bool bAllowGain;

// Fog
int iFog = -1;
float mapFogStart = 0.0;
float mapFogEnd = 350.0;
float mapFogDensity = 0.98;

// Countdown
int iCounter = ROUND_START_COUNTDOWN;
Handle hCounter = null;
int iRounds;

// Combos
float fEngineTime;
Handle hComboSynchronizer;
Handle hComboSynchronizer2;
Handle hHudSynchronizer;
Handle hAnnouncerSynchronizer;

// Menu enum
enum MenuIndex{

	MENUINDEX_Weapons,  // Option 0
	MENUINDEX_Items,  // 1
	MENUINDEX_Classes,  // 2
	MENUINDEX_Points, // 3
	MENUINDEX_Vip,  // 4
	MENUINDEX_UNSTUCK   // 5
};

// Gamemodes
int modeCount = 20;
int modesForced = VIP_MODES_QUANTITY;
bool bForcedMode;
ZGameMode ActualMode;
int lastMode;
int iModeMaxTimes[] = { 1000, 1000, 1000, 1000, 1000, 1000, 20, 1, 1, 1, 1, 1, 1, 1, 1 };
int iSelectedMode;

// Coef
#define coefDif 	300

// Round vars
bool newRound = false;
bool endRound = false;
bool startedRound = false;

// Mute bullets var
bool g_bHooked;

//temporal
ArrayList users;
ArrayList passwords;
ArrayList characterNames;

//Temporal
ArrayList charactersNames[MAXPLAYERS+1];
ArrayList charactersLevels[MAXPLAYERS+1];
ArrayList charactersResets[MAXPLAYERS+1];
int numCharacters[MAXPLAYERS+1];

// AuraShields cache
ArrayList AuraShields;

ArrayList Players;

int BeamSprite, GlowSprite, g_beamsprite, g_halosprite;

bool bWarmupEnded = false;

#define PLUGIN_VERSION	"0.8 EARLY ACCESS"

public Plugin myinfo =
{
	name = "MASSIVE INFECTCOCK",
	author = "PIU-PETES",
	description = "SON BIEN MOGOLICOS",
	version = PLUGIN_VERSION,
	url = ""
};

public OnPluginStart(){
	Players = CreateArray(2);
	
	// Aura shield array
	AuraShields = CreateArray(2);
	
	// Zombie classes arrays
	gZClassName = CreateArray(15);
	gZClassModel = CreateArray(ByteCountToCells(300));
	gZClassArms = CreateArray(ByteCountToCells(300));
	
	// Human classes arrays
	gHClassName = CreateArray(15);
	gHClassModel = CreateArray(ByteCountToCells(300));
	gHClassArms = CreateArray(ByteCountToCells(300));
	
	// Human alignments
	gHAlignmentName = CreateArray(15);
	gHAlignmentDesc = CreateArray(ByteCountToCells(300));
	
	// Zombie alignments
	gZAlignmentName = CreateArray(15);
	gZAlignmentDesc = CreateArray(ByteCountToCells(300));
	
	// Primary weapons arrays
	primaryWeaponsName = CreateArray(10);
	primaryWeaponsEnt = CreateArray(ByteCountToCells(32));
	primaryWeaponsModel = CreateArray(ByteCountToCells(64));
	primaryWeaponsWorldModel = CreateArray(ByteCountToCells(64));
	
	// Secondary weapons arrays
	secondaryWeaponsName = CreateArray(10);
	secondaryWeaponsEnt = CreateArray(10);
	secondaryWeaponsModel = CreateArray(25);
	
	gGrenadePackLevel = CreateArray(5);
	gGrenadePackReset = CreateArray(5);
	// Grenade packs arrays
	for (int i; i < GRENADE_SLOTS; i++){
		grenadeType[i] = CreateArray(5);
		grenadeCount[i] = CreateArray(5);
	}
	
	// Gamemodes arrays
	gModeName = CreateArray(10);
	gInfection = CreateArray(2);
	gKill = CreateArray(2);
	gRespawn = CreateArray(2);
	gRespawnZombieOnly = CreateArray(2);
	gAntidoteAvailable = CreateArray(2);
	gZombieMadnessAvailable = CreateArray(2);
	gMinUsers = CreateArray(2);
	gMaxTimesPerMap = CreateArray(2);
	gMinVipToForce = CreateArray(2);
	gStaffOnly = CreateArray(2);
	gProbability = CreateArray(2);
	
	// Combo arrays
	gComboName = CreateArray(10);
	gDifficulty = CreateArray(4);
	gBonusExpGain = CreateArray(4);
	gRed = CreateArray(4);
	gGreen = CreateArray(4);
	gBlue = CreateArray(4);
	
	//Logros arrays
	gLNames = CreateArray(10);
	gLDesc = CreateArray(10);
	gLZKills = CreateArray(4);
	gLZCuchiKills = CreateArray(4);
 	gLHKills = CreateArray(4);
	gLNemeKills = CreateArray(4);
	gLSurviKills = CreateArray(4);
	gLGunsKills = CreateArray(4);
	gLAssaKills = CreateArray(4);
	gLSnipKills = CreateArray(4);
	gLHeadshots = CreateArray(4);
	gLInfections = CreateArray(4);
 	gLGrenadeInfections = CreateArray(4);
	gLInfBullBuyed = CreateArray(4);
	gLAntiBuyed = CreateArray(4);
	gLFuriBuyed = CreateArray(4);
 	gLHPoints = CreateArray(4);
	gLZPoints = CreateArray(4);
	gLHGPoints = CreateArray(4);
	gLZGPoints = CreateArray(4);
	
	// Accounts arrays
	users = CreateArray(8, MAXPLAYERS+1);
	passwords = CreateArray(8, MAXPLAYERS+1);
	characterNames = CreateArray(8, MAXPLAYERS+1);
	
	//Partys
	gPartys = CreateArray(8, MAXPLAYERS+1);
	for(int i = 0; i < PARTY_MAX_COUNT; i++){
		gPartyMembers[i] = CreateArray(8, MAXPLAYERS+1);
	}
	
	// Register stuff
	LoadClasses();
	LoadAlignments();
	LoadWeapons();
	LoadGameModes();
	LoadCombos();
	LoadLogros();
	
	g_iToolsRagdoll = FindSendPropInfo("CCSPlayer", "m_hRagdoll");
	
	for(int i = 1; i < MAXPLAYERS+1; i++){
		charactersNames[i] = CreateArray(6, MAXCHARACTERS);
		charactersLevels[i] = CreateArray(3, MAXCHARACTERS);
		charactersResets[i] = CreateArray(3, MAXCHARACTERS);
	}
	
	HookEvent("weapon_fire", EventWeaponFire, EventHookMode_Post);
	HookEvent("player_given_c4", EventPlayerGivenC4, EventHookMode_Pre);
	
	// Round hooks
	HookEvent("round_prestart", EventPreRoundStart, EventHookMode_Pre);
	HookEvent("round_start", EventPostRoundStart, EventHookMode_Post);
	HookEvent("round_end", EventRoundEnd, EventHookMode_Pre);
	HookEvent("cs_win_panel_round", EventBlockBroadCast, EventHookMode_Pre);
	
	// Death hook
	HookEvent("player_death", EventPlayerDeath, EventHookMode_Post);
	HookEvent("weapon_reload", EventWeaponReload, EventHookMode_Pre);
	
	// Spawn hooks
	HookEvent("player_spawn", EventPrePlayerSpawn, EventHookMode_Pre);
	HookEvent("player_spawn", EventPostPlayerSpawn, EventHookMode_Post);
	
	// Hook event messages
	HookEvent("player_team", EventPrePlayerTeam, EventHookMode_Pre);
	HookEvent("player_changename", EventPrePlayerChangeName, EventHookMode_Pre);
	HookEvent("player_radio", EventRadioMessage, EventHookMode_Pre);
	
	// Hook stupid warmup message
	//HookUserMessage(GetUserMessageId("TextMsg"), TextMsg, true); 
	
	// Mute bullets sounds
	AddTempEntHook("Shotgun Shot", ShotgunShotEntHook);
	AddNormalSoundHook(NormalSoundHook);
	
	// Lasermine hook
	// Hooks entity env_beam ouput events
	HookEntityOutput("env_beam", "OnTouchedByEntity", LaserOnTouch);
	
	// Block commands
	AddCommandListener(BlockCommand, "kill");
	AddCommandListener(BlockCommand, "explode");
	AddCommandListener(BlockCommand, "explodevector");
	
	// Block name change
	HookEvent("player_changename", OnNameChange, EventHookMode_Pre);
	
	// User commands
	RegConsoleCmd("mainmenu", showMainMenu);
	RegConsoleCmd("receive", changeReciveOp);
	RegConsoleCmd("leave", leaveParty);
	RegConsoleCmd("party", showMenuParty);
	RegConsoleCmd("hh", happyHour);
	RegConsoleCmd("ayuda", infoCommands);
	RegConsoleCmd("flashlight", flashLight);
	RegConsoleCmd("nightvision", nightVision);
	RegConsoleCmd("modo", infoMode);
	RegConsoleCmd("mutebullets", muteBullets);
	RegConsoleCmd("infopoints", infopoints);
	RegConsoleCmd("vip", infoVip);
	RegConsoleCmd("unstuck", unStuck);
	RegConsoleCmd("top", showTop50);
	RegConsoleCmd("reglas", infoRules);
	RegConsoleCmd("estadisticas", showMenuEstadisticas);
	RegConsoleCmd("discord", infoDiscord);
	RegConsoleCmd("pruebavip", showPruebaVipMenu);
	RegConsoleCmd("reward", showRewardReset);
	
	// Extra items binds
	RegConsoleCmd("item1", buyItems);
	RegConsoleCmd("item2", buyItems);
	RegConsoleCmd("item3", buyItems);
	
	// Staff cmds
	RegConsoleCmd("makemezombie", makemeZombie);
	RegConsoleCmd("makemenemesis", makemeNemesis);
	RegConsoleCmd("makemehuman", makemeHuman);
	RegConsoleCmd("makemesurvivor", makemeSurvivor);
	RegConsoleCmd("makemegunslinger", makemeGunslinger);
	RegConsoleCmd("makemesniper", makemeSniper);
	RegConsoleCmd("makemeassassin", makemeAssassin);
	RegConsoleCmd("givemenades", givemenades);
	RegConsoleCmd("aiminfect", AimInfect);
	RegConsoleCmd("lightlevel", lightLevel);
	RegConsoleCmd("fog", valveFog);
	RegConsoleCmd("lightsoff", lightSwitch);
	RegConsoleCmd("zpoints", staffzpoints);
	RegConsoleCmd("hpoints", staffhpoints);
	RegConsoleCmd("make", staffmake);
	RegConsoleCmd("exp", staffexp);
	RegConsoleCmd("banchat", staffbanchat);
	RegConsoleCmd("banchatvoice", staffbanchatvoice);
	RegConsoleCmd("vencimiento", getVencimientoTime);
	
	RegConsoleCmd("eventmode", eventMode);
	RegConsoleCmd("eventmode2", eventMode2);
	RegConsoleCmd("test", VAmbienceTest);
	RegConsoleCmd("screenfade", screenfade);
	RegConsoleCmd("vfx", testEffects);
	RegConsoleCmd("resting", testRestantes);
	//RegConsoleCmd("aura", givemeaura);
	
	
	//RegConsoleCmd("makemeinvulnerable", makemeInvulnerable);
	//RegConsoleCmd("saveme", saveme);
	
	// Server cmds
	RegServerCmd("getTime", time);
	RegConsoleCmd("debug", infServerNow, "all|basic|gamemodes|players|zclasses|hclasses|pweapons|sweapons|gpacks");
	RegServerCmd("give_zpoints", givezpoints);
	RegServerCmd("give_hpoints", givehpoints);
	RegServerCmd("make_player", makePlayer);
	RegServerCmd("give_exp", givexp);
	RegServerCmd("adminto", adminMe);
	//RegConsoleCmd("reset", resetear);
	RegServerCmd("saveAll", saveAllcmd);
	
	// NPC shit
	RegConsoleCmd("npc", CreateMercenary);
	RegConsoleCmd("deletenpc", DeleteMercenary);
	//RegConsoleCmd("sequence", ChangeAnimation);
	RegConsoleCmd("savelocation", SaveNpcLocation);
	//RegConsoleCmd("readlocation", ReadLocations);
	AddNormalSoundHook(NormalSHook);
	
	RegConsoleCmd("resetpoints", rpoints);
	
	HookEvent("player_connect_full", EventOnPlayerConnectedFull, EventHookMode_Post);
}



public Action EventOnPlayerConnectedFull(Handle event, char[] name, bool dontBroadcast){

	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	RequestFrame(view_as<RequestFrameCallback>(MoveToSpectator), client);
}
public Action MoveToSpectator(any data){
	ZPlayer player = ZPlayer(data);
	
	if (!IsPlayerExist(player.id))
		return;
	
	ChangeClientTeam(player.id, CS_TEAM_NONE);
	showMainMenu(player.id, 0);
	hideHUD(player.id);
}

public Action happyHour(int client, int args){
	if(gHappyHour) PrintToChatAll("%s Estamos en\x0F HAPPY HOUR\x01, disfruta del \x0Fdoble\x01 de ganancias.", SERVERSTRING);
	else if(gAfterHour)  PrintToChatAll("%s Estamos en\x0F AFTER HOUR\x01, disfruta del \x0Ftriple\x01 de ganancias.", SERVERSTRING);
	else PrintToChatAll("%s Estamos en horario \x0FESTANDAR\x01, ganancia: \x0Fnormal\x01.", SERVERSTRING);
}

public Action showRewardReset(int client, int args){
	ZPlayer p = ZPlayer(client);
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	PrintToChat(client, "%s Al iniciar la \x0Fversion oficial\x01, recibiras:", SERVERSTRING);
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT SUM(boost)*1.5 FROM Admin WHERE charId=?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, p.iPjSeleccionado, false);
	if (!SQL_Execute(hUserStmt)) {
		SQL_GetError(hUserStmt, error, sizeof(error));
		PrintToChatAll("Error: %s", error);
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	
	
	PrintToChat(client, "%s Por haber jugado en la beta: \x0F1\x01 punto \x0Bhumano\x01 y \x0Bzombie\x01 por cada 15 reset que tengas ahora.", SERVERSTRING);
	PrintToChat(client, "%s Ademas \x0Fun HAT\x01 unico.", SERVERSTRING);
	PrintToChat(client, "%s Recibiras \x0F20 PUNTOS\x01 \x0Bhumanos\x01 y \x0Bzombies\x01 extra.", SERVERSTRING);
	PrintToChat(client, "%s Podras usar nuevamente el \x0Fvip de prueba\x01.", SERVERSTRING);
	PrintToChat(client, "%s Los vips \x0Fx6\x01 y \x0Fx7\x01 no se venderan mas.", SERVERSTRING);
	
	if(SQL_GetRowCount(hUserStmt) > 0){
		int points;
		while(SQL_FetchRow(hUserStmt)){
			points = RoundToNearest(SQL_FetchFloat(hUserStmt, 0));
			
		}
		
		PrintToChat(client, "%s Por tus vips comprados: \x0F%d\x01 puntos \x0Bhumanos\x01 y \x0Bzombies\x01.", SERVERSTRING, points);
		PrintToChat(client, "%s Ademas recibiras \x0Fun HAT\x01 unico por tu aporte a la comunidad en la beta.", SERVERSTRING);
		PrintToChat(client, "%s Tendras \x0Fun MES\x01 gratis de vip x5(El mas caro de la nueva season).", SERVERSTRING);
	}
	
	AdjustProb();
	delete hUserStmt;
	delete db;
	
	return Plugin_Handled;
}

public Action getVencimientoTime(int client, int args){
	ZPlayer p = ZPlayer(client);
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT DATE_FORMAT(fechaVencimiento, '%d/%m/%y a las %H:%i:%S'), DATE_FORMAT(fechaVencimiento, '%d/%m/%y a las %H:%i:%S') FROM Admin WHERE charId=? AND vencido=0", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, p.iPjSeleccionado, false);
	if (!SQL_Execute(hUserStmt)) {
		SQL_GetError(hUserStmt, error, sizeof(error));
		PrintToChatAll("Error: %s", error);
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	if(SQL_GetRowCount(hUserStmt) > 0){
		while(SQL_FetchRow(hUserStmt)){
			char fechaI[64], fechaV[64];
			SQL_FetchString(hUserStmt, 0, fechaI, sizeof(fechaI));
			SQL_FetchString(hUserStmt, 1, fechaV, sizeof(fechaV));
			PrintToChat(client, "%s Tu \x0FVIP\x01 inicio el \x0B%s\x01 horas, y vence el \x0B%s\x01 horas.", SERVERSTRING, fechaI, fechaV);
		}
	}
	else{
		PrintToChat(client, "%s No tienes vip.", SERVERSTRING);
	}
	
	
	delete hUserStmt;
	delete db;
	
	return Plugin_Handled;
}

// Hook warmup message
/*
public Action TextMsg(UserMsg msg_id, Protobuf msg, const int[] players, int playersNum, bool reliable, bool init){
	
	char buffermsg[512]; 
	PbReadString(msg, "params", buffermsg, sizeof(buffermsg), 0);
	 
	if (StrContains(buffermsg, "#SFUI_Notice_Match_Will_Start_") != -1 || StrEqual(buffermsg, "#SFUI_Notice_Warmup_Has_Ended", false)){
		PrintToChatAll("%s INICIANDO PARTIDA.", SERVERSTRING);
		ServerCommand("mp_warmup_end");
		CS_TerminateRound(0.1, CSRoundEnd_GameStart);
		bWarmupEnded = true;
		return Plugin_Stop;
	} 
	
	return Plugin_Continue;
}*/

// Dispatch map variables
public void DispatchFog(){
	int ent; 
	ent = FindEntityByClassname(-1, "env_fog_controller");
	if (ent != -1) 
		iFog = ent;
	else{
		iFog = CreateEntityByName("env_fog_controller");
		DispatchSpawn(iFog);
	}
	CreateFog();
	AcceptEntityInput(iFog, "TurnOff");
}
public void CreateFog(){
	if(iFog != -1) {
		DispatchKeyValue(iFog, "fogblend", "0");
		DispatchKeyValue(iFog, "fogcolor", "0 0 0");
		DispatchKeyValue(iFog, "fogcolor2", "0 0 0");
		DispatchKeyValueFloat(iFog, "fogstart", mapFogStart);
		DispatchKeyValueFloat(iFog, "fogend", mapFogEnd);
		DispatchKeyValueFloat(iFog, "fogmaxdensity", mapFogDensity);
	}
}
public Action EventRadioMessage(Handle event, const char[] name, bool dontBroadcast){
	return Plugin_Handled;
}
public Action EventPrePlayerChangeName(Handle event, const char[] name, bool dontBroadcast){
	if(!dontBroadcast){
		Handle new_event = CreateEvent("player_changename", true);
		
		char oldname[32];
		char newname[32];
		
		GetEventString(event, "oldname", oldname, sizeof(oldname));
		GetEventString(event, "newname", newname, sizeof(newname));
		
		SetEventInt(new_event, "userid", GetEventInt(event, "userid"));
		SetEventString(new_event, "oldname", oldname);
		SetEventString(new_event, "newname", newname);
		
		FireEvent(new_event, true);
		
		
		
		return Plugin_Handled;
	}
	return Plugin_Handled; 
}
public Action EventPrePlayerTeam(Handle event, const char[] name, bool dontBroadcast){
	if(!dontBroadcast){
		Handle new_event = CreateEvent("player_team", true);
		
		SetEventInt(new_event, "userid", GetEventInt(event, "userid"));
		SetEventInt(new_event, "team", GetEventInt(event, "team"));
		SetEventInt(new_event, "oldteam", GetEventInt(event, "oldteam"));
		SetEventBool(new_event, "disconnect", GetEventBool(event, "disconnect"));
		
		FireEvent(new_event, true);
		
		
		
		return Plugin_Handled;
	}
	return Plugin_Continue; 
}

// Dispatch mercenary based on kv file
public Action SaveNpcLocation(int client, int args){
	float pos[3];
	int targetIndex = GetClientAimTarget(client, false);
	char sClassname[16];
	GetEdictClassname(targetIndex, sClassname, sizeof(sClassname));
	
	if (!IsValidEntity(targetIndex) || !StrEqual(sClassname, NPC_CLASSNAME))
		return Plugin_Handled;
		
	KeyValues MyKv = CreateKeyValues("spawn");
	
	// Read file if it exist
	MyKv.ImportFromFile("mercenaryspawn.txt");
	
	char buffer[PLATFORM_MAX_PATH];
	GetCurrentMap(buffer, sizeof(buffer));
	
	GetEntPropVector(targetIndex, Prop_Send, "m_vecOrigin", pos);
	
	// Create "mapname" section
	MyKv.JumpToKey(buffer, true);

	int count;
	
	// If "mapname" section have previous savings, coordinates
	if(MyKv.GotoFirstSubKey(false)){
		// do count how many savings under mapname section
		do
		{
			count++;
		}
		while(MyKv.GotoNextKey(false));
	
		// remember go back to "mapname" section root
		MyKv.GoBack();
	}
	
	ReplyToCommand(client, "\n");
	ReplyToCommand(client, buffer);
	
	// save new coordinate into "mapname" section
	Format(buffer, sizeof(buffer), "%i", count);
	MyKv.SetVector(buffer, pos);
	
	ReplyToCommand(client, "Saved position %i %f %f %f", count, pos[0], pos[1], pos[2]);
	
	// go top of keyvalue structure
	MyKv.Rewind();
	
	// save file
	MyKv.ExportToFile("mercenaryspawn.txt");
	delete MyKv;
	return Plugin_Handled;
}
stock Action ReadLocations(int client, int args){
	KeyValues MyKv = CreateKeyValues("spawn");
	// Read file if it exist
	MyKv.ImportFromFile("mapconfigs.txt");
	
	char buffer[PLATFORM_MAX_PATH];
	GetCurrentMap(buffer, sizeof(buffer));
	
	float pos[3];
	
	// "mapname" section not found
	if(!MyKv.JumpToKey(buffer, false)){
		ReplyToCommand(client, "No records in \"%s\" section", buffer);
		delete MyKv;
		return Plugin_Handled;
	}
	
	ReplyToCommand(client, "\n");
	ReplyToCommand(client, buffer);
	
	// If "mapname" section have previous savings, coordinates
	if(MyKv.GotoFirstSubKey(false)){
		
		// do count how many savings under mapname section
		do{
			MyKv.GetSectionName(buffer, sizeof(buffer));
			MyKv.GetVector(NULL_STRING, pos, NULL_VECTOR);
			Format(buffer, sizeof(buffer), "%20s %f %f %f", buffer, pos[0], pos[1], pos[2]);
			ReplyToCommand(client, buffer);
		}
		while(MyKv.GotoNextKey(false));
		
		// remember go back to "mapname" section root
		MyKv.GoBack();
    }
    delete MyKv;
	return Plugin_Handled;
}
public void LocateMercenary(){
	KeyValues MyKv = CreateKeyValues("spawn");
	// Read file if it exist
	MyKv.ImportFromFile("mercenaryspawn.txt");
	
	char buffer[PLATFORM_MAX_PATH];
	GetCurrentMap(buffer, sizeof(buffer));
	
	float pos[3];
	
	// "mapname" section not found
	if(!MyKv.JumpToKey(buffer, false)){
		PrintToServer("No records in \"%s\" section", buffer);
		delete MyKv;
		return;
	}
	PrintToServer("\n");
	PrintToServer("%s", buffer);
	
	// If "mapname" section have previous savings, coordinates
	if(MyKv.GotoFirstSubKey(false)){
		
		// do count how many savings under mapname section
		do
		{
			MyKv.GetSectionName(buffer, sizeof(buffer));
			MyKv.GetVector(NULL_STRING, pos, NULL_VECTOR);
			/*Format(buffer, sizeof(buffer), "%20s %f %f %f", buffer, pos[0], pos[1], pos[2]);
			ReplyToCommand(client, buffer);*/
			DispatchMercenary(pos);
			
		}
		while(MyKv.GotoNextKey(false));
		
		// remember go back to "mapname" section root
		MyKv.GoBack();
	}
	delete MyKv;
}

// Adjust lightlevel and skyname for each map
public void LoadKvMapConfig(){
	KeyValues MyKv = CreateKeyValues("lighting");
	
	// Read file if it exist
	if (!MyKv.ImportFromFile("mapconfigs.txt")){
		PrintToServer("[MAPCONFIG] No mapconfigs file");
		delete MyKv;
		return;
	}
	
	// Parse mapname
	char sMapname[PLATFORM_MAX_PATH];
	GetCurrentMap(sMapname, sizeof(sMapname));
	
	// No data for this map
	if(!MyKv.JumpToKey(sMapname, false)){
		PrintToServer("[MAPCONFIG] No records in \"%s\" section, creating default keyvalues", sMapname);
		SaveKvMapConfig();
		delete MyKv;
		return;
	}
	
	char sDefault[6];
	MyKv.GetString("default", sDefault, sizeof(sDefault), "no");
	
	if (StrEqual(sDefault, "yes")){
		PrintToServer("[MAPCONFIG] Map config is entirely default, guachin");
		delete MyKv;
		return;
	}
	
	// Parse lightlevel
	char sLightLevel[2];
	MyKv.GetString("lightlevel", sLightLevel, sizeof(sLightLevel), "d");
	
	// Parse skyname
	char sSkyname[32];
	MyKv.GetString("skyname", sSkyname, sizeof(sSkyname), "sky_csgo_night02b");
	
	if (!StrEqual(sSkyname, ""))
		ServerCommand("sv_skyname %s", sSkyname);
	if (!StrEqual(sLightLevel, ""))
		SetLightStyle(0, sLightLevel);
	
	PrintToServer("[MAPCONFIG] Map %s adjusted to %s lightlevel and %s skyname", sMapname, StrEqual(sLightLevel, "") ? "default" : sLightLevel, StrEqual(sSkyname, "") ? "default" : sSkyname);
	
	//bMapConfigLoaded = true;
	MyKv.GoBack();
	delete MyKv;
}

// Save data in kv file
void SaveKvMapConfig(){
	KeyValues MyKv = CreateKeyValues("lighting");
	
	// Read file if it exist
	if (!MyKv.ImportFromFile("mapconfigs.txt")){
		PrintToServer("[MAPCONFIG] No mapconfigs file");
		delete MyKv;
		return;
	}
	
	// Parse mapname
	char sMapname[PLATFORM_MAX_PATH];
	GetCurrentMap(sMapname, sizeof(sMapname));
	
	// No data for this map
	MyKv.JumpToKey(sMapname, true);
	
	// Create keyvalues
	PrintToServer("[MAPCONFIG] No records in \"%s\" section, creating default keyvalues", sMapname);
	MyKv.SetString("lightlevel", "e");
	MyKv.SetString("skyname", "sky_csgo_night02b");
	MyKv.Rewind();
	MyKv.ExportToFile("mapconfigs.txt");
	delete MyKv;
		
	LoadKvMapConfig();
}

// Native forwards
public OnMapStart(){

	// Reset important variables
	iRounds = 0;
	eventmode = false;
	iSelectedMode = view_as<int>(NO_MODE);
	bWarmupEnded = false;
	GetExclusionList();
	
	for(int x = 0; x < sizeof(g_cRootDirectoryName); x++){
		RegisterResources(x, view_as<DirectoryListing>(INVALID_HANDLE), g_cRootDirectoryName[x]);
	}
	
	for(int y = 0; y < sizeof(g_cRootDirectoryName); y++){
		delete g_FileExclusionList[y];
	}
	//PrecacheSound("sound/zbm3/mine/mine_activate.mp3");
	
	// Reset important variables
	modesForced = VIP_MODES_QUANTITY;
	bForcedMode = false;
	lastMode = view_as<int>(MODE_INFECTION);
	hCounter = null;
	iCounter = ROUND_START_COUNTDOWN;
	
	// Reset forced modes counter
	for (int i = 0; i <= view_as<int>(MODE_APOCALYPSIS); i++){
		gMaxTimesPerMap.Set(i, iModeMaxTimes[i]);
	}
	
	// Clear party arrays
	gPartys.Clear();
	for (int i = 0; i < PARTY_MAX_COUNT; i++){
		gPartyMembers[i].Clear();
	}
	
	// Create hud synchronizers
	hComboSynchronizer = CreateHudSynchronizer();
	hComboSynchronizer2 = CreateHudSynchronizer();
	hHudSynchronizer = CreateHudSynchronizer();
	hAnnouncerSynchronizer = CreateHudSynchronizer();
	
	PrecacheModel("materials/sprites/purplelaser1.vmt", true);
	
	BeamSprite = PrecacheModel("materials/sprites/laserbeam.vmt");
	GlowSprite = PrecacheModel("materials/sprites/blueglow1.vmt");
	g_beamsprite = PrecacheModel("materials/sprites/laserbeam.vmt");
	g_halosprite = PrecacheModel("materials/sprites/halo01.vmt");

////////////////////////////////////////////////
	// Precache smoke model
	decalSmoke = PrecacheModel("sprites/steam1.vmt");
	// Precache blood decals
	decalBloodDecal = PrecacheDecal("decals/bloodstain_001.vtf");
////////////////////////////////////////////////
	
	PrecacheSound(SOUND_FREEZE);
	PrecacheSound(SOUND_FREEZE_EXPLODE);
	
	// Adjust map lighting level
	LoadKvMapConfig();
	
	// Fog
	DispatchFog();
	
	// Apply ambience effects
	VAmbienceApplySunDisable();
	VAmbienceApplyLightStyle();
}
public OnMapEnd(){
	saveAll();
	for(int i = 1; i <= MaxClients; i++){
		ZPlayer player = ZPlayer(i);
		
		users.SetString(i, "");
		passwords.SetString(i, "");
		characterNames.SetString(i, "");
		
		if (!IsPlayerExist(player.id))
			continue;
		
		if (player.hComboHandle != null){
			if (player.iCombo > 1)
				FinishCombo(player.hComboHandle, player.id);
			else
				player.hComboHandle = null;
		}
	}
	Players.Clear();
	
	// Reset aura shields cache
	AuraShields.Clear();
	
	// Clear party arrays
	gPartys.Clear();
	for (int i = 0; i < PARTY_MAX_COUNT; i++){
		gPartyMembers[i].Clear();
	}
}
public OnClientPutInServer(int client){

	if (!IsPlayerExist(client) || IsFakeClient(client))
		return;
	
	ZPlayer player = ZPlayer(client);
	player.iTeamNum = 3;
	users.SetString(client, "");
	passwords.SetString(client, "");
	characterNames.SetString(client, "");
	player.Reset();
	if(IsPlayerExist(client)){
		CreateTimer(0.5, PrintMsg, client, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
	for(int i; i < MAXCHARACTERS; i++){
		player.setSlotEmpty(i, true);
	}
	
	// Hook client events
	SDKHooksClientInit(client);
}

// Initialize client SDK Hooks
void SDKHooksClientInit(int client){
	
	// Initialize clients hooks
	WeaponsClientInit(client);
	
	SDKHook(client, SDKHook_PreThink, PreThink);
	SDKHook(client, SDKHook_TraceAttack, OnDamageTraceAttack);
	SDKHook(client, SDKHook_OnTakeDamageAlive, OnTakeDamage);
}

// Initialize client Weapons SDK Hooks
void WeaponsClientInit(int client){
	SDKHook(client, SDKHook_WeaponCanUse, WeaponsOnCanUse);
	SDKHook(client, SDKHook_WeaponEquip, WeaponsOnEquip);
	SDKHook(client, SDKHook_WeaponDropPost, WeaponsOnDropPost);
	SDKHook(client, SDKHook_PostThinkPost, WeaponsOnAnimationFix);
}

//=====================================================
//				HOOK CLIENT DISCONNECT
//=====================================================
public OnClientDisconnect(int client){
	ZPlayer player = ZPlayer(client);
	
	if (!IsPlayerExist(player.id))
		return;
	
	// Finish any combo
	FinishCombo(player.hComboHandle, player.id);
	
	// Remove from party
	if (player.isInParty()){
		ZParty party = ZParty(findPartyByUID(player.iPartyUID));
		if(party.length()-1 <= 1) FinishComboParty(party.hComboHandle, client);
		party.removeMember(player.id);
	}
	
	// Save user data
	saveme(player.id, 0);
	
	// Reset zplayer vars
	player.Reset();
	
	// Remove any planted lasermine
	RemoveLasermines(player.id);
	
	if (IsPlayerExist(Players.FindValue(client)))
		Players.Erase(Players.FindValue(client));
	
	// Reset cache
	users.SetString(client, "");
	passwords.SetString(client, "");
	characterNames.SetString(client, "");
	
	//
	if (IsClientInGame(client))
		ExtinguishEntity(client);
	
	// Damage
	SDKUnhook(player.id, SDKHook_OnTakeDamageAlive, OnTakeDamage);
	SDKUnhook(player.id, SDKHook_TraceAttack, OnDamageTraceAttack);
	// Weapons
	SDKUnhook(player.id, SDKHook_WeaponSwitchPost, OnClientWeaponSwitchPost);
	SDKUnhook(player.id, SDKHook_WeaponDropPost, OnClientWeaponDropPost);
	SDKUnhook(player.id, SDKHook_WeaponCanUse, WeaponsOnCanUse);
	SDKUnhook(player.id, SDKHook_WeaponDropPost, WeaponsOnDropPost);
	SDKUnhook(player.id, SDKHook_WeaponEquip, WeaponsOnEquip);
	// Prethink
	SDKUnhook(player.id, SDKHook_PreThink, PreThink);
	
	// End round if needed
	RoundEndOnClientDisconnect();
	
	// Check how many users are playing
	CheckQuantityPlaying();
}
void RoundEndOnClientDisconnect(){
	if (newRound || endRound){
		return;
	}

	int nHumans  = fnGetHumans();
	int nZombies = fnGetZombies();
	
	if (!nZombies && nHumans){
		CS_TerminateRound(3.0, CSRoundEnd_CTWin, false);
		return;
	}
	
	if (nZombies && !nHumans){
		CS_TerminateRound(3.0, CSRoundEnd_TerroristWin, false);
		return;
	}
}

// Hook client commands
public Action OnClientCommand(int client, int args){

	if (!IsPlayerExist(client))
		return Plugin_Changed;
		
	ZPlayer player = ZPlayer(client);
	char cmd[16];

	GetCmdArg(0, cmd, sizeof(cmd));
	
	if ( StrEqual(cmd, "jointeam") || StrEqual(cmd, "chooseteam")){
		if(player.iTeamNum == CS_TEAM_NONE || player.iTeamNum == CS_TEAM_SPECTATOR){
			showMainMenu(player.id, 0);
		}
		return Plugin_Handled;
	}
	else if(StrEqual(cmd, "buy")){
		PrintHintText(player.id,"You cant buy");
		return Plugin_Handled;
	}
	else if(StrEqual(cmd, "name")) return Plugin_Handled;
	else if(StrEqual(cmd, "kill")) return Plugin_Handled;
	
	return Plugin_Continue;
}
public Action OnClientSayCommand(int client, const char[] command, const char[] sArgs){
	ZPlayer player = ZPlayer(client);
	
	if (!IsPlayerExist(player.id))
		return Plugin_Handled;
	
	if(player.bChatBanned){
		PrintToChat(client, "%s No puedes escribir porque estas baneado.");
		return Plugin_Handled;
	}
	if(player.bInUser){
		char name[64];
		strcopy(name, 64, sArgs);
		switch(isValidInput(name, 10, 50)){
			case INPUT_SHORT:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email muy corto.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_LARGE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email muy largo.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_INVALID_SIMBOL:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email contiene simbolos.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_INVALID_SPACE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Email contiene espacios.");
				showIndicacionesRegister(client, false);
			}
			case INPUT_OK: {//makeValidToSQL(name, namev, DATA_NAME);
				users.SetString(client, name);
				PrintHintText(client, "El email ingresado es: %s", name);
				showIndicacionesRegister(client, true);
			}
		}
		return Plugin_Handled;
	}
	if(player.bInPassword) {
		char pass[32];
		strcopy(pass, 32, sArgs);
		switch(isValidInput(pass, 5, 16)){
			case INPUT_SHORT:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña muy corto.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_LARGE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña muy largo.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_INVALID_SIMBOL:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña contiene simbolos.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_INVALID_SPACE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Contraseña contiene espacios.");
				showIndicacionesPasswordRegister(client, false);
			}
			case INPUT_OK: {
				passwords.SetString(client, pass);
				PrintHintText(client, "La contraseña ingresado es: %s", pass);
				showIndicacionesPasswordRegister(client, true);
			}
		}
		return Plugin_Handled;
	}
	if(player.bInCreatingCharacter){
		char name[32];
		strcopy(name, 32, sArgs);
		switch(isValidInput(name)){
			case INPUT_SHORT:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre muy corto.");
				showCrearCharacter(client, false);
			}
			case INPUT_LARGE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre muy largo.");
				showCrearCharacter(client, false);
			}
			case INPUT_INVALID_SIMBOL:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre contiene simbolos.");
				showCrearCharacter(client, false);
			}
			case INPUT_INVALID_SPACE:{
				PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre contiene espacios.");
				showCrearCharacter(client, false);
			}
			case INPUT_OK: {//makeValidToSQL(name, namev, DATA_NAME);
				characterNames.SetString(client, name);
				PrintHintText(client, "El nombre ingresado es: %s", name);
				showCrearCharacter(client, true);
			}
		}
		return Plugin_Handled;
	}
	
	if(player.bStaff || player.bAdmin){
		char msg[128];
		strcopy(msg, sizeof(msg), sArgs);
		if(msg[0] == '@' && msg[1] == '@'){
			ReplaceStringEx(msg, sizeof(msg), "@", "");
			ReplaceStringEx(msg, sizeof(msg), "@", "");
			printVipSay(client, msg);
			return Plugin_Handled;
		}
	}
	
	return Plugin_Continue;
}
public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon){
	ZPlayer player = ZPlayer(client);
	
	// Crucial condition
	if (!IsPlayerAlive(player.id)){
		buttons &= (~IN_USE);
		return Plugin_Changed;
	}
	
	// Autobhop
	if (buttons & IN_JUMP){
		if(!(GetEntityMoveType(player.id) & MOVETYPE_LADDER) && !(GetEntityFlags(player.id) & FL_ONGROUND)){
			// Disable crouching bhop for zombies: NOT NEEDED ANYMORE
			/*if (player.isType(PT_ZOMBIE) && (GetEntityFlags(player.id) & FL_DUCKING || buttons & IN_DUCK)){
				return Plugin_Changed;
			}*/
			
			if(waterCheck(player.id) < 2)
				buttons &= ~IN_JUMP;
		}
	}
	
	// BUGFIX: Gravity resetting when touching a ladder
	if (GetEntityMoveType(client) == MOVETYPE_LADDER) 
		player.bLadder = true; 
	else{
		if(player.bLadder){
			player.setGravity();
			player.bLadder = false; 
		} 
	}
	
	// Lasermines check
	if (buttons & IN_DUCK){
		// Cooldown? return
		if (buttons & IN_USE || buttons & IN_RELOAD)
			if (IsOnCooldown(player.id, 0.8))
				return Plugin_Continue;
		
		// Plant with CTRL+E
		if (buttons & IN_USE){
			if (newRound)
				return Plugin_Continue;
			
			if (player.iTeamNum != CS_TEAM_CT || player.bInLmAction)
				return Plugin_Continue;
			
			if (player.iLasermine)
				PlantLasermine(player.id);
		}
		// Defuse with CTRL+R
		else if (buttons & IN_RELOAD){
			if (player.iTeamNum != CS_TEAM_CT || player.bInLmAction)
				return Plugin_Continue;
			
			DefuseLasermine(player.id);
		}
	}
	
	// Detect "USE" in the Mercenary
	if (buttons & IN_USE){
		int targetIndex = GetClientAimTarget(player.id, false);
		
		if (!IsValidEdict(targetIndex))
			return Plugin_Continue;
		
		char sClassname[32];
		float entVec[3], userVec[3];
		GetEdictClassname(targetIndex, sClassname, sizeof(sClassname));
		GetEntPropVector(targetIndex, Prop_Send, "m_vecOrigin", entVec);
		GetClientAbsOrigin(player.id, userVec);
		
		if (!StrEqual(sClassname, NPC_CLASSNAME))
			return Plugin_Continue;
			
		if (GetVectorDistance(entVec, userVec, true) <= NPC_MAX_DISTANCE_SQUARED)
			showMenuLogros(client);
	}
	
	// If user can leap
	if (player.bCanLeap){
		if (GetEntityFlags(player.id) & FL_ONGROUND){
			if (buttons & IN_DUCK && buttons & IN_JUMP)
				DoLeap(player.id);
		}
	}
	return Plugin_Continue;
}
stock int waterCheck(int client){
	return GetEntProp(client, Prop_Data, "m_nWaterLevel");
}
void DoLeap(int client){
	ZPlayer player = ZPlayer(client);
	
	if (!IsPlayerExist(player.id))
		return;
	
	if (IsOnCooldown(player.id, LEAP_DELAY))
		return;
	
	float x, y;
	if (player.isType(PT_NEMESIS) || player.isType(PT_ASSASSIN)){
		x = -40.0;
		y = 650.0;
	}
	else if (player.isType(PT_SURVIVOR) || player.isType(PT_GUNSLINGER) || player.isType(PT_SNIPER)){
		x = -25.0;
		y = 650.0;
	}
	
	float ClientEyeAngle[3], ClientAbsOrigin[3], Velocity[3];
	
	GetClientAbsOrigin(player.id, ClientAbsOrigin);
	GetClientEyeAngles(player.id, ClientEyeAngle);
	
	float EyeAngleZero = ClientEyeAngle[0];
	
	ClientEyeAngle[0] = x;
	GetAngleVectors(ClientEyeAngle, Velocity, NULL_VECTOR, NULL_VECTOR);
	
	ScaleVector(Velocity, y);
	ClientEyeAngle[0] = EyeAngleZero;
	TeleportEntity(player.id, ClientAbsOrigin, ClientEyeAngle, Velocity);
}

// Damage hooks
public Action OnDamageTraceAttack(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &ammotype, int hitbox, int hitgroup){
	if (!IsPlayerExist(attacker))
		return Plugin_Handled;
		
	ZPlayer Victim = ZPlayer(victim);
	ZPlayer Attacker = ZPlayer(attacker);
	
	if (Attacker.iTeamNum == CS_TEAM_CT && Victim.iTeamNum == CS_TEAM_CT)
		return Plugin_Handled;
		
	if (Victim.bInvulnerable)
		return Plugin_Handled;
	
	return Plugin_Continue;
}
public Action OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype){

	if (!IsPlayerExist(victim, true))
		return Plugin_Handled;
		
	// Initialize variables
	ZPlayer Attacker = ZPlayer(attacker);
	ZPlayer Victim = ZPlayer(victim);
	
	// Avoid damages whenever it should not be possible to get hurt
	if (damagetype & DMG_FALL) return Plugin_Handled;
	if (newRound || endRound) return Plugin_Handled;	
	
	// Emit hurt sounds
	EmitHurtSound(Victim.id);
	
	if (Victim.bInvulnerable)
		return Plugin_Handled;
	
	// Initialize another damage variable
	float dmg = damage;
	
	// Hooking different types of damage
	if (Victim.iTeamNum == CS_TEAM_T && damagetype & DMG_DIRECT){
		dmg = (Victim.iHp*0.35)/100.0;
		//PrintToChat(Victim.id, "%s Daño traducido por ignición: \x09%d\x01.", SERVERSTRING, RoundToZero(dmg));
		if (Victim.iHp <= RoundToZero(dmg))	Victim.iHealth = 0;
		else	Victim.iHp -= RoundToZero(dmg);
		return Plugin_Handled;
	}
	
	if (!IsPlayerExist(attacker, true))
		return Plugin_Handled;
	
	// Conditions bypassed, initialize variables
	int gain;
	
	// Depending on the attacker's type
	switch (Attacker.iType){
		case PT_ZOMBIE:{
			ZClass class = ZClass(Attacker.iZombieClass);
			ZAlignment alignment = ZAlignment(Attacker.iZombieAlignment);
			
			//char Aname[32];
			//GetClientName(Attacker.id, Aname, 32);
			dmg *= (Attacker.getZombieDamage() * class.flDmg) * alignment.flDamageMul;
			
			if(Victim.iArmor){
				if(RoundToZero(dmg) > Victim.iArmor) Victim.iArmor = 0;
				else Victim.iArmor -= RoundToZero(dmg);
			}
			else{
				int humans = GetValidPlayingHumans();
				if(ActualMode.bInfection) {
					if(humans > 1){
						Victim.bRecentlyInfected = true;
						Event event = CreateEvent("player_death");
						if (event == null) return Plugin_Handled;
						
						// Dispatch event data
						event.SetInt("userid", GetClientUserId(Victim.id));
						event.SetInt("attacker", (IsPlayerExist(Attacker.id)) ? GetClientUserId(Attacker.id) : GetClientUserId(Victim.id));
						event.SetString("weapon", "knife");
						event.SetBool("headshot", true);
						event.Fire();
						
						// Infect victim
						Victim.Zombiefy(ZOMBIE);
						Attacker.iInfections++;
						
						// Calculate gain
						//gain = RoundToFloor(dmg*REWARD_ZOMBIE_MULTIPLIER * Attacker.flExpBoost);
						int iRest = NextLevel(Attacker.iLevel+1, Attacker.iReset)-Attacker.iExp;
						int iTotal =  Attacker.calculateGain(RoundToNearest(iRest*INFECTION_PERCENTAGE_REWARD+((Victim.iLevel*Victim.iReset)/10))+INFECTION_BASE_REWARD);
						if (Attacker.iLevel < RESET_LEVEL){
							if (bAllowGain)
								Attacker.iExp += iTotal;
							
							PrintToChat(Attacker.id, "%s \x09INFECCIÓN!\x01 Has ganado \x09%d\x01 ammopacks!", SERVERSTRING, iTotal);
						}
						
						PrintHintText(Attacker.id, "<font size='37' color='#FF0000'>INFECCIÓN!</font>\nDaño realizado: <font color='#ff0000'>%d</font>", RoundToZero(dmg));
					}
					else{
						if((Victim.iHp+Victim.iArmor) <= dmg) Victim.iHealth = 0;
						else Victim.iHp -= (RoundToZero(dmg) - Victim.iArmor);
					}
				}
			}
				
			if(ActualMode.bKill){
				if((Victim.iHp+Victim.iArmor) <= dmg) Victim.iHealth = 0;
				else Victim.iHp -= (RoundToZero(dmg) - Victim.iArmor);
			}			
		}
		case PT_NEMESIS:{
			dmg *= NEMESIS_DAMAGE * Attacker.getZombieDamage();
			
			if(Victim.iHp <= dmg) Victim.iHealth = 0;
			else Victim.iHp -= RoundToZero(dmg);
			
			// Calculate gain
			//gain = RoundToZero((RoundToZero(dmg) / coefDif)*NEMESIS_EXP_BOOST);
		}
		case PT_ASSASSIN:{
			dmg *= NEMESIS_DAMAGE * Attacker.getZombieDamage();
			
			if(Victim.iHp <= dmg) Victim.iHealth = 0;
			else Victim.iHp -= RoundToZero(dmg);
			
			// Calculate gain
			//gain = RoundToZero((RoundToZero(dmg) / coefDif)*NEMESIS_EXP_BOOST);
		}
		case PT_HUMAN:{
			char weapon[32];
			GetClientWeapon(Attacker.id, weapon, sizeof(weapon));
			int pWeap = GetPlayerWeaponSlot(Attacker.id, CS_SLOT_PRIMARY);
			int sWeap = GetPlayerWeaponSlot(Attacker.id, CS_SLOT_SECONDARY);
			int iWeapon = GetEntPropEnt(Attacker.id, Prop_Send, "m_hActiveWeapon");
			
			if(pWeap == iWeapon){
				ZWeaponPrimary weap = ZWeaponPrimary(Attacker.iWeaponPrimary);
				dmg *= weap.flDamage;
				
				if (weap)
					DamageOnClientKnockBack(Victim.id, Attacker.id, weap.flKnockback);
			}
			else if(sWeap == iWeapon){
				ZWeaponSecondary weap = ZWeaponSecondary(Attacker.iWeaponSecondary);
				dmg *= weap.flDamage;
			}
			
			HAlignment hAlignment = HAlignment(Attacker.iHumanAlignment);
			
			dmg *= Attacker.getHumanDamage() * hAlignment.flDamageMul;
			
			//RealDamage
			float rd = 1.0-(Attacker.getHumanDamage(true)-Victim.getZombieResistance(true));
			if(Attacker.getHumanDamage(true)<Victim.getZombieResistance(true))	rd = 1.0;
			
			//total
			dmg *= rd;
			
			gain = RoundToZero(dmg) / coefDif;
			
			if (Victim.iHp <= dmg)	Victim.iHealth = 0;
			else	Victim.iHp -= RoundToZero(dmg);
			
			
			// Special weapon check
			char sWeapon[32];
			GetEdictClassname(iWeapon, sWeapon, sizeof(sWeapon));
			if (StrEqual(sWeapon, "weapon_awp", false)){
				if (IsPlayerExist(Victim.id, true)){
					if (Attacker.iWeaponPrimary == iFireAwp)
						IgniteEntity(Victim.id, Victim.isBoss() ? 1.0 : 3.0);
					else if (Attacker.iWeaponPrimary == iFreezeAwp)
						Freeze(Victim.id, Attacker.id, 0.5);
				}
			}
		}
		case PT_SURVIVOR:{
			dmg *= SURVIVOR_DAMAGE * Attacker.getHumanDamage();
			
			if(Victim.iHp <= dmg) Victim.iHealth = 0;
			else Victim.iHp -= RoundToZero(dmg);
			
			gain = RoundToZero((RoundToZero(dmg) / coefDif)*SURVIVOR_EXP_BOOST);
		}
		case PT_GUNSLINGER:{
			dmg *= GUNSLINGER_DAMAGE * Attacker.getHumanDamage();
		
			if(Victim.iHp<= dmg) Victim.iHealth = Victim.iHp = 0;
			else Victim.iHp -= RoundToZero(dmg);
			
			gain = RoundToZero((RoundToZero(dmg) / coefDif)*GUNSLINGER_EXP_BOOST);
		}
		case PT_SNIPER:{
			dmg *= SNIPER_DAMAGE * Attacker.getHumanDamage();
		
			if(Victim.iHp<= dmg) Victim.iHealth = Victim.iHp = 0;
			else Victim.iHp -= RoundToZero(dmg);
			
			gain = RoundToZero((RoundToZero(dmg) / coefDif)*SNIPER_EXP_BOOST);
		}
	}
	
	// Stack combo if attacker is any type of human
	if (!IsFakeClient(Attacker.id)){
		
		// Cooldown to show the HUD
		fEngineTime = GetEngineTime();
		if (Attacker.iTeamNum == CS_TEAM_CT){
			if(!Attacker.isInParty()){
				Attacker.iDamageDealt += RoundToFloor(dmg);
			
				while (Attacker.iDamageDealt >= NextCombo(Attacker.iCombo, Attacker.iReset))
					Attacker.iCombo++;
				
				if (Attacker.fComboTime < fEngineTime){
					// Show the info & update cooldown
					Attacker.fComboTime = fEngineTime+0.1;
					ShowComboInfo(Attacker.id, RoundToZero(dmg));	
				}
			} 
			else{
				ZParty pt = ZParty(findPartyByUID(Attacker.iPartyUID));
				if(pt.avgLevel() != -1){
					pt.iDamageDealt += RoundToFloor(dmg);
					while (pt.iDamageDealt >= NextCombo(pt.iCombo, pt.avgLevel()))
						pt.iCombo++;
					
					if (pt.fComboTime < fEngineTime){
						// Show the info & update cooldown
						pt.fComboTime = fEngineTime+0.1;
						ShowComboInfoParty(Attacker.id, RoundToZero(dmg));	
					}
				}
			}
			
		}
	}
	
	// Check if gain is enabled according to the ammount of players
	if (bAllowGain){
		// Depending on the victim's type
		switch (Victim.iType){
			case PT_NEMESIS, PT_ASSASSIN:{
				Attacker.iHDamageDealt += RoundToZero(dmg);
				checkPointsUp(Attacker.id);
			}
			case PT_SURVIVOR, PT_GUNSLINGER, PT_SNIPER:{
				Attacker.iZDamageDealt += RoundToZero(dmg);
				checkPointsUp(Attacker.id);
				
				int iRest = NextLevel(Attacker.iLevel+1, Attacker.iReset)-Attacker.iExp;
				
				//int iTotal =  Attacker.calculateGain(RoundToNearest(iRest*0.01+((Victim.iLevel*Victim.iReset)/10))+INFECTION_BASE_REWARD);
				int iTotal =  Attacker.calculateGain(RoundToNearest(iRest*0.002))+(INFECTION_BASE_REWARD*2);
				Attacker.iExp += iTotal;
				
				PrintHintText(Attacker.id, "Daño realizado: <font color='#ff0000'>%d</font>\nAmmopacks: <font color='#ff0000'>%d</font>", RoundToZero(dmg), iTotal);
			}
		}
		
		if (gain >= 1 && Attacker.iLevel < RESET_LEVEL)
			Attacker.iExp += gain;
		
		/*if (Attacker.iTeamNum == CS_TEAM_T)
			PrintHintText(Attacker.id, "Daño realizado: <font color='#ff0000'>%d</font>", RoundToZero(dmg));*/
		
		// Check level
		checkLevelUp(Attacker.id);
	}
	
	// Deny the original damage
	return Plugin_Handled;
}

// ------------
stock int calculateChances(int chances, int successValue, int failureValue){
	int value;
	if (GetRandomInt(0, 100) >= 100-chances)
		value = successValue;
	else
		value = failureValue;
	
	return value;
}
public void checkPointsUp(int client){
	ZPlayer p = ZPlayer(client);
	
	if(p.iHDamageDealt >= DAMAGE_TO_HPOINTS){
		int x;
		
		// Get values according to vip chances
		switch (p.flExpBoost){
			case 1.0: x = calculateChances(5, 2, 1);
			case 2.0: x = calculateChances(15, 2, 1);
			case 3.0: x = calculateChances(25, 2, 1);
			case 4.0: x = calculateChances(35, 2, 1);
			case 5.0: x = calculateChances(55, 2, 1);
			case 6.0: x = calculateChances(70, 2, 1);
			case 7.0: x = calculateChances(25, 3, 2);
			case 8.0: x = calculateChances(40, 3, 2);
		}
		
		if(p.bStaff)	x = 5;
		p.iHDamageDealt = 0;
		p.iHPoints += x;
		
		if (x == 1)
			PrintToChat(p.id, "%s Bien hecho! has ganado\x0F %d\x01 punto%s \x09humano%s\x01!", SERVERSTRING, x, (x > 1) ? "s" : "", (x > 1) ? "s" : "");
		else
			PrintToChat(p.id, "%s Tuviste suerte! Ganaste\x0F %d\x01 puntos \x09humanos\x01!", SERVERSTRING, x);
	}
	if(p.iZDamageDealt >= DAMAGE_TO_ZPOINTS){
		int x;
		
		// Get values according to vip chances
		switch (p.flExpBoost){
			case 1.0: x = calculateChances(5, 2, 1);
			case 2.0: x = calculateChances(15, 2, 1);
			case 3.0: x = calculateChances(25, 2, 1);
			case 4.0: x = calculateChances(35, 2, 1);
			case 5.0: x = calculateChances(55, 2, 1);
			case 6.0: x = calculateChances(70, 2, 1);
			case 7.0: x = calculateChances(25, 3, 2);
			case 8.0: x = calculateChances(40, 3, 2);
		}

		if(p.bStaff)	x = 5;
		p.iZDamageDealt = 0;
		p.iZPoints += x;
		
		if (x == 1)
			PrintToChat(p.id, "%s Bien hecho! has ganado\x0F %d\x01 punto%s \x09zombie\x01!", SERVERSTRING, x,  (x > 1) ? "s" : "");
		else
			PrintToChat(p.id, "%s Tuviste suerte! Ganaste\x0F %d\x01 puntos \x09zombie\x01!", SERVERSTRING, x);
	}
}
public Action infopoints(int client, int args){
	char hDamage[32];
	char zDamage[32];
	char hDamageN[32];
	char zDamageN[32];
	ZPlayer p = ZPlayer(client);
	AddPoints(p.iHDamageDealt, hDamage, 32);
	AddPoints(p.iZDamageDealt, zDamage, 32);
	AddPoints(DAMAGE_TO_HPOINTS, hDamageN, 32);
	AddPoints(DAMAGE_TO_ZPOINTS, zDamageN, 32);
	PrintToChat(p.id, "%s Daño para el siguiente punto humano: \x0F[%s / %s]\x01", SERVERSTRING, hDamage, hDamageN);
	PrintToChat(p.id, "%s Daño para el siguiente punto zombie: \x0F[%s / %s]\x01", SERVERSTRING, zDamage, zDamageN);
	return Plugin_Handled;
}

// INDIVIDUAL combo functions
public void ShowComboInfo(int client, int damagehit){

	if (!IsPlayerExist(client)){
		PrintToServer("[DEBUG] Usuario no válido (index: %i)", client);
		LogError("[DEBUG] Usuario no válido (index: %i)", client);
		return;
	}

	ZPlayer player = ZPlayer(client);
		
	// Adjust combo type
	ZCombo combo = ZCombo(player.iComboType);
	combo.AdjustType(player.id);
	
	// String buffer
	char msg[512];
	
	// Format the hud
	formatComboHUD(player.id, msg, damagehit);
	
	// Set parameters
	if (combo.id >= view_as<int>(COMBO_MASSIVE))
		SetHudTextParams(GetRandomFloat(0.42, 0.38), GetRandomFloat(0.67, 0.63), COMBO_DURATION, GetRandomInt(100, 255), GetRandomInt(150, 255), GetRandomInt(150, 255), 1, 1, 0.2, 0.01, 0.6);
	else
		SetHudTextParams(0.42, 0.65, COMBO_DURATION, combo.iRed, combo.iGreen, combo.iBlue, 1, 1, 0.5, 0.01, 0.8);
	
	ShowSyncHudText(player.id, hComboSynchronizer, msg);
	
	// Reset handles
	if (player.hComboHandle != null){
		delete player.hComboHandle;
		player.hComboHandle = null;
	}
	
	// Set a timer to finish the combo
	player.hComboHandle = CreateTimer(COMBO_DURATION, FinishCombo, player.id, TIMER_FLAG_NO_MAPCHANGE);
}
public Action FinishCombo(Handle hTimer, any client){
	ZPlayer player = ZPlayer(client);
	
	// Not valid? Kill timer
	if (!IsPlayerExist(player.id)){
		player.hComboHandle = null;
		return Plugin_Stop;
	}
	
	if(player.hComboHandle == null){
		return Plugin_Stop;
	}
	
	// Initialize combo method
	ZCombo combo = ZCombo(player.iComboType);
	combo.AdjustType(player.id);
	
	///////////////////////////////////////////
	
	// Calculate gain
	int rawgain = RoundToFloor(player.iCombo * COMBO_REWARD_BASE * combo.fBonusExpGain);
	
	// Apply gain according to VIP & happyhour levels
	int totalgain = player.calculateGain(rawgain);
	
	// Initialize vars
	char msg[512];
	
	// Format HUD
	formatComboHUD(player.id, msg, totalgain, true);
	
	// Set parameters
	SetHudTextParams(0.42, 0.20, 2.5, combo.iRed, combo.iGreen, combo.iBlue, 1, 1, 1.2, 0.02, 0.6);
	
	// Print the info to the user
	ShowSyncHudText(player.id, hComboSynchronizer2, msg);
	
	// Apply gain
	if (bAllowGain && player.iLevel < RESET_LEVEL)
		player.iExp += totalgain;
	
	// Reset vars
	player.iCombo = 1;
	player.iComboType = view_as<int>(COMBO_DEFAULT);
	player.iDamageDealt = 0;
	player.hComboHandle = null;
	
	// Check player's level
	checkLevelUp(player.id);
	
	return Plugin_Stop;
}

// PARTY combo functions
public void ShowComboInfoParty(int client, int damagehit){
	ZPlayer player = ZPlayer(client);
	
	if (!IsPlayerExist(player.id)){
		PrintToServer("[DEBUG] Usuario no válido (index: %i)", player.id);
		LogError("[DEBUG] Usuario no válido (index: %i)", player.id);
		return;
	}
	
	if (!player.isInParty()){
		PrintToServer("[DEBUG] Este usuario no está en party (index: %i)", player.id);
		LogError("[DEBUG] Este usuario no está en party (index: %i)", player.id);
		return;
	}
	
	// Adjust combo type
	ZCombo combo = ZCombo(0);
	
	// String buffer
	char msg[512];

	SetHudTextParams(0.42, 0.65, COMBO_DURATION, combo.iRed, combo.iGreen, combo.iBlue, 1, 1, 0.5, 0.01, 0.8);

	ZParty pt = ZParty(findPartyByUID(player.iPartyUID));
	for(int i; i < pt.length(); i++){
		ZPlayer ptPlayer = ZPlayer(gPartyMembers[pt.id].Get(i));
		if(ptPlayer.iTeamNum != CS_TEAM_CT) continue;
		
		// Format the hud
		formatComboHUD(ptPlayer.id, msg, damagehit);
		ShowSyncHudText(ptPlayer.id, hComboSynchronizer, msg);
	}
	
	// Reset handles
	if (pt.hComboHandle != null){
		delete pt.hComboHandle;
		pt.hComboHandle = null;
	}
	
	// Set a timer to finish the combo
	pt.hComboHandle = CreateTimer(COMBO_DURATION, FinishComboParty, player.id, TIMER_FLAG_NO_MAPCHANGE);
}
public Action FinishComboParty(Handle hTimer, any client){
	ZPlayer player = ZPlayer(client);
	
	// Not valid? Kill timer
	if (!IsPlayerExist(player.id)){
		PrintToServer("[DEBUG] Usuario no válido (index: %i) >>>>> FinishComboParty", player.id);
		LogError("[DEBUG] Usuario no válido (index: %i) >>>>> FinishComboParty", player.id);
		if(player.isInParty()){
			ZParty pt = ZParty(findPartyByUID(player.iPartyUID));
			PrintToServer("[DEBUG] Un usuario no válido está en party (index: %i - ptIndex: %i)", player.id, pt);
			LogError("[DEBUG] Un usuario no válido está en party (index: %i - ptIndex: %i)", player.id, pt);
			pt.hComboHandle = null;
		}
		return Plugin_Stop;
	}
	
	ZParty pt = ZParty(findPartyByUID(player.iPartyUID));
	ZCombo combo = ZCombo(0);
	
	if(pt.hComboHandle == null){
		return Plugin_Stop;
	}
	
	// Initialize vars
	char msg[512];
	
	// Calculate gain
	//int rawgain = RoundToCeil( pt.iCombo * (COMBO_PARTY_REWARD_PU * (pt.Length()/pt.GetHumans())) );
	int rawgain = RoundToNearest((pt.iCombo * (1+pt.getTotalBoost()))*pt.getValidPlayers());
	// Set parameters
	SetHudTextParams(0.42, 0.20, 2.5, combo.iRed, combo.iGreen, combo.iBlue, 1, 1, 1.2, 0.02, 0.6);
	
	for(int i; i < pt.length(); i++){
		ZPlayer ptPlayer = ZPlayer(gPartyMembers[pt.id].Get(i));
		if(ptPlayer.iTeamNum == CS_TEAM_T) continue;
		
		// Apply gain according to VIP & happyhour levels
		int totalgain = ptPlayer.calculateGain(rawgain);
		
		// Format HUD
		formatComboHUD(player.id, msg, totalgain, true);
		
		// Print the info to the user
		ShowSyncHudText(ptPlayer.id, hComboSynchronizer2, msg);
		
		// Apply gain
		if (bAllowGain && player.iLevel < RESET_LEVEL && totalgain > 0)
			ptPlayer.iExp += totalgain;
		
		// Check player's level
		checkLevelUp(ptPlayer.id);
	}
	
	// Reset vars to the entire party
	pt.iCombo = 1;
	pt.iDamageDealt = 0;
	pt.hComboHandle = null;
	//delete pt.hComboHandle;
	
	// Kill timer
	return Plugin_Stop;
}

// COMBOS' FORMULAS
stock int NextCombo(int iCombo, int iReset){
	int partial = RoundToNearest(1+(iReset*0.01));
	//int total = RoundToNearest(((iCombo*iCombo*0.0015)+500)*partial);
	int iRoundedCombo = RoundToNearest(iCombo*0.15);
	int total = RoundToNearest((((iRoundedCombo+500)*partial)/100.0)*iCombo);
	
	return total;
}
stock int NextComboPt(int iCombo, int iLevel){
	//int total = RoundToCeil(((iCombo*iCombo*0.00375)+1000)*(1+(iLevel*0.01)));
	
	int partial = RoundToNearest((iCombo*iCombo*0.375)/100.0);
	
	int partial2 = RoundToNearest(1+(iLevel*0.01));
	
	int total = RoundToCeil(partial+1000)*partial2;
	
	return total;
}

// Combo HUD formatting
stock void formatComboHUD(int client, char buffer[512], int value, bool bFinish = false){
	ZPlayer player = ZPlayer(client);
	
	// Initialize buffers
	char sCombo[15], sDamage[15];
	
	if (player.isInParty()){
		ZParty pt = ZParty(findPartyByUID(player.iPartyUID));
		
		AddPoints(pt.iCombo, sCombo, sizeof(sCombo));
		AddPoints(pt.iDamageDealt, sDamage, sizeof(sDamage));
		
		if (!bFinish){
			FormatEx(buffer, sizeof(buffer), "Ganancias acumuladas: %s AP\nDaño acumulado: %s\n", sCombo, sDamage);
		}
		else{
			char sGain[15];
			if(!pt.getValidPlayers()) return; 
			
			AddPoints(value, sGain, sizeof(sGain));
			
			FormatEx(buffer, sizeof(buffer), "COMBO FINALIZADO\nGanancias totales: %s AP\nDaño totales: %s\nRecibes: %s AP", sCombo, sDamage, sGain);
		}
	}
	else{
		AddPoints(player.iCombo, sCombo, sizeof(sCombo));
		AddPoints(player.iDamageDealt, sDamage, sizeof(sDamage));
	
		if (!bFinish){
			char sDamageHit[15];
			AddPoints(value, sDamageHit, sizeof(sDamageHit));
			
			///////////////////////////////////////////////////////////
			ZCombo combo = ZCombo(player.iComboType);
			combo.AdjustType(player.id);
			
			char sName[32];
			gComboName.GetString(player.iComboType, sName, sizeof(sName));
			
			FormatEx(buffer, sizeof(buffer), "Combo %s de ammopacks\n%s Daño acumulado: %s\nÚltimo disparo: %s de daño", sCombo, sName, sDamage, sDamageHit);
		}
		else{
			char sGain[15];
			// Add "." to potentially large numbers
			AddPoints(value, sGain, sizeof(sGain));
			
			// Format hud message depending on the gain
			if (player.flExpBoost > 1.0){
				FormatEx(buffer, sizeof(buffer), "%s\nDaño realizado: %s\nGanancia: %s AP (VIP x%i)", sCombo, sDamage, sGain, RoundToZero(player.flExpBoost));
			}
			else{
				// Standard user
				FormatEx(buffer, sizeof(buffer), "%s\nDaño realizado: %s\nGanancia: %s AP", sCombo, sDamage, sGain);
			}
			
			// Cache hud info
			char sTemp[256];
			strcopy(sTemp, sizeof(sTemp), buffer);
			
			///////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////
			
			FormatEx(buffer, sizeof(buffer), "Combo finalizado: %s\n", sTemp);
		}
	}
}

// Player reset
public Action resetear(int client){

	if (!IsPlayerExist(client))
		return Plugin_Handled;
		
	ZPlayer p = ZPlayer(client);
	
	if (IsPlayerAlive(p.id)) ForcePlayerSuicide(p.id);
	
	if (p.hComboHandle != null)
		FinishCombo(p.hComboHandle, p.id);
	
	// Restart user values
	p.iLevel = 1;
	p.iExp = 1;
	p.iSelectedPrimaryWeapon = 0;
	p.iSelectedSecondaryWeapon = 0;
	p.iWeaponPrimary = 0;
	p.iWeaponSecondary = 0;
	p.iNextWeaponPrimary = 0;
	p.iNextWeaponSecondary = 0;
	p.iGrenadePack = 0;
	p.iNextGrenadePack = 0;
	p.iZombieClass = 0;
	p.iHumanClass = 0;
	p.iNextHumanClass = 0;
	p.iNextZombieClass = 0;
	p.iCombo = 1;
	
	if (p.isInParty()){
		ZParty party = ZParty(findPartyByUID(p.iPartyUID));
		if(party.length()-1 <= 1) FinishComboParty(party.hComboHandle, client);
		party.removeMember(p.id);
	}
	
	// Give the rewards
	p.iZGoldenPoints += RESET_ZGPOINTS_REWARD;
	p.iHGoldenPoints += RESET_HGPOINTS_REWARD;
	p.iReset++;
	
	// Respawn the guy
	if (ActualMode.bRespawn)
		p.iRespawnPlayer(ActualMode.bRespawnZombieOnly);
	
	// Print some motivation bullshit
	char sName[32];
	GetClientName(p.id, sName, sizeof(sName));
	PrintToChat(p.id, "%s Felicidades \x09%s\x01, has reseteado!", SERVERSTRING, sName);
	PrintToChat(p.id, "%s Has obtenido puntos \x09GOLDEN\x01, revisa las mejoras!", SERVERSTRING);
	return Plugin_Handled;
}

// OnCient functions
public void OnClientPostAdminCheck(int client){
	EmitSoundToClient(client, WELCOME_SOUND, SOUND_FROM_PLAYER, SNDCHAN_STATIC, SNDLEVEL_NORMAL,_,_,_,_,_,_,_,5.0);
	ZPlayer player = ZPlayer(client);
	SDKHook(player.id, SDKHook_WeaponSwitchPost, OnClientWeaponSwitchPost);
	SDKHook(player.id, SDKHook_WeaponDropPost, OnClientWeaponDropPost);
}

//=====================================================
//					SERVER COMMANDS
//=====================================================
public Action givezpoints(int args){
	char player[16];
	char cPoints[16];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, cPoints, 16);
	int points = StringToInt(cPoints);
	for(int i=1; i < MAXPLAYERS+1; i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player) && IsPlayerExist(i)){
				ZPlayer p = ZPlayer(i);
				p.iZPoints += points;
				PrintToChatAll("Se dieron %d puntos zombie a %s", points, name);
			}
		}
	}
	return Plugin_Handled;
}
public Action givexp(int args){
	char player[16];
	char cPoints[16];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, cPoints, 16);
	int points = StringToInt(cPoints);
	for(int i=1; i < MAXPLAYERS+1; i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(i);
				p.iExp += points;
				checkLevelUp(i);
				PrintToChatAll("Se dieron %d ammopacks a %s", points, name);
			}
		}
	}
	return Plugin_Handled;
}
public Action makePlayer(int args){
	char player[16];
	char type[16];
	char what[32];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, type, 16);
	for(int i; i < Players.Length; i++){
		int id = Players.Get(i);
		if(IsPlayerExist(id)){
			char name[32];
			GetClientName(id,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(id);
				if(StrEqual(type, "human")){
					p.Humanize(HUMAN);
					p.giveWeapons();
					Format(what, 32, "Humano");
				}
				else if(StrEqual(type, "zombie")){
					p.Zombiefy(ZOMBIE);
					Format(what, 32, "Zombie");
					GivePlayerItem(p.id, "weapon_flashbang");
				}
				else if (StrEqual(type, "assassin")){
					p.Zombiefy(ASSASSIN);
					Format(what, 32, "Assassin");
				}
				else if(StrEqual(type, "gunslinger")){
					p.Humanize(GUNSLINGER);
					Format(what, 32, "gunslinger");
				}
				else if(StrEqual(type, "survivor")){
					p.Humanize(SURVIVOR);
					Format(what, 32, "Survivor");
				}
				else if (StrEqual(type, "sniper")){
					p.Humanize(SNIPER);
					Format(what, 32, "Sniper");
				}
				else if(StrEqual(type, "nemesis")){
					p.Zombiefy(NEMESIS);
					Format(what, 32, "Nemesis");
				}
				else if(StrEqual(type, "alive")){
					p.iRespawnPlayer();
					Format(what, 32, "Vivo");
				}
				else if(StrEqual(type, "invulnerable")){
					p.bInvulnerable = !p.bInvulnerable;
					Format(what, 32, "Invulnerable");
				}
				else{
					PrintToServer("%s no encontrado", type);
					return Plugin_Handled;
				}
				PrintToChatAll("Se transformo a %s en %s", name, what);
			}
		}		
	}
	return Plugin_Handled;
}
public Action givehpoints(int args){
	char player[16];
	char cPoints[16];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, cPoints, 16);
	int points = StringToInt(cPoints);
	for(int i=1; i < MAXPLAYERS+1; i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(i);
				p.iHPoints += points;
				PrintToChatAll("Se dieron %d puntos humanos a %s", points, name);
			}
		}
		
	}
	return Plugin_Handled;
}
public Action infServerNow(int client, int args){
	ZPlayer player = ZPlayer(client);
	if(!player.bStaff) return Plugin_Handled;
	bool acc;
	bool gmode;
	bool zclass;
	bool hclass;
	bool pweaps;
	bool sweaps;
	bool basic;
	bool gpacks;
	bool party;
	bool all;
	
	char arg[32];
	for(int i = 1; i <= GetCmdArgs(); i++){
		GetCmdArg(i, arg, 32);
		if(StrEqual(arg, "all")) all = true;
		if(StrEqual(arg, "basic")) basic = true;
		if(StrEqual(arg, "players")) acc = true;
		if(StrEqual(arg, "gamemodes")) gmode = true;
		if(StrEqual(arg, "zclasses")) zclass = true;
		if(StrEqual(arg, "hclasses")) hclass = true;
		if(StrEqual(arg, "pweapons")) pweaps = true;
		if(StrEqual(arg, "sweapons")) sweaps = true;
		if(StrEqual(arg, "gpacks")) gpacks = true;
		if(StrEqual(arg, "party")) party = true;
	}
	
	if(basic || all){
		PrintToConsole(client, "\nBasic info...\n");
		PrintToConsole(client, "Actual Mode: %d\nCoef Difficulty: %d\nnewRound: %b\nstartedRound: %b\nMode Count: %d", ActualMode.id, coefDif, newRound, startedRound, modeCount);
	}
	if(acc || all){
		PrintToConsole(client, "\nPlayers...\n");
		for(int i; i< Players.Length; i++){
			char name[32];
			int id = Players.Get(i);
			if(IsPlayerExist(id)){
				ZPlayer p = ZPlayer(id);
				GetClientName(id, name, 32);
				PrintToConsole(client, "------------------------------------------------------");
				PrintToConsole(client, "Id: %d\tPjSelec: %d\tUser: %s\tLvl: %d\tHClass: %d\tZClass: %d\tZPoints: %d\tHPoints: %d\nZKills: %d\tHKills: %d\tZCKills: %d\tNemKills: %d\tSurKills: %d\tGunKills: %d\tAsKills: %d\tSnipKills: %d\nHS: %d\tInf: %d\tBackInf: %d\tGreInf: %d\tInfiBought: %d\tAntiBought: %d\tMadBought: %d", 
								id, p.iPjSeleccionado, name, p.iLevel, p.iHumanClass, p.iZombieClass, p.iZPoints, p.iHPoints, p.iZKills, p.iHKills, p.iZCuchiKills, p.iNemeKills, p.iSurviKills, p.iGunsKills, p.iAssaKills, p.iSnipKills, p.iHeadshots, p.iInfections, p.iBackInfection, p.iGrenadeInfection, p.iInfBullBuyed, p.iAntiBuyed, p.iFuriBuyed);
				PrintToConsole(client, "------------------------------------------------------");
			}
		}
	}
	if(gmode || all) {
		PrintToConsole(client, "\nGamemodes...\n");
		for(int i; i < gModeName.Length; i++){
			ZGameMode mode = ZGameMode(i);
			char name[32];
			gModeName.GetString(i, name, 32);
			PrintToConsole(client, "ID: %d\tName: %s\nInfection: %b\tKill: %b\tRespawn: %b\tZombie Only: %b",
							mode.id, name, mode.bInfection, mode.bKill, mode.bRespawn, mode.bRespawnZombieOnly);
		}
	}
	if(zclass || all){
		PrintToConsole(client, "\nZombie Classes...\n");
		for(int i; i< gZClassName.Length; i++){
			ZClass class = ZClass(i);
			char names[32];
			char models[255];
			char arms[255];
			gZClassName.GetString(i, names, 32);
			gZClassModel.GetString(i, models, 255);
			gZClassArms.GetString(i, arms, 255);
			PrintToConsole(client, "Id: %d\nName: %s\nModel: %s\nArms: %s\nProperties:\nHp: %d\tSpeed: %f\tGravity: %f\n", i, names, models, arms,class.iHealth, class.flSpeed, class.flGravity);
		}
	}
	if(hclass || all){
		PrintToConsole(client, "\nHuman Classes...\n");
		for(int i; i< gHClassName.Length; i++){
			HClass class = HClass(i);
			char names[32];
			char models[255];
			char arms[255];
			gHClassName.GetString(i, names, 32);
			gHClassModel.GetString(i, models, 255);
			gHClassArms.GetString(i, arms, 255);
			PrintToConsole(client, "Id: %d\tName: %s\nModel: %s\nArms: %s\nHp: %d\tArmor: %d\tSpeed: %f\tGravity: %f\tLvl: %d\tReset: %d\n", i, names, models, arms, class.iHealth, class.iArmor, class.flSpeed, class.flGravity, class.iLevelReq, class.iResetReq);
		}
	}
	if(pweaps ||all) {
		PrintToConsole(client, "\nPrimary Weapons...\n");
		for(int i; i< primaryWeaponsName.Length; i++){
			char names[32];
			char ent[32];
			char model[255];
			primaryWeaponsName.GetString(i, names, 32);
			primaryWeaponsEnt.GetString(i, ent, 32);
			primaryWeaponsModel.GetString(i, model, 255);
			PrintToConsole(client, "Name: %s\tEnt: %s\tModel: %s", names, ent, model);
		}
	}
	if(sweaps || all) {
		PrintToConsole(client, "\nSecondary Weapons...\n");
		for(int i; i< secondaryWeaponsName.Length; i++){
			char names[32];
			char ent[32];
			char model[255];
			secondaryWeaponsName.GetString(i, names, 32);
			secondaryWeaponsEnt.GetString(i, ent, 32);
			secondaryWeaponsModel.GetString(i, model, 255);
			PrintToConsole(client, "Name: %s\tEnt: %s\tModel: %s", names, ent, model);
		}
	}
	if(gpacks || all){
		PrintToConsole(client, "\nGrenade Packs...\n");
		for(int i; i < gGrenadePackLevel.Length; i++){
			ZGrenadePack p = ZGrenadePack(i);
			PrintToConsole(client, "Id: %d\tLevel: %d\tReset: %d\tTypes:{%d,%d,%d,%d}\tCounts:{%d,%d,%d,%d}\thasGrenade:{%b,%b,%b,%b}\tgCount:{%d,%d,%d,%d}", i, p.iLevel, p.iReset, 
						p.iFirstGrenadeType, p.iSecondGrenadeType, p.iThirdGrenadeType, p.iFourthGrenadeType,
						p.iFirstGrenadeCount, p.iSecondGrenadeCount, p.iThirdGrenadeCount, p.iFourthGrenadeCount,
						p.hasGrenade(p.iFirstGrenadeType), p.hasGrenade(p.iSecondGrenadeType), p.hasGrenade(p.iThirdGrenadeType), p.hasGrenade(p.iFourthGrenadeType),
						p.getGrenadeCount(p.iFirstGrenadeType), p.getGrenadeCount(p.iSecondGrenadeType), p.getGrenadeCount(p.iThirdGrenadeType), p.getGrenadeCount(p.iFourthGrenadeType));
		}
	}
	if(party || all){
		PrintToConsole(client, "\nPartys...\n");
		for(int i; i < gPartys.Length; i++){
			ZParty p = ZParty(i);
			
			PrintToConsole(client, "id:%d\tUID:%d\tlength:%d\tavgLevel:%d\ttotalBoost:%f\t", i, p.iUID, p.length(), p.avgLevel(), p.getTotalBoost());
		}
	}
	
	PrintToConsole(client, "\nDebug ended...\n");
	return Plugin_Handled;
}
public Action adminMe(int args){
	char player[16];
	GetCmdArg(1, player, 16);
	for(int i=1; i < GetClientCount(); i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(i);
				p.bAdmin = true;
			}
		}
	}
	return Plugin_Handled;
}

//=====================================================
//					USER COMMANDS
//=====================================================
public Action infoCommands(int client, int args){
	ZPlayer player = ZPlayer(client);
	PrintToChat(player.id, "\x01 \x0B[INFO]\x01 Para \x09abrir el menú\x01: \x0Bbind z mainmenu\x01 en \x0Bconsola\x01.");
	PrintToChat(player.id, "\x01 \x0B[INFO]\x01 Para \x09utilizar la linterna\x01: \x0Bbind f flashlight\x01.");
	PrintToChat(player.id, "\x01 \x0B[INFO]\x01 Para \x09plantar minas\x01: presiona \x0BCTRL+E\x01.");
	PrintToChat(player.id, "\x01 \x0B[INFO]\x01 Para \x09quitar minas\x01: presiona \x0BCTRL+R\x01");
	PrintToChat(player.id, "\x01 \x0B[INFO]\x01 Para \x09apagar la nightvision\x01: \x0Bbind n nightvision\x01 en consola.");
	PrintToChat(player.id, "\x01 \x0B[INFO]\x01 Para \x09destrabarte\x01: \x0B!unstuck\x01 en chat.");
	PrintToChat(player.id, "\x01 \x0B[INFO]\x01 Recuerda revisar el menu de \x09Ajustes\x01 y las reglas para evitar problemas (\x0B!reglas\x01).");
	
	return Plugin_Handled;
}
public Action infoMode(int client, int args){
	ZPlayer player = ZPlayer(client);
	// Get mode name
	char name[32];
	gModeName.GetString(ActualMode.id, name, sizeof(name));
	
	PrintToChat(player.id, "%s Modo actual: \x09%s %s\x01.", SERVERSTRING, (ActualMode.id) ? name : "esperando modo", bForcedMode ? "(\x0FForzado\x01)" : "");
	return Plugin_Handled;
}
public Action time(int args){
	char times[32];
	FormatTime(times, 32, "%H:%M:%S", GetTime());
	PrintToServer("%s", times);
	return Plugin_Handled;
}
public void saveAll(){
	
	for(int i = 1; i <= MaxClients; i++){
		if (!IsPlayerExist(i))
			continue;
			
		ZPlayer player = ZPlayer(i);
		if(player.bLogged){
			saveCharacterData(player.id);
		}
	}
}
public Action saveAllcmd(int args){
	saveAll();
	PrintToServer("[DATABASE] Saving all users' data!");
	return Plugin_Handled;
}
public Action saveme(int client, int args){
	ZPlayer player = ZPlayer(client);
	if(IsPlayerExist(client) && player.bLogged) saveCharacterData(player.id);
	return Plugin_Handled;
}
public Action flashLight(int client, int args){
	ZPlayer player = ZPlayer(client);
	if(player.isType(PT_HUMAN) || player.isType(PT_GUNSLINGER) || player.isType(PT_SURVIVOR) || player.isType(PT_SNIPER)) player.bFlashlight = !player.bFlashlight;
	return Plugin_Handled;
}
public Action nightVision(int client, int args){
	ZPlayer player = ZPlayer(client);
	if (player.iTeamNum == CS_TEAM_T) player.iNightVision = !player.iNightVision;
	return Plugin_Handled;
}
public Action makemeHuman(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	char name[16];
	GetClientName(player.id, name, sizeof(name));
	
	if (!StrEqual(name, "Deco") && !StrEqual(name, "Codes"))
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id) && !player.isType(PT_HUMAN)){
		PrintHintText(player.id, "[ZP] You're a Human!");
		player.Humanize(HUMAN);
		player.giveWeapons();
	}
	return Plugin_Handled;
}
public Action infoVip(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	PrintToChat(player.id, "%s Está disponible la venta de \x09ganancias aumentadas\x01 (conocidas como VIP) hasta x7.", SERVERSTRING);
	PrintToChat(player.id, "%s El \x09VIP\x01 ofrece características adicionales a los usuarios que lo posean", SERVERSTRING);
	PrintToChat(player.id, "%s Para más información, buscanos en facebook \x09facebook.com/PiuBrks\x01.", SERVERSTRING);
	PrintToChat(player.id, "%s \x05VIPs\x01 disponibles: desde \x07x2\x01 hasta \x07x7\x01.", SERVERSTRING);
	
	PrintToChat(player.id, "%s Puedes probar \x05VIP x3\x01 de manera \x05gratuita\x01 por \x052\x01 días escribiendo \x05!pruebavip\x01!", SERVERSTRING);
	
	
	return Plugin_Handled;
}
public Action infoRules(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	PrintToChat(player.id, "%s Está \x09explícitamente PROHIBIDO:\x01", SERVERSTRING);
	PrintToChat(player.id, "%s \x091\x01: Dejarse matar", SERVERSTRING);
	PrintToChat(player.id, "%s \x092\x01: Insultar ofensivamente a otros usuarios", SERVERSTRING);
	PrintToChat(player.id, "%s \x093\x01: Pasar direcciones de otros servidores", SERVERSTRING);
	PrintToChat(player.id, "%s \x094\x01: Abusar del chat de texto o voz", SERVERSTRING);
	PrintToChat(player.id, "%s \x095\x01: No atacar", SERVERSTRING);
	PrintToChat(player.id, "%s \x096\x01: Invitar a los usuarios a jugar fuera del servidor", SERVERSTRING);
	PrintToChat(player.id, " \x09------------------------------------------------\x01");
	PrintToChat(player.id, "%s \x09No cumplir con estas reglas es motivo\x01 \x02PENALIZABLE\x01", SERVERSTRING);
	
	PrintToConsole(player.id, "%s Está EXPLÍCITAMENTE PROHIBIDO:", SERVERSTRING);
	PrintToConsole(player.id, "%s 1: Dejarse matar", SERVERSTRING);
	PrintToConsole(player.id, "%s 2: Insultar ofensivamente a otros usuarios", SERVERSTRING);
	PrintToConsole(player.id, "%s 3: Pasar direcciones de otros servidores", SERVERSTRING);
	PrintToConsole(player.id, "%s 4: Abusar del chat de texto o voz", SERVERSTRING);
	PrintToConsole(player.id, "%s 5: No atacar", SERVERSTRING);
	PrintToConsole(player.id, "%s 6: Invitar a los usuarios a jugar fuera del servidor", SERVERSTRING);
	PrintToConsole(player.id, "------------------------------------------------");
	PrintToConsole(player.id, "%s No cumplir con estas reglas es motivo PENALIZABLE", SERVERSTRING);
	
	return Plugin_Handled;
}
public Action infoDiscord(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	PrintToChat(player.id, "%s Ingresa al siguiente enlace para ingresar a nuestro servidor de \x09Discord\x01! (\x09https://discord.gg/TAX3JDw\x01).", SERVERSTRING);	
	PrintToChat(player.id, "%s Este mensaje también será enviado a tu consola para que puedas copiar y pegar.", SERVERSTRING);
	
	PrintToConsole(player.id, "[PIU] El enlace para ingresar al servidor de Discord es https://discord.gg/TAX3JDw");
	PrintToConsole(player.id, "[PIU] TE ESPERAMOS!");
}

// Extra items binds
public Action buyItems(int client, int args){
	
	ZPlayer player = ZPlayer(client);
	char sBuffer[16];
	GetCmdArg(0, sBuffer, sizeof(sBuffer));
	
	if (StrEqual(sBuffer, "item1")){
		buyExtraItem(player.id, 0);
	}
	else if (StrEqual(sBuffer, "item2")){
		buyExtraItem(player.id, 1);
	}
	else if (StrEqual(sBuffer, "item3")){
		buyExtraItem(player.id, 2);
	}
	
	return Plugin_Handled;
}

// Unstuck command
public Action unStuck(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!IsPlayerExist(player.id))
		return Plugin_Handled;
		
	if (!player.bAlive)
		return Plugin_Handled;
	
	// Initialize vector variables
	float flOrigin[3];
	float flMax[3]; 
	float flMin[3]; 

	// Get client's location
	GetClientAbsOrigin(player.id, flOrigin);
	
	// Get the client's min and max size vector
	GetClientMins(player.id, flMin);
	GetClientMaxs(player.id, flMax);

	// Starts up a new trace hull using a global trace result and a customized trace ray filter
	TR_TraceHullFilter(flOrigin, flOrigin, flMin, flMax, MASK_SOLID, FilterStuck, player.id);

	// Returns if there was any kind of collision along the trace ray
	if(TR_DidHit()){
	
		int entity = TR_GetEntityIndex();
		
		if (!IsValidEntity(entity))
			return Plugin_Handled;
		
		if (IsEntityLasermine(entity)){
			PrintToChat(player.id, "%s No estás trabado.", SERVERSTRING);
			return Plugin_Handled;
		}
		
		char sEnt[32];
		GetEntityClassname(entity, sEnt, sizeof(sEnt));
		if (StrContains(sEnt, "_projectile")  != -1 || StrContains(sEnt, "weapon")  != -1){
			PrintToChat(player.id, "%s No estás trabado.", SERVERSTRING);
			return Plugin_Handled;
		}
		
		if (flOrigin[0] != gfSpawnOrigins[player.id][0] && flOrigin[1] != gfSpawnOrigins[player.id][1])
			TeleportEntity(player.id, gfSpawnOrigins[player.id], NULL_VECTOR, NULL_VECTOR);
		else{
			/*float iVec[3];
			iVec = gfSpawnOrigins[player.id];
			iVec[0] += GetRandomFloat(25.0, 80.0);
			iVec[1] += GetRandomFloat(25.0, 80.0);
			iVec[2] += GetRandomFloat(10.0, 30.0);*/
			
			int ent;
			while((ent = FindEntityByClassname(ent, "info_player_terrorist")) != -1){
				float coords[3];
				GetEntPropVector(ent, Prop_Data, "m_vecOrigin", coords);
				TeleportEntity(player.id, coords, NULL_VECTOR, NULL_VECTOR);
				break;
			}
		}
	}
	else
		PrintToChat(player.id, "%s No estás trabado.", SERVERSTRING);
	
	return Plugin_Handled;
}
public bool FilterStuck(int client, int contentsMask, any victim){
    return (client != victim);
}

//=====================================================
//					STAFF COMMANDS
//=====================================================
public Action makemeSurvivor(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id)){
		PrintHintText(player.id, "[ZP] You're a SURVIVOR!");
		player.Humanize(SURVIVOR);
	}
	return Plugin_Handled;
}
public Action makemeGunslinger(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id)){
		PrintHintText(player.id, "[ZP] You're a GUNSLINGER!");
		player.Humanize(GUNSLINGER);
	}
	return Plugin_Handled;
}
public Action makemeSniper(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id)){
		PrintHintText(player.id, "[STAFF] You're a SNIPER!");
		player.Humanize(SNIPER);
	}
	return Plugin_Handled;
}
public Action makemeZombie(int client, int args){
	ZPlayer player = ZPlayer(client);
	if (!player.bStaff)
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id) && !player.isType(PT_ZOMBIE)){
		PrintHintText(player.id, "[ZP] You're a Zombie!");
		player.Zombiefy(ZOMBIE);
	}
	return Plugin_Handled;
}
public Action makemeNemesis(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id)){
		PrintHintText(player.id, "[ZP] You're a NEMESIS!");
		player.Zombiefy(NEMESIS);
	}
	return Plugin_Handled;
}
public Action makemeAssassin(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id)){
		PrintHintText(player.id, "[STAFF] You're a ASSASSIN!");
		player.Zombiefy(ASSASSIN);
	}
	return Plugin_Handled;
}
public Action givemenades(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	if(IsPlayerAlive(player.id)){
		GivePlayerItem(player.id, "weapon_hegrenade");
		GivePlayerItem(player.id, "weapon_flashbang");
		GivePlayerItem(player.id, "weapon_smokegrenade");
	}
		
	return Plugin_Handled;
}
public Action AimInfect(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	int targetIndex = GetClientAimTarget(player.id, true);
	ZPlayer target = ZPlayer(targetIndex);
	if (!IsPlayerExist(target.id) || !IsPlayerAlive(target.id))
		return Plugin_Handled;
	
	target.Zombiefy(ZOMBIE, false);
	return Plugin_Handled;
}
public Action lightLevel(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	ServerCommand("sv_skyname sky_csgo_night02b");
	SetLightStyle(0, "b");
	
	return Plugin_Handled;
}
public Action valveFog(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
	
	static bool bFog = true;
	
	AcceptEntityInput(iFog, bFog ? "TurnOff" : "TurnOn");
	bFog = !bFog;
	
	return Plugin_Handled;
}
public Action lightSwitch(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff)
		return Plugin_Handled;
		
	static bool setting;
	int ent = -1;
	while((ent = FindEntityByClassname(ent, "light"))!=-1){
		AcceptEntityInput(ent, (setting) ? "TurnOn" : "TurnOff");
		PrintToChat(player.id, "%b", setting);
		setting = !setting;
		PrintToChat(player.id, "%b", setting);
		break;
	}
	return Plugin_Handled;
}
public Action staffzpoints(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	char player[16];
	char cPoints[16];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, cPoints, 16);
	int points = StringToInt(cPoints);
	for(int i=1; i < MAXPLAYERS; i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player) && IsPlayerExist(i)){
				ZPlayer p = ZPlayer(i);
				p.iZPoints += points;
			}
		}
	}
	return Plugin_Handled;
}
public Action staffbanchat(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	char player[16];
	GetCmdArg(1, player, 16);
	for(int i=1; i < MAXPLAYERS; i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(i);
				p.bChatBanned = !p.bChatBanned;
				PrintToChatAll("%s Se %s el chat de \x0B%s\x01.", SERVERSTRING, p.bChatBanned ? "baneo" : "desbaneo", name);
			}
		}
	}
	return Plugin_Handled;
}
public Action staffbanchatvoice(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	char player[16];
	GetCmdArg(1, player, 16);
	for(int i=1; i < MAXPLAYERS; i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(i);
				p.bChatVoiceBanned = !p.bChatVoiceBanned;
				PrintToChatAll("%s Se %s el chatvoice de \x0B%s\x01.", SERVERSTRING, p.bChatVoiceBanned ? "baneo" : "desbaneo", name);
			}
		}
	}
	return Plugin_Handled;
}
public Action staffexp(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	char player[16];
	char cPoints[16];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, cPoints, 16);
	int points = StringToInt(cPoints);
	for(int i=1; i < GetClientCount(); i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(i);
				p.iExp += points;
			}
		}
	}
	return Plugin_Handled;
}
public Action staffmake(int client, int args){
	ZPlayer PIBE = ZPlayer(client);
	
	if (!PIBE.bStaff)
		return Plugin_Handled;
	
	char player[16];
	char type[16];
	char what[32];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, type, 16);
	for(int i; i < Players.Length; i++){
		int id = Players.Get(i);
		if(IsPlayerExist(id)){
			char name[32];
			GetClientName(id,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(id);
				if(StrEqual(type, "human")){
					p.Humanize(HUMAN);
					p.giveWeapons();
					Format(what, 32, "Humano");
				}
				else if(StrEqual(type, "zombie")){
					p.Zombiefy(ZOMBIE);
					Format(what, 32, "Zombie");
					GivePlayerItem(p.id, "weapon_flashbang");
				}
				else if(StrEqual(type, "gunslinger")){
					p.Humanize(GUNSLINGER);
					Format(what, 32, "Gunslinger");
				}
				else if(StrEqual(type, "sniper")){
					p.Humanize(SNIPER);
					Format(what, 32, "Sniper");
				}
				else if(StrEqual(type, "survivor")){
					p.Humanize(SURVIVOR);
					Format(what, 32, "Survivor");
				}
				else if(StrEqual(type, "nemesis")){
					p.Zombiefy(NEMESIS);
					Format(what, 32, "Nemesis");
				}
				else if(StrEqual(type, "assassin")){
					p.Zombiefy(ASSASSIN);
					Format(what, 32, "Assassin");
				}
				else if(StrEqual(type, "alive")){
					p.iRespawnPlayer();
					Format(what, 32, "Vivo");
				}
				else if(StrEqual(type, "invulnerable")){
					p.bInvulnerable = !p.bInvulnerable;
					Format(what, 32, "Invulnerable");
				}
				else{
					PrintToServer("%s no encontrado", type);
					return Plugin_Handled;
				}
				PrintToChatAll("Se transformo a %s en %s", name, what);
			}
		}		
	}
	return Plugin_Handled;
}
public Action staffhpoints(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	char player[16];
	char cPoints[16];
	GetCmdArg(1, player, 16);
	GetCmdArg(2, cPoints, 16);
	int points = StringToInt(cPoints);
	for(int i=1; i < MAXPLAYERS; i++){
		if(IsPlayerExist(i)){
			char name[32];
			GetClientName(i,name, 32);
			if(StrEqual(name, player)){
				ZPlayer p = ZPlayer(i);
				p.iHPoints += points;
				PrintToChatAll("Se dieron %d puntos humanos a %s", points, name);
			}
		}
		
	}
	return Plugin_Handled;
}
public Action eventMode(int client, int args){

	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	eventmode = true;
	//StartMode(view_as<int>());
		
	ScreenFadeAll(RoundToNearest(0.6 * 1000.0), RoundToNearest(0.6 * 1000.0), FFADE_OUT, { 0, 0, 0, 255 });
	CreateTimer(1.3, ScreenFadeIn);
		
	// Turn on fog
	//switchFog(true);
	
	EmitSoundToAll(MODE_NEMESIS_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
	EmitSoundToAll(AMBIENT_SOUND_ZBOSS, SOUND_FROM_PLAYER, SNDCHAN_STATIC,_,_, 0.2);
	
	newRound = false;
	endRound = false;
	startedRound = true;
	
	for (int i = 1; i <= MaxClients; i++){
	
		if (!IsPlayerExist(i, true))
			continue;
		
		ZPlayer user = ZPlayer(i);
		
		if (user.bStaff){
			user.Zombiefy(NEMESIS, true);
			user.iHp *= 20;
			VEffectSpawnEffect(user.id);
		}
		else user.Humanize(SURVIVOR);
		
		PrintToChatAll("%s \x09MODO DE EVENTO: HYPERNEMESIS\x01.", SERVERSTRING);
	}
	
	iCounter = ROUND_START_COUNTDOWN;
	delete hCounter;
	
	CreateTimer(15.0, TurnZombiesIntoNemesis, INVALID_HANDLE, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
	PrintToChatAll("%s \x09TODOS LOS ZOMBIES SERÁN TRANSFORMADOS EN NEMESIS CADA 15 SEGUNDOS\x01.", SERVERSTRING);
	
	return Plugin_Handled;
}
public Action TurnZombiesIntoNemesis(Handle hTimer){
	if (!eventmode)
		return Plugin_Stop;
	
	for (int i = 1; i <= MaxClients; i++){
		ZPlayer user = ZPlayer(i);
		
		if (!IsPlayerExist(user.id, true))
			continue;
		
		if (user.isType(PT_ZOMBIE))
			user.Zombiefy(NEMESIS);
	}
	
	PrintToChatAll("%s \x09TODOS LOS ZOMBIES TRANSFORMADOS EN NEMESIS\x01.", SERVERSTRING);
	
	return Plugin_Handled;
}
public Action eventMode2(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	eventmode = true;
	
	ScreenFadeAll(RoundToNearest(0.6 * 1000.0), RoundToNearest(0.6 * 1000.0), FFADE_OUT, { 0, 0, 0, 255 });
	CreateTimer(1.3, ScreenFadeIn);
		
	// Turn on fog
	//switchFog(true);
	
	EmitSoundToAll(MODE_NEMESIS_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
	EmitSoundToAll(AMBIENT_SOUND_ZBOSS, SOUND_FROM_PLAYER, SNDCHAN_STATIC,_,_, 0.2);
	
	newRound = false;
	endRound = false;
	startedRound = true;
	
	
	for (int i = 1; i <= MaxClients; i++){
		ZPlayer user = ZPlayer(i);
		
		if (!IsPlayerExist(user.id, true))
			continue;
		
		if (user.bStaff){
			user.Zombiefy(ZOMBIE, true);
			user.iHp *= 20;
			//VEffectSpawnEffect(user.id);
			SetEntityRenderMode(user.id, RENDER_TRANSCOLOR);
			SetEntityRenderColor(user.id, 0, 120, 255, 35);
		}
		
		PrintToChatAll("%s \x09MODO DE EVENTO: PANDEMIA\x01.", SERVERSTRING);
	}
	
	
	
	iCounter = ROUND_START_COUNTDOWN;
	delete hCounter;
	
	CreateTimer(15.0, TransformRandomIntoZombie, INVALID_HANDLE, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
	PrintToChatAll("%s \x09TRANSFORMACIONES AL AZAR CADA 15 SEGUNDOS\x01.", SERVERSTRING);
	
	CreateTimer(30.0, TurnZombiesIntoNemesis, INVALID_HANDLE, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
	
	return Plugin_Handled;
}
public Action TransformRandomIntoZombie(Handle hTimer){
	if (!eventmode)
		return Plugin_Stop;
	
	int nAlive = fnGetAlive();
	ZPlayer player = ZPlayer(fnGetRandomAlive(GetRandomInt(1, nAlive)));
	player.Zombiefy(ZOMBIE, false);
	
	PrintToChatAll("%s \x09MUTACIÓN REALIZADA\x01.", SERVERSTRING);
	
	return Plugin_Handled;
}

//=====================================================
//					BLOCK COMMANDS
//=====================================================
public Action BlockCommand(int client, const char[] command, int args){
	ZPlayer player = ZPlayer(client);
	
	PrintToConsole(player.id, "Comando %s no permitido.", command);
	return Plugin_Handled;
}
public Action OnNameChange(Handle event, const char[] name, bool dontBroadcast){
	int userId = GetEventInt(event, "userid");
	int client = GetClientOfUserId(userId);
	ZPlayer player = ZPlayer(client);

	if (IsClientInGame(player.id) && !IsFakeClient(player.id)){
		if (player.bCanChangeName){
			player.bCanChangeName = false;
			return Plugin_Continue;
		}
	}
	
	PrintToConsole(player.id, "No se puede realizar un cambio de nombre ahora.");
	return Plugin_Changed;
}
public Action EventPlayerGivenC4(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	return Plugin_Handled;
}

//=====================================================
//					ROUND EVENTS
//=====================================================
void BalanceTeams(){
	int nPlayers = fnGetPlaying();

	if (!nPlayers){
		return;
	}

	for (int i = 1; i <= MaxClients; i++){
		ZPlayer player = ZPlayer(i);
		
		if(!player.bInGame) continue;
		if(!IsPlayerExist(player.id, false))
			continue;
		
		player.iTeamNum = !(i % 2) ? CS_TEAM_CT : CS_TEAM_T;
	}
}
public Action EventPreRoundStart(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	newRound = true;
	endRound = false;
	startedRound = false;
	ActualMode = ZGameMode(view_as<int>(NO_MODE));
	
	for (int client = 1; client <= MaxClients; client++){
		ZPlayer player = ZPlayer(client);
		
		if (player.hFreezeTimer != INVALID_HANDLE){
			KillTimer(player.hFreezeTimer);
			player.hFreezeTimer = INVALID_HANDLE;
		}
	}
	
	/*printVipSay(100, "Se actualizó la fórmula ganancia de AP y de requisitos de nivel.");
	printVipSay(100, "Éstos fueron facilitados aunque los niveles hayan bajado!");*/
	
	BalanceTeams();
}
public Action EventPostRoundStart(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	
	// Finish warmup
	if (GameRules_GetProp("m_bWarmupPeriod") == 1){
		ServerCommand("mp_warmup_end");
		CS_TerminateRound(0.1, CSRoundEnd_GameStart);
		PrintToChatAll("%s \x09INICIANDO PARTIDA\x01.", SERVERSTRING);
		bWarmupEnded = true;
	}
    
	LocateMercenary();
	
	// Happy hour check
	PrintToServer("Checking happy hour");
	gHappyHour = isHappyHourTime();
	gAfterHour = isAfterHourTime();
	happyHour(0, 0);
	
	// Print some info
	SendAnnouncement();
	
	for(int i = 1; i <= MaxClients; i++){
		ZPlayer player = ZPlayer(i);
		if(!IsPlayerExist(i, true))
			continue;
		
		#if defined USE_OVERLAYS
		ToolsOverlays(i, OVERLAY_RESET);
		#endif
		
		player.resetExtraItems();
		if(player.bInGame) player.Humanize(HUMAN);
	}
	
	if(ActualMode.id == view_as<int>(NO_MODE)){
		if (hCounter != null){
			delete hCounter;
			hCounter = null;
			iCounter = ROUND_START_COUNTDOWN;
		}
		hCounter = CreateTimer(1.0, TimerShowCountdown, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
	
	if (iRounds >= ROUNDS_TO_END_MAP)
		StartMapVote();
}
public Action EventRoundEnd(Event gEventHook, const char[] gEventName, bool dontBroadcast){

	// Save everyone's data
	saveAll();
	
	// Clean aura shields array
	AuraShields.Clear();

	// If ended mode is assassin mode, turn off fog
	if (ActualMode == ZGameMode(view_as<int>(MODE_ASSASSIN)) || ActualMode == ZGameMode(view_as<int>(MODE_APOCALYPSIS)))
		switchFog(false);
		
	// Reset actual gamemode
	ActualMode = ZGameMode(view_as<int>(NO_MODE));
	
	// Refresh booleans
	newRound = false;
	endRound = true;
	startedRound = false;
	eventmode = false;
	
	// If the counter is still alive, kill it
	if(hCounter != null){
		delete hCounter;
		hCounter = null;
		iCounter = ROUND_START_COUNTDOWN;
	}
	
	// Load overlays
	int reason = gEventHook.GetInt("reason");
	for(int i = 1; i < MAXPLAYERS+1; i++){
		ZPlayer player = ZPlayer(i);
		if(IsPlayerExist(player.id)){
			player.resetExtraItems();

			if(reason == 9){
				#if defined USE_OVERLAYS
				ToolsOverlays(i, OVERLAY_ZOMBIEWIN);
				#endif
				
				PrintHintTextToAll("<font size='22' color='#XXXXXX'>Los</font><font size='37' color='#FF0000'> ZOMBIES </font><font size='22' color='#XXXXXX'>han dominado!</font>");
			}
			else if(reason == 8){
				#if defined USE_OVERLAYS
				ToolsOverlays(i, OVERLAY_HUMANWIN);
				#endif
				
				PrintHintTextToAll("<font size='22' color='#XXXXXX'>Los</font><font size='37' color='#FF0000'> HUMANOS </font><font size='22' color='#XXXXXX'>han sobrevivido!</font>");
			}
		}
		if (IsPlayerExist(player.id))
			player.bFlashlight = false;
		
		//UnhookGlow(player.id);
	}
	
	// Add ended rounds number
	iRounds = iRounds+1;
	PrintToServer("Ronda %d", iRounds);
	
	// Show screenfade
	if (bWarmupEnded || iRounds > 1){
		
		ScreenFadeAll(RoundToNearest(2.4 * 1000.0), RoundToNearest(2.4 * 1000.0), FFADE_OUT, { 0, 0, 0, 255 });
		CreateTimer(4.9, ScreenFadeIn);
	}
	
	return Plugin_Continue;
}
public Action CS_OnTerminateRound(float &delay, CSRoundEndReason &reason){
	
	// Save everyone's data
	saveAll();
	
	return Plugin_Continue;
}
public Action EventBlockBroadCast(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	if (!dontBroadcast) {
		SetEventBroadcast(gEventHook, true);
	}
}

// Server announcements
stock void SendAnnouncement(){
	// Initialize randomization
	int number = GetRandomInt(0, 15);
	
	// Initialize info buffer
	char sBuffer[128];
	
	switch (number){
		case 0: FormatEx(sBuffer, sizeof(sBuffer), "%s ¿Te gustaría poder elegir los modos y contar con mejores ganancias? Para más información escribe \x09!vip\x01 en el chat.", SERVERSTRING);
		case 1: FormatEx(sBuffer, sizeof(sBuffer), "%s \x09No infectar\x01 o \x09dejar que tus enemigos\x01 saquen provecho de manera intencional es \x02PENALIZABLE\x01!", SERVERSTRING);
		case 2: FormatEx(sBuffer, sizeof(sBuffer), "%s Aprovecharse de bugs o errores es \x02PENALIZABLE\x01 en caso de queja, \x09no olvides reportarlos al\x01 \x0FSTAFF\x01!", SERVERSTRING);
		case 3:{
			FormatEx(sBuffer, sizeof(sBuffer), "%s El servidor cuenta con \x09HAPPY HOURS\x01 (de \x09%d\x01 a \x09%d\x01) y \x09AFTER HOURS\x01 (de \x09%d\x01 a \x09%d\x01)", SERVERSTRING, 
			HH_START, HH_END, AH_START, AH_END);
			PrintToChatAll("%s \x09Happy Hours\x01: Ganancias aumentadas \x092\x01 veces.", SERVERSTRING);
			PrintToChatAll("%s \x09After Hours\x01: Ganancias aumentadas \x093\x01 veces.", SERVERSTRING);
		}
		case 4: FormatEx(sBuffer, sizeof(sBuffer), "%s No olvides revisar las opciones del \x09menú\x01 y el \x09comando de ayuda\x01! (\x05!mainmenu\x01 y \x09!ayuda\x01 en el chat)", SERVERSTRING);
		//case 5: FormatEx(sBuffer, sizeof(sBuffer), "%s Recuerda que a partir del\x09 1º de diciembre \x01 y hasta el\x09 15 de diciembre\x01! estan disponibles las \04Ofertas de verano\x01. \x09Aprovechalas!\x01.", SERVERSTRING);
		case 5: FormatEx(sBuffer, sizeof(sBuffer), "%s Puedes ingresar a nuestro server de \x09Discord\x01! (\x09https://discord.gg/TAX3JDw\x01)", SERVERSTRING);
		case 6: FormatEx(sBuffer, sizeof(sBuffer), "%s Ya conoces nuestro servidor \x0BCasual\x01?\nEntra a \x09cs.piu-games.com:27017\x01 y conocelo!", SERVERSTRING);
		case 7: FormatEx(sBuffer, sizeof(sBuffer), "%s Ya conoces nuestro servidor \x0BJailbreak\x01?\nEntra a \x09cs.piu-games.com:27018\x01 y conocelo!", SERVERSTRING);
		case 8: FormatEx(sBuffer, sizeof(sBuffer), "%s Ya estas registrado en nuestro foro? Enterate de todas las novedades de la comunidad!\nURL: \x09foro.piu-games.com\x01.", SERVERSTRING);
		case 9: FormatEx(sBuffer, sizeof(sBuffer), "%s Ya estas registrado en nuestro foro? Enterate de todas las novedades de la comunidad!\nURL: \x09foro.piu-games.com\x01.", SERVERSTRING);
		case 10: FormatEx(sBuffer, sizeof(sBuffer), "%s Sabias que el \x09VIP x7\x01 puede ganar hasta \x09puntos X3\x01?", SERVERSTRING);
		case 11: FormatEx(sBuffer, sizeof(sBuffer), "%s Seguinos en \x0Bwww.facebook.com/PiuBrks\x01!.", SERVERSTRING);
		case 12: FormatEx(sBuffer, sizeof(sBuffer), "%s Sabes hacer algo que crees que aporte a la comunidad?\nComunicate con los \x0FSTAFF\x01 para conversarlo!", SERVERSTRING);
		case 13: FormatEx(sBuffer, sizeof(sBuffer), "%s Por la compra de mas de un mes de cualquier \x09VIP\x01 tenes descuentos! Consultalos en nuestra pagina de \x0BFacebook\x01!", SERVERSTRING);
		case 14: FormatEx(sBuffer, sizeof(sBuffer), "%s El comando \x09!estadisticas\x01 para ver las estadisticas de tu personaje!", SERVERSTRING);
		case 15: FormatEx(sBuffer, sizeof(sBuffer), "%s Los \x09logros\x01 se entregan en el hombre brillante a cambio de \x09PUNTOS\x01!!", SERVERSTRING);
	
	}
	
	// Print the announcement
	PrintToChatAll(sBuffer);
	
	for (int i = 1; i <= MaxClients; i++){
		if (IsPlayerExist(i))
			PrintToConsole(i, sBuffer);
	}
}

// Used to announce and start votemap
public void StartMapVote(){

	InitiateMapChooserVote(MapChange_RoundEnd);
	PrintToChatAll("%s Última ronda, iniciando votación de mapas.", SERVERSTRING);
}

//=====================================================
//					PLAYER HOOKS
//=====================================================
public Action EventPrePlayerSpawn(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	ZPlayer player = ZPlayer(GetClientOfUserId(gEventHook.GetInt("userid")));
	
	if(!player.bInGame || !player.bLogged) return Plugin_Handled;
	
	if (player.isType(PT_HUMAN))
		player.bBoughtWeapons = false;
	
	return Plugin_Continue;
}
public Action EventPostPlayerSpawn(Handle event, const char[] name, bool dontBroadcast){
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	ZPlayer player =  ZPlayer(client);

	// He doesnt exist, doesn't matter
	if (!IsPlayerExist(player.id))
		return Plugin_Handled;
	
	// If BOT, randomize vars
	if(IsFakeClient(client)){
		char nam[32];
		GetClientName(client, nam, 32);
		player.iHumanClass = GetRandomInt(0, 11);
		player.iZombieClass = GetRandomInt(0, 7);
		player.iWeaponPrimary = GetRandomInt(0, 23);
		player.iWeaponSecondary = GetRandomInt(0, 8);
		player.iZDamageLevel = GetRandomInt(1, MAX_ZOMBIE_DAMAGE_LEVEL);
		player.iZHealthLevel = GetRandomInt(1, MAX_ZOMBIE_HEALTH_LEVEL);
		player.iZDexterityLevel = GetRandomInt(1, MAX_ZOMBIE_DEXTERITY_LEVEL);
		player.iZResistanceLevel = GetRandomInt(1, MAX_ZOMBIE_RESISTANCE_LEVEL);
		player.iHDamageLevel = GetRandomInt(1, MAX_HUMAN_DAMAGE_LEVEL);
		player.iHResistanceLevel = GetRandomInt(1, MAX_HUMAN_RESISTANCE_LEVEL);
		player.iHDexterityLevel = GetRandomInt(1, MAX_HUMAN_DEXTERITY_LEVEL);
		player.iHPenetrationLevel = GetRandomInt(1, MAX_HUMAN_PENETRATION_LEVEL);
	}
	
	// If respawned and there is no active gamemode
	if(newRound){
		player.Humanize(HUMAN);
		player.removeWeapons();
		/*ZWeaponPrimary weapon1 = ZWeaponPrimary(player.iWeaponPrimary);
		ZWeaponSecondary weapon2 = ZWeaponSecondary(player.iWeaponSecondary);
		if(player.iLevel >= weapon1.iLevel && player.iReset >= weapon1.iReset && player.iLevel >= weapon2.iLevel && player.iReset >= weapon2.iReset){*/
		player.giveWeapons();
		/*}
		else{
			PrintToChat(player.id, "%s Bajaste de nivel y no puedes usar una de tus armas seleccionadas\x01.", SERVERSTRING);
		}*/
	}
	
	// Cache important variables
	player.iPredictedViewModelIndex = Weapon_GetViewModelIndex(player.id, -1);
	
	// Store client's spawn origins
	GetClientAbsOrigin(player.id, gfSpawnOrigins[player.id]);
	
	// Remove any existing lasermine and give him the default value
	RemoveLasermines(player.id);
	
	return Plugin_Continue;
}
public Action EventPlayerDeath(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	ZPlayer victim = ZPlayer(GetClientOfUserId(gEventHook.GetInt("userid")));
	ZPlayer attacker = ZPlayer(GetClientOfUserId(gEventHook.GetInt("attacker")));
	bool head = gEventHook.GetBool("headshot");
	
	if (!IsPlayerExist(victim.id))
		return Plugin_Handled;
		
	if(victim.id == attacker.id)
		return Plugin_Handled;
	
	// Turn off his flashlight
	victim.bFlashlight = false;
	
	// If simulated death event
	if(victim.bRecentlyInfected){
		victim.bRecentlyInfected = false;
		return Plugin_Handled;
	}
	
	// If gamemode allows respawn
	if (ActualMode.bRespawn) CreateTimer(2.0, respawnPlayer, victim);
	
	// If client commited suicide
	if(victim.id == attacker.id) return Plugin_Handled;
	
	switch (attacker.iType){
		case PT_ZOMBIE, PT_NEMESIS, PT_ASSASSIN:{
				if(victim.isType(PT_HUMAN)) attacker.iHKills++;
				else if(victim.isType(PT_SURVIVOR)) attacker.iSurviKills++;
				else if(victim.isType(PT_GUNSLINGER)) attacker.iGunsKills++;
				else if(victim.isType(PT_SNIPER)) attacker.iSnipKills++;
				
				int iRest = NextLevel(attacker.iLevel+1, attacker.iReset)-attacker.iExp;
				int iTotal =  attacker.calculateGain(RoundToNearest(iRest*INFECTION_PERCENTAGE_REWARD+((victim.iLevel*victim.iReset)/15))+INFECTION_BASE_REWARD);
				
				if (attacker.iLevel < RESET_LEVEL && bAllowGain)
					attacker.iExp += iTotal;
				
				PrintHintText(attacker.id, "<font size='37' color='#FF0000'>ASESINATO!</font>\nAmmopacks: <font color='#ff0000'>%d</font>", iTotal);
			
			}
		case PT_HUMAN, PT_SURVIVOR, PT_GUNSLINGER, PT_SNIPER:{
			if(victim.isType(PT_ZOMBIE)) attacker.iZKills++;
			else if(victim.isType(PT_NEMESIS)) attacker.iNemeKills++;
			else if(victim.isType(PT_ASSASSIN)) attacker.iAssaKills++;
			
			if(head) attacker.iHeadshots++;
		}
	}
	
	// If the victim is a boss, give bonus gold
	if(victim.isType(PT_NEMESIS) || victim.isType(PT_ASSASSIN) || victim.isType(PT_SURVIVOR) || victim.isType(PT_GUNSLINGER) || victim.isType(PT_SNIPER)){
		EmitDeathSound(victim.id);
	}
	else if(victim.isType(PT_ZOMBIE)){
		EmitDeathSound(victim.id);
	}
	
	// Freeze grenade timer's reset
	if (IsClientInGame(victim.id))
		ExtinguishEntity(victim.id);
	
	if (victim.hFreezeTimer != INVALID_HANDLE){
		KillTimer(victim.hFreezeTimer);
		victim.hFreezeTimer = INVALID_HANDLE;
	}
	
	/*// Let's deploy this shit
	int rand = GetRandomInt(0, 100);
	if(rand >= 100-CHEST_DROP_RATIO){
		float origin[3];
		GetEntPropVector(victim.id, Prop_Send, "m_vecOrigin", origin);
		int chestId = CreateChest(attacker.id, origin);
		CreateTimer(CHEST_TIME_DISAPPEAR, DeleteChest, EntIndexToEntRef(chestId), TIMER_FLAG_NO_MAPCHANGE);
	}*/
	
	if (victim.iTeamNum == CS_TEAM_T)
		RagdollOnClientDeath(victim.id);
	
	return Plugin_Continue;
}
public Action EventWeaponReload(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	ZPlayer player = ZPlayer(GetClientOfUserId(gEventHook.GetInt("userId")));
	if(!IsPlayerExist(player.id))
		return Plugin_Handled;

	int weapon = GetEntPropEnt(player.id, Prop_Data, "m_hActiveWeapon");
	
	SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 200);
	return Plugin_Continue;
}
public Action EventWeaponFire(Event gEventHook, const char[] gEventName, bool dontBroadcast){
	ZPlayer player = ZPlayer(GetClientOfUserId(gEventHook.GetInt("userId")));
	
	if(!IsClientConnected(player.id))
		return Plugin_Handled;
	
	int weapon = GetEntPropEnt(player.id, Prop_Data, "m_hActiveWeapon");
	int ammo = GetEntProp(weapon, Prop_Data, "m_iClip1");
	if(player.bInfiniteAmmo){
		
		SetEntProp(weapon, Prop_Send, "m_iClip1", ammo+1);
	}
	if(ammo <= 1){
		SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 200);
	}
	
	return Plugin_Handled;
}

//=====================================================
//					HUD FUNCTIONS
//=====================================================
public Action PrintMsg(Handle timer, int client){
	ZPlayer player = ZPlayer(client);
	if(!IsPlayerExist(client) || IsFakeClient(client))
		return Plugin_Stop;
		
	if(!player.bInGame)
		return Plugin_Handled;

	// Hide CSGO HUD
	hideHUD(player.id);
	
	// Is our player spectating someone?
	int iSpecMode = GetEntProp(player.id, Prop_Send, "m_iObserverMode");
	
	// Initialize message string
	char msg[2048];
	
	// Parse data in both cases
	if (player.bAlive){
		// Format the HUD
		formatHUD(player.id, msg);
	}
	else if (iSpecMode == SPECMODE_FIRSTPERSON || iSpecMode == SPECMODE_3RDPERSON){
		int iTarget = GetEntPropEnt(player.id, Prop_Send, "m_hObserverTarget");
		
		if (!IsPlayerExist(iTarget, true))
			return Plugin_Handled;
		
		ZPlayer target = ZPlayer(iTarget);
		
		formatHUD(target.id, msg, true);
	}
	
	// Show hud message
	SetHudTextParams(0.02, 0.02, 0.6, 102, 178, 255, 255, 1, 0.2, 0.8, 0.4);
	ShowSyncHudText(player.id, hHudSynchronizer, msg);
	
	return Plugin_Continue;
}
void formatHUD(int client, char buffer[2048], bool bSpec = false){
	ZPlayer player = ZPlayer(client);
	
	// Parse variables
	char sPointed1[15];
	char sPointed2[15];
	char sPointed3[24];
	char sPointed5[24];
	int restantes = (NextLevel(player.iLevel+1, player.iReset)-player.iExp);
	if(restantes < 0 || player.iLevel >= RESET_LEVEL) restantes = 0;
	// Add "." to the numbers
	AddPoints(player.iHp, sPointed1, sizeof(sPointed1));
	AddPoints(player.iArmor, sPointed5, sizeof(sPointed5));
	AddPoints(player.iExp, sPointed2, sizeof(sPointed2));
	AddPoints(restantes, sPointed3, sizeof(sPointed3));
	
	// Calculate his type
	char sType[16];
	
	switch(player.iType){
		case PT_HUMAN: sType = "Humano";
		case PT_SURVIVOR: sType = "Survivor";
		case PT_GUNSLINGER: sType = "Gunslinger";
		case PT_SNIPER: sType = "Sniper";
		case PT_ZOMBIE: sType = "Zombie";
		case PT_NEMESIS: sType = "Nemesis";
		case PT_ASSASSIN: sType = "Assassin";
	}
	
	// Ending
	char sEnding[64];
	if (player.isType(PT_HUMAN) || player.isType(PT_ZOMBIE)){
		char sClassname[32];
		char sAlineacion[32];
		if (player.isType(PT_HUMAN)){
			gHClassName.GetString(player.iHumanClass, sClassname, sizeof(sClassname));
			gHAlignmentName.GetString(player.iHumanAlignment, sAlineacion, sizeof(sAlineacion));
		}
		else{
			gZClassName.GetString(player.iZombieClass, sClassname, sizeof(sClassname));
			gZAlignmentName.GetString(player.iZombieAlignment, sAlineacion, sizeof(sAlineacion));
		}
		FormatEx(sEnding, sizeof(sEnding), "Tipo: %s\nClase: %s\nAlineación: %s\n", sType, sClassname, sAlineacion);
	}
	else
		FormatEx(sEnding, sizeof(sEnding), "Tipo: %s\n", sType);
	
	// Party or single HUD
	if (player.isInParty()){
		char sPartyInfo[512];
		ZParty pt = ZParty(findPartyByUID(player.iPartyUID));
		for(int i; i < pt.length(); i++){
			ZPlayer ptPlayer = ZPlayer(gPartyMembers[pt.id].Get(i));
			
			char sName[32];
			GetClientName(ptPlayer.id, sName, sizeof(sName));
			
			// Format the hud
			char sTemp[64];
			FormatEx(sTemp, sizeof(sTemp), "\n%s - %s (Level: %d)", sName, ptPlayer.iTeamNum == CS_TEAM_T ? "Zombie" : "Humano", ptPlayer.iLevel);
			StrCat(sPartyInfo, sizeof(sTemp), sTemp);
			//FormatEx(sPartyInfo, sizeof(sPartyInfo), "\n%s", sTemp);
			
		}
		FormatEx(buffer, sizeof(buffer), "%sVida: %s\nArmor: %s\nAmmopacks: %s\nRestantes: %s\nLevel: %d\nResets: %d\n%s%s\n", bSpec ? "OBSERVANDO\n" : "",
		sPointed1, sPointed5, sPointed2, sPointed3, player.iLevel, player.iReset, sEnding, sPartyInfo);
	}
	else
		FormatEx(buffer, sizeof(buffer), "%sVida: %s\nArmor: %s\nAmmopacks: %s\nRestantes: %s\nLevel: %d\nResets: %d\n%s\n", bSpec ? "OBSERVANDO\n" : "",
		sPointed1, sPointed5, sPointed2, sPointed3, player.iLevel, player.iReset, sEnding);
}
public void hideHUD(int client){
	ZPlayer player = ZPlayer(client);
	
	if(IsPlayerExist(player.id))
		SetEntProp(player.id, Prop_Send, "m_iHideHUD", HIDEHUD_RADARANDTIMER);
}
public Action respawnPlayer(Handle timer, ZPlayer player){
	if (newRound || endRound)
		return Plugin_Handled;
	
	if(ActualMode.bRespawn && player.bLogged && player.bInGame){
		if(ActualMode.bRespawnZombieOnly || GetValidPlayingHumans() == 1){
			player.iRespawnPlayer(true);
		}
		else {
			if(RoundToCeil(fnGetAlive()*2.0/3.0) <= fnGetZombies()){
				int chance = GetRandomInt(1, 100);
				if(chance > CHANCE_TO_RESPAWN_HUMAN) player.iRespawnPlayer();
				else player.iRespawnPlayer(true);
			}
			else player.iRespawnPlayer(true);
		}
	}
	return Plugin_Handled;
}
public Action printVipSay(int client, char text[128]){
	ZPlayer player = ZPlayer(client);
	
	if (client != 100){
		if (!player.bAdmin && !player.bStaff)
			return Plugin_Handled;
	}
	
	char name[64];
	if (client != 100)
		GetClientName(player.id, name, sizeof(name));
	else name = "INFO";
	
	char msg[192];
	FormatEx(msg, sizeof(msg), "%s %s: %s", GetUserFlagBits(player.id) & ADMFLAG_CHANGEMAP ? "[MOD]" : "", name, text);
	
	Handle hRotativeSynchronizer = CreateHudSynchronizer();
	
	// Show hud message
	SetHudTextParams(0.03, GetRandomFloat(0.35, 0.7), VIP_SAY_DURATION, GetRandomInt(55, 255), GetRandomInt(55, 255), GetRandomInt(55, 255), 255, 1, 3.0, 0.4, 0.4);
	for (int i = 1; i <= MAXPLAYERS; i++){
		ZPlayer puto = ZPlayer(i);
		
		if (!IsPlayerExist(puto.id))
			continue;

		ShowSyncHudText(puto.id, hRotativeSynchronizer, msg);
		PrintToConsole(puto.id, msg);
	}
	//CloseHandle(hRotativeSynchronizer);
	delete hRotativeSynchronizer;
	return Plugin_Handled;
}

//=====================================================
//				USER CONFIGS FUNCTIONS
//=====================================================
void CheckHooks(){
	bool bShouldHook = false;
	
	for (int i = 1; i <= MaxClients; i++){
		ZPlayer player = ZPlayer(i);
		if (player.bStopSound){
			bShouldHook = true;
			break;
		}
	}
	
	// Fake (un)hook because toggling actual hooks will cause server instability.
	g_bHooked = bShouldHook;
}
public Action muteBullets(int client, any args){
	ZPlayer player = ZPlayer(client);
	player.bStopSound = !player.bStopSound;
	PrintToChat(player.id, "%s Silenciar disparos ajenos: \x09%s\x01.", SERVERSTRING, player.bStopSound ? "activado" : "desactivado");
	CheckHooks();
}
public Action autopBuy(int client){
	ZPlayer player = ZPlayer(client);
	player.bAutoPrimaryBuy = !player.bAutoPrimaryBuy;
	PrintToChat(player.id, "%sAumento autómatico de \x09arma primaria\x01: \x09%s\x01.", SERVERSTRING, player.bAutoPrimaryBuy ? "Activado" : "Desactivado");
}
public Action autosBuy(int client){
	ZPlayer player = ZPlayer(client);
	player.bAutoSecondaryBuy = !player.bAutoSecondaryBuy;
	PrintToChat(player.id, "%s Aumento autómatico de \x09arma secundaria\x01: \x09%s\x01.", SERVERSTRING, player.bAutoSecondaryBuy ? "Activado" : "Desactivado");
}
public Action autogBuy(int client){
	ZPlayer player = ZPlayer(client);
	player.bAutoGrenadeBuy = !player.bAutoGrenadeBuy;
	PrintToChat(player.id, "%s Aumento autómatico de \x09pack de granadas\x01: \x09%s\x01.", SERVERSTRING, player.bAutoGrenadeBuy ? "Activado" : "Desactivado");
}
public Action autozClass(int client){
	ZPlayer player = ZPlayer(client);
	player.bAutoZClass = !player.bAutoZClass;
	PrintToChat(player.id, "%s Cambio de clase Zombie automatico: \x09%s\x01.", SERVERSTRING, player.bAutoZClass ? "Activado" : "Desactivado");
}
public Action NormalSoundHook(int clients[64], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags){
	// Ignore non-weapon sounds.
	if (!g_bHooked || !(strncmp(sample, "weapons", 7) == 0 || strncmp(sample[1], "weapons", 7) == 0))
		return Plugin_Continue;
	
	int i, j;
	
	for (i = 0; i < numClients; i++){
		ZPlayer player = ZPlayer(clients[i]);
		if (player.bStopSound){
			// Remove the client from the array.
			for (j = i; j < numClients-1; j++){
				clients[j] = clients[j+1];
			}
			
			numClients--;
			i--;
		}
	}
	
	return (numClients > 0) ? Plugin_Changed : Plugin_Stop;
}
public Action ShotgunShotEntHook(const char[] te_name, const int[] iPlayers, int numClients, float delay){
	if (!g_bHooked)
		return Plugin_Continue;
		
	// Check which clients need to be excluded.
	int newClients[MAXPLAYERS+1];
	int i;
	ZPlayer player;
	int newTotal = 0;
	
	for (i = 0; i < numClients; i++){
		player = ZPlayer(iPlayers[i]);
		
		if (!player.bStopSound){
			newClients[newTotal++] = player.id;
		}
	}
	
	// No clients were excluded.
	if (newTotal == numClients)
		return Plugin_Continue;
	
	// All clients were excluded and there is no need to broadcast.
	else if (newTotal == 0)
		return Plugin_Stop;
	
	// Re-broadcast to clients that still need it.
	float vTemp[3];
	TE_Start("Shotgun Shot");
	TE_ReadVector("m_vecOrigin", vTemp);
	TE_WriteVector("m_vecOrigin", vTemp);
	TE_WriteFloat("m_vecAngles[0]", TE_ReadFloat("m_vecAngles[0]"));
	TE_WriteFloat("m_vecAngles[1]", TE_ReadFloat("m_vecAngles[1]"));
	TE_WriteNum("m_weapon", TE_ReadNum("m_weapon"));
	TE_WriteNum("m_iMode", TE_ReadNum("m_iMode"));
	TE_WriteNum("m_iSeed", TE_ReadNum("m_iSeed"));
	TE_WriteNum("m_iPlayer", TE_ReadNum("m_iPlayer"));
	TE_WriteFloat("m_fInaccuracy", TE_ReadFloat("m_fInaccuracy"));
	TE_WriteFloat("m_fSpread", TE_ReadFloat("m_fSpread"));
	TE_Send(newClients, newTotal, delay);
	
	return Plugin_Stop;
}

//=====================================================
//					GAMEMODES
//=====================================================
public Action TimerShowCountdown(Handle timer){
	// When counter finishes
	if(iCounter <= 0){
		if(ActualMode.id == view_as<int>(NO_MODE)) {
			if (iSelectedMode > view_as<int>(IN_WAIT)){
				StartMode(view_as<int>(iSelectedMode));
				iSelectedMode = view_as<int>(NO_MODE);
			}
			else{
				int r = GetRandomInt(0, 100);
				if(modeCount > 10 || bForcedMode)
					StartMode(GetRandomInt(view_as<int>(MODE_INFECTION), view_as<int>(MODE_MASSIVE_INFECTION)));
				else 
					StartMode(GetRandomInt(view_as<int>(MODE_INFECTION), gModeName.Length-1));
				
				bForcedMode = false;
			}
		}
		iCounter = ROUND_START_COUNTDOWN;
		hCounter = null;
		return Plugin_Stop;
	}
	
	// When counter is active
	if(ActualMode.id == view_as<int>(NO_MODE)){
		iCounter--;
		PrintHintTextToAll("<font size='25' color='#XXXXXX'>Amenaza en:</font>\n<font size='35' color='#FF0000'>%d</font>", iCounter);
		if(iCounter <= 10 && iCounter > 0) {
			char buf[64];
			FormatEx(buf, 64, "*/MassiveInfection/round/%d.mp3", iCounter);
			EmitSoundToAll(buf, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
		}
	}
	else{
		iCounter = ROUND_START_COUNTDOWN;
		hCounter = null;
		return Plugin_Stop;
	}
	return Plugin_Handled;
}
stock void StartMode(int mode, bool bypass = false){

	if (!newRound)
		return;
	
	// Don't let the modes repeat each round
	if (lastMode == mode && mode > view_as<int>(MODE_MASSIVE_INFECTION)){
		StartMode(GetRandomInt(view_as<int>(MODE_INFECTION), gModeName.Length-1));
		return;
	}
	
	int nAlive = fnGetAlive();
	
	// Check if the quantity of users reaches the minimum
	if (!bypass){
		if (!nAlive){
			PrintToServer("[StartMode] No hay usuarios suficientes para iniciar modos.");
			PrintToServer("[StartMode] Iniciando modo de espera de usuarios.");
			//LogError("[StartMode] No hay usuarios suficientes para iniciar algún modo.");
			
			// Initialize mode vars
			ActualMode = ZGameMode(view_as<int>(IN_WAIT));
			lastMode = view_as<int>(IN_WAIT);
			iCounter = ROUND_START_COUNTDOWN;
			
			// Initialize booleans
			newRound = false;
			endRound = false;
			startedRound = true;
			return;
		}
		else if (nAlive < gMinUsers.Get(mode)){
			StartMode(GetRandomInt(view_as<int>(MODE_INFECTION), view_as<int>(MODE_MASSIVE_INFECTION)));
			return;
		}
	}
	
	// Initialize mode vars
	ActualMode = ZGameMode(mode);
	lastMode = mode;
	iCounter = ROUND_START_COUNTDOWN;
	
	// Initialize max amount of zombies
	int nMaxZombies;
	
	// Initialize booleans
	newRound = false;
	endRound = false;
	startedRound = true;
	
	// Get mode name
	char name[32];
	gModeName.GetString(mode, name, sizeof(name));
	float volume = 0.2;
	switch(mode){
		case MODE_INFECTION:{
			nMaxZombies = 1;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE, true);
			modeCount-=2;
			EmitSoundToAll(AMBIENT_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, volume);
		}
		case MODE_MULTIPLE_INFECTION:{
			nMaxZombies = RoundToCeil(nAlive / 5.0);
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE);
			modeCount-=1;
			EmitSoundToAll(AMBIENT_SOUND, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_ANIHHILATION:{
			nMaxZombies = 1;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE, true);
			modeCount -= 2;
			EmitSoundToAll(AMBIENT_SOUND_SWARM, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_MASSIVE_INFECTION:{
			nMaxZombies = RoundToFloor(nAlive / 3.0);
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE);
			modeCount--;
			EmitSoundToAll(AMBIENT_SOUND_SWARM, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_SWARM:{
			EmitSoundToAll(MODE_SWARM_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
			nMaxZombies = RoundToCeil(nAlive / 2.0);
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE);
			modeCount++;
			EmitSoundToAll(AMBIENT_SOUND_INFECTION, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_NEMESIS:{
			EmitSoundToAll(MODE_NEMESIS_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
			nMaxZombies = 1;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, NEMESIS, true);
			
			/*ZPlayer player = ZPlayer(fnGetRandomAlive(GetRandomInt(1, nAlive)));
			player.Zombiefy(NEMESIS);*/
			modeCount += 3;
			EmitSoundToAll(AMBIENT_SOUND_ZBOSS, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_SURVIVOR:{
			EmitSoundToAll(MODE_SURVIVOR_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
			ZPlayer player = ZPlayer(fnGetRandomAlive(GetRandomInt(1, nAlive)));
			player.Humanize(SURVIVOR);
			
			for (int i = 1; i < MAXPLAYERS; i++){
				ZPlayer p = ZPlayer(i);
				
				if (!p.isType(PT_SURVIVOR) && IsPlayerExist(p.id, true))
					p.Zombiefy(ZOMBIE);
			}
			
			//nMaxZombies = nAlive-1;
			//GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE);
			////GameModesTurnIntoHuman(1, nAlive, SURVIVOR, true);
			
			modeCount +=3;
			EmitSoundToAll(AMBIENT_SOUND_HBOSS, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_GUNSLINGER:{
			EmitSoundToAll(MODE_SURVIVOR_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
			ZPlayer player = ZPlayer(fnGetRandomAlive(GetRandomInt(1, nAlive)));
			player.Humanize(GUNSLINGER);
			
			for (int i = 1; i < MAXPLAYERS; i++){
				ZPlayer p = ZPlayer(i);
				
				if (!p.isType(PT_GUNSLINGER) && IsPlayerExist(p.id, true))
					p.Zombiefy(ZOMBIE);
			}
			
			/*nMaxZombies = nAlive-1;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE);*/
			////GameModesTurnIntoHuman(1, nAlive, GUNSLINGER, true);
			modeCount +=3;
			EmitSoundToAll(AMBIENT_SOUND_HBOSS, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_ASSASSIN:{
			EmitSoundToAll(MODE_NEMESIS_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
			nMaxZombies = 1;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ASSASSIN, true);
			
			// Turn on fog
			switchFog(true);
			
			/*ZPlayer player = ZPlayer(fnGetRandomAlive(GetRandomInt(1, nAlive)));
			player.Zombiefy(NEMESIS);*/
			modeCount += 3;
			EmitSoundToAll(AMBIENT_SOUND_ZBOSS, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_SNIPER:{
			EmitSoundToAll(MODE_SURVIVOR_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
			ZPlayer player = ZPlayer(fnGetRandomAlive(GetRandomInt(1, nAlive)));
			player.Humanize(SNIPER, true);
			
			for (int i = 1; i <= MaxClients; i++){
				ZPlayer p = ZPlayer(i);
				
				if (!p.isType(PT_SNIPER) && IsPlayerExist(p.id, true))
					p.Zombiefy(ZOMBIE);
			}
			
			/*nMaxZombies = nAlive-1;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ZOMBIE);*/
			////GameModesTurnIntoHuman(1, nAlive, GUNSLINGER, true);
			modeCount +=3;
			EmitSoundToAll(AMBIENT_SOUND_HBOSS, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_MULTIPLE_NEMESIS:{
			nMaxZombies = (fnGetPlaying() >= 15) ? 3 : 2;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, NEMESIS);
			GameModesTurnIntoHuman(1, nAlive, SURVIVOR, true);
			modeCount += 5;
			EmitSoundToAll(AMBIENT_SOUND_ARMAGEDON, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
		case MODE_ARMAGEDDON:{
			StartArmageddonIntro();
		}
		case MODE_APOCALYPSIS:{
			EmitSoundToAll(MODE_SWARM_SOUND, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
			nMaxZombies = RoundToCeil(nAlive / 2.0);
			int nHumans = nAlive - nMaxZombies;
			GameModesTurnIntoZombie(nMaxZombies, nAlive, ASSASSIN);
			GameModesTurnIntoHuman(nHumans, nAlive, GUNSLINGER);
			
			// Turn on fog
			switchFog(true);
			
			for (int i = 1; i <= MaxClients; i++){
				// Get real player index from event key
				ZPlayer player = ZPlayer(i);
				
				// Verify that the client is exist
				if(!IsPlayerExist(player.id, true))
					continue;
		
				// Verify that the client is human
				if(player.iTeamNum != CS_TEAM_CT)
					continue;
					
				if(player.isType(PT_HUMAN))	player.Zombiefy(ASSASSIN);
			}
			modeCount += 5;
			EmitSoundToAll(AMBIENT_SOUND_ARMAGEDON, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_,  volume);
		}
	}
	PrintHintTextToAll("<font size='22' color='#XXXXXX'>Modo:</font>\n<font size='37' color='#FF0000'>%s</font>", name);

	//EmitSoundToAll(AMBIENT_SOUND, SOUND_FROM_PLAYER,_,_,_,0.7);
	//RoundEndOnValidate();
}
void GameModesTurnIntoZombie(int nMaxZombies, int nAlive, ZombieType iType, bool firstZombie = false){
	int nZombies;

	while (nZombies < nMaxZombies){
		int random = fnGetRandomAlive(GetRandomInt(1, nAlive));
		
		if(!IsPlayerExist(random))
			continue;
		
		ZPlayer player = ZPlayer(random);
		
		if(player.isType(PT_ZOMBIE) || player.isType(PT_SURVIVOR) || player.isType(PT_GUNSLINGER) || player.isType(PT_SNIPER))
			continue;
		
		player.Zombiefy(iType, firstZombie);
		
		nZombies++;
	}
}
void GameModesTurnIntoHuman(int nMaxHumans, int nAlive, HumanType iType, bool bWhile = false){
	int nHumans;
	
	if (bWhile){
		while (nHumans < nMaxHumans){
			ZPlayer player = ZPlayer(fnGetRandomAlive(GetRandomInt(1, nAlive)));
			
			if(!IsPlayerExist(player.id))
				continue;
			
			if(player.isType(PT_ZOMBIE) || player.isType(PT_NEMESIS) || player.isType(PT_ASSASSIN))
				continue;
			
			player.Humanize(iType);
			
			nHumans++;
		}
	}
	else{
		// i = client index
		for (int i = 1; i <= MaxClients; i++){
			// Get real player index from event key
			ZPlayer player = ZPlayer(i);
			
			// Verify that the client is exist
			if(!IsPlayerExist(player.id, true))
				continue;
	
			// Verify that the client is human
			if(player.isType(PT_ZOMBIE) || player.isType(PT_NEMESIS) || player.isType(PT_ASSASSIN))
				continue;
			
			if (nHumans < nMaxHumans){
				player.Humanize(iType);
				nHumans++;
			}
			else break;
		}
	}
}
bool RoundEndOnValidate(bool validateRound = true){
	int nPlayers = fnGetPlaying();
	
	if (!nPlayers){
		return false;
	}
	
	int nHumans  = fnGetHumans();
	int nZombies = fnGetZombies();
	
	if(validateRound){
		if(newRound){
			int nAlive = fnGetAlive();
			
			if(nAlive > 1){
				return false;
			}
		}
		
		if (nZombies > 0 && nHumans > 0){
			return false;
		}
	}
	
	if (nHumans > 0){
		CS_TerminateRound(3.0, CSRoundEnd_CTWin, false);
	}
	else{
		CS_TerminateRound(1.0, CSRoundEnd_TerroristWin, false);
	}
	return true;
}
void switchFog(bool boolean = false){
	AcceptEntityInput(iFog, boolean ? "TurnOn" : "TurnOff");
}

// Levels funcs
stock int NextLevel(int level, int reset){
	return RoundToCeil((24 * level * (level+(level/2)) + 300)*(1+(reset*0.05)));
	//return RoundToCeil((25 * level * (level+(level/2)) + 500)(1+(reset*0.25)));
	//(NextLevel(player.iLevel+1, player.iReset)-player.iExp);
	//NextLevel(201, 6);
}
public Action testRestantes(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	int lvl = NextLevel(player.iLevel, player.iReset);
	int nextlvl = NextLevel(player.iLevel+1, player.iReset);
	int sobrantes = nextlvl - lvl + player.iExp;
	int resting = nextlvl - lvl;
	int resting2 = (NextLevel(player.iLevel+1, player.iReset)-player.iExp);
	PrintToChat(player.id, "%s AP.lvl: %d, AP.nxtlvl: %d, AP.sobrantes: %d, restantes1: %d, restantes2: %d", SERVERSTRING, lvl, nextlvl, sobrantes, resting, resting2);
}
public void checkLevelUp(int client){

	if (!IsPlayerExist(client))
		return;
	
	ZPlayer player = ZPlayer(client);
	
	bool upped = false;
	
	while (player.iExp >= NextLevel(player.iLevel, player.iReset) && player.iLevel < RESET_LEVEL){
		player.iLevel++;
		upped = true;
	}

	if(upped){
		if(player.bAutoPrimaryBuy) checkWeaponPrimaryUp(client);
		if(player.bAutoSecondaryBuy) checkWeaponSecondaryUp(client);
		if(player.bAutoGrenadeBuy) checkGrenadePackUp(client);
		checkHumanClassUp(client);
		if(player.bAutoZClass) checkZombieClassUp(client);
	}
	
	while (player.iExp < NextLevel(player.iLevel-1, player.iReset) && player.iLevel > 1){
		player.iLevel--;
	}
}

//=====================================================
//				AUTOMATIC UPGRADE/DOWNGRADE
//=====================================================
public void checkWeaponPrimaryUp(int client){
	ZPlayer p = ZPlayer(client);
	
	if(p.iLevel == 400) return;
	
	int length = primaryWeaponsName.Length-1;
	char buff[32];
	bool change = false;
	
	if(p.iNextWeaponPrimary >= length) return;
	
	ZWeaponPrimary weap = ZWeaponPrimary(p.iNextWeaponPrimary+1);
	while(p.iLevel >= weap.iLevel && p.iReset >= weap.iReset && p.bAutoPrimaryBuy){
		if(p.iNextWeaponPrimary+1 >= length) break;
		p.iNextWeaponPrimary++;
		primaryWeaponsName.GetString(weap.id, buff, sizeof(buff));
		change = true;
		weap = ZWeaponPrimary(p.iNextWeaponPrimary+1);
	}
	if(change) PrintToChat(client, "%s Tu próxima \x03arma primaria\x01 será: \x0F%s\x01.", SERVERSTRING, buff);
}
public void checkWeaponSecondaryUp(int client){
	ZPlayer p = ZPlayer(client);
	
	if(p.iLevel == 400) return;
	int length = secondaryWeaponsName.Length-1;
	char buff[32];
	bool change = false;
	
	if(p.iNextWeaponSecondary >= length) return;
	
	ZWeaponSecondary weap = ZWeaponSecondary(p.iNextWeaponSecondary+1);
	while(p.iLevel >= weap.iLevel && p.iReset >= weap.iReset && p.bAutoSecondaryBuy){
		if(p.iNextWeaponSecondary+1 >= length) break;
		p.iNextWeaponSecondary++;
		secondaryWeaponsName.GetString(weap.id, buff, sizeof(buff));
		change = true;
		weap = ZWeaponSecondary(p.iNextWeaponSecondary+1);
	}
	if(change) PrintToChat(client, "%s Tu próxima \x03arma secundaria\x01 será: \x0F%s\x01.", SERVERSTRING, buff);
}
public void checkHumanClassUp(int client){
	ZPlayer p = ZPlayer(client);
	if(p.iLevel == 400) return;
	
	int length = gHClassName.Length-1;
	char buff[32];
	bool change = false;
	
	if(p.iNextHumanClass+1 >= length) return;
	
	HClass class = HClass(p.iNextHumanClass+1);
	while(p.iLevel >= class.iLevelReq && p.iReset >= class.iResetReq){
		if(p.iNextHumanClass+1 >= length) break;
		
		if (class.id >= iVipClass[0]){
			if ((class.id == iVipClass[0] && p.flExpBoost < 5.0) || (class.id == iVipClass[1] && p.flExpBoost < 6.0) 
			|| (class.id == iVipClass[2] && p.flExpBoost < 7.0) || (class.id == iVipClass[3] && p.flExpBoost < 8.0))
				break;
		}
		
		p.iNextHumanClass++;
		gHClassName.GetString(class.id, buff, sizeof(buff));
		change = true;
		class = HClass(p.iNextHumanClass+1);
	}
	if(change) PrintToChat(client, "%s Tu próxima clase \x03Humana\x01 será: \x0F%s\x01.", SERVERSTRING, buff);
}
public void checkZombieClassUp(int client){
	ZPlayer p = ZPlayer(client);
	if(p.iLevel == 400) return;
	int length = gZClassName.Length-1;
	char buff[32];
	bool change = false;
	
	if(p.iNextZombieClass+1 >= length) return;
	
	ZClass class = ZClass(p.iNextZombieClass+1);
	while(p.iLevel >= class.iLevelReq && p.iReset >= class.iResetReq && p.bAutoZClass){
		if(p.iNextZombieClass+1 >= length) break;
		p.iNextZombieClass++;
		gZClassName.GetString(class.id, buff, sizeof(buff));
		change = true;
		class = ZClass(p.iNextZombieClass+1);
	}
	if(change) PrintToChat(client, "%s Tu próxima clase \x03Zombie\01 será: \x0F%s\x01.", SERVERSTRING, buff);
}
public void checkGrenadePackUp(int client){
	ZPlayer p = ZPlayer(client);
	
	if(p.iLevel == 400) return;
	
	int length = gGrenadePackLevel.Length-1;
	bool change = false;
	
	if(p.iNextGrenadePack >= length) return;
	
	ZGrenadePack pack = ZGrenadePack(p.iNextGrenadePack+1);
	while(p.iLevel >= pack.iLevel && p.iReset >= pack.iReset && p.bAutoGrenadeBuy){
		if(p.iNextGrenadePack+1 >= length) break;
		p.iNextGrenadePack++;
		change = true;
		pack = ZGrenadePack(p.iNextGrenadePack+1);
	}
	if(change) PrintToChat(client, "%s Tu próximo \x03pack de granadas\x01 será el número: \x0F%d\x01.", SERVERSTRING, p.iNextGrenadePack);
}

//=====================================================
//				WEAPONS HOOKS
//=====================================================
public Action WeaponsOnCanUse(int client, int weaponIndex){
	ZPlayer player = ZPlayer(client);
	
	if(!IsValidEdict(weaponIndex) || !IsPlayerExist(player.id)){
		return Plugin_Handled;
	}
	
	char weap[32];
	GetEdictClassname(weaponIndex, weap, 32);
	
	if(player.isType(PT_ZOMBIE)){
		if(StrEqual(weap, "weapon_knife")) return Plugin_Continue;
		if(StrEqual(weap, "weapon_flashbang")) return Plugin_Continue;
		return Plugin_Handled;
	}
	else if (player.isType(PT_NEMESIS) || player.isType(PT_ASSASSIN)){
		if (StrEqual(weap, "weapon_knife")) return Plugin_Continue;
		return Plugin_Handled;
	}
	
	if (player.isType(PT_GUNSLINGER)){
		if (StrEqual(weap, "weapon_knife")) return Plugin_Continue;
		if (StrEqual(weap, "weapon_flashbang")) return Plugin_Continue;
		if (StrEqual(weap, "weapon_smokegrenade")) return Plugin_Continue;
		if (StrEqual(weap, "weapon_deagle")) return Plugin_Continue;
		return Plugin_Handled;
	}
	else if (player.isType(PT_SNIPER)){
		if (StrEqual(weap, "weapon_knife")) return Plugin_Continue;
		if (StrEqual(weap, "weapon_decoy")) return Plugin_Continue;
		if (StrEqual(weap, "weapon_awp")) return Plugin_Continue;
		return Plugin_Handled;
	}
	return Plugin_Continue;
}
public Action PreThink(int client){
	SetEntPropFloat(client, Prop_Send, "m_flStamina", 0.0);
}
public Action WeaponsOnDropPost(int clientIndex, int weaponIndex){
	if(IsValidEdict(weaponIndex)) {
		CreateTimer(TIME_TO_REMOVE_WEAPONS, WeaponsRemoveDropedWeapon, weaponIndex, TIMER_FLAG_NO_MAPCHANGE);
	}
}
public Action WeaponsRemoveDropedWeapon(Handle hTimer, any weaponIndex){
	if(!IsValidEdict(weaponIndex))
		return Plugin_Stop;
	
	if(GetEntPropEnt(weaponIndex, Prop_Data, "m_pParent") == -1)
		RemoveEdict(weaponIndex);
	
	return Plugin_Stop;
}
public Action WeaponsOnEquip(int client, int weapon){
	ZPlayer player = ZPlayer(client);
	
	char sWeapon[32];
	GetEntPropString(weapon, Prop_Data, "m_iName", sWeapon, sizeof(sWeapon));
	
	if (StrContains(sWeapon, "primary") != -1){
		player.iWeaponPrimary = StringToInt(sWeapon[8]);
		//PrintToChat(player.id, "%s PWeapon: %d", SERVERSTRING, StringToInt(sWeapon[8]));
	}
	else if (StrContains(sWeapon, "secondary") != -1){
		player.iWeaponSecondary = StringToInt(sWeapon[10]);
		//PrintToChat(player.id, "%s SWeapon: %d", SERVERSTRING, StringToInt(sWeapon[10]));
	}
	
	return Plugin_Continue;
}
public void OnClientWeaponSwitchPost(int client, int weapon){
	ZPlayer player = ZPlayer(client);
	
	if(!IsPlayerExist(player.id) || !IsValidEntity(weapon))
		return;
	
	char sWpn[64]; 
	GetEdictClassname(weapon, sWpn, sizeof(sWpn));
	
	int iWeaponid = player.iWeaponPrimary;
	char sWeapModel[64], sWeapWorldModel[64], sWeapEntName[16], arms[128];
	primaryWeaponsModel.GetString(iWeaponid, sWeapModel, sizeof(sWeapModel));
	primaryWeaponsWorldModel.GetString(iWeaponid, sWeapWorldModel, sizeof(sWeapWorldModel));
	primaryWeaponsEnt.GetString(iWeaponid, sWeapEntName, sizeof(sWeapEntName));
	gZClassArms.GetString(player.iZombieClass, arms, sizeof(arms));
	
	//PrintToChat(player.id, "weapon: %d, sWpn: %s, sWeapModel: %s, sWeapEntName: %s", weapon, sWpn, sWeapModel, sWeapEntName);
	
	// Change models in each case
	if (StrEqual(sWpn, "weapon_knife") && player.isType(PT_ZOMBIE)){
		SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(arms));
		ZClass class = ZClass(player.iZombieClass);
		if (class.bHideKnife)
			hideWorldModel(weapon);
	}
	else if (StrEqual(sWpn, "weapon_m249") && player.isType(PT_SURVIVOR)){
		SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(SURVIVOR_VMODEL_WEAPON));
  		SetWorldModel(weapon, GetModelIndex(SURVIVOR_WMODEL_WEAPON));
  		//return;
	}
	/*else if (StrEqual(sWpn, "weapon_awp") && player.isType(PT_SNIPER)){
		SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(SNIPER_VMODEL_WEAPON));
  		SetWorldModel(weapon, GetModelIndex(SNIPER_WMODEL_WEAPON));
	}*/
	
	if (StrEqual(sWpn, "weapon_smokegrenade") && player.iTeamNum == CS_TEAM_CT){
		ZGrenadePack pack = ZGrenadePack(player.iGrenadePack);
		if (pack.hasGrenade(AURA_GRENADE)){
			SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(AURA_VMODEL));
			SetWorldModel(weapon, GetModelIndex(AURA_WMODEL));
		}
	}
	
	// Else
	if (StrEqual(sWeapWorldModel, ""))
		return;
	
	if (StrEqual(sWpn, sWeapEntName) || StrEqual(sWeapModel, "")){
		/*char test[64], test2[64];
		GetEntPropString(weapon, Prop_Send, "m_szCustomName", test, sizeof(test));
		GetEntPropString(weapon, Prop_Send, "m_iName", test2, sizeof(test2));
		PrintToChat(player.id, "m_szCustomName: %s, m_iName: %s", test, test2);*/
		
		SetViewModel(weapon, player.iPredictedViewModelIndex, GetModelIndex(sWeapModel));
		SetWorldModel(weapon, GetModelIndex(sWeapWorldModel));
	}
}

// Apply selected weapon dropped skin when users drop them
public void OnClientWeaponDropPost(int client, int weapon){
	if (!IsValidEdict(weapon))
		return;
	
	if (!IsPlayerExist(client))
		return;
	
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	
	char sDroppedModel[64], sWeapon[32];
	GetEntPropString(weapon, Prop_Data, "m_iName", sWeapon, sizeof(sWeapon));
	
	if (StrContains(sWeapon, "primary") != -1){
		ZWeaponPrimary zWeapon = ZWeaponPrimary(StringToInt(sWeapon[8]));
		//PrintToChat(player.id, "%s PWeapon: %d", SERVERSTRING, StringToInt(sWeapon[8]));
		primaryWeaponsWorldModel.GetString(zWeapon.id, sDroppedModel, sizeof(sDroppedModel));
	}
	
	
	if (!StrEqual(sDroppedModel, "")){
	
		// Send data to the next frame
		DataPack hPack = new DataPack();
		hPack.WriteCell(weapon);
		hPack.WriteString(sDroppedModel);
		
		RequestFrame(view_as<RequestFrameCallback>(SetDroppedModel), hPack);
	}
}
public void SetDroppedModel(any hPack){
	// Resets the position in the datapack
	ResetPack(hPack);
	
	// Get the weapon index from the datapack
	int weapon = ReadPackCell(hPack);
	
	// Get the world model from the datapack
	static char sDroppedModel[PLATFORM_MAX_PATH];
	ReadPackString(hPack, sDroppedModel, sizeof(sDroppedModel));
	
	if (!IsValidEdict(weapon))
		return;
	
	SetEntityModel(weapon, sDroppedModel);
	
	delete view_as<DataPack>(hPack);
}

// Modify entity details to store info
public Action CS_OnCSWeaponDrop(int client, int weaponIndex){
	if (!IsValidEdict(weaponIndex))
		return Plugin_Handled;
	
	ZPlayer player = ZPlayer(client);
	
	if (player.isType(PT_SURVIVOR) || player.isType(PT_GUNSLINGER) || player.isType(PT_SNIPER))
		return Plugin_Handled;
	
	// Initialize vars
	int primary, secondary;
	char sPrimary[32], sSecondary[32];
	
	// Get primary weapon id
	primary = GetPlayerWeaponSlot(player.id, CS_SLOT_PRIMARY);
	
	// Get secondary weapon id
	secondary = GetPlayerWeaponSlot(player.id, CS_SLOT_SECONDARY);
	
	// Store to primary weapon id
	if(primary == weaponIndex){
		FormatEx(sPrimary, sizeof(sPrimary), "primary_%d", player.iWeaponPrimary);
		SetEntPropString(weaponIndex, Prop_Data, "m_iName", sPrimary);
		player.iWeaponPrimary = 0;
	}
	// Store to secondary weapon id
	else if(secondary == weaponIndex){
		FormatEx(sSecondary, sizeof(sSecondary), "secondary_%d", player.iWeaponSecondary);
		SetEntPropString(weaponIndex, Prop_Data, "m_iName", sSecondary);
		player.iWeaponSecondary = 0;
	}
	
	return Plugin_Continue;
}

//=====================================================
//					MENUS SECTION
//=====================================================

// VIP MENU
public Action showVipMenu(int client){
	ZPlayer player = ZPlayer(client);
	if(!player.bAdmin && !player.bStaff) return Plugin_Handled;
	Menu menu = new Menu(VipMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Menu de VIP");
	menu.AddItem("0", "Iniciar Votacion mapa");
	menu.AddItem("1", "Iniciar Modo");
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;

}
public int VipMenuHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0: PrintToChat(client, "En construcción");
				case 1: showMenuModos(client);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// MODES MENU
public Action showMenuModos(int client){
	ZPlayer player = ZPlayer(client);
	PrintToServer("Iniciando Menu Modos..");
	
	if(!player.bAdmin && !player.bStaff)
		return Plugin_Handled;
	
	Menu menu = new Menu(ModsMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	char title[64];
	char nam[32];
	Format(title, 64, "Seleccionar modo (Modos restantes: %d)", modesForced);
	menu.SetTitle(title);
	for(int i = view_as<int>(IN_WAIT)+1; i < gModeName.Length; i++){
		ZGameMode mode = ZGameMode(i);
		
		if (mode.bStaffOnly)
			continue;
		
		// Get mode name
		gModeName.GetString(i, nam, 32);
		
		// Store index into the buffer
		char buffer[4];
		IntToString(i, buffer, sizeof(buffer));
		
		
		if ((i == lastMode && i > view_as<int>(MODE_MASSIVE_INFECTION)) || !modesForced || ActualMode.id)
			menu.AddItem(buffer, nam, ITEMDRAW_DISABLED);
		else if (gMinVipToForce.Get(i) > player.flExpBoost && !player.bStaff)
			menu.AddItem(buffer, "Requiere mayor VIP!", ITEMDRAW_DISABLED);
		else if (fnGetAlive() < gMinUsers.Get(i))
			menu.AddItem(buffer, "Usuarios insuficientes", ITEMDRAW_DISABLED);
		else if (gMaxTimesPerMap.Get(i) == 0 || bForcedMode)
			menu.AddItem(buffer, nam, ITEMDRAW_DISABLED);
		else
			menu.AddItem(buffer, nam);
	}
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int ModsMenuHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			if(newRound){
				// Get mode index
				char buffer[4];
				menu.GetItem(selection, buffer, sizeof(buffer));
				
				// Store mode index as option selected
				int option = StringToInt(buffer);
				
				if (fnGetAlive() < gMinUsers.Get(option)){
					return 0;
				}
				else{
					if (bForcedMode)
						return 0;
						
					int iMaxTimes = gMaxTimesPerMap.Get(option);
					
					if (!iMaxTimes){
						return 0;
					}
					
					// Initialize vars
					char name[32];
					char mod[32];
					
					// Get mode activator data
					GetClientName(client, name, sizeof(name));
					ZPlayer player = ZPlayer(client);
					
					// Get mode data
					gModeName.GetString(option, mod, 32);
					
					// Send announcement
					PrintToChatAll("%s %s %s:\x01 iniciar modo \x0F%s", SERVERSTRING, (player.bStaff) ? "\x09STAFF" : "\x0FVIP", name, mod);
					
					// Decrease quantity of modes available
					modesForced--;
					gMaxTimesPerMap.Set(option, iMaxTimes-1);
					bForcedMode = true;
					
					// Select desired mode
					iSelectedMode = option;
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// STAFF MODES MENU
public Action showMenuModosStaff(int client){
	ZPlayer player = ZPlayer(client);
	
	if(!player.bAdmin && !player.bStaff)
		return Plugin_Handled;
	
	Menu menu = new Menu(StaffModsMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	char title[64];
	char nam[32];
	Format(title, 64, "Seleccionar modo (Modos restantes: %d)", modesForced);
	menu.SetTitle(title);
	for(int i = view_as<int>(IN_WAIT)+1; i < gModeName.Length; i++){
		
		// Get mode name
		gModeName.GetString(i, nam, 32);
		
		// Store index into the buffer
		char buffer[4];
		IntToString(i, buffer, sizeof(buffer));
		
		if ((i == lastMode && i > view_as<int>(MODE_MASSIVE_INFECTION)) || ActualMode.id)
			menu.AddItem(buffer, nam, ITEMDRAW_DISABLED);
		else
			menu.AddItem(buffer, nam);
	}
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int StaffModsMenuHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			if(newRound){
				// Get mode index
				char buffer[8];
				menu.GetItem(selection, buffer, sizeof(buffer));
				
				// Store mode index as option selected
				int option = StringToInt(buffer);
				
				// Initialize vars
				char name[32];
				char mod[32];
				
				// Get mode activator data
				GetClientName(client, name, sizeof(name));
				ZPlayer player = ZPlayer(client);
				
				// Decrease quantity of modes available
				bForcedMode = true;
				
				// Select desired mode
				iSelectedMode = option;
				
				// Get mode data
				gModeName.GetString(option, mod, 32);
				
				// Send announcement
				PrintToChatAll("%s %s %s:\x01 iniciar modo \x0F%s", SERVERSTRING, (player.bStaff) ? "\x09STAFF" : "\x0FVIP", name, mod);
				
				StartMode(option, true);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// MAIN HANDLER MENU
public Action showMainMenu(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!IsPlayerExist(player.id))
		return Plugin_Handled;
	
	if((player.iTeamNum == CS_TEAM_NONE || player.iTeamNum == CS_TEAM_SPECTATOR) && !player.bLogged) {
		//player.iTeamNum = CS_TEAM_SPECTATOR;
		showLoginMenu(player.id);
	}
	else if ((player.iTeamNum == CS_TEAM_NONE || player.iTeamNum == CS_TEAM_SPECTATOR) && player.bLogged && !player.bInGame) loadCharacters(player.id);
	else{
		Menu menu = new Menu(MainMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
		menu.SetTitle("Menú principal\nMassive Infection %s", PLUGIN_VERSION);
		menu.AddItem("0", "Equipamiento");
		menu.AddItem("1", "Mis items extra");
		menu.AddItem("2", "Clases");
		menu.AddItem("3", "Mejoras");
		menu.AddItem("4", "Menu Vip");
		menu.AddItem("5", "Ajustes");
		menu.ExitButton = true;
		menu.Display(client, MENU_TIME_FOREVER);
	}
	
	return Plugin_Handled;
}
public int MainMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer p = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case MENUINDEX_Weapons: showMenuWeapons(client);
				case MENUINDEX_Items: showMenuItemsExtra(client);
				case MENUINDEX_Classes: showMenuClasses(client);
				case MENUINDEX_Points: showMejorasMenu(client);
				case MENUINDEX_Vip: {
					if(p.bAdmin) showVipMenu(client);
					else if (p.bStaff) showMenuModosStaff(client);
					else	PrintToChat(client, "%s No eres\x0F VIP\x01.", SERVERSTRING);
				}
				case MENUINDEX_UNSTUCK: showMenuConfigs(client);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// EXTRA ITEMS MENU
public Action showMenuItemsExtra(int client){
	ZPlayer p = ZPlayer(client);
	Menu menu = new Menu(ItemsExtraMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Items extra");
	char sOption[32];
	char sOption2[32];
	char sOption3[32];
	
	char sBuff[32];
	char sBuff2[32];
	char sBuff3[32];
	
	int costoInf = bAllowGain ? (p.iLevel * INFI_COST) : 0;
	int costoAnt = bAllowGain ? (p.iLevel * ANTIDOTE_COST) : 0;
	int costoMad = bAllowGain ? (p.iLevel * MADNESS_COST) : 0;
	
	AddPoints(costoInf, sBuff, sizeof(sBuff));
	AddPoints(costoAnt, sBuff2, sizeof(sBuff2));
	AddPoints(costoMad, sBuff3, sizeof(sBuff3));
	
	FormatEx(sOption, sizeof(sOption), "Balas infinitas (%s AP)", sBuff);
	FormatEx(sOption2, sizeof(sOption2), "Antídoto (%s AP) %d/%d", sBuff2, p.iAntidotes, MAX_ANTIDOTES);
	FormatEx(sOption3, sizeof(sOption3), "Furia Zombie (%s AP) %d/%d", sBuff3, p.iMadness, MAX_MADNESS);
	
	
	menu.AddItem("0", sOption, (p.iExp >= costoInf && p.iType == view_as<int>(PT_HUMAN) && !p.bInfiniteAmmo) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("0", sOption2, (p.iAntidotes > 0 && p.iExp >= costoAnt && p.isType(PT_ZOMBIE) && ActualMode.bAntidoteAvailable) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("0", sOption3, (p.iMadness > 0 && p.iExp >= costoMad && p.isType(PT_ZOMBIE) && ActualMode.bZombieMadnessAvailable) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int ItemsExtraMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			buyExtraItem(player.id, selection);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}
public void buyExtraItem(int client, int selection){
	ZPlayer player = ZPlayer(client);
	
	switch(selection){
		case 0:{
			int cost = bAllowGain ? (player.iLevel * INFI_COST) : 0;
			if(player.iExp >= cost && player.iType == view_as<int>(PT_HUMAN)){
				player.bInfiniteAmmo = true;
				player.iExp -= cost;
				player.iInfBullBuyed++;
				PrintToChat(player.id, "%s \x0FBalas infinitas\x01 adquiridas!", SERVERSTRING);
			}
		}
		case 1:{
			if (!player.isType(PT_ZOMBIE)){
				PrintToChat(player.id, "%s No eres \x0FZombie\x01!", SERVERSTRING);
				return;
			}
				
			if (fnGetZombies() <= 1){
				PrintToChat(player.id, "%s El \x0Fúnico zombie\x01 no puede usar \x09antídoto\x01!", SERVERSTRING);
				return;
			}
				
			if(!IsPlayerAlive(player.id)){
				PrintToChat(player.id, "%s No estás \x0Fvivo\x01!", SERVERSTRING);
				return;
			}
			
			int cost = bAllowGain ? (player.iLevel*ANTIDOTE_COST) : 0;
			if(player.iAntidotes > 0 && player.iExp >= cost && player.iType == view_as<int>(PT_ZOMBIE) && ActualMode.bAntidoteAvailable){
				RemoveLasermines(player.id);
				
				// Stop crowd control effects
				ExtinguishEntity(player.id);
				Unfreeze(player.hFreezeTimer, player.id);
				
				// Turn into human
				player.Humanize(HUMAN);
				player.giveWeapons();
				
				// Apply costs
				player.iAntiBuyed++;
				player.iAntidotes--;
				player.iExp -= cost;
				
				PrintToChat(player.id, "%s Has utilizado \x0Fun antídoto\x01!", SERVERSTRING);
			}
		}
		case 2: {
			if (!player.isType(PT_ZOMBIE)){
				PrintToChat(player.id, "%s No eres \x0FZombie\x01!", SERVERSTRING);
				return;
			}
			
			if(!IsPlayerAlive(player.id)){
				PrintToChat(player.id, "%s No estás \x0Fvivo\x01!", SERVERSTRING);
				return;
			}
			
			int cost = bAllowGain ? (player.iLevel * MADNESS_COST) : 0;
			if(player.iMadness > 0 && player.iExp >= cost && player.isType(PT_ZOMBIE)){
				if (!ZombieMadnessBegin(client))
					return;
				
				player.iMadness--;
				player.iFuriBuyed++;
				player.iExp -= cost;
				
				PrintToChat(player.id, "%s Has utilizado \x0Ffuria zombie\x01!", SERVERSTRING);
			}
		
		}
	}
	checkLevelUp(client);
}

// WEAPONS MENUS
public Action showMenuWeapons(int client){
	ZPlayer player = ZPlayer(client);
	Menu menu = new Menu(MenuWeaponsHandler, MenuAction_Start|MenuAction_Select|MenuAction_End|MenuAction_DisplayItem);
	char weap[64];
	char pWeap[32];
	primaryWeaponsName.GetString(player.iWeaponPrimary, pWeap, 32);
	Format(weap, sizeof(weap), "Arma principal: %s", pWeap);
	
	
	menu.SetTitle("Menú de ARMAS");
	menu.AddItem("0", weap, ITEMDRAW_RAWLINE);
	menu.AddItem("1", "Armas primarias");
	menu.AddItem("2", "Armas secundarias");
	menu.AddItem("3", "Pack de granadas");
	menu.ExitButton = true;
	SetMenuPagination(menu, 8);
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuWeaponsHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 1: showInventarioPrimaryWeapons(client);
				case 2: showInventarioSecondaryWeapons(client);
				case 3: showGrenadePacks(client);
			}
		}
		
		case MenuAction_DisplayItem:{
			
		}
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// PRIMARY WEAPONS MENU
public Action showInventarioPrimaryWeapons(int client){
	ZPlayer p = ZPlayer(client);
	Menu menu = new Menu(MenuInventarioPrimaryWeaponsHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Selecciona tu arma primaria");
	for(int i; i < primaryWeaponsName.Length; i++) {
		ZWeaponPrimary weapon = ZWeaponPrimary(i);
		char nf[4];
		char op[64];
		char pWeap[32];
		IntToString(i, nf, 4);
		primaryWeaponsName.GetString(weapon.id, pWeap, 32);
		if(p.iLevel >= weapon.iLevel && p.iReset >= weapon.iReset){
			if (weapon.iReset)
				Format(op, 64, "%s (%drr)", pWeap, weapon.iReset);
			else
				Format(op, 64, "%s", pWeap);
			menu.AddItem(nf, op);
		}
		else if (p.iReset < weapon.iReset){
			Format(op, 64, "%s (Level %d | Reset %d)", pWeap, weapon.iLevel, weapon.iReset);
			menu.AddItem(nf, op, ITEMDRAW_DISABLED);
		}
		else{
			Format(op, 64, "%s (Level %d)", pWeap, weapon.iLevel);
			menu.AddItem(nf, op, ITEMDRAW_DISABLED);
		}
		
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuInventarioPrimaryWeaponsHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			char pWeap[32];
			ZPlayer player = ZPlayer(client);
			ZWeaponPrimary weapon = ZWeaponPrimary(selection);
			if(player.iLevel >= weapon.iLevel && player.iReset >= weapon.iReset){
				primaryWeaponsName.GetString(weapon.id, pWeap, 32);
				player.iNextWeaponPrimary = weapon.id;
				PrintToChat(player.id, "%s Tu arma \x09primaria\x01 cambiará cuando respawnees, y será: \x09%s\x01.", SERVERSTRING, pWeap);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// SECONDARY WEAPONS MENU
public Action showInventarioSecondaryWeapons(int client){
	ZPlayer p = ZPlayer(client);
	Menu menu = new Menu(MenuInventarioSecondaryWeaponsHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Selecciona tu arma secundaria");
	for(int i; i < secondaryWeaponsName.Length; i++) {
	ZWeaponSecondary weapon = ZWeaponSecondary(i);
	char nf[4];
	char op[64];
	char pWeap[32];
	IntToString(i, nf, 4);
	secondaryWeaponsName.GetString(weapon.id, pWeap, 32);
	if(p.iLevel >= weapon.iLevel && p.iReset >= weapon.iReset){
		Format(op, 64, "%s", pWeap);
		menu.AddItem(nf, op);
	}
	else{
		Format(op, 64, "%s | Level: %d", pWeap, weapon.iLevel);
		menu.AddItem(nf, op, ITEMDRAW_DISABLED);
	}
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuInventarioSecondaryWeaponsHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			char pWeap[32];
			ZPlayer player = ZPlayer(client);
			ZWeaponSecondary weapon = ZWeaponSecondary(selection);
			if(player.iLevel >= weapon.iLevel && player.iReset >= weapon.iReset){
				secondaryWeaponsName.GetString(weapon.id, pWeap, 32);
				player.iNextWeaponSecondary = weapon.id;
				PrintToChat(player.id, "%s Tu arma \x09secundaria\x01 cambiará cuando respawnees, y será: \x09%s\x01.", SERVERSTRING, pWeap);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// UPGRADES MENUS
public Action showMejorasMenu(int client){
	Menu menu = new Menu(MejorasMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	ZPlayer player = ZPlayer(client);
	char op1[128];
	Format(op1, 128, "Resetear | Level: %d", RESET_LEVEL, RESET_POINTS);
	menu.SetTitle("Menu de mejoras");
	menu.AddItem("0", "Mejoras Humanas");
	menu.AddItem("1", "Mejoras Zombies");
	menu.AddItem("","", ITEMDRAW_SPACER);
	menu.AddItem("2", "Mejoras Humanas Golden");
	menu.AddItem("3", "Mejoras Zombies Golden");
	menu.AddItem("5", op1, (player.iLevel >= RESET_LEVEL)  ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int MejorasMenuHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0: showMenuHMejoras(client);
				case 1: showMenuZMejoras(client);
				case 3: showMenuHGMejoras(client);
				case 4: showMenuZGMejoras(client);
				case 5: resetear(client);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// GOLDEN HUMAN UPGRADES MENU
public Action showMenuHGMejoras(int client){
	ZPlayer player = ZPlayer(client);
	
	char title[64];
	char opResistance[64];
	char opPenetration[64];
	char opDamage[64];
	char opDexterity[64];
	
	FormatEx(opResistance, 64, "Resistencia (HGPoints: %d) (%d / %d)", RoundToCeil((player.iHGResistanceLevel+1)/GOLDEN_SCALE), player.iHGResistanceLevel, GOLDEN_HUMAN_RESISTANCE_MAX);
	FormatEx(opPenetration, 64, "Penetracion (HGPoints: %d) (%d / %d)", RoundToCeil((player.iHGPenetrationLevel+1)/GOLDEN_SCALE), player.iHGPenetrationLevel, GOLDEN_HUMAN_PENETRATION_MAX);
	FormatEx(opDamage, 64, "Daño (HGPoints: %d) (%d / %d)", RoundToCeil((player.iHGDamageLevel+1)/GOLDEN_SCALE), player.iHGDamageLevel, GOLDEN_HUMAN_DAMAGE_MAX);
	FormatEx(opDexterity, 64, "Agilidad (HGPoints: %d) (%d / %d)", RoundToCeil((player.iHGDexterityLevel+1)/GOLDEN_SCALE), player.iHGDexterityLevel, GOLDEN_HUMAN_DEXTERITY_MAX);
	
	FormatEx(title, 64,"Mejoras Humanas Golden\nPuntos disponibles: %d", player.iHGoldenPoints);
	
	Menu menu = new Menu(MejorasHGMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle(title);
	
	menu.AddItem("0", opDamage);
	menu.AddItem("1", opPenetration);
	menu.AddItem("2", opResistance);
	menu.AddItem("3", opDexterity);
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int MejorasHGMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer p = ZPlayer (client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case H_DAMAGE: {
					if(p.iHGoldenPoints >= RoundToCeil((p.iHGDamageLevel+1)/GOLDEN_SCALE) && p.iHGDamageLevel < GOLDEN_HUMAN_DAMAGE_MAX) {
						p.iHGoldenPoints -= RoundToCeil((p.iHGDamageLevel+1)/GOLDEN_SCALE);
						p.iHGDamageLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>DAÑO GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHGDamageLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points Humanos para comprar esta mejora.");
				}
				case H_PENETRATION:{
					if(p.iHGoldenPoints >= RoundToCeil((p.iHGPenetrationLevel+1)/GOLDEN_SCALE) && p.iHGPenetrationLevel < GOLDEN_HUMAN_PENETRATION_MAX){
						p.iHGoldenPoints -= RoundToCeil((p.iHGPenetrationLevel+1)/GOLDEN_SCALE);
						p.iHGPenetrationLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>PENETRACION GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHGPenetrationLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points Humanos para comprar esta mejora.");
				}
				case H_RESISTANCE: {
					if(p.iHGoldenPoints >= RoundToCeil((p.iHGResistanceLevel+1)/GOLDEN_SCALE) && p.iHGResistanceLevel < GOLDEN_HUMAN_RESISTANCE_MAX) {
						p.iHGoldenPoints -= RoundToCeil((p.iHGResistanceLevel+1)/GOLDEN_SCALE);
						p.iHGResistanceLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>RESISTENCIA GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHGResistanceLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points para comprar esta mejora.");
				}
				case H_DEXTERITY: {
					if(p.iHGoldenPoints >= RoundToCeil((p.iHGDexterityLevel+1)/GOLDEN_SCALE) && p.iHGDexterityLevel < GOLDEN_HUMAN_DEXTERITY_MAX) {
						p.iHGoldenPoints -= RoundToCeil((p.iHGDexterityLevel+1)/GOLDEN_SCALE);
						p.iHGDexterityLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>AGILIDAD GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHGDexterityLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points Humanos para comprar esta mejora.");
				}
			}
			showMenuHGMejoras(client);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// GOLDEN ZOMBIE UPGRADES MENU
public Action showMenuZGMejoras(int client){
	ZPlayer player = ZPlayer(client);
	char title[64];
	char opHealth[64];
	char opResistance[64];
	char opDexterity[64];
	char opDamage[64];
	FormatEx(opHealth, 64, "Vida (ZGPoints: %d) (%d / %d)", RoundToCeil((player.iZGHealthLevel+1)/GOLDEN_SCALE), player.iZGHealthLevel, GOLDEN_ZOMBIE_HEALTH_MAX);
	FormatEx(opDamage, 64, "Daño (ZGPoints: %d) (%d / %d)", RoundToCeil((player.iZGDamageLevel+1)/GOLDEN_SCALE), player.iZGDamageLevel, GOLDEN_ZOMBIE_DAMAGE_MAX);
	FormatEx(opDexterity, 64, "Agilidad (ZGPoints: %d) (%d / %d)", RoundToCeil((player.iZGDexterityLevel+1)/GOLDEN_SCALE), player.iZGDexterityLevel, GOLDEN_ZOMBIE_DEXTERITY_MAX);
	FormatEx(opResistance, 64, "Resistencia (ZGPoints: %d) (%d / %d)", RoundToCeil((player.iZGResistanceLevel+1)/GOLDEN_SCALE), player.iZGResistanceLevel, GOLDEN_ZOMBIE_RESISTANCE_MAX);
	FormatEx(title, 64,"Mejoras Zombies Golden\nPuntos disponibles: %d", player.iZGoldenPoints);
	Menu menu = new Menu(MejorasZGMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle(title);
	
	menu.AddItem("0", opHealth);
	menu.AddItem("1", opResistance);
	menu.AddItem("2", opDamage);
	menu.AddItem("3", opDexterity);
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int MejorasZGMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer p = ZPlayer (client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case Z_HEALTH: {
					if(p.iZGoldenPoints >= RoundToCeil((p.iZGHealthLevel+1)/GOLDEN_SCALE) && p.iZGHealthLevel < GOLDEN_ZOMBIE_HEALTH_MAX){
						p.iZGoldenPoints -= RoundToCeil((p.iZGHealthLevel+1)/GOLDEN_SCALE);
						p.iZGHealthLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>VIDA GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZGHealthLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points Zombies para comprar esta mejora.");
				}
				case Z_RESISTANCE:{
					if(p.iZGoldenPoints >= RoundToCeil((p.iZGResistanceLevel+1)/GOLDEN_SCALE) && p.iZGResistanceLevel < GOLDEN_ZOMBIE_RESISTANCE_MAX){
						p.iZGoldenPoints -= RoundToCeil((p.iZGResistanceLevel+1)/GOLDEN_SCALE);
						p.iZGResistanceLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>RESISTENCIA GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZGResistanceLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points Zombies para comprar esta mejora.");
				}
				case Z_DAMAGE: {
					if(p.iZGoldenPoints >= RoundToCeil((p.iZGDamageLevel+1)/GOLDEN_SCALE) && p.iZGDamageLevel < GOLDEN_ZOMBIE_DAMAGE_MAX){
						p.iZGoldenPoints -= RoundToCeil((p.iZGDamageLevel+1)/GOLDEN_SCALE);
						p.iZGDamageLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>DAÑO GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZGDamageLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points Zombies para comprar esta mejora.");
				}
				case Z_DEXTERITY: {
					if(p.iZGoldenPoints >= RoundToCeil((p.iZGDexterityLevel+1)/GOLDEN_SCALE) && p.iZGDexterityLevel < GOLDEN_ZOMBIE_DEXTERITY_MAX){
						p.iZGoldenPoints -= RoundToCeil((p.iZGDexterityLevel+1)/GOLDEN_SCALE);
						p.iZGDexterityLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>AGILIDAD GOLDEN</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZGDexterityLevel);
					}
					else PrintHintText(client, "No tienes suficientes Golden Points Zombies para comprar esta mejora.");
				}
			}
			showMenuZGMejoras(client);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// COMMON HUMAN UPGRADES MENU
public Action showMenuHMejoras(int client){
	ZPlayer player = ZPlayer(client);
	char title[64];
	char opResistance[64];
	char opPenetration[64];
	char opDamage[64];
	char opDexterity[64];
	char opResetear[32];
	FormatEx(opResistance, 64, 	"Resistencia (HPoints: %d)(%d / %d)", 	(HUMAN_RESISTANCE_COST * (player.iHResistanceLevel+1)), player.iHResistanceLevel, MAX_HUMAN_RESISTANCE_LEVEL);
	FormatEx(opPenetration, 64, "Penetracion (HPoints: %d)(%d / %d)", 	(HUMAN_PENETRATION_COST * (player.iHPenetrationLevel+1)), player.iHPenetrationLevel, MAX_HUMAN_PENETRATION_LEVEL);
	FormatEx(opDamage, 64, 		"Daño (HPoints: %d)(%d / %d)", 			(HUMAN_DAMAGE_COST * (player.iHDamageLevel+1)), player.iHDamageLevel, MAX_HUMAN_DAMAGE_LEVEL);
	FormatEx(opDexterity, 64, 	"Agilidad (HPoints: %d)(%d / %d)", 		(HUMAN_DEXTERITY_COST * (player.iHDexterityLevel+1)), player.iHDexterityLevel, MAX_HUMAN_DEXTERITY_LEVEL);
	
	FormatEx(title, 64,"Mejoras Humanas\nPuntos disponibles: %d", player.iHPoints);
	FormatEx(opResetear, 32,"Reestablecer puntos (Costo: %d)", RESET_BONUS_COST);
	
	Menu menu = new Menu(MejorasHMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle(title);
	
	menu.AddItem("0", opDamage);
	menu.AddItem("1", opPenetration);
	menu.AddItem("2", opResistance);
	menu.AddItem("3", opDexterity);
	menu.AddItem("5", opResetear, player.iHPoints >= RESET_BONUS_COST ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int MejorasHMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer p = ZPlayer (client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case H_DAMAGE: {
					if(p.iHPoints >= (p.iHDamageLevel+1)*HUMAN_DAMAGE_COST && p.iHDamageLevel < MAX_HUMAN_DAMAGE_LEVEL){
						p.iHPoints -= (p.iHDamageLevel+1)*HUMAN_DAMAGE_COST;
						p.iHDamageLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>DAÑO</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHDamageLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points Humanos para comprar esta mejora.");
				}
				case H_PENETRATION:{
					if(p.iHPoints >= (p.iHPenetrationLevel+1)*HUMAN_PENETRATION_COST && p.iHPenetrationLevel < MAX_HUMAN_PENETRATION_LEVEL){
						p.iHPoints -= (p.iHPenetrationLevel+1)*HUMAN_PENETRATION_COST;
						p.iHPenetrationLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>PENETRACION</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHPenetrationLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points Humanos para comprar esta mejora.");
				}
				case H_RESISTANCE: {
					if(p.iHPoints >= (p.iHResistanceLevel+1)*HUMAN_RESISTANCE_COST && p.iHResistanceLevel < MAX_HUMAN_RESISTANCE_LEVEL){
						p.iHPoints -= (p.iHResistanceLevel+1)*HUMAN_RESISTANCE_COST;
						p.iHResistanceLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>RESISTENCIA</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHResistanceLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points para comprar esta mejora.");
				}
				case H_DEXTERITY: {
					if(p.iHPoints >= (p.iHDexterityLevel+1)*HUMAN_DEXTERITY_COST && p.iHDexterityLevel < MAX_HUMAN_DEXTERITY_LEVEL){
						p.iHPoints -= (p.iHDexterityLevel+1)*HUMAN_DEXTERITY_COST;
						p.iHDexterityLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>AGILIDAD</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iHDexterityLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points Humanos para comprar esta mejora.");
				}
				case H_RESET:{
					if(p.iHPoints >= RESET_BONUS_COST){
						p.iHPoints -= RESET_BONUS_COST;
						p.resetPoints();
					}
				}
			}
			showMenuHMejoras(client);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// COMMON ZOMBIE UPGRADES MENU
public Action showMenuZMejoras(int client){
	ZPlayer player = ZPlayer(client);
	char title[64];
	char opHealth[64];
	char opResistance[64];
	char opDexterity[64];
	char opDamage[64];
	char opResetear[32];
	FormatEx(opDamage, 64, "Daño (ZPoints: %d)(%d / %d)", ((player.iZDamageLevel +1) * ZOMBIE_DAMAGE_COST),player.iZDamageLevel, MAX_ZOMBIE_DAMAGE_LEVEL);
	FormatEx(opHealth, 64, "Vida (ZPoints: %d)(%d / %d)", ((player.iZHealthLevel+1) * ZOMBIE_HEALTH_COST),player.iZHealthLevel, MAX_ZOMBIE_HEALTH_LEVEL);
	FormatEx(opResistance, 64, "Resistencia (ZPoints: %d)(%d / %d)", ((player.iZResistanceLevel+1) * ZOMBIE_RESISTANCE_COST),player.iZResistanceLevel, MAX_ZOMBIE_RESISTANCE_LEVEL);
	FormatEx(opDexterity, 64, "Agilidad (ZPoints: %d)(%d / %d)", ((player.iZDexterityLevel+1) * ZOMBIE_DEXTERITY_COST),player.iZDexterityLevel, MAX_ZOMBIE_DEXTERITY_LEVEL);
	FormatEx(title, 64,"Mejoras Zombie\nPuntos disponibles: %d", player.iZPoints);
	FormatEx(opResetear, 32,"Reestablecer puntos (Costo: %d)", RESET_BONUS_COST);
	
	Menu menu = new Menu(MejorasZMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle(title);
	
	menu.AddItem("0", opHealth);
	menu.AddItem("1", opResistance);
	menu.AddItem("2", opDamage);
	menu.AddItem("3", opDexterity);
	menu.AddItem("5", opResetear, player.iZPoints >= RESET_BONUS_COST ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}
public int MejorasZMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer p = ZPlayer (client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case Z_HEALTH: {
					if(p.iZPoints >= (p.iZHealthLevel+1)*ZOMBIE_HEALTH_COST && p.iZHealthLevel < MAX_ZOMBIE_HEALTH_LEVEL){
						p.iZPoints -= (p.iZHealthLevel+1)*ZOMBIE_HEALTH_COST;
						p.iZHealthLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>VIDA</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZHealthLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points Zombies para comprar esta mejora.");
				}
				case Z_RESISTANCE:{
					if(p.iZPoints >= (p.iZResistanceLevel+1)*ZOMBIE_RESISTANCE_COST && p.iZResistanceLevel < MAX_ZOMBIE_RESISTANCE_LEVEL){
						p.iZPoints -= (p.iZResistanceLevel+1)*ZOMBIE_RESISTANCE_COST;
						p.iZResistanceLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>RESISTENCIA</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZResistanceLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points Zombies para comprar esta mejora.");
				}
				case Z_DAMAGE: {
					if(p.iZPoints >= (p.iZDamageLevel+1)*ZOMBIE_DAMAGE_COST && p.iZDamageLevel < MAX_ZOMBIE_DAMAGE_LEVEL){
						p.iZPoints -= (p.iZDamageLevel+1)*ZOMBIE_DAMAGE_COST;
						p.iZDamageLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>DAÑO</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZDamageLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points Zombies para comprar esta mejora.");
				}
				case Z_DEXTERITY: {
					if(p.iZPoints >= (p.iZDexterityLevel+1)*ZOMBIE_DEXTERITY_COST && p.iZDexterityLevel < MAX_ZOMBIE_DEXTERITY_LEVEL){
						p.iZPoints -= (p.iZDexterityLevel+1)*ZOMBIE_DEXTERITY_COST;
						p.iZDexterityLevel++;
						PrintHintText(client, "Nivel de <font size='25' color='#FF0000'>AGILIDAD</font> mejorado al nivel <font size='25' color='#FF0000'>%d</font>", p.iZDexterityLevel);
					}
					else PrintHintText(client, "No tienes suficientes Points Zombies para comprar esta mejora.");
				}
				case Z_RESET:{
					if(p.iZPoints >= RESET_BONUS_COST){
						p.iZPoints -= RESET_BONUS_COST;
						p.resetPoints(true);
					}
				}
			}
			showMenuZMejoras(client);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// CLASSES MENUS
public Action showMenuClasses(int client){
	Menu menu = new Menu(MenuClassesHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Menú de CLASES");
	menu.AddItem("1", "Clases Zombie");
	//menu.AddItem("2", "Clases Humanas");
	menu.AddItem("2", "Alineación Zombie");
	menu.AddItem("3", "Alineación Humana");
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuClassesHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0:{
					showMenuClassesZombie(client);
				}
				/*case 1:{
					showMenuClassesHumana(client);
				}*/
				case 1:{
					showMenuZombieAlignment(client);
				}
				case 2:{
					showMenuHumanAlignment(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// ZOMBIE CLASSES MENU
public Action showMenuClassesZombie(int client){
	Menu menu = new Menu(MenuClassesZombieHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	ZPlayer player = ZPlayer(client);
	menu.SetTitle("Selecciona tu clase ZOMBIE");
	for(int i; i < gZClassName.Length; i++) {
		ZClass class = ZClass(i);
		char nf[4];
		char op[64];
		char sName[32];
		IntToString(i, nf, 4);
		gZClassName.GetString(i, sName, 32);
		if(player.iLevel >= class.iLevelReq && player.iReset >= class.iResetReq){
			FormatEx(op, 64, "%s", sName);
			menu.AddItem(nf, op);
		}
		else {
			FormatEx(op, 64, "%s | Lvl: [%d] | Reset: [%d]", sName, class.iLevelReq, class.iResetReq);
			menu.AddItem(nf, op, ITEMDRAW_DISABLED);
		}
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuClassesZombieHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			ZPlayer player = ZPlayer(client);
			char sName[32];
			player.iNextZombieClass = selection;
			gZClassName.GetString(selection, sName, 32);
			PrintToChat(player.id, "%s Tu \x09clase zombie\x01 cambiará cuando respawnees, y será: \x09%s\x01.", SERVERSTRING, sName);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// HUMAN ALIGNMENT MENU
public Action showMenuHumanAlignment(int client){
	Menu menu = new Menu(MenuHumanAlignmentHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Selecciona tu alineación HUMANA");
	for(int i; i < gHAlignmentName.Length; i++) {
		char op[96];
		char sName[32];
		char sDescription[64];
		gHAlignmentName.GetString(i, sName, sizeof(sName));
		gHAlignmentDesc.GetString(i, sDescription, sizeof(sDescription));
		// Simply format the fkin menu
		FormatEx(op, 64, "%s (%s)", sName, sDescription);
		menu.AddItem("", op);
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuHumanAlignmentHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			ZPlayer player = ZPlayer(client);
			char sName[32];
			player.iNextHumanAlignment = selection;
			gHAlignmentName.GetString(selection, sName, 32);
			PrintToChat(player.id, "%s Tu \x09ALINEACIÓN HUMANA\x01 cambiará cuando respawnees, y será: \x09%s\x01.", SERVERSTRING, sName);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// ZOMBIE ALIGNMENT MENU
public Action showMenuZombieAlignment(int client){
	Menu menu = new Menu(MenuZombieAlignmentHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Selecciona tu alineación ZOMBIE");
	for(int i; i < gZAlignmentName.Length; i++) {
		char op[96];
		char sName[32];
		char sDescription[64];
		gZAlignmentName.GetString(i, sName, sizeof(sName));
		gZAlignmentDesc.GetString(i, sDescription, sizeof(sDescription));
		
		// Simply format the fkin menu
		FormatEx(op, 64, "%s (%s)", sName, sDescription);
		menu.AddItem("", op);
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuZombieAlignmentHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			ZPlayer player = ZPlayer(client);
			char sName[32];
			player.iNextZombieAlignment = selection;
			gZAlignmentName.GetString(selection, sName, 32);
			PrintToChat(player.id, "%s Tu \x09ALINEACIÓN ZOMBIE\x01 cambiará cuando respawnees, y será: \x09%s\x01.", SERVERSTRING, sName);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// LOGIN MENU
public Action showLoginMenu(int client){
	Menu menu = new Menu(LoginMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Menú de REGISTRO");
	menu.AddItem("0", "Iniciar Sesion");
	menu.AddItem("1", "Registrarse");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int LoginMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			//player.bInUser = true;
			switch(selection){
				case 0: loginPlayer(player.id, true);//showIndicacionesLogin(client);
				case 1: {
					player.bInUser = true;
					showIndicacionesRegister(client, false);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// SECOND LOGIN MENU
public Action showIndicacionesLogin(int client, bool canAccept){
	Menu menu = new Menu(IndLoginMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Para iniciar sesion, debes escribir en\nel chat tu email sin espacios.\nLuego pulsa siguiente..");
	menu.AddItem("0", "Siguiente...", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndLoginMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0: {
					player.bInUser = false;
					player.bInPassword = true;
					showIndicacionesPassword(client);
				}
				case 1: {
					player.bInUser = false;
					showLoginMenu(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// THIRD LOGIN MENU
public Action showIndicacionesPassword(int client){
	Menu menu = new Menu(IndLoginPasswordMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("..Ahora debes, de la misma manera,\ningresar tu contraseña.\nLuego pulsa aceptar.");
	menu.AddItem("0", "Aceptar");
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndLoginPasswordMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0:{
					player.bInPassword = false;
					loginPlayer(client, false);
				}
				
				case 1:{
					player.bInPassword = false;
					showLoginMenu(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// REGISTER MENU
public Action showIndicacionesRegister(int client, bool canAccept){
	Menu menu = new Menu(IndRegisterMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Para registrarte, debes escribir en\nel chat tu email sin espacios.\nLuego pulsa siguiente..");
	menu.AddItem("0", "Siguiente...", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndRegisterMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					player.bInUser = false;
					player.bInPassword = true;
					showIndicacionesPasswordRegister(client, false);
				}
				case 1: {
					player.bInUser = false;
					showLoginMenu(client);
				}
				
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// SECOND REGISTER MENU
public Action showIndicacionesPasswordRegister(int client, bool canAccept){
	Menu menu = new Menu(IndRegisterPassMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("..Ahora debes, de la misma manera,\ningresar tu contraseña.\nLuego pulsa aceptar.");
	menu.AddItem("0", "Aceptar", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndRegisterPassMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					player.bInPassword = false;
					registerPlayer(client);
				}
				case 1:{
					player.bInPassword = false;
					showLoginMenu(client);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// ACCOUNT MENU
public Action showMenuCuenta(int client, bool noCharacters){
	char title[64];
	char pName[32];
	GetClientName(client, pName, sizeof(pName));
	Format(title, sizeof(title), "Bienvenido %s", pName);
	
	Menu menu = new Menu(CuentaMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle(title);
	if(!noCharacters){
		for(int i; i < numCharacters[client]; i++){
			char name[32];
			char op[128];
			char caca[4];
			IntToString(i, caca, 4);
			charactersNames[client].GetString(i, name, 32);
			Format(op, 128, "%s (Nivel: %d | Resets: %d)", name, charactersLevels[client].Get(i), charactersResets[client].Get(i));
			menu.AddItem(caca, op);
		}
		if((MAXCHARACTERS-numCharacters[client] > 0)){
			for(int j; j <(5-numCharacters[client]); j++){
				menu.AddItem("crear", "Crear personaje");
			}
		}
	} else {
		for(int i; i < MAXCHARACTERS; i++){
			menu.AddItem("crear", "Crear personaje");
		}
	}
	menu.ExitButton = false;
	
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int CuentaMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			if(player.isSlotEmpty(selection)){
				player.bInCreatingCharacter = true;
				showCrearCharacter(client, false);
			} else {
				player.iPjEnMenu = selection;//player.getIdPjInSlot(selection);
				showMenuCharacter(client);
				/*
				loadCharacterData(client, player.getIdPjInSlot(selection));
				player.bInCreatingCharacter = false;
				player.bInGame = true;
				*/
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// CHARACTERS MENU
public Action showMenuCharacter(int client){
	Menu menu = new Menu(IndCrearCharacterMenuHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	ZPlayer p = ZPlayer(client);
	char title[64];
	char name[32];
	charactersNames[client].GetString(p.iPjEnMenu, name, 32);
	Format(title, 64, "Personaje: %s", name);
	menu.SetTitle(title);
	menu.AddItem("0", "Jugar");
	menu.AddItem("1", "Borrar");
	menu.AddItem("2", "Atrás");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int IndCrearCharacterMenuHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					player.iPjSeleccionado = player.getIdPjInSlot(player.iPjEnMenu);
					loadCharacterData(client, player.iPjSeleccionado);
					player.bInCreatingCharacter = false;
					player.bInGame = true;
				}
				case 1: showConfirmacionBorrar(client);
				case 2:{
					player.iPjEnMenu = -1;
					loginPlayer(player.id, true);
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// DELETE CONFIRM MENU
public Action showConfirmacionBorrar(int client){
	Menu menu = new Menu(confirmarBorrarHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	ZPlayer p = ZPlayer(client);
	char title[128];
	char name [32];
	charactersNames[client].GetString(p.iPjEnMenu, name, 32);
	Format(title, 128, "Estás seguro que deseas borrar el personaje %s?\n(No podrás recuperarlo)", name);
	
	menu.SetTitle(title);
	menu.AddItem("0", "Sí");
	menu.AddItem("1", "No");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int confirmarBorrarHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					deleteCharacter(client);
					player.setSlotEmpty(player.iPjEnMenu, true);
					player.iPjEnMenu = -1;
					loginPlayer(client, true);
					PrintHintText(client, "Personaje eliminado con exito");
				}
				case 1: showMenuCharacter(client);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// CREATE CHARACTER MENU
public Action showCrearCharacter(int client, bool canAccept){
	Menu menu = new Menu(crearCharacterHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Debes ingresar por chat el nombre\nque deseas para tu personaje.");
	menu.AddItem("0", "Aceptar", canAccept ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	menu.AddItem("1", "Cancelar");
	menu.ExitButton = false;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int crearCharacterHandler(Menu menu, MenuAction action, int client, int selection){
	ZPlayer player = ZPlayer(client);
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection) {
				case 0: {
					player.bInCreatingCharacter = false;
					createCharacter(client);
				}
				case 1: loadCharacters(client);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// PARTY MENU
public Action showMenuParty(int client, int args){
	ZPlayer player = ZPlayer(client);
	
	if (!player.bStaff){
		PrintToChat(player.id, "%s Party en desarrollo.", SERVERSTRING);
		return Plugin_Handled;
	}
	
	/*if (fnGetPlaying() < PLAYERS_TO_GAIN){
		PrintToChat(player.id, "%s No hay suficientes usuarios para utilizar party.", SERVERSTRING);
		return Plugin_Handled;
	}*/
	
	char buffer[128], names[32], buf[8];

	Menu menu = new Menu(MenuPartyHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Enviar invitación de party (BETA)");
	for(int i = 1; i < MaxClients; i++){
		ZPlayer p = ZPlayer(i);
		if(!IsPlayerExist(i, true) || i == client) continue;
		if(!p.bLogged) continue;
		if(p.isInParty()) continue;
		
		// obtains possible targets name
		GetClientName(i, names, sizeof(names));
		
		FormatEx(buffer, sizeof(buffer), "%s (Level: %d)", names, p.iLevel);
		
		//obtains it's id and transform it to int
		IntToString(i, buf, sizeof(buf));
		
		//lets put it in the menu
		menu.AddItem(buf, buffer);
	}
	
	menu.ExitButton = true;
	
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuPartyHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			// Buffer for info item selected
			char inv[32];
			
			//Buffer for id invited
			int invited;
			
			//obtains id of invited as string
			menu.GetItem(selection, inv, sizeof(inv));
			
			//Converts to int
			StringToIntEx(inv, invited);
			ZPlayer target = ZPlayer(invited);
			if(IsPlayerExist(invited)){
				char sender[32];
				char invitedName[32];
				
				//obtains sender name
				GetClientName(client, sender, sizeof(sender));
				
				//obtains invited name
				GetClientName(invited, invitedName, sizeof(invitedName));
				if(target.bRecivePartyInv && !target.isInParty()){
					showMenuInvParty(invited, client);
				}
				else if(target.isInParty()){
					PrintToChat(client, "%s %s Ya está en party", SERVERSTRING, invitedName);
				}
				else
					PrintToChat(client, "%s %s tiene las invitaciones de party \x09desactivadas\x01.", SERVERSTRING, invitedName);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// PARTY INVITATIONS MENU
public Action showMenuInvParty(int client, int senderid){
	char buffer[32];
	char sender[8];
	char title [128];
	ZPlayer p = ZPlayer(client);
	if(p.isInParty()){
		PrintToChat(senderid, "%s Este usuario ya esta en party.", SERVERSTRING);
		return Plugin_Handled;
	}
	
	GetClientName(senderid, buffer, sizeof(buffer));
	IntToString(senderid, sender, sizeof(sender));
	FormatEx(title, sizeof(title), "Aceptar invitación de party de: %s", buffer);
	Menu menu = new Menu(MenuInvPartyHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle(title);
	menu.AddItem(sender, "Sí");
	menu.AddItem(sender, "No");
	menu.ExitButton = true;
	
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuInvPartyHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			char sender[32];
			int senderid;
			menu.GetItem(selection, sender, sizeof(sender));
			StringToIntEx(sender, senderid);
			if(IsPlayerExist(client) && IsPlayerExist(senderid)){
			
				char senderName[32];
				char clientName[32];
				
				GetClientName(client, clientName, sizeof(clientName));
				GetClientName(senderid, senderName, sizeof(senderName));
				bool created = false;
				bool added = false;
				
				switch(selection){
					case 0:{
						ZPlayer senderp = ZPlayer(senderid);
						ZPlayer invitado = ZPlayer(client);
						if(!invitado.isInParty()){
							if(senderp.isInParty()) {
								ZParty pt = ZParty(findPartyByUID(senderp.iPartyUID));
								added = pt.addMember(invitado.id);
								
							}
							else{
								created = CreateParty(senderid, client);
							}
							PrintToChat(client, "%s Aceptaste la invitación de party de \x09%s\x01.", SERVERSTRING, senderName);
							PrintToChat(senderid, "%s \x09%s\x01 aceptó tu invitación de party.", SERVERSTRING, clientName);
						} else{
							PrintToChat(client, "%s No se pudo crear la party.", SERVERSTRING);
						}
						if(added){
							PrintToChatAll("Added");
							FinishCombo(invitado.hComboHandle,invitado.id);
							ZParty pt = ZParty(findPartyByUID(invitado.iPartyUID));
							FinishComboParty(pt.hComboHandle, invitado.id);
						}
						if(created){
							PrintToChatAll("Created");
							FinishCombo(invitado.hComboHandle,invitado.id);
							FinishCombo(senderp.hComboHandle,invitado.id);
						}
					}
					case 1:{
						PrintToChat(client, "%s Rechazaste la invitación de party de \x09%s\x01.", SERVERSTRING, senderName);
						PrintToChat(senderid, "%s \x09%s\x01 rechazó tu invitación de party.", SERVERSTRING, clientName);
					}
				}
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action leaveParty(int client, int args){
	ZPlayer player = ZPlayer(client);
	ZParty p = ZParty(gPartys.FindValue(player.iPartyUID));
	if(player.isInParty()){
		if(p.length()-1 <= 1) FinishComboParty(p.hComboHandle, client);
		p.removeMember(player.id);
	}
	else PrintToChat(client, "%s No estas en party.", SERVERSTRING);
	return Plugin_Handled;
}

// CONFIGURATIONS MENU
public Action showMenuConfigs(int client){
	ZPlayer player = ZPlayer(client);
	Menu menu = new Menu(MenuCfgsHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	
	char autopbuy[64], autosbuy[64], autogbuy[64], autozclass[64], recive[64], bullets[64];
	
	FormatEx(autopbuy, sizeof(autopbuy), "Auto cambio de arma Primaria: %s", player.bAutoPrimaryBuy ? "Activado" : "Desactivado");
	FormatEx(autosbuy, sizeof(autosbuy), "Auto cambio de arma Secundaria: %s", player.bAutoSecondaryBuy ? "Activado" : "Desactivado");
	FormatEx(autogbuy, sizeof(autogbuy), "Auto cambio de Pack de granadas: %s", player.bAutoGrenadeBuy ? "Activado" : "Desactivado");
	FormatEx(autozclass, sizeof(autozclass), "Auto cambio de clase Zombie: %s", player.bAutoZClass ? "Activado" : "Desactivado");
	FormatEx(bullets, sizeof(bullets), "Silenciar balas ajenas: %s", player.bStopSound ? "Activado" : "Desactivado");
	FormatEx(recive, sizeof(recive), "Recibir inv. party: %s", player.bRecivePartyInv ?  "Activado" : "Desactivado");
	
	menu.SetTitle("Menú de AJUSTES");
	menu.AddItem("a", autopbuy);
	menu.AddItem("b", autosbuy);
	menu.AddItem("c", autogbuy);
	menu.AddItem("d", autozclass);
	menu.AddItem("e", bullets);
	menu.AddItem("f", recive);
	
	menu.ExitButton = true;
	
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuCfgsHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			switch(selection){
				case 0: autopBuy(client);
				case 1: autosBuy(client);
				case 2: autogBuy(client);
				case 3: autozClass(client);
				case 4: muteBullets(client, 0);
				case 5: changeReciveOp(client, 0);
			}
			showMenuConfigs(client);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

public Action changeReciveOp(int client, int args){
	ZPlayer player = ZPlayer(client);
	player.bRecivePartyInv = !player.bRecivePartyInv;
	PrintToChat(player.id, "\x01\x0D%s\x01 las invitaciones de party", player.bRecivePartyInv ? "Activaste" : "Desactivaste");
	
	return Plugin_Handled;
}

// TOP MENU
public Action showTop50(int client, int args){	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	/* SQL QUERY */
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT nombre, c.level, c.reset FROM Characters c ORDER BY c.reset DESC, c.level DESC LIMIT 60", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Didn't execute query");
		
		delete hUserStmt;
		delete db;
		
		return Plugin_Handled;
	}
	/* --------*/
	
	int rows = SQL_GetRowCount(hUserStmt);
	
	char nfo[255];
	char nam[64];
	
	Menu menu = new Menu(Top50Handler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Top 60 jugadores:");
	for(int i; i < rows; i++) {
		if(SQL_FetchRow(hUserStmt)){
			SQL_FetchString(hUserStmt, 0, nam, sizeof(nam));
			FormatEx(nfo, sizeof(nfo), "%d)%s [R: %d]|[L: %d]", i+1, nam, SQL_FetchInt(hUserStmt, 2), SQL_FetchInt(hUserStmt, 1));
			menu.AddItem("", nfo, ITEMDRAW_DISABLED);
		}
	}
	menu.ExitButton = true;
	
	delete hUserStmt;
	delete db;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int Top50Handler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// STATISTICS MENU
public Action showMenuEstadisticas(int client, int args){
	ZPlayer player = ZPlayer(client);
	Menu menu = new Menu(MenuEstadisticasHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	
	char hKills[32], headshots[32], zKills[32], nemeKills[32], surviKills[32], assaKills[32], gunsKills[32], sniperKills[32], infections[32], grenInfections[32], infBullBought[32], antiBought[32], furiBought[32]; 
	
	FormatEx(hKills, sizeof(hKills), "Humanos asesinados: %d", player.iHKills);
	FormatEx(headshots, sizeof(headshots), "Headshots: %d", player.iHeadshots);
	FormatEx(zKills, sizeof(zKills), "Zombies asesinados: %d", player.iZKills);
	FormatEx(nemeKills, sizeof(nemeKills), "Nemesis asesinados: %d", player.iNemeKills);
	FormatEx(surviKills, sizeof(surviKills), "Survivors asesinados: %d", player.iSurviKills);
	FormatEx(assaKills, sizeof(assaKills), "Assassins asesinados: %d", player.iAssaKills);
	FormatEx(gunsKills, sizeof(gunsKills), "Gunslingers asesinados: %d", player.iGunsKills);
	FormatEx(sniperKills, sizeof(sniperKills), "Snipers asesinados: %d", player.iSnipKills);
	FormatEx(infections, sizeof(infections), "Infecciones: %d", player.iInfections);
	FormatEx(grenInfections, sizeof(grenInfections), "Infeciones con granada: %d", player.iGrenadeInfection);
	FormatEx(infBullBought, sizeof(infBullBought), "Balas infinitas compradas: %d", player.iInfBullBuyed);
	FormatEx(antiBought, sizeof(antiBought), "Antidotos comprados: %d", player.iAntiBuyed);
	FormatEx(furiBought, sizeof(furiBought), "Furias compradas: %d", player.iFuriBuyed);
	
	menu.SetTitle("Menú de Estadisticas");
	menu.AddItem("a", zKills, ITEMDRAW_DISABLED);
	menu.AddItem("a", hKills, ITEMDRAW_DISABLED);
	menu.AddItem("a", headshots, ITEMDRAW_DISABLED);
	menu.AddItem("a", infections, ITEMDRAW_DISABLED);
	menu.AddItem("a", grenInfections, ITEMDRAW_DISABLED);
	menu.AddItem("a", nemeKills, ITEMDRAW_DISABLED);
	menu.AddItem("a", surviKills, ITEMDRAW_DISABLED);
	menu.AddItem("a", assaKills, ITEMDRAW_DISABLED);
	menu.AddItem("a", gunsKills, ITEMDRAW_DISABLED);
	menu.AddItem("a", sniperKills, ITEMDRAW_DISABLED);
	menu.AddItem("a", infBullBought, ITEMDRAW_DISABLED);
	menu.AddItem("a", antiBought, ITEMDRAW_DISABLED);
	menu.AddItem("a", furiBought, ITEMDRAW_DISABLED);
	
	menu.ExitButton = true;
	
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuEstadisticasHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// ACHIEVEMENTS MENU
public Action showMenuLogros(int client){
	//ZPlayer p = ZPlayer(client);
	Menu menu = new Menu(MenuLogrosHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Menu de Logros");
	char nam[63], desc[127], op[255];
	for(int i; i < gLNames.Length; i++) {
		gLNames.GetString(i, nam, sizeof(nam));
		gLDesc.GetString(i, desc, sizeof(desc));
		Format(op, sizeof(op), "%s - %s", nam, desc);
		if(canHaveLogro(client, i)){
			if(!hasLogroOfId(client, i)) menu.AddItem("", op, ITEMDRAW_DEFAULT);
			else menu.AddItem("", "RECLAMADO", ITEMDRAW_DISABLED);
		} else{
			menu.AddItem("", op, ITEMDRAW_DISABLED);
		}
		
		
	}
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuLogrosHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			showMenuDetalleLogro(client, selection);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// DETAILED ACHIEVEMENT MENU
public Action showMenuDetalleLogro(int client, int logro){
	Menu menu = new Menu(MenuDetalleLogrosHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	char nlogro[64],titulo[128], l[8];
	gLNames.GetString(logro, nlogro, sizeof(nlogro));
	bool hasLogro = hasLogroOfId(client, logro);
	Format(titulo, sizeof(titulo), "Logro: %s %s", nlogro, hasLogro ?  "(Desbloqueado)" : "");
	
	IntToString(logro,l, sizeof(l));
	
	menu.SetTitle(titulo);
	menu.AddItem(l, "Reclamar", hasLogro ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuDetalleLogrosHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			char l[8];
			int logro;
			menu.GetItem(selection, l, sizeof(l));
			StringToIntEx(l, logro);
			if(!hasLogroOfId(client, logro)){
				if(canHaveLogro(client, logro)) giveLogroTo(client, logro);
				else PrintToChat(client, "%s No cumples los requisitos para este logro.", SERVERSTRING);
			}else{
				PrintToChat(client, "%s Ya tienes este logro.", SERVERSTRING);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

// GREANDE PACKS MENU
public Action showGrenadePacks(int client){
	ZPlayer p = ZPlayer(client);
	Menu menu = new Menu(MenuGrenadePackHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);
	menu.SetTitle("Selecciona tu pack de granadas");
	for(int i; i < gGrenadePackLevel.Length; i++) {
		ZGrenadePack gp = ZGrenadePack(i);
		char nf[4];
		char fire[16], ice[16], light[16], aura[16], op[128];
		IntToString(i, nf, 4);
		if(gp.hasGrenade(FIRE_GRENADE)){
			Format(fire, sizeof(fire), "Fuego: %d", gp.getGrenadeCount(FIRE_GRENADE));
		} 
		if(gp.hasGrenade(FREEZE_GRENADE)){
			Format(ice, sizeof(ice), "Hielo: %d", gp.getGrenadeCount(FREEZE_GRENADE));
		} 
		if(gp.hasGrenade(LIGHT_GRENADE)){
			Format(light, sizeof(light), "Luz: %d", gp.getGrenadeCount(LIGHT_GRENADE));
		} 
		if(gp.hasGrenade(AURA_GRENADE)){
			Format(aura, sizeof(aura), "Aura: %d", gp.getGrenadeCount(AURA_GRENADE));
		}
		
		Format(op, sizeof(op), "Pack %d:\n%s\n%s\n%s\n%s", i, fire, ice, light, aura);
		menu.AddItem(nf, op, (gp.iLevel <= p.iLevel && gp.iReset <= p.iReset) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	}
	menu.ExitButton = true;
	menu.Pagination = 2;
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int MenuGrenadePackHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			ZPlayer player = ZPlayer(client);
			ZGrenadePack pack = ZGrenadePack(selection);
			
			if(player.iLevel >= pack.iLevel && player.iReset >= pack.iReset){
				player.iNextGrenadePack = pack.id;
				PrintToChat(player.id, "%s Pack %d de granadas \x09exitosamente seleccionado\x01! ", SERVERSTRING, selection);
			}
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}

//=====================================================
//				ACHIEVEMENTS
//=====================================================
public bool canHaveLogro(int client, int logro){
	ZPlayer p = ZPlayer(client);
	ZLogro l = ZLogro(logro);
	return p.iHKills >= l.HKills && 
	p.iZKills >= l.ZKills &&
	p.iZCuchiKills >= l.ZCuchiKills &&
	p.iNemeKills >= l.NemeKills &&
	p.iSurviKills >= l.SurviKills &&
	p.iAssaKills >= l.AssaKills &&
	p.iSnipKills >= l.SniperKills &&
	p.iGunsKills >= l.GunslingerKills &&
	p.iHeadshots >= l.HeadKills &&
	p.iInfections >= l.Infections &&
	p.iGrenadeInfection >= l.GrenadeInfections &&
	p.iInfBullBuyed >= l.InfiBulletsBuyed &&
	p.iAntiBuyed >= l.AntidotesBuyed &&
	p.iFuriBuyed >= l.MadnessBuyed;
}
public Action giveLogroTo(int client, int logro){
	ZLogro l = ZLogro(logro);
	char desc[128], nombre[128];
	gLNames.GetString(logro, nombre, sizeof(nombre));
	gLDesc.GetString(logro, desc, sizeof(desc));
	
	PrintToChat(client, "%s Logro desbloqueado \x09%s\x01! Has ganado \x05%s\x01", SERVERSTRING, nombre, desc);

	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	ZPlayer p = ZPlayer(client);
	
	
	/* Check if we haven't already created the statement */
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "INSERT INTO LogrosXCharacter(idCharacter, idLogro) VALUES(?,?)", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	
	SQL_BindParamInt(hUserStmt, 0, p.iPjSeleccionado, false);
	SQL_BindParamInt(hUserStmt, 1, logro+1, false);
	if (!SQL_Execute(hUserStmt)){
		char error3[255];
		SQL_GetError(hUserStmt, error3, sizeof(error3));
		PrintToServer(error3);
		
		delete hUserStmt;
		delete db;
		
		return Plugin_Handled;
	}
	

	
	
	p.iHPoints += l.HumanPoints;
	p.iZPoints += l.ZombiePoints;
	p.iHGoldenPoints += l.HumanGPoints;
	p.iZGoldenPoints += l.ZombieGPoints;
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public bool hasLogroOfId(int clientId, int logroId){
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	bool ret = false;
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	ZPlayer p = ZPlayer(clientId);
	
	
	/* Check if we haven't already created the statement */
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT * FROM LogrosXCharacter WHERE idLogro=? AND idCharacter=?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return ret;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, logroId+1, false);
	SQL_BindParamInt(hUserStmt, 1, p.iPjSeleccionado, false);
	if (!SQL_Execute(hUserStmt)){
		delete hUserStmt;
		delete db;
		
		return ret;
	}
	if(SQL_GetRowCount(hUserStmt)>0){
		ret = true;
	}
	
	delete hUserStmt;
	delete db;
	return ret;
}

//=====================================================
//				SQL MAIN DATA MANAGEMENT
//=====================================================
public Action registerPlayer(int client){
	int steam_id = GetSteamAccountID(client);
	char mail[32];
	char pass[32];
	users.GetString(client, mail, 32);
	passwords.GetString(client, pass, 32);
		
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (isEntryValidToSQL(mail, DATA_MAIL) != INPUT_OK){
		PrintToServer("Email invalido, vuelve a intentar..");
		showLoginMenu(client);
		
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	
	/* Check if we haven't already created the statement */
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "INSERT INTO Players(email, contrasenia, steamid) VALUES(?, ?, ?);", error2, sizeof(error2));

		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	SQL_BindParamString(hUserStmt, 0, mail, false);
	SQL_BindParamString(hUserStmt, 1, pass, false);
	SQL_BindParamInt(hUserStmt, 2, steam_id, false);
	if (!SQL_Execute(hUserStmt)){
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	
	PrintHintText(client, "Registrado correctamente como\nMail: %s\nContraseña: %s ", mail, pass);
	showLoginMenu(client);
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public Action loginPlayer(int client, bool autolog){
	int steam_id = GetSteamAccountID(client);
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if(autolog){
		if (hUserStmt == null){
			char error2[255];
			hUserStmt = SQL_PrepareQuery(db, "SELECT P.steamid FROM Players P WHERE P.steamid=?", error2, sizeof(error2));
			if (hUserStmt == null){
				PrintToServer(error2);
				
				delete hUserStmt;
				delete db;
				
				return Plugin_Handled;
			}
		}
		SQL_BindParamInt(hUserStmt, 0, steam_id, false);
		if (!SQL_Execute(hUserStmt)){
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
		GetValues(client, hUserStmt, true);
	}
	else {
		char mail[32];
		char pass[32];
		users.GetString(client, mail, 32);
		passwords.GetString(client, pass, 32);
		if(isEntryValidToSQL(mail, DATA_MAIL) != INPUT_OK){
			PrintHintText(client, "Email invalido, vuelve a intentar...");
			loadCharacters(client);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
		
		/* Check if we haven't already created the statement */
		if (hUserStmt == null){
			char error2[255];
			hUserStmt = SQL_PrepareQuery(db, "SELECT P.email, P.contrasenia, P.steamid FROM Players P WHERE P.email=? AND P.contrasenia=? AND P.steamid=?", error2, sizeof(error2));
			if (hUserStmt == null){
				PrintToServer(error2);
				
				delete hUserStmt;
				delete db;
				
				return Plugin_Handled;
			}
		}
		
		SQL_BindParamString(hUserStmt, 0, mail, false);
		SQL_BindParamString(hUserStmt, 1, pass, false);
		SQL_BindParamInt(hUserStmt, 2, steam_id, false);
		if (!SQL_Execute(hUserStmt)){
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
		GetValues(client, hUserStmt, false);
	}
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public Action GetValues(int client, Handle query, bool autolog){
	ZPlayer player = ZPlayer(client);
	if(autolog){
		int rows = SQL_GetRowCount(query);
		PrintToServer("GetValueReturns: %d", rows);
		if(rows == 0 ){
			showLoginMenu(client);
			return Plugin_Handled;
		}
		player.bLogged = true;
		loadCharacters(client);
	}
	else{
		int rows = SQL_GetRowCount(query);
		if(rows == 0 ){
			PrintHintText(client, "Login incorrecto, vuelve a intentarlo.");
			showLoginMenu(client);
			return Plugin_Handled;
		}
		
		char mail[32];
		char pass[32];
		int steamid;
		// "SELECT P.email, P.contrasenia, P.steamid, C.nombre, C.level, C.reset, C.money FROM Players P JOIN Characters C ON (P.id = C.idPlayer) WHERE P.email=?, P.contrasenia=?, P.steamid=?"
		while(SQL_FetchRow(query))
		{
			SQL_FetchString(query, 0, mail, 32);
			SQL_FetchString(query, 1, pass, 32);
			steamid = SQL_FetchInt(query, 2);
		}
		char bufferMail[32];
		char bufferPass[32];
		users.GetString(client, bufferMail, 32);
		passwords.GetString(client, bufferPass, 32);
		if(!StrEqual(mail, bufferMail) && !StrEqual(pass, bufferPass) && steamid == GetSteamAccountID(client)) {
			PrintHintText(client, "Login incorrecto, vuelve a intentarlo.");
			showLoginMenu(client);
			return Plugin_Handled;
		}
		player.bLogged = true;
		PrintHintText(client, "Logueado correctamente como\nMail: %s\nContraseña: %s ", mail, pass);
		loadCharacters(client);
	}
	
	
	return Plugin_Handled;
}
public Action loadCharacters(int client){
	int steamid = GetSteamAccountID(client);
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	
	/* Check if we haven't already created the statement */
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT c.id, c.nombre, c.level, c.reset FROM Players p RIGHT JOIN Characters c ON (p.id = c.idPlayer) WHERE p.steamid = ?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	
	SQL_BindParamInt(hUserStmt, 0, steamid, false);
	if (!SQL_Execute(hUserStmt)){
	
		delete hUserStmt;
		delete db;
		
		return Plugin_Handled;
	}
	
	
	
	GetCharacterValues(client, hUserStmt);
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public Action GetCharacterValues(int client, Handle query){
	ZPlayer player = ZPlayer(client);
	int rows = SQL_GetRowCount(query);
	if(rows <= 0 ){
		showMenuCuenta(client, true);
		return Plugin_Handled;
	}
	char name[32];
	int level;
	int reset;
	int id;
	for(int i; i < rows; i++) {
		if(SQL_FetchRow(query)) {
			id = SQL_FetchInt(query, 0);
			SQL_FetchString(query, 1, name, 32);
			level = SQL_FetchInt(query, 2);
			reset = SQL_FetchInt(query, 3);
			charactersNames[client].SetString(i, name);
			charactersLevels[client].Set(i, level);
			charactersResets[client].Set(i, reset);
			player.setIdPjInSlot(i, id);
			player.setSlotEmpty(i, false);
		}
		else break;
	}
	numCharacters[client] = rows;
	showMenuCuenta(client, false);
	
	return Plugin_Handled;
	
}
public Action createCharacter(int client){
	int steamid = GetSteamAccountID(client, true);
	ZPlayer player = ZPlayer(client);
	char name[32];
	characterNames.GetString(client, name, 32);
	PrintToServer("%s, %d", name, steamid);

	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	/* Check if we haven't already created the statement */
	
	
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "CALL createCharacter(?, ?)", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	
	SQL_BindParamInt(hUserStmt, 0, steamid, false);
	SQL_BindParamString(hUserStmt, 1, name, false);
	
	if (!SQL_Execute(hUserStmt)) {
		PrintHintText(client, "<font color='#ff0000' size='30'>Error!</font> Nombre en uso.");
		player.bInCreatingCharacter = true;
		
		delete hUserStmt;
		delete db;
		
		showCrearCharacter(client, false);
		return Plugin_Handled;
	}
	
	
	
	PrintHintText(client, "Personaje %s creado con exito!", name);
	PrintToChat(player.id, "%s Bienvenido a \x09Massive Infection\x01! Para conocer los binds y la información básica escribe \x0B!ayuda\x01 en el chat.", SERVERSTRING);
	PrintToChat(player.id, "%s \x09RECOMENDACIÓN\x01: Escribe !reglas para \x09evitar penalizaciones\x01!", SERVERSTRING);
	loadCharacters(client);
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}

//=====================================================
//				CHARACTER DATA LOAD
//=====================================================
public Action checkAdmin(int client, int charId){
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	//DBStatement hUserStmt = null;
	char query[128];
	Format(query, sizeof(query), "CALL check_fecha_admin(%d)", charId);
	SQL_FastQuery(db, query);
	
	delete db;
	return Plugin_Handled;
}
public Action checkVipPrueba(int client, int charId){
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	
	char query[128];
	Format(query, sizeof(query), "CALL checkFechaPrueba(%d)", charId);
	SQL_FastQuery(db, query);

	delete db;
	return Plugin_Handled;
}
public Action loadCharacterData(int client, int charId){
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	checkAdmin(client, charId);
	checkVipPrueba(client, charId);
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	/* Check if we haven't already created the statement */
	
	
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT nombre, experiencia, level, reset, hClass, zClass, hDamagelevel, hPenetrationlevel, hDexteritylevel, zDamagelevel, zResistancelevel, zDexteritylevel, weaponPSelected, weaponSSelected, HPoints, ZPoints, zHealthlevel, hResistanceLevel, expboost, hGDamageLevel, hGPenetrationLevel, hGDexterityLevel, hGResistanceLevel, zGDamageLevel, zGResistanceLevel, zGDexterityLevel, zGHealthLevel, ZGPoints, HGPoints, hDamageDealt, zDamageDealt, partyInv, autozClass, autopWeap, autosWeap, bullets, zKills, zCuchiKills, hKills, nemeKills, surviKills, assaKills, gunsKills, headshots, infections, backInfections, grenInfections, infBullBuyed, antiBuyed, furiBuyed, hAlineacion, zAlineacion, gPack, autoGPack FROM Characters WHERE id = ?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	
	SQL_BindParamInt(hUserStmt, 0, charId, false);
	
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Didn't execute query");
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	
	
	
	GetCharacterData(client, hUserStmt);
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public Action GetCharacterData(int client, Handle query){
	ZPlayer player = ZPlayer(client);
	int rows = SQL_GetRowCount(query);
	if(rows <= 0 ){
		PrintToServer("Personaje no encontrado");
		return Plugin_Handled;
	}
	
	// Initialize variables
	char name[32];
	int exp;
	int level;
	int reset;
	int zClass;
	int hClass;
	int hDamageLevel;
	int hPenetrationLevel;		// hVelocidad = hPenetration
	int hDexterityLevel;		// hGravedad = hDexterity
	int zDamageLevel;
	int zResistanceLevel; 		// zVelocidad = zResistance
	int zDexterityLevel;		// zGravedad = zDexterity
	int iWeaponPSelected;
	int iWeaponSSelected;
	int iHPoints;
	int iZPoints;
	int iZombieHealthLevel;
	int hResistanceLevel;		// hHealth = hResistance
	//int iHumanArmorLevel;      no need.
	float flExpBoost;
	int hGDamageLevel;
	int hGPenetrationLevel;		// hVelocidad = Penetration
	int hGDexterityLevel;		// hGravedad = Dexterity
	int hGResistanceLevel;		// hHealth = Resistance
	//int hGArmorLevel; no need.
	int zGDamageLevel;
	int zGResistanceLevel; 		// zVelocidad = zResistance
	int zGDexterityLevel;		// zGravedad = zDexterity
	int zGHealthLevel;
	int ZGPoints;
	int HGPoints;
	int hDamageDealt;
	int zDamageDealt;
	bool partyInv, autoZClass, autopWeap, autosWeap, bullets;
	int iZKills;
	int iZCuchiKills;
	int iHKills;
	int iNemeKills;
	int iSurviKills;
	int iAssaKills;
	int iGunsKills;
	int iHeadshots;
	int iInfections;
	int iBackInfection;
	int iGrenadeInfection;
	int iInfBullBuyed;
	int iAntiBuyed;
	int iFuriBuyed;
	int hAlineacion;
	int zAlineacion;
	int gPack;
	bool bAutoPack;

	// Fetch SQL data
	for(int i; i < rows; i++) {
		if(SQL_FetchRow(query)) {
			SQL_FetchString(query, 0, name, 32);
			exp = SQL_FetchInt(query, 1);
			level = SQL_FetchInt(query, 2);
			reset = SQL_FetchInt(query, 3);
			hClass = SQL_FetchInt(query, 4);
			zClass = SQL_FetchInt(query, 5);
			hDamageLevel = SQL_FetchInt(query, 6);
			hPenetrationLevel = SQL_FetchInt(query, 7);
			hDexterityLevel = SQL_FetchInt(query, 8);
			zDamageLevel = SQL_FetchInt(query, 9);
			zResistanceLevel = SQL_FetchInt(query, 10);
			zDexterityLevel = SQL_FetchInt(query, 11);
			iWeaponPSelected = SQL_FetchInt(query, 12);
			iWeaponSSelected = SQL_FetchInt(query, 13);
			iHPoints = SQL_FetchInt(query, 14);
			iZPoints = SQL_FetchInt(query, 15);
			iZombieHealthLevel = SQL_FetchInt(query, 16);
			hResistanceLevel = SQL_FetchInt(query, 17);
			flExpBoost = SQL_FetchFloat(query, 18);
			hGDamageLevel = SQL_FetchInt(query, 19);
			hGPenetrationLevel = SQL_FetchInt(query, 20);
			hGDexterityLevel = SQL_FetchInt(query, 21);
			hGResistanceLevel = SQL_FetchInt(query, 22);
			zGDamageLevel = SQL_FetchInt(query, 23);
			zGResistanceLevel = SQL_FetchInt(query, 24);
			zGDexterityLevel = SQL_FetchInt(query, 25);
			zGHealthLevel = SQL_FetchInt(query, 26);
			ZGPoints = SQL_FetchInt(query, 27);
			HGPoints = SQL_FetchInt(query, 28);
			hDamageDealt = SQL_FetchInt(query, 29);
			zDamageDealt = SQL_FetchInt(query, 30);
			partyInv = view_as<bool>(SQL_FetchInt(query, 31));
			autoZClass = view_as<bool>(SQL_FetchInt(query, 32));
			autopWeap = view_as<bool>(SQL_FetchInt(query, 33));
			autosWeap = view_as<bool>(SQL_FetchInt(query, 34));
			bullets = view_as<bool>(SQL_FetchInt(query, 35));
			iZKills = SQL_FetchInt(query, 36);
			iZCuchiKills = SQL_FetchInt(query, 37);
			iHKills = SQL_FetchInt(query, 38);
			iNemeKills = SQL_FetchInt(query, 39);
			iSurviKills = SQL_FetchInt(query, 40);
			iAssaKills = SQL_FetchInt(query, 41);
			iGunsKills = SQL_FetchInt(query, 42);
			iHeadshots = SQL_FetchInt(query, 43);
			iInfections = SQL_FetchInt(query, 44);
			iBackInfection = SQL_FetchInt(query, 45);
			iGrenadeInfection = SQL_FetchInt(query, 46);
			iInfBullBuyed = SQL_FetchInt(query, 47);
			iAntiBuyed = SQL_FetchInt(query, 48);
			iFuriBuyed = SQL_FetchInt(query, 49);
			hAlineacion = SQL_FetchInt(query, 50);
			zAlineacion = SQL_FetchInt(query, 51);
			gPack = SQL_FetchInt(query, 52);
			bAutoPack = view_as<bool>(SQL_FetchInt(query, 53));
		}
		else break;
	}
	
	// Store data in user variables
	player.iExp = exp;
	player.iLevel = level;
	player.iReset = reset;
	player.iZombieClass = zClass;
	player.iNextZombieClass = zClass;
	player.iHumanClass = hClass;
	player.iNextHumanClass = hClass;
	player.iHDamageLevel = hDamageLevel;
	player.iHPenetrationLevel = hPenetrationLevel;
	player.iHDexterityLevel = hDexterityLevel;
	player.iZDamageLevel = zDamageLevel;
	player.iZResistanceLevel = zResistanceLevel;
	player.iZDexterityLevel = zDexterityLevel;
	player.iSelectedPrimaryWeapon = iWeaponPSelected;
	player.iSelectedSecondaryWeapon = iWeaponSSelected;
	player.iNextWeaponPrimary = iWeaponPSelected;
	player.iNextWeaponSecondary = iWeaponSSelected;
	player.iGrenadePack = gPack;
	player.iNextGrenadePack = gPack;
	player.iHPoints = iHPoints;
	player.iZPoints = iZPoints;
	player.iZHealthLevel = iZombieHealthLevel;
	player.iHResistanceLevel = hResistanceLevel;
	//player.iHArmorLevel = iHumanArmorLevel;
	player.flExpBoost = flExpBoost;
	player.iHGDamageLevel = hGDamageLevel;
	player.iHGPenetrationLevel = hGPenetrationLevel;
	player.iHGDexterityLevel = hGDexterityLevel;
	player.iHGResistanceLevel = hGResistanceLevel;
	//player.iHGArmorLevel = hGArmorLevel;
	player.iZGDamageLevel = zGDamageLevel;
	player.iZGResistanceLevel = zGResistanceLevel;
	player.iZGDexterityLevel = zGDexterityLevel;
	player.iZGHealthLevel = zGHealthLevel;
	player.iZGoldenPoints = ZGPoints;
	player.iHGoldenPoints = HGPoints;
	player.iHDamageDealt = hDamageDealt;
	player.iZDamageDealt = zDamageDealt;
	
	player.iZKills = iZKills;
	player.iZCuchiKills = iZCuchiKills;
	player.iHKills = iHKills;
	player.iNemeKills = iNemeKills;
	player.iSurviKills = iSurviKills;
	player.iAssaKills = iAssaKills;
	player.iGunsKills = iGunsKills;
	player.iHeadshots = iHeadshots;
	player.iInfections = iInfections;
	player.iBackInfection = iBackInfection;
	player.iGrenadeInfection = iGrenadeInfection;
	player.iInfBullBuyed = iInfBullBuyed;
	player.iAntiBuyed = iAntiBuyed;
	player.iFuriBuyed = iFuriBuyed;
	player.iHumanAlignment = hAlineacion;
	player.iZombieAlignment = zAlineacion;
	
	//Testing
	player.bRecivePartyInv = partyInv;
	player.bAutoZClass =  autoZClass;
	player.bAutoPrimaryBuy =  autopWeap;
	player.bAutoSecondaryBuy = autosWeap;
	player.bAutoGrenadeBuy = bAutoPack;
	player.bStopSound = bullets;
	
	player.iPartyUID = PARTY_UNDEFINED;
	
	Players.Push(player.id);
	StopSound(player.id, SNDCHAN_STATIC,"*/MassiveInfection/bienvenidoapiubreakers.mp3");
		
	// Change name
	player.bCanChangeName = true;
	SetClientInfo(client, "name", name);
	CS_SetClientClanTag(client, "");
	bool staff = (StrEqual(name, "Deco") || StrEqual(name, "Codes"));
	
	// Augmented gain?
	if(flExpBoost > 1.0 && !staff){
		char VipType[32];
		bool legacy = flExpBoost > 7.0 && !staff;
		player.bAdmin = true;
		Format(VipType, sizeof(VipType), "%s VIP x%d %s |", legacy ? "✪":"", RoundToCeil(flExpBoost), legacy ? "✪":"");
		CS_SetClientClanTag(client, VipType);
		PrintToChatAll("%s Bienvenido \x0FVIP x%d %s\x01.", SERVERSTRING, RoundToCeil(flExpBoost), name);
	}
	
	// Now, he's officially in game
	player.bInGame = true;
	
	// Should he respawn?
	if(ActualMode.id == 0 || ActualMode.bRespawn && !endRound) player.iRespawnPlayer();
	
	// Join to CT
	player.iTeamNum = CS_TEAM_CT;
	
	// Check level and points
	checkLevelUp(player.id);
	checkPointsUp(player.id);
	
	CheckQuantityPlaying();
	
	// Print a message if he is a VIP
	if(staff){
		PrintToChatAll("%s Bienvenido %s %s\x01.", SERVERSTRING, (staff) ? "\x09STAFF" : "\x0FVIP", name);
		CS_SetClientClanTag(client, "☣ STAFF ☣ |");
	}
	
	return Plugin_Handled;
}
public Action saveCharacterData(int client){
	ZPlayer player = ZPlayer(client);
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	char name[32];
	GetClientInfo(client, "name", name, 32);
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "UPDATE Characters SET experiencia = ?, reset = ?, hClass = ?, zClass = ?, hDamageLevel = ?, hPenetrationlevel = ?, hDexteritylevel = ?, zDamagelevel = ?, zResistancelevel = ?, zDexteritylevel = ?, weaponPSelected = ?, weaponSSelected = ?, HPoints = ?, ZPoints = ?, zHealthLevel = ?, hResistanceLevel = ?, hGDamageLevel = ?, hGPenetrationLevel = ?, hGDexterityLevel = ?, hGResistanceLevel = ?, zGDamageLevel = ?, zGResistanceLevel = ?, zGDexterityLevel = ?, zGHealthLevel = ?, ZGPoints = ?, HGPoints = ?, hDamageDealt = ?, zDamageDealt = ?, partyInv = ?, autozClass = ?, autopWeap = ?, autosWeap = ?, bullets = ?, zKills=?, zCuchiKills=?, hKills=?, nemeKills=?, surviKills=?, assaKills=?, gunsKills=?, headshots=?, infections=?, backInfections=?, grenInfections=?, infBullBuyed=?, antiBuyed=?, furiBuyed=?, hAlineacion=?, zAlineacion=?, gPack=?, autoGPack=? WHERE id = ?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, player.iExp, false);
	SQL_BindParamInt(hUserStmt, 1, player.iReset, false);
	SQL_BindParamInt(hUserStmt, 2, player.iHumanClass, false);
	SQL_BindParamInt(hUserStmt, 3, player.iZombieClass, false);
	SQL_BindParamInt(hUserStmt, 4, player.iHDamageLevel, false);
	SQL_BindParamInt(hUserStmt, 5, player.iHPenetrationLevel, false);
	SQL_BindParamInt(hUserStmt, 6, player.iHDexterityLevel, false);
	SQL_BindParamInt(hUserStmt, 7, player.iZDamageLevel, false);
	SQL_BindParamInt(hUserStmt, 8, player.iZResistanceLevel, false);
	SQL_BindParamInt(hUserStmt, 9, player.iZDexterityLevel, false);
	SQL_BindParamInt(hUserStmt, 10, player.iSelectedPrimaryWeapon, false);
	SQL_BindParamInt(hUserStmt, 11, player.iSelectedSecondaryWeapon, false);
	SQL_BindParamInt(hUserStmt, 12, player.iHPoints, false);
	SQL_BindParamInt(hUserStmt, 13, player.iZPoints, false);
	SQL_BindParamInt(hUserStmt, 14, player.iZHealthLevel, false);
	SQL_BindParamInt(hUserStmt, 15, player.iHResistanceLevel, false);
	SQL_BindParamInt(hUserStmt, 16, player.iHGDamageLevel, false);
	SQL_BindParamInt(hUserStmt, 17, player.iHGPenetrationLevel, false);
	SQL_BindParamInt(hUserStmt, 18, player.iHGDexterityLevel, false);
	SQL_BindParamInt(hUserStmt, 19, player.iHGResistanceLevel, false);
	SQL_BindParamInt(hUserStmt, 20, player.iZGDamageLevel, false);
	SQL_BindParamInt(hUserStmt, 21, player.iZGResistanceLevel, false);
	SQL_BindParamInt(hUserStmt, 22, player.iZGDexterityLevel, false);
	SQL_BindParamInt(hUserStmt, 23, player.iZGHealthLevel, false);
	SQL_BindParamInt(hUserStmt, 24, player.iZGoldenPoints, false);
	SQL_BindParamInt(hUserStmt, 25, player.iHGoldenPoints, false);
	SQL_BindParamInt(hUserStmt, 26, player.iHDamageDealt, false);
	SQL_BindParamInt(hUserStmt, 27, player.iZDamageDealt, false);
	SQL_BindParamInt(hUserStmt, 28, player.bRecivePartyInv, false);
	SQL_BindParamInt(hUserStmt, 29, player.bAutoZClass, false);
	SQL_BindParamInt(hUserStmt, 30, player.bAutoPrimaryBuy, false);
	SQL_BindParamInt(hUserStmt, 31, player.bAutoSecondaryBuy, false);
	SQL_BindParamInt(hUserStmt, 32, player.bStopSound, false);
	SQL_BindParamInt(hUserStmt, 33, player.iZKills, false);
	SQL_BindParamInt(hUserStmt, 34, player.iZCuchiKills, false);
	SQL_BindParamInt(hUserStmt, 35, player.iHKills, false);
	SQL_BindParamInt(hUserStmt, 36, player.iNemeKills, false);
	SQL_BindParamInt(hUserStmt, 37, player.iSurviKills, false);
	SQL_BindParamInt(hUserStmt, 38, player.iAssaKills, false);
	SQL_BindParamInt(hUserStmt, 39, player.iGunsKills, false);
	SQL_BindParamInt(hUserStmt, 40, player.iHeadshots, false);
	SQL_BindParamInt(hUserStmt, 41, player.iInfections, false);
	SQL_BindParamInt(hUserStmt, 42, player.iBackInfection, false);
	SQL_BindParamInt(hUserStmt, 43, player.iGrenadeInfection, false);
	SQL_BindParamInt(hUserStmt, 44, player.iInfBullBuyed, false);
	SQL_BindParamInt(hUserStmt, 45, player.iAntiBuyed, false);
	SQL_BindParamInt(hUserStmt, 46, player.iFuriBuyed, false);
	SQL_BindParamInt(hUserStmt, 47, player.iHumanAlignment, false);
	SQL_BindParamInt(hUserStmt, 48, player.iZombieAlignment, false);
	SQL_BindParamInt(hUserStmt, 49, player.iGrenadePack, false);
	SQL_BindParamInt(hUserStmt, 50, player.bAutoGrenadeBuy, false);
	//JAMAS MOVER
	SQL_BindParamInt(hUserStmt, 51, player.iPjSeleccionado, false);
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Didn't execute query");
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}
public Action deleteCharacter(int client){
		ZPlayer player = ZPlayer(client);
		int idChar = player.getIdPjInSlot(player.iPjEnMenu);
		char error[255];
		Database db = SQL_DefConnect(error, sizeof(error));
		DBStatement hUserStmt = null;
		
		char name[32];
		GetClientInfo(client, "name", name, 32);
		
		if (db == null){
			PrintToServer("Could not connect: %s", error);
		}
		
		
		if (hUserStmt == null){
			char error2[255];
			hUserStmt = SQL_PrepareQuery(db, "DELETE FROM Characters WHERE id=?", error2, sizeof(error2));
			if (hUserStmt == null)
			{
				PrintToServer(error2);
				
				delete hUserStmt;
				delete db;
				
				return Plugin_Handled;
			}
		}
		SQL_BindParamInt(hUserStmt, 0, idChar);
	
		
		if (!SQL_Execute(hUserStmt)) {
			PrintToServer("Didn't execute query");
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
		
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}

//=====================================================
//				METHOD REGISTERED STUFF
//=====================================================
// Classes
public void LoadClasses(){
	
	//============================================================================
	//							HUMAN CLASSES
	//============================================================================
	
	//CreateHClass(const char[] name, const char[] model, const char[] arms, int hp, int armor, float speed, float gravity, int levelReq, int resetReq)
	CreateHClass("Civil", "models/player/custom_player/kuristaja/l4d2/ellis/ellisv2.mdl", "models/player/custom_player/kuristaja/l4d2/ellis/ellis_arms.mdl",      				100, 0, 1.0, 1.0, 1, 0);
	CreateHClass("Guardia", "models/player/custom_player/kuristaja/cso2/lincoln/lincoln.mdl", "models/player/custom_player/kuristaja/cso2/lincoln/lincoln_arms.mdl",    		100, 0, 1.0, 1.0, 20, 0);
	CreateHClass("Oficial", "models/player/custom_player/kuristaja/cso2/gign/gign.mdl", "models/player/custom_player/kuristaja/cso2/gign/gign_arms.mdl",      					100, 0, 1.0, 1.0, 45, 0);
	CreateHClass("Soldado PFA", "models/player/custom_player/kuristaja/hunk/hunk.mdl", "models/player/custom_player/kuristaja/hunk/hunk_arms.mdl",         						100, 5, 1.0, 1.0, 60, 0);
	CreateHClass("Investigadora", "models/player/custom_player/kuristaja/cso2/carrie/carrie.mdl", "models/player/custom_player/kuristaja/cso2/carrie/carrie_arms.mdl",    		110, 10, 1.0, 1.0, 90, 0);
	CreateHClass("Ricky", "models/player/custom_player/kuristaja/cso2/karachenko/karachenko.mdl", "models/player/custom_player/kuristaja/cso2/karachenko/karachenko_arms.mdl",  110, 15, 1.0, 1.0, 125, 0);
	CreateHClass("Sargento", "models/player/custom_player/kuristaja/cso2/mila/mila.mdl", "models/player/custom_player/kuristaja/cso2/mila/mila_arms.mdl",       				115, 20, 1.0, 1.0, 160, 0);
	CreateHClass("Brigada", "models/player/custom_player/kuristaja/re6/chris/chrisv4.mdl", "models/player/custom_player/kuristaja/re6/chris/chris_arms.mdl",      				115, 25, 1.0, 1.0, 195, 0);
	CreateHClass("Teniente", "models/player/custom_player/kuristaja/cso2/emma/emma.mdl", "models/player/custom_player/kuristaja/cso2/emma/emma_arms.mdl",       				120, 30, 1.0, 1.0, 220, 0);
	CreateHClass("Capitán", "models/player/custom_player/kuristaja/cso2/707/707.mdl", "models/player/custom_player/kuristaja/cso2/707/707_arms.mdl",        					120, 35, 1.0, 1.0, 255, 0);
	CreateHClass("Fuerzas Especiales", "models/player/custom_player/kuristaja/cso2/lisa/lisa.mdl", "models/player/custom_player/kuristaja/cso2/lisa/lisa_arms.mdl",    			125, 40, 1.0, 1.0, 280, 0);
	CreateHClass("Guerrera Khaz'El", "models/player/custom_player/hekut/talizorah/talizorah.mdl", "models/player/custom_player/hekut/talizorah/talizorah_arms.mdl",    			125, 45, 1.0, 1.0, 300, 1);
	CreateHClass("Super Soldado", "models/player/custom_player/kuristaja/nanosuit/nanosuitv3.mdl", "models/player/custom_player/kuristaja/nanosuit/nanosuit_arms.mdl",    		130, 50, 1.0, 1.0, 320, 2);
	/*
	CreateHClass("Civil", "models/player/custom_player/kuristaja/l4d2/ellis/ellisv2.mdl", "models/player/custom_player/kuristaja/l4d2/ellis/ellis_arms.mdl", 					100, 0, 1.0, 1.0, 1, 0);
	CreateHClass("Guardia", "models/player/custom_player/kuristaja/cso2/lincoln/lincoln.mdl", "models/player/custom_player/kuristaja/cso2/lincoln/lincoln_arms.mdl", 			105, 0, 1.0, 1.0, 20, 0);
	CreateHClass("Oficial", "models/player/custom_player/kuristaja/cso2/gign/gign.mdl", "models/player/custom_player/kuristaja/cso2/gign/gign_arms.mdl",						110, 0, 1.0, 1.0, 60, 0);
	CreateHClass("Soldado PFA", "models/player/custom_player/kuristaja/hunk/hunk.mdl", "models/player/custom_player/kuristaja/hunk/hunk_arms.mdl", 								115, 0, 1.0, 1.0, 100, 0);
	CreateHClass("Investigadora", "models/player/custom_player/kuristaja/cso2/carrie/carrie.mdl", "models/player/custom_player/kuristaja/cso2/carrie/carrie_arms.mdl", 			120, 0, 1.0, 1.0, 140, 0);
	CreateHClass("Ricky", "models/player/custom_player/kuristaja/cso2/karachenko/karachenko.mdl", "models/player/custom_player/kuristaja/cso2/karachenko/karachenko_arms.mdl", 	125, 0, 1.0, 1.0, 175, 0);
	CreateHClass("Sargento", "models/player/custom_player/kuristaja/cso2/mila/mila.mdl", "models/player/custom_player/kuristaja/cso2/mila/mila_arms.mdl", 						130, 0, 1.0, 1.0, 210, 0);
	CreateHClass("Brigada", "models/player/custom_player/kuristaja/re6/chris/chrisv4.mdl", "models/player/custom_player/kuristaja/re6/chris/chris_arms.mdl", 					135, 0, 1.0, 1.0, 240, 0);
	CreateHClass("Teniente", "models/player/custom_player/kuristaja/cso2/emma/emma.mdl", "models/player/custom_player/kuristaja/cso2/emma/emma_arms.mdl", 						140, 20, 1.0, 1.0, 270, 0);
	CreateHClass("Capitán", "models/player/custom_player/kuristaja/cso2/707/707.mdl", "models/player/custom_player/kuristaja/cso2/707/707_arms.mdl", 							145, 40, 1.0, 1.0, 290, 0);
	CreateHClass("Fuerzas Especiales", "models/player/custom_player/kuristaja/cso2/lisa/lisa.mdl", "models/player/custom_player/kuristaja/cso2/lisa/lisa_arms.mdl", 			150, 120, 1.0, 1.0, 298, 1);
	CreateHClass("Guerrera Khaz'El", "models/player/custom_player/hekut/talizorah/talizorah.mdl", "models/player/custom_player/hekut/talizorah/talizorah_arms.mdl", 			155, 145, 1.0, 1.0, 306, 1);
	CreateHClass("Super Soldado", "models/player/custom_player/kuristaja/nanosuit/nanosuitv3.mdl", "models/player/custom_player/kuristaja/nanosuit/nanosuit_arms.mdl", 			160, 180, 1.0, 1.0, 320, 2);*/
	
	// These are legacy classes (Halloween)
	iVipClass[0] = CreateHClass("Jigsaw", "models/player/custom_player/kuristaja/billy/billy_normal.mdl", "models/player/custom_player/kuristaja/billy/billy_arms.mdl", 								130, 60, 1.0, 1.0, 300, 0);
	iVipClass[1] = CreateHClass("Michael Myers", "models/player/custom_player/kuristaja/myers/myers.mdl", "models/player/custom_player/kuristaja/myers/myers_arms.mdl", 								130, 70, 1.0, 1.0, 300, 0);
	iVipClass[2] = CreateHClass("Freddy Krueger", "models/player/custom_player/kuristaja/krueger/krueger.mdl", "models/player/custom_player/kuristaja/krueger/krueger_arms2.mdl", 						130, 80, 1.0, 1.0, 300, 0);
	iVipClass[3] = CreateHClass("Texas' Leatherface", "models/player/custom_player/kuristaja/leatherface/leatherface.mdl", "models/player/custom_player/kuristaja/leatherface/leatherface_arms.mdl", 	130, 90, 1.0, 1.0, 300, 0);
	
	//============================================================================
	//							ZOMBIE CLASSES
	//============================================================================
	
	//CreateZClass(const char[] name, const char[] model, const char[] arms, int hp, float dmg, float speed, float gravity, int levelReq, int resetReq, int alpha, bool hideKnife)
	CreateZClass("Infectado",		"models/player/kuristaja/zombies/gozombie/gozombie.mdl",		"models/zombie/normal/hand/hand_zombie_normal_fix.mdl", 				80000, 	1.1, 	1.0, 	1.0,	1,		0, 255, true);
	CreateZClass("Ex oficial",		"models/player/kuristaja/zombies/police/police.mdl", 			"models/zombie/normal_f/hand/hand_zombie_normal_f.mdl", 				85000, 	1.15, 	1.1, 	1.0,	30,		0, 255, true);
	CreateZClass("Heavy Origin",	"models/player/custom_player/kodua/zombie_heavy/heavy_origin.mdl", "models/zombie/normalhost/hand/hand_zombie_normalhost.mdl", 			95000, 	1.3, 	0.93, 	1.00,	65,		0, 255, true);
	CreateZClass("Walker", 			"models/player/custom_player/cso2_zombi/zombie.mdl", 			"models/zombie/normal_f/hand/hand_zombie_normal_f.mdl",					80000,	1.2, 	1.2,	0.95,	90,		0, 255, true);
	CreateZClass("Ágil", 			"models/player/kuristaja/zombies/skinny/skinny.mdl", 			"models/zombie/normalhost_female/hand/hand_zombie_normalhost_f.mdl", 	85000, 	1.25, 	1.225, 	0.93,	125,	0, 255, true);
	CreateZClass("Soldado Caído", 	"models/player/nf/dead_effect_2/soldier.mdl", 					"models/zombie/normalhost/hand/hand_zombie_normalhost.mdl", 			87000, 	1.3, 	1.235, 	0.9, 	155,	0, 255, false);
	CreateZClass("Zombie Policía",	"models/player/custom_player/cso2_zombi/police.mdl",			"models/zombie/normalhost_female/hand/hand_zombie_normalhost_f.mdl",	92000,	1.35, 	1.237,	0.88,	170,	0, 255, true);
	CreateZClass("Simio Mutante", 	"models/player/custom_player/kodua/eliminator/eliminator.mdl", 	"models/zombie/normal/hand/hand_zombie_normal_fix.mdl", 				77500, 	1.37, 	1.24, 	0.8, 	200,	0, 255, false);
	CreateZClass("Inferno",			"models/player/custom_player/cso2_zombi/normalhost.mdl", 		"models/zombie/normalhost/hand/hand_zombie_normalhost.mdl", 			97000,	1.39, 	1.26,	0.75,	235,	0, 255, true);
	//CreateZClass("Espectro", 		"models/player/kuristaja/wraith/wraith.mdl", 					"models/zombie/normal_f/hand/hand_zombie_normal_f.mdl", 				38000, 	1.34, 	1.29, 	0.65,	290,	0, 255);
	CreateZClass("Mórbido",			"models/player/custom_player/kodua/bloatv2/bloat.mdl", 			"models/zombie/normal/hand/hand_zombie_normal_fix.mdl", 				100000, 1.3, 	0.93, 	1.01,	260,	0, 255, true);
	CreateZClass("Tundra", 			"models/player/custom_player/kodua/bo/nazi_frozen.mdl",	 		"models/zombie/normal_f/hand/hand_zombie_normal_f.mdl", 				103000, 1.41, 	1.29, 	0.65,	290,	0, 255, true);
	CreateZClass("Transparente", 	"models/player/kodua/invisible_bitch/stalker.mdl",				"models/zombie/normal_f/hand/hand_zombie_normal_f.mdl", 				95000, 	1.35, 	1.4,	0.8, 	320, 	5, 70, true);
	CreateZClass("Triturador",		"models/player/custom_player/kodua/fleshpoundv2/fleshpound.mdl", "models/zombie/normal/hand/hand_zombie_normal_fix.mdl", 				93000, 	1.33, 	1.3, 	0.7,	340,	1, 255, true);
	CreateZClass("Nightmare", 		"models/player/custom_player/kodua/scrake_albino/scrake.mdl", 	"models/zombie/normalhost/hand/hand_zombie_normalhost.mdl", 			95000, 	1.35, 	1.32, 	0.65,	350,	3, 255, true);
	CreateZClass("Radioactivo",		"models/player/mapeadores/morell/ghoul/ghoulfix.mdl",			"models/zombie/normalhost/hand/hand_zombie_normalhost.mdl",				137000,	1.35,	1.25,	0.65,	350,	7, 255, true);
	//CreateZClass("", "", "", 9000, 1.3, 1.3, 0.8, 320, 0);
}

// Alignments
public void LoadAlignments(){
	
	//============================================================================
	//							HUMAN ALIGNMENTS
	//============================================================================
	
	//CreateHAlignment(const char[] name, const char[] description, float damageMul, float penMul, float hpMul, int armorAdd, float speedMul, float gravityMul)
	
	/*
	Ágil
	Resistente
	Inmune
	Letal
	*/
	CreateHAlignment("Ágil", 		"Vel: +25% Grav: -20%",				1.0, 1.0, 1.0,		0,	1.25, 0.8);
	CreateHAlignment("Resistente", 	"Vida: +80% Armor: +20 Vel: +20%",	0.9, 1.0, 1.8,		20,	1.2, 1.0);
	CreateHAlignment("Inmune", 		"Armor: +85 Vel: -10%",				1.0, 1.0, 1.0,		85,	0.9, 1.0);
	CreateHAlignment("Letal", 		"Dmg: +6.5% Pen: +8% Vida: -35%",	1.065, 1.08, 0.65,	0,	0.9, 1.0);
	CreateHAlignment("Berserker",	"Dmg: +28% Pen: +1.5% Vida: -25%",	1.28, 1.015, 0.75,	1,	0.9, 1.0);
	
	//============================================================================
	//							ZOMBIE ALIGNMENTS
	//============================================================================
	
	//CreateZAlignment(const char[] name, const char[] description, float damageMul, float hpMul, float armorRatio, float speedMul, float gravityMul)

	CreateZAlignment("Raptor", 	"Dmg: -10%  Hp: -5% Vel: +25% Grav: -20%",	0.9, 0.95, 	1.0, 	1.25, 0.8);
	CreateZAlignment("Fuerte", 	"Dmg: +80% Hp: +10% Ress: +10%",			1.8, 1.1, 	1.1, 	1.0, 1.0);
	CreateZAlignment("Coloso", 	"Hp: +20% Ress: +5% Vel: +5%",				1.0, 1.2, 	1.05, 	1.05, 1.0);
	CreateZAlignment("Titán", 	"Dmg: +30% Ress: +30%  Vel: -10%",			1.3, 1.0, 	1.30,	0.9, 1.0);
}

// Weapons
public void LoadWeapons(){
	char AK117V[128], AK117W[128], NAVYV[128], NAVYW[128];
	Format(AK117V, sizeof(AK117V), "models/weapons/ak117/v_rif_ak117.mdl");
	Format(AK117W, sizeof(AK117W), "models/weapons/ak117/w_rif_ak117.mdl");
	Format(NAVYV, sizeof(NAVYV), "models/weapons/mp5/v_smg_mp5.mdl");
	Format(NAVYW, sizeof(NAVYW), "models/weapons/mp5/w_smg_mp5.mdl");
	
	
	//============================================================================
	//							PRIMARY WEAPONS
	//============================================================================
	//AlternativeLoadWeapons();
	
	//CreateWeaponPrimary(const char[] name, const char[] entityName, const char[] modelName, int level, int reset, float dmg, float knockback)
	CreateWeaponPrimary("Mac-10", 	"weapon_mac10", 	"", "", 	1, 0, 1.25, 	0.1);
	CreateWeaponPrimary("Bizon", 	"weapon_bizon", 	"", "", 	15, 0, 1.5, 	0.1);
	CreateWeaponPrimary("MP7", 		"weapon_mp7",		"", "", 	33, 0, 1.65, 	0.1);
	CreateWeaponPrimary("MP9", 		"weapon_mp9", 		"", "", 	56, 0, 1.85, 	0.1);
	CreateWeaponPrimary("UMP-45", 	"weapon_ump45", 	"", "", 	74, 0, 2.45,	0.25);
	CreateWeaponPrimary("Nova", 	"weapon_nova", 		"", "", 	90, 0, 5.9, 	1.0);
	CreateWeaponPrimary("P90", 		"weapon_p90", 		"", "", 	110, 0, 3.25, 	0.15);
	CreateWeaponPrimary("XM1014", 	"weapon_xm1014", 	"", "", 	132, 0, 5.7, 	0.75);
	iFreezeAwp = CreateWeaponPrimary("Cryogenic Sniper", 	"weapon_awp", "", "",  143, 3, 15.85, 	5.0);
	CreateWeaponPrimary("Famas", 	"weapon_famas", 	"", "", 	160, 0, 4.0, 	0.3);
	CreateWeaponPrimary("Galil AR", "weapon_galilar", 	"", "", 	188, 0, 4.35, 	0.3);
	CreateWeaponPrimary("Legacy MP5-Navy", "weapon_mp7", NAVYV, NAVYW, 197, 1, 6.45, 	0.25);
	CreateWeaponPrimary("Aug", 		"weapon_aug", 		"", "", 	210, 0, 4.5, 	0.3);
	CreateWeaponPrimary("M4A4", 	"weapon_m4a1", 		"", "", 	232, 0, 4.7, 	0.35);
	iFireAwp = CreateWeaponPrimary("Volcanic Sniper", "weapon_awp", "", "", 270, 5, 17.5, 	5.0);
	CreateWeaponPrimary("Sig 553", 	"weapon_sg556", 	"", "", 	255, 0, 5.0, 	1.0);
	CreateWeaponPrimary("M4A1-S",	"weapon_m4a1_silencer", "", "",  290, 0, 5.25, 	0.35);
	CreateWeaponPrimary("Sawed-Off","weapon_sawedoff", 	"", "", 	298, 9, 10.75, 	1.0);
	CreateWeaponPrimary("AK-47", 	"weapon_ak47", 		"", "", 	312, 0, 5.5, 	0.5);
	CreateWeaponPrimary("SCAR-20",	"weapon_scar20", 	"", "", 	335, 0, 5.8, 	0.7);
	CreateWeaponPrimary("AK-117",	"weapon_ak47", AK117V, AK117W,  346, 7, 8.8, 	0.6);
	CreateWeaponPrimary("G3SG1",	"weapon_g3sg1", 	"", "", 	368, 0, 6.0,	0.7);
	//CreateWeaponPrimary("LAWEAROTA", "weapon_awp", "models/weapons/eminem/dsr_50/v_dsr_50.mdl", "models/weapons/eminem/dsr_50/w_dsr_50.mdl", 0, 0, 40.0, 5.0);
	//CreateWeaponPrimary("Negev", "weapon_negev", "", "", 			300, 0, 2.08);
	//CreateWeaponPrimary("LAWEAROTA", "weapon_awp", "models/weapons/v_snip_awp.mdl", "models/weapons/w_snip_awp.mdl", 0, 0, 40.0, 5.0);
	
	
	//============================================================================
	//							SECONDARY WEAPONS
	//============================================================================
	
	//CreateWeaponSecundary(const char[] name, const char[] entityName, const char[] modelName, int level, int reset, int cost, float dmg)
	CreateWeaponSecondary("Glock 18", "weapon_glock", "", 			1,   0,  1.0);
	CreateWeaponSecondary("USP-S", "weapon_usp_silencer", "", 		16,  0,  1.2);
	CreateWeaponSecondary("P2000", "weapon_hkp2000", "", 			32,  0,  1.4);
	CreateWeaponSecondary("P250", "weapon_p250", "", 				64,  0,  1.6);
	CreateWeaponSecondary("Five-SeveN", "weapon_fiveseven", "", 	105, 0, 1.8);
	CreateWeaponSecondary("Dual Berettas", "weapon_elite", "", 		150, 0, 2.0);
	CreateWeaponSecondary("CZ75-Auto", "weapon_cz75a", "", 			200, 0, 2.2);
	CreateWeaponSecondary("Tec-9", "weapon_tec9", "", 				250, 0, 2.4);
	CreateWeaponSecondary("Desert Eagle", "weapon_deagle", "", 		300, 0, 2.6);
	
	//============================================================================
	//							GRENADE PACKS
	//============================================================================
	// CreateGrenadePack(int level, int reset, GrenadeType first, GrenadeType second, GrenadeType third, GrenadeType fourth, int firstCount, int secondCount, int thirdCount, int fourthCount)
	CreateGrenadePack(	1, 0,
					 	{FIRE_GRENADE, FREEZE_GRENADE, LIGHT_GRENADE, NULL_GRENADE},
					 	{1, 1, 1, 0});
 	CreateGrenadePack(	75, 0,
					 	{FIRE_GRENADE, FREEZE_GRENADE, LIGHT_GRENADE, NULL_GRENADE},
					 	{2, 2, 1, 0});
	CreateGrenadePack(	200, 0,
					 	{FIRE_GRENADE, FREEZE_GRENADE, LIGHT_GRENADE, NULL_GRENADE},
					 	{2, 3, 2, 0});
	CreateGrenadePack(	320, 0,
						{FIRE_GRENADE, FREEZE_GRENADE, AURA_GRENADE, NULL_GRENADE},
						{1, 2, 1, 0});
	CreateGrenadePack(	250, 1,
						{FIRE_GRENADE, FREEZE_GRENADE, AURA_GRENADE, NULL_GRENADE},
						{2, 3, 1, 0});
}

// SQL alternative weapons load
public Action AlternativeLoadWeapons(){
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	/* Check if we haven't already created the statement */
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT name, entity, vModel, pModel, lvlReq, resReq, dmg, orden, id FROM PrimaryWeapons ORDER BY orden ASC", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Didn't executed query");
		
		delete hUserStmt;
		delete db;
		
		return Plugin_Handled;
	}
	
	
	int rows = SQL_GetRowCount(hUserStmt);
	if(rows <= 0 ){
		PrintToServer("Ningun Arma encontrada");
		return Plugin_Handled;
	}
	
	char name[32];
	char entity[32];
	char vModel[256];
	char wModel[256];
	int lvl;
	int rs;
	float dmg;
	// Fetch SQL data
	for(int i; i < rows; i++) {
		if(SQL_FetchRow(hUserStmt)) {
			
			SQL_FetchString(hUserStmt, 0, name, 32);
			SQL_FetchString(hUserStmt, 1, entity, 32);
			SQL_FetchString(hUserStmt, 2, vModel, 256);
			SQL_FetchString(hUserStmt, 3, wModel, 256);
			lvl = SQL_FetchInt(hUserStmt, 4);
			rs = SQL_FetchInt(hUserStmt, 5);
			CreateWeaponPrimary(name, entity, vModel, wModel, lvl, rs, dmg, 1.0);
			
		}
		else break;
	}
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}

// Gamemodes
public void LoadGameModes(){
//	CreateGameMode(const char[] name, 	bool infection, bool kill, bool respawn, bool respawnOnlyZombie, bool antidoteAvailable, bool zombiemadnessAvailable, int minUsers, int maxTimesPerMap, float minVipToForce)
	CreateGameMode("Esperando amenaza..", false, false, true, false, 	false, false, 	0, 1000, 	0.0, true, -1);
	CreateGameMode("Esperando usuarios", false, false, false, false, 	false, false, 	0, 1000, 	0.0, true, -1);
	CreateGameMode("Infección", 		true, false, true, false, 		true, true, 	1, 1000, 	2.0, false, 10);
	CreateGameMode("Infección Múltiple", true, false, true, false, 		true, true, 	2, 1000, 	2.0, false, 10);
	CreateGameMode("Aniquilación", 		true, false, true, true, 		false, true, 	1, 1000, 	2.0, false, 10);
	CreateGameMode("Infección Masiva", 	true, false, true, true, 		true, true, 	4, 1000, 	2.0, false, 10);
	CreateGameMode("Swarm", 			false, true, false, false, 		false, true, 	4, 4, 		2.0, false, 10);
	CreateGameMode("Nemesis", 			false, true, false, false, 		false, false, 	6, 4, 		3.0, false, 10);
	CreateGameMode("Survivor", 			false, true, false, false, 		false, false, 	6, 4, 		3.0, false, 10);
	CreateGameMode("Gunslinger", 		false, true, false, false, 		false, false, 	6, 4, 		4.0, false, 10);
	CreateGameMode("Assassin", 			false, true, false, false, 		false, false,	6, 4, 		4.0, false, 10);
	CreateGameMode("Sniper", 			false, true, false, false, 		false, false,	6, 4, 		4.0, false, 10);
	CreateGameMode("Multinemesis",		false, true, true, true, 		false, true,	8, 1, 		5.0, false, 10);
	CreateGameMode("Armageddon", 		false, true, false, false, 		false, false,	10, 1,		7.0, true, 10);
	CreateGameMode("Apocalypsis", 		false, true, false, false, 		false, false,	10, 1, 		7.0, false, 10);
}

// Combos
public void LoadCombos(){
	// CreateCombo(const char[] name,	float difficulty, float bonusExpGain,	int red, int green, int blue)
	CreateCombo("Default", 			0.0, 	1.0, 	232, 160, 128);
	CreateCombo("Nice!", 			10.0, 	1.2, 	102, 178, 255);
	CreateCombo("Amazing!",			25.0, 	1.4, 	232, 255, 168);
	CreateCombo("Godlike!",			40.0, 	1.6, 	232, 204, 140);
	CreateCombo("Bloody!",			55.0, 	1.8, 	232, 160, 128);
	CreateCombo("AGGRESSIVE!",		70.0, 	2.0, 	232, 130, 104);
	CreateCombo("MASSIVE!",			85.0, 	2.2, 	255, 255, 255);
	CreateCombo("ULTRA MASSIVE!",	100.0, 	2.4,	255, 255, 255);
	CreateCombo("FLAWLESS!!",		115.0, 	2.6, 	255, 255, 255);
}

// Achievements
public Action LoadLogros(){
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	/* Check if we haven't already created the statement */
	
	
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT nombre, descripcion, zKills, zCuchiKills, hKills, nemeKills, surviKills, gunsKills, assaKills, snipKills, headshots, infections, grenadeInfections, infBullBuy, antidoteBuy, madnesBuy, hPoints, zPoints, hGPoints, zGPoints FROM Logros", error2, sizeof(error2));
		
		if (hUserStmt == null){
			PrintToServer(error2);
			delete db;
			return Plugin_Handled;
		}
	}
	
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Didn't executed query");
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	
	
	if(SQL_GetRowCount(hUserStmt) == 0){
		PrintToServer("Sin logros por cargar");
		delete hUserStmt;
		delete db;
		return Plugin_Handled;
	}
	char name[64], desc[64];
	int zKills, zCuchiKills, hKills, nemeKills, surviKills, gunsKills, assaKills, snipKills, headshots, infections, grenInfections, infBullBought, antidoteBought, madnesBought, hPoints, zPoints, hGPoints, zGPoints;
	while(SQL_FetchRow(hUserStmt)){
		SQL_FetchString(hUserStmt, 0, name, sizeof(name));
		SQL_FetchString(hUserStmt, 1, desc, sizeof(desc));
		zKills = SQL_FetchInt(hUserStmt, 2);
		zCuchiKills = SQL_FetchInt(hUserStmt, 3);
		hKills = SQL_FetchInt(hUserStmt, 4);
		nemeKills = SQL_FetchInt(hUserStmt, 5);
		surviKills = SQL_FetchInt(hUserStmt, 6);
		gunsKills = SQL_FetchInt(hUserStmt, 7);
		assaKills = SQL_FetchInt(hUserStmt, 8);
		snipKills = SQL_FetchInt(hUserStmt, 9);
		headshots = SQL_FetchInt(hUserStmt, 10);
		infections = SQL_FetchInt(hUserStmt, 11);
		grenInfections = SQL_FetchInt(hUserStmt, 12);
		infBullBought = SQL_FetchInt(hUserStmt, 13);
		antidoteBought = SQL_FetchInt(hUserStmt, 14);
		madnesBought = SQL_FetchInt(hUserStmt, 15);
		hPoints = SQL_FetchInt(hUserStmt, 16);
		zPoints = SQL_FetchInt(hUserStmt, 17);
		hGPoints = SQL_FetchInt(hUserStmt, 18);
		zGPoints = SQL_FetchInt(hUserStmt, 19);
		CreateZLogros(name, desc, zKills, hKills, zCuchiKills, nemeKills, surviKills, assaKills, snipKills, gunsKills, headshots, infections, grenInfections, infBullBought, antidoteBought, madnesBought, hPoints, zPoints, hGPoints, zGPoints);
	}
	
	//SELECT nombre, zKills, zCuchiKills, hKills, nemeKills, surviKills, gunsKills, assaKills, snipKills, headshots, infections, grenadeInfections, infBullBuy, antidoteBuy, madnesBuy, hPoints, zPoints, hGPoints, zGPoints 
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}

//=====================================================
//					MERCENARY
//=====================================================
public Action CreateMercenary(int client, int args){
	int entity = CreateEntityByName("prop_dynamic_glow");
	
	if (IsValidEntity(entity)){
		// Move entity to origins
		float vector[3];
		GetAimCoords(client, vector);
		PrintToChat(client, "%f, %f, %f", vector[0], vector[1], vector[2]);
		TeleportEntity(entity, vector, NULL_VECTOR, NULL_VECTOR);
		
		DispatchKeyValue(entity, "Classname", NPC_CLASSNAME); // used to detect while users aim and press E
		DispatchKeyValue(entity, "model", NPC_MODEL);
		DispatchKeyValue(entity, "Solid", "6");
		DispatchKeyValue(entity, "RandomAnimation", "0");
		DispatchKeyValue(entity, "DefaultAnim", NPC_DEFAULT_ANIM);
		/*DispatchKeyValueFloat(entity, "MinAnimTime", 200.0);
		DispatchKeyValueFloat(entity, "MaxAnimTime", 200.0);
		DispatchKeyValue(entity, "SetGlowEnabled", "1");*/
		
		DispatchSpawn(entity);
		ActivateEntity(entity);
		
		//SetEntProp(entity, Prop_Send, "m_bShouldGlow", true, true);
		SetEntPropFloat(entity, Prop_Send, "m_flGlowMaxDist", 10000000.0);
		SetGlowColor(entity, "255 255 255");

		/*SetVariantString("Death1");
		AcceptEntityInput(entity, "Enable");*/
	}
	return Plugin_Handled;
}
public void DispatchMercenary(float vector[3]){
	int entity = CreateEntityByName("prop_dynamic_glow");
	
	if (IsValidEntity(entity)){
		// Move entity to origins
		TeleportEntity(entity, vector, NULL_VECTOR, NULL_VECTOR);
		
		DispatchKeyValue(entity, "Classname", NPC_CLASSNAME); // used to detect while users aim and press E
		DispatchKeyValue(entity, "model", NPC_MODEL);
		DispatchKeyValue(entity, "Solid", "6");
		DispatchKeyValue(entity, "RandomAnimation", "0");
		DispatchKeyValue(entity, "DefaultAnim", NPC_DEFAULT_ANIM);
		/*DispatchKeyValueFloat(entity, "MinAnimTime", 200.0);
		DispatchKeyValueFloat(entity, "MaxAnimTime", 200.0);
		DispatchKeyValue(entity, "SetGlowEnabled", "1");*/
		
		DispatchSpawn(entity);
		ActivateEntity(entity);
		
		SetEntProp(entity, Prop_Send, "m_bShouldGlow", true, true);
		SetEntPropFloat(entity, Prop_Send, "m_flGlowMaxDist", 10000.0);
		SetGlowColor(entity, "255 255 255");

		/*SetVariantString("Death1");
		AcceptEntityInput(entity, "Enable");*/
	}
}
public Action DeleteMercenary(int client, int args){
	int targetIndex = GetClientAimTarget(client, false);
		
	if (!IsValidEdict(targetIndex))
		return Plugin_Continue;
	
	// Mejor manejo de entidades por referencias
	int targetRef = EntIndexToEntRef(targetIndex);
	
	char sClassname[32];
	GetEdictClassname(targetRef, sClassname, sizeof(sClassname));
	if (strcmp(sClassname, NPC_CLASSNAME) == 0)
		AcceptEntityInput(targetRef, "Kill");
		
	return Plugin_Handled;
}
public Action ChangeAnimation(int client, int args){
	int targetIndex = GetClientAimTarget(client, false);
	char sArg[16];
	GetCmdArg(args, sArg, sizeof(sArg));
	int iSequence = StringToInt(sArg);
		
	if (!IsValidEdict(targetIndex))
		return Plugin_Continue;
	
	char sClassname[32];
	GetEdictClassname(targetIndex, sClassname, sizeof(sClassname));
	if (strcmp(sClassname, NPC_CLASSNAME) == 0)
	{
		SetEntProp(targetIndex, Prop_Send, "m_flAnimTime", 1);
		SetEntProp(targetIndex, Prop_Send, "m_nSequence", iSequence);
		//SetEntProp(targetRef, Prop_Data, "m_bHoldAnimation", true);
		//DispatchKeyValue(targetRef, "RandomAnimation", "On");
		SetEntityRenderColor(targetIndex, 255, 255, 255, 64);
		
		PrintToChat(client, "NPC Sequence changed to: %s", sArg);
	}
	return Plugin_Handled;
}

// Chests
/*public int CreateChest(int owner, float vector[3]){
	int entity = CreateEntityByName("prop_dynamic_glow");
	
	if (IsValidEntity(entity)){
		// Move entity to origins
		vector[2] -= 50.0;
		TeleportEntity(entity, vector, NULL_VECTOR, NULL_VECTOR);
		DispatchKeyValue(entity, "Classname", CHEST_CLASSNAME);
		DispatchKeyValue(entity, "model", CHEST_MODEL);
		
		SetEntProp(entity, Prop_Send, "m_usSolidFlags", 12);
		//SetEntProp(entity, Prop_Data, "m_nSolidType", 6);
		SetEntProp(entity, Prop_Data, "m_nSolidType", 4);
		//SetEntProp(entity, Prop_Send, "m_CollisionGroup", 1);
		SetEntProp(entity, Prop_Send, "m_CollisionGroup", 5);
		
		// Set entity's owner
		SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", owner);
		
		DispatchSpawn(entity);
		ActivateEntity(entity);
		
		SetEntProp(entity, Prop_Send, "m_bShouldGlow", true, true);
		SetEntPropFloat(entity, Prop_Send, "m_flGlowMaxDist", 1000.0);
		SetGlowColor(entity, "30 224 231");
		
		SDKHook(entity, SDKHook_StartTouch, HookTouchChest);
		
		// Rotation
		int m_iRotator = CreateEntityByName("func_rotating");
		DispatchKeyValueVector(m_iRotator, "origin", vector);
		DispatchKeyValue(m_iRotator, "targetname", "Item");
		DispatchKeyValue(m_iRotator, "maxspeed", "70");
		DispatchKeyValue(m_iRotator, "friction", "0");
		DispatchKeyValue(m_iRotator, "dmg", "0");
		DispatchKeyValue(m_iRotator, "solid", "0");
		DispatchKeyValue(m_iRotator, "spawnflags", "64");
		DispatchSpawn(m_iRotator);
		
		SetVariantString("!activator");
		AcceptEntityInput(entity, "SetParent", m_iRotator, m_iRotator);
		AcceptEntityInput(m_iRotator, "Start");
		
		SetEntPropEnt(entity, Prop_Send, "m_hEffectEntity", m_iRotator);
		
		return entity;
	}
	return -1;
}
public Action DeleteChest(Handle timer, int ref){
	if (ref == INVALID_ENT_REFERENCE)
		return;
	
	int entity = EntRefToEntIndex(ref);
	
	if (!IsValidEntity(entity))
		return;
	
	char sClassname[32];
	GetEntityClassname(entity, sClassname, sizeof(sClassname));
	
	if (StrEqual(sClassname, CHEST_CLASSNAME))
		AcceptEntityInput(entity, "FadeAndKill");
}
public Action HookTouchChest(int entity, int other){
	if (!IsValidEntity(entity))
		return;
		
	// Get owner's id
	int client = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
	ZPlayer p = ZPlayer(client);
	
	if (!IsPlayerExist(client, true) || other != client)
		return;
	
	int gain = RoundToFloor(GetRandomInt(CHEST_MIN_GOLD, CHEST_MAX_GOLD) * p.flGoldBoost);
	if(gHappyHour) gain = RoundToFloor(gain*HAPPY_HOUR_MULTIPLIER);
	p.iMoney += gain;
	PrintToChat(p.id, "%s Bien! El \x09cofre\x01 contenía \x09%d\x01 de oro!", SERVERSTRING, gain);
	AcceptEntityInput(entity, "FadeAndKill");
	SDKUnhook(entity, SDKHook_StartTouch, HookTouchChest);
}*/

//=====================================================
//					LASERMINES
//=====================================================
void PlantLasermine(int client){
	if(ActualMode.id == view_as<int>(MODE_ASSASSIN)) return;
	
	if (!IsPlayerExist(client, true)) return;
	
	ZPlayer player = ZPlayer(client);
	Handle hTrace = TraceRay(player.id);
	
	if (TR_DidHit(hTrace) && TR_GetEntityIndex(hTrace) < 1){
		int iMine = CreateEntityByName("prop_physics_override");
		int iBeam = CreateEntityByName("env_beam");

		if(iMine && iBeam){
			float flOrigin[3];
			float flNormal[3]; 
			float flBeamEnd[3];
			
			char  sClassname[32];
			char  sDispatch[32];
			//char  sMineClassname[32];

			// End trace
			TR_GetEndPosition(flOrigin, hTrace);
			TR_GetPlaneNormal(hTrace, flNormal);
			
			// Anlges
			GetVectorAngles(flNormal, flNormal);
			
			// End vector
			TR_TraceRayFilter(flOrigin, flNormal, CONTENTS_SOLID, RayType_Infinite, FilterAll);
			TR_GetEndPosition(flBeamEnd, INVALID_HANDLE);
			
			// Remove Lasermine
			player.iLasermine--;

			/////////////////////////////////////////////////////
			// 					Mine parameters
			/////////////////////////////////////////////////////
			//Format(sMineClassname, sizeof(sMineClassname), "Mine_%i", iMine);
			//PrintToChat(player.id, sMineClassname);
			//Format(sDispatch, sizeof(sDispatch), "%s,Break,,0,-1", sMineClassname);
			//PrintToChat(player.id, sDispatch);
			
			DispatchKeyValue(iMine, "targetname", sClassname);
			
			// Mine model
			SetEntityModel(iMine, MODEL_MINE);
			
			// Set explosion fake damage for the Lasermine
			DispatchKeyValue(iMine, "ExplodeDamage", "1");
			DispatchKeyValue(iMine, "ExplodeRadius", "1");
			
			// Spawn the Lasermine into the world
			DispatchKeyValue(iMine, "spawnflags", "3");
			DispatchSpawn(iMine);
			
			// Disable any physics
			AcceptEntityInput(iMine, "DisableMotion");
			SetEntityMoveType(iMine, MOVETYPE_NONE);
			
			// Teleport the Lasermine
			TeleportEntity(iMine, flOrigin, flNormal, NULL_VECTOR);

			////////////////////////////////////
			//SetEntProp(iMine, Prop_Send, "m_usSolidFlags", 12); // FSOLID_NOT_SOLID|FSOLID_TRIGGER 
			SetEntProp(iMine, Prop_Data, "m_nSolidType", 6); // SOLID_VPHYSICS 
			SetEntProp(iMine, Prop_Send, "m_CollisionGroup", 1); // COLLISION_GROUP_DEBRIS  
			////////////////////////////////////
			
			// Set health for the Lasermine
			SetEntProp(iMine, Prop_Data, "m_takedamage", 2);
			
			bool bDiffer;
			if (player.iLasermineDefused >= 1){
				SetEntProp(iMine, Prop_Data, "m_iHealth", 75);
				player.iLasermineDefused--;
				bDiffer = true;
			}
			else
				SetEntProp(iMine, Prop_Data, "m_iHealth", LASERMINE_HEALTH);
			
			SetEntPropFloat(iMine, Prop_Send, "m_flModelScale", 1.1);
			
			// Increase mins and maxs
			float vecMins[3], vecMaxs[3];
			GetEntPropVector(iMine, Prop_Send, "m_vecMins", vecMins);
			GetEntPropVector(iMine, Prop_Send, "m_vecMaxs", vecMaxs);
			
			ScaleVector(vecMins, 1.1);
			ScaleVector(vecMaxs, 1.1);
			
			SetEntPropVector(iMine, Prop_Send, "m_vecMins", vecMins); 
			SetEntPropVector(iMine, Prop_Send, "m_vecMaxs", vecMaxs);
			//*********************************************************************
			//* 		   					BEAM          						  *
			//*********************************************************************
			
			// Set modified flags on the beam
			Format(sClassname, sizeof(sClassname), "Beam%i_%i", iBeam, iMine);
			Format(sDispatch, sizeof(sDispatch), "%s,Kill,,0,-1", sClassname);
			DispatchKeyValue(iMine, "OnBreak", sDispatch);
			
			// Set other values on the beam
			DispatchKeyValue(iBeam, "targetname", sClassname);
			DispatchKeyValue(iBeam, "damage", "0");
			DispatchKeyValue(iBeam, "framestart", "0");
			DispatchKeyValue(iBeam, "BoltWidth", "4.0");
			DispatchKeyValue(iBeam, "renderfx", "0");
			DispatchKeyValue(iBeam, "TouchType", "3");
			DispatchKeyValue(iBeam, "framerate", "0");
			DispatchKeyValue(iBeam, "decalname", "Bigshot");
			DispatchKeyValue(iBeam, "TextureScroll", "35");
			DispatchKeyValue(iBeam, "HDRColorScale", "1.0");
			DispatchKeyValue(iBeam, "texture", "materials/sprites/purplelaser1.vmt");
			DispatchKeyValue(iBeam, "life", "0"); 
			DispatchKeyValue(iBeam, "StrikeTime", "1"); 
			DispatchKeyValue(iBeam, "LightningStart", sClassname);
			DispatchKeyValue(iBeam, "spawnflags", "0"); 
			DispatchKeyValue(iBeam, "NoiseAmplitude", "0"); 
			DispatchKeyValue(iBeam, "Radius", "256");
			DispatchKeyValue(iBeam, "renderamt", "100");
			DispatchKeyValue(iBeam, "rendercolor", "0 0 0");
			
			// Turn off the beam
			AcceptEntityInput(iBeam, "TurnOff");
			
			// Set model for the beam
			SetEntityModel(iBeam, "materials/sprites/purplelaser1.vmt");
			
			// Teleport the beam
			TeleportEntity(iBeam, flBeamEnd, NULL_VECTOR, NULL_VECTOR); 
			
			// Set size of the model
			SetEntPropVector(iBeam, Prop_Data, "m_vecEndPos", flOrigin);
			SetEntPropFloat(iBeam, Prop_Data, "m_fWidth", 3.0);
			SetEntPropFloat(iBeam, Prop_Data, "m_fEndWidth", 3.0);
			
			// Sets the owner of the beam
			SetEntPropEnt(iBeam, Prop_Data, "m_hOwnerEntity", player.id);
			SetEntPropEnt(iMine, Prop_Data, "m_hMoveChild", iBeam);
			SetEntPropEnt(iBeam, Prop_Data, "m_hEffectEntity", iMine);
			
			//*********************************************************************
			//* 		   				   OTHER          					  	  *
			//*********************************************************************
			
			// Send data to the timer
			DataPack hPack = new DataPack();
			hPack.WriteCell(iMine);
			hPack.WriteCell(iBeam);
			hPack.WriteCell(bDiffer);
			hPack.WriteCell(player.id);
			hPack.WriteString(sClassname);
			
			// Create activation time
			CreateTimer(0.8, OnActivateLaser, hPack, TIMER_FLAG_NO_MAPCHANGE);
			
			// Hook damage of the Lasermine
			SDKHook(iMine, SDKHook_OnTakeDamage, MineOnTakeDamage);
			
			#if defined LASERMINES_USE_SOUNDS
			// Emit sound
			EmitSoundToAll("*/MassiveInfection/mine_deploy2.mp3", player.id, SNDCHAN_STATIC);
			EmitSoundToAll("*/MassiveInfection/mine_charge2.mp3", iMine, SNDCHAN_STATIC);
			#endif
		}
	}

	// Close trace handle
	delete hTrace;
}
public Action OnActivateLaser(Handle hTimer, any hPack){
	ResetPack(hPack);
	
	int iMine = ReadPackCell(hPack);
	int iBeam = ReadPackCell(hPack);
	bool bDamaged = ReadPackCell(hPack);
	int iOwner = ReadPackCell(hPack);
	
	if (!IsPlayerExist(iOwner, true)){
		AcceptEntityInput(iMine, "break");
		return Plugin_Stop;
	}
	
	char sClassname[32];
	ReadPackString(hPack, sClassname, sizeof(sClassname));
	
	#if defined LASERMINES_USE_SOUNDS
	// If entity is valid, then emit activation sound
	if (IsValidEdict(iMine)){
		EmitSoundToAll("*/MassiveInfection/mine_activate2.mp3", iMine, SNDCHAN_STATIC);
	}
	#endif
	
	if (!IsValidEdict(iMine)){
		LogError("Mine inválida con index %d", iMine);
		delete view_as<DataPack>(hPack);
		return Plugin_Stop;
	}
	
	if(IsValidEdict(iBeam)){
		char sDispatch[32];
		
		AcceptEntityInput(iBeam, "TurnOn");
		
		if (bDamaged)
			SetEntityRenderColor(iBeam, 255, 70, 70);
		else
			SetEntityRenderColor(iBeam, 0, 255, 0);
		
		Format(sDispatch, sizeof(sDispatch), "%s,TurnOff,,0.001,-1", sClassname);
		DispatchKeyValue(iBeam, "OnTouchedByEntity", sDispatch);
		Format(sDispatch, sizeof(sDispatch), "%s,TurnOn,,0.002,-1", sClassname);
		DispatchKeyValue(iBeam, "OnTouchedByEntity", sDispatch);
		//AcceptEntityInput(iMine, "break" );*/
		
		// Set modified flags on Lasermine
		SetEntProp(iMine, Prop_Data, "m_nSolidType", 6);
		SetEntProp(iMine, Prop_Data, "m_CollisionGroup", 5);
		
		// Check if a player is stuck with the mine
		for (int i = 1; i <= MaxClients; i++){
			ZPlayer obstructor = ZPlayer(i);
			if (IsPlayerStuck(obstructor.id, iMine)){
				if (GetClientByLasermine(iMine) != obstructor.id && obstructor.iTeamNum == CS_TEAM_CT){
					//ZPlayer owner = ZPlayer(GetClientByLasermine(iMine));
					ZPlayer owner = ZPlayer(iOwner);
					
					if (GetEntProp(iMine, Prop_Data, "m_iHealth") < LASERMINE_HEALTH)
						owner.iLasermineDefused++;
					
					owner.iLasermine++;
					
					PrintToChat(owner.id, "%s Se te ha devuelto \x0Funa\x01 mina láser.", SERVERSTRING);
				}
				
				AcceptEntityInput(iMine, "break" );
			}
		}
	}
	delete view_as<DataPack>(hPack);
	return Plugin_Stop;
}
Handle TraceRay(int client){
	#pragma unused client
	
	// Iniliaze vectors
	float flStartEnt[3];
	float flAngle[3]; 
	float flEnd[3];
	
	// Get owner's eye position
	GetClientEyePosition(client, flStartEnt);
	
	// Get owner's eye angles
	GetClientEyeAngles(client, flAngle);
	
	// Get owner's head position
	GetAngleVectors(flAngle, flEnd, NULL_VECTOR, NULL_VECTOR);
	
	// Normalize the vector (equal magnitude at varying distances)
	NormalizeVector(flEnd, flEnd);

	// Counting the vectors start origin
	flStartEnt[0] = flStartEnt[0] + flEnd[0] * 10.0;
	flStartEnt[1] = flStartEnt[1] + flEnd[1] * 10.0;
	flStartEnt[2] = flStartEnt[2] + flEnd[2] * 10.0;

	// Counting the vectors end origin
	flEnd[0] = flStartEnt[0] + flEnd[0] * 80.0;
	flEnd[1] = flStartEnt[1] + flEnd[1] * 80.0;
	flEnd[2] = flStartEnt[2] + flEnd[2] * 80.0;
	
	// Return result of the trace
	return TR_TraceRayFilterEx(flStartEnt, flEnd, CONTENTS_SOLID, RayType_EndPoint, FilterPlayers);
}
public bool FilterAll(int entity, int contentsMask){
	return false;
}
public bool FilterPlayers(int entity, int contentsMask){
	return !(1 <= entity <= MaxClients);
}
stock bool IsEntityLasermine(int entity){
	if (entity <= MaxClients || !IsValidEdict(entity))
		return false;
	
	char sModel[64];
	GetEntPropString(entity, Prop_Data, "m_ModelName", sModel, sizeof(sModel));
	
	return (StrEqual(sModel, MODEL_MINE, false) && GetEntPropEnt(entity, Prop_Data, "m_hMoveChild") != -1) ? true : false;
}
public Action MineOnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype){
	ZPlayer iAttacker = ZPlayer(attacker);
	if (IsEntityLasermine(victim)){
		if (IsPlayerExist(iAttacker.id)){
			if (iAttacker.isType(PT_ZOMBIE)){
				ZClass class = ZClass(iAttacker.iZombieClass);
				damage *= iAttacker.getZombieDamage() * class.flDmg;
				SetEntProp(victim, Prop_Data, "m_iHealth", RoundToFloor(GetEntProp(victim, Prop_Data, "m_iHealth")-damage));
			}
			else if (iAttacker.isType(PT_NEMESIS)){
				damage *= iAttacker.getZombieDamage() * NEMESIS_DAMAGE_TOLM;
				SetEntProp(victim, Prop_Data, "m_iHealth", RoundToFloor(GetEntProp(victim, Prop_Data, "m_iHealth")-damage));
			}
			else if (iAttacker.isType(PT_ASSASSIN)){
				damage = 500.0;
				SetEntProp(victim, Prop_Data, "m_iHealth", RoundToFloor(GetEntProp(victim, Prop_Data, "m_iHealth")-damage));
			}
			else
				return Plugin_Handled;
		}
	}
	return Plugin_Continue;
}
public void LaserOnTouch(const char[] sOutput, int nEntity, int iActivator, float flDelay){
	//PrintToChatAll("laser touched, activator: %d", iActivator);
	ZPlayer player = ZPlayer(iActivator);
	
	if (!IsValidEdict(nEntity))
		return;
		
	//PrintToChatAll("1");
	if (!IsPlayerExist(player.id))
		return;
		
	//PrintToChatAll("2");
	
	if (player.iTeamNum != CS_TEAM_T)
		return;
		
	//PrintToChatAll("3");
	int owner = GetEntPropEnt(nEntity, Prop_Data, "m_hOwnerEntity");
	//PrintToChatAll("owner: %d", owner);
	
	if (!IsPlayerExist(owner, false))
		return;
	//PrintToChatAll("entity: %d", nEntity);
	
	char targetname[64];
	GetEntPropString(nEntity, Prop_Data, "m_iName", targetname, sizeof(targetname));

	char buffers[2][32];
	ExplodeString(targetname, "_", buffers, 2, 32);

	int ent_mine = StringToInt(buffers[1]);

	AcceptEntityInput(ent_mine, "Break");
	AcceptEntityInput(nEntity, "Kill");
	
	//SDKHooks_TakeDamage(activatorIndex, nEntity, ownerIndex, GetConVarFloat(gCvarList[CVAR_LASERMINE_DAMAGE]), DMG_BURN);
}
stock int GetClientByLasermine(int nEntity){
	int iBeam = GetBeamByLasermine(nEntity);
	
	if (iBeam != -1)
		return GetEntPropEnt(iBeam, Prop_Data, "m_hOwnerEntity");
	return -1;
}
stock int GetBeamByLasermine(int entity){
	if (IsEntityLasermine(entity))
		return GetEntPropEnt(entity, Prop_Data, "m_hMoveChild");
	return -1;
}
void DefuseLasermine(int client){
	ZPlayer player = ZPlayer(client);
	Handle hTrace = TraceRay(client);
	int entity = -1;
	
	if (TR_DidHit(hTrace) && (entity = TR_GetEntityIndex(hTrace)) > MaxClients){
		if (GetClientByLasermine(entity) == player.id){
			player.bInLmAction = true;
			
			if (GetEntProp(entity, Prop_Data, "m_iHealth") < LASERMINE_HEALTH){
				PrintToChat(player.id, "%s Quitaste una mina \x0Adañada\x01, plantarla nuevamente le dará vida reducida.", SERVERSTRING);
				player.iLasermineDefused++;
			}
			
			AcceptEntityInput(entity, "Kill");
			player.iLasermine++;
			player.bInLmAction = false;
			
			#if defined LASERMINES_USE_SOUNDS
			// Emit sound
			EmitSoundToAll("items/itempickup.wav", entity, SNDCHAN_STATIC);
			#endif
		}
		else if (GetClientByLasermine(entity) != player.id){
			if (IsPlayerExist(GetClientByLasermine(entity), false)){
				char name[32];
				GetClientName(GetClientByLasermine(entity), name, sizeof(name));
				PrintToChat(player.id, "%s Esta lasermine pertenece a \x09%s\x01!", SERVERSTRING, name);
			}
			else{
				if (!IsEntityLasermine(entity))
					return;
				
				PrintToChat(player.id, "%s Esta lasermine no te pertenece!", SERVERSTRING);
				PrintToChat(player.id, "%s \x09Reporte de errores: eliminando mina\x01", SERVERSTRING);
				LogError("[DEBUG] Lasermines: Hay una mina plantada sin dueño conectado! (player.id: %d)", player.id);
				AcceptEntityInput(entity, "Kill");
			}
		}
	}
	delete hTrace;
}
stock void RemoveLasermines(int client){
	ZPlayer player = ZPlayer(client);
	
	int nGetMaxEnt = GetMaxEntities();
	
	for (int nEntity = 0; nEntity <= nGetMaxEnt; nEntity++){
		if (GetClientByLasermine(nEntity) == player.id){
			AcceptEntityInput(nEntity, "Kill");
		}
	}
	
	player.iLasermine = LASERMINE_QUANTITY;
	player.iLasermineDefused = 0;
}

//=====================================================
//					GRENADES & HOOKS
//=====================================================
public void OnEntityCreated(entity, const char[] classname){
	if(StrContains(classname, "_projectile") != -1) SDKHook(entity, SDKHook_SpawnPost, GrenadeSpawnPost);
}
public void GrenadeSpawnPost(int entity){
	int client = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
	if (client == -1) return;
	
	ZPlayer owner = ZPlayer(client);
	
	char classname[64];
	GetEdictClassname(entity, classname, 64);
	
	// Hook every grenade projectile entity
	// -> HERE!
	if (!strcmp(classname, "hegrenade_projectile")){
		int iReference = EntIndexToEntRef(entity);
		
		// Create a proper event for our purposes
		CreateTimer(1.3, CreateEventBurnDetonate, iReference, TIMER_FLAG_NO_MAPCHANGE);
		
		// Cancel original explosion
		CreateTimer(0.1, TimerOnGrenadeCreated, iReference);
		
		BeamFollowCreate(entity, BurnColor);
	}
	else if (!strcmp(classname, "flashbang_projectile")){
		// Lets create a datapack to ensure effectivity
		DataPack hPack = new DataPack();
		hPack.WriteCell(entity);
		
		// Is owner zombie or human?
		hPack.WriteCell(owner.isType(PT_HUMAN) || owner.isType(PT_SURVIVOR) || owner.isType(PT_GUNSLINGER) || owner.isType(PT_SNIPER));
		
		if (owner.isType(PT_ZOMBIE))
			SetEntPropString(entity, Prop_Data, "m_iName", "infectgrenade_projectile");
		
		// Now we can continue
		BeamFollowCreate(entity, owner.isType(PT_ZOMBIE) ? InfectColor : FreezeColor);
		CreateTimer(1.3, CreateEventGrenadeDetonate, hPack, TIMER_FLAG_NO_MAPCHANGE);
		
		int iReference = EntIndexToEntRef(entity);
		CreateTimer(0.1, TimerOnGrenadeCreated, iReference);
	}
 	else if (!strcmp(classname, "smokegrenade_projectile")){
 		// Get owner's pack
 		ZGrenadePack pack = ZGrenadePack(owner.iGrenadePack); 
 		DataPack hPack = new DataPack();
 		hPack.WriteCell(EntIndexToEntRef(entity));
 		hPack.WriteCell(pack.hasGrenade(AURA_GRENADE));
 		
 		if (pack.hasGrenade(AURA_GRENADE)){
 			SetEntityModel(entity, AURA_DROPPED_GRENADE);
 		}
 		
		CreateTimer(1.3, DoFlashLight, hPack, TIMER_FLAG_NO_MAPCHANGE);
		BeamFollowCreate(entity, FlashColor);
		
		int iReference = EntIndexToEntRef(entity);
		CreateTimer(0.1, TimerOnGrenadeCreated, iReference);
	} 
}
public Action TimerOnGrenadeCreated(Handle timer, any ref){
	int entity = EntRefToEntIndex(ref);
	if(entity != INVALID_ENT_REFERENCE){
		SetEntProp(entity, Prop_Data, "m_nNextThinkTick", -1);
	}
}

// Create again detonation event
public Action CreateEventGrenadeDetonate(Handle timer, any hPack){
	ResetPack(hPack);
	
	int entity = ReadPackCell(hPack);
	bool bFreeze = ReadPackCell(hPack);
	
	if (!IsValidEdict(entity))
		return Plugin_Stop;
	
	char g_szClassname[64];
	GetEdictClassname(entity, g_szClassname, sizeof(g_szClassname));
	
	if (!strcmp(g_szClassname, "flashbang_projectile", false)){
		float origin[3];
		GetEntPropVector(entity, Prop_Send, "m_vecOrigin", origin);
		int client = GetEntPropEnt(entity, Prop_Send, "m_hThrower");
		
		bFreeze ? FreezeSplash(client, origin) : ZombiefySplash(client, origin);
		
		AcceptEntityInput(entity, "kill");
	}
	delete view_as<DataPack>(hPack);
	return Plugin_Stop;
}

// Create burning grenade detonation event
public Action CreateEventBurnDetonate(Handle timer, any ref){
	int entity = EntRefToEntIndex(ref);
	
	if (!IsValidEdict(entity))
		return Plugin_Stop;
		
	char sClassname[64];
	GetEdictClassname(entity, sClassname, sizeof(sClassname));
	
	if (!strcmp(sClassname, "hegrenade_projectile", false)){
		float origin[3];
		GetEntPropVector(entity, Prop_Send, "m_vecOrigin", origin);
		int client = GetEntPropEnt(entity, Prop_Send, "m_hThrower");
		
		BurningSplash(client, origin);
		
		AcceptEntityInput(entity, "kill");
	}
	return Plugin_Stop;
}

// Projectile's beam
void BeamFollowCreate(int entity, int color[4]){
	TE_SetupBeamFollow(entity, BeamSprite,	0, 1.0, 10.0, 10.0, 5, color);
	TE_SendToAll();	
}

// Kill original projectile
public Action DeleteEntity(Handle timer, any entref){
	int entity = EntRefToEntIndex(entref);
	if (IsValidEdict(entity))
		AcceptEntityInput(entity, "kill");
}
public bool FilterTarget(int entity, int contentsMask, any data){
	return (data == entity);
}

//=====================================================
//					INFECTION GRENADE
//=====================================================
void ZombiefySplash(int client, float origin[3]){
	ZPlayer attacker = ZPlayer(client);
	origin[2] += 10.0;
	
	float targetOrigin[3];
	
	int infected = 0;
	
	for (int i = 1; i <= MaxClients; i++){
		if (!IsClientInGame(i) || !IsPlayerAlive(i))
			continue;
		
		ZPlayer victim = ZPlayer(i);
		
		GetClientAbsOrigin(i, targetOrigin);
		targetOrigin[2] += 2.0;
		if (GetVectorDistance(origin, targetOrigin) <= FREEZE_DISTANCE){
			Handle trace = TR_TraceRayFilterEx(origin, targetOrigin, MASK_SOLID, RayType_EndPoint, FilterTarget, i);
			if ((TR_DidHit(trace) && TR_GetEntityIndex(trace) == i) || (GetVectorDistance(origin, targetOrigin) <= 100.0)){
				if(fnGetHumans() <= 1) continue;
				if(victim.isType(PT_HUMAN)){
					victim.Zombiefy(ZOMBIE);
					PrintHintText(victim.id, "Fuiste infectado por una granada de infección");
					infected++;
					attacker.iGrenadeInfection++;
				}
				delete trace;
			}
			else
			{
				delete trace;
				
				GetClientEyePosition(i, targetOrigin);
				targetOrigin[2] -= 2.0;
		
				trace = TR_TraceRayFilterEx(origin, targetOrigin, MASK_SOLID, RayType_EndPoint, FilterTarget, i);
			
				if ((TR_DidHit(trace) && TR_GetEntityIndex(trace) == i) || (GetVectorDistance(origin, targetOrigin) <= 100.0)){
					if(fnGetHumans() <= 1) continue;
					if(victim.isType(PT_HUMAN)){
						victim.Zombiefy(ZOMBIE);
						PrintHintText(victim.id, "Fuiste infectado por una granada de infección");
						infected++;
					}
				}
				delete trace;
			}
		}
	}
	if(fnGetHumans() < 1){
		RoundEndOnValidate(true);
	}
	
	if (infected){
		DataPack hPack = new DataPack();
		hPack.WriteCell(attacker.id);
		hPack.WriteCell(infected);
		
		CreateTimer(2.0, PrintInfectionMessage, hPack);
	}
	
	TE_SetupBeamRingPoint(origin, 10.0, FREEZE_DISTANCE, g_beamsprite, g_halosprite, 1, 1, 0.2, 100.0, 1.0, InfectColor, 0, 0);
	TE_SendToAll();
	LightCreate(FLASH, origin);
}
public Action PrintInfectionMessage(Handle hTimer, any hPack){
	ResetPack(hPack);

	int client = ReadPackCell(hPack);
	int infected = ReadPackCell(hPack);
	
	ZPlayer player = ZPlayer(client);
	
	int total = player.calculateGain(infected*250);
	PrintToChat(player.id, "%s Lograste \x09%d\x01 ammopacks con tu granada de infección.", SERVERSTRING, total);
	if (total >= 1 && player.iLevel < RESET_LEVEL && bAllowGain)
		player.iExp += total;
	
	delete view_as<DataPack>(hPack);
	return Plugin_Stop;
}

//=====================================================
//					FREEZE GRENADE
//=====================================================
void FreezeSplash(int client, float origin[3]){
	origin[2] += 10.0;
	
	float targetOrigin[3];
	for (int i = 1; i <= MaxClients; i++){
		if (!IsClientInGame(i) || !IsPlayerAlive(i))
			continue;
		
		GetClientAbsOrigin(i, targetOrigin);
		targetOrigin[2] += 2.0;
		if (GetVectorDistance(origin, targetOrigin) <= FREEZE_DISTANCE){
			Handle trace = TR_TraceRayFilterEx(origin, targetOrigin, MASK_SOLID, RayType_EndPoint, FilterTarget, i);
			if ((TR_DidHit(trace) && TR_GetEntityIndex(trace) == i) || (GetVectorDistance(origin, targetOrigin) <= 100.0)){
				Freeze(i, client, FREEZE_DURATION);
				delete trace;
			}
			else
			{
				delete trace;
				
				GetClientEyePosition(i, targetOrigin);
				targetOrigin[2] -= 2.0;
		
				trace = TR_TraceRayFilterEx(origin, targetOrigin, MASK_SOLID, RayType_EndPoint, FilterTarget, i);
			
				if ((TR_DidHit(trace) && TR_GetEntityIndex(trace) == i) || (GetVectorDistance(origin, targetOrigin) <= 100.0)){
					Freeze(i, client, FREEZE_DURATION);
				}
				
				delete trace;
			}
		}
	}
	
	TE_SetupBeamRingPoint(origin, 10.0, FREEZE_DISTANCE, g_beamsprite, g_halosprite, 1, 1, 0.2, 100.0, 1.0, FreezeColor, 0, 0);
	TE_SendToAll();
	LightCreate(FLASH, origin);
}
bool Freeze(int client, int attacker, float time){
	ZPlayer player = ZPlayer(client);
	#pragma unused attacker
	float dummy_duration = time;
	if(!player.isType(PT_ZOMBIE)) return false;
	
	if (player.hFreezeTimer != INVALID_HANDLE){
		KillTimer(player.hFreezeTimer);
		player.hFreezeTimer = INVALID_HANDLE;
	}
	
	SetEntityMoveType(client, MOVETYPE_NONE);
	TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, NULL_VELOCITY);
	
	float vec[3];
	GetClientEyePosition(client, vec);
	vec[2] -= 50.0;
	//EmitAmbientSound(SOUND_FREEZE, vec, client, SNDLEVEL_RAIDSIREN);
	EmitSoundToAll(SOUND_FREEZE, SOUND_FROM_WORLD, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL, -1, vec);

	TE_SetupGlowSprite(vec, GlowSprite, dummy_duration, 2.0, 50);
	TE_SendToAll();
	
	player.hFreezeTimer = CreateTimer(dummy_duration, Unfreeze, client, TIMER_FLAG_NO_MAPCHANGE);
	
	return true;
}
public Action Unfreeze(Handle timer, any client){
	ZPlayer player = ZPlayer(client);
	
	if (player.hFreezeTimer != INVALID_HANDLE){
		SetEntityMoveType(client, MOVETYPE_WALK);
		player.hFreezeTimer = INVALID_HANDLE;
	}
}

// Start of light grenade
void LightCreate(int grenade, float pos[3]){
	int iEntity = CreateEntityByName("light_dynamic");
	DispatchKeyValue(iEntity, "inner_cone", "0");
	DispatchKeyValue(iEntity, "cone", "80");
	DispatchKeyValue(iEntity, "brightness", "1");
	DispatchKeyValueFloat(iEntity, "spotlight_radius", 150.0);
	DispatchKeyValue(iEntity, "pitch", "90");
	DispatchKeyValue(iEntity, "style", "1");
	switch(grenade){
		case SMOKE :{
			DispatchKeyValue(iEntity, "_light", "128 128 128 255");
			DispatchKeyValueFloat(iEntity, "distance", LIGHT_DISTANCE);
			//EmitSoundToAll("items/nvg_on.wav", iEntity, SNDCHAN_WEAPON);
			EmitSoundToAll("items/nvg_on.wav", SOUND_FROM_WORLD, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL, -1, pos);
			CreateTimer(LIGHT_DURATION, DeleteEntity, EntIndexToEntRef(iEntity), TIMER_FLAG_NO_MAPCHANGE);
		}
		case FLASH :{
			DispatchKeyValue(iEntity, "_light", "75 75 255 255");
			DispatchKeyValueFloat(iEntity, "distance", FREEZE_DISTANCE);
			//EmitSoundToAll(SOUND_FREEZE_EXPLODE, iEntity, SNDCHAN_WEAPON);
			EmitSoundToAll(SOUND_FREEZE_EXPLODE, SOUND_FROM_WORLD, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL, -1, pos);
			CreateTimer(0.2, DeleteEntity, EntIndexToEntRef(iEntity), TIMER_FLAG_NO_MAPCHANGE);
		}
	}
	DispatchSpawn(iEntity);
	TeleportEntity(iEntity, pos, NULL_VECTOR, NULL_VECTOR);
	AcceptEntityInput(iEntity, "TurnOn");
}

// Start of burning grenade
void BurningSplash(int client, float origin[3]){
	origin[2] += 10.0;
	
	#pragma unused client
	
	float targetOrigin[3];
	for (int i = 1; i <= MaxClients; i++){
		if (!IsClientInGame(i) || !IsPlayerAlive(i))
			continue;
		
		GetClientAbsOrigin(i, targetOrigin);
		targetOrigin[2] += 2.0;
		if (GetVectorDistance(origin, targetOrigin) <= BURN_DISTANCE){
			Handle trace = TR_TraceRayFilterEx(origin, targetOrigin, MASK_SOLID, RayType_EndPoint, FilterTarget, i);
			if ((TR_DidHit(trace) && TR_GetEntityIndex(trace) == i) || (GetVectorDistance(origin, targetOrigin) <= 100.0)){
				ZPlayer victim = ZPlayer(i);
				if (victim.iTeamNum == CS_TEAM_T)
					IgniteEntity(victim.id, victim.isBoss() ? 1.0 : BURN_DURATION);
				delete trace;
			}
			else{
				delete trace;
				
				GetClientEyePosition(i, targetOrigin);
				targetOrigin[2] -= 2.0;
		
				trace = TR_TraceRayFilterEx(origin, targetOrigin, MASK_SOLID, RayType_EndPoint, FilterTarget, i);
			
				if ((TR_DidHit(trace) && TR_GetEntityIndex(trace) == i) || (GetVectorDistance(origin, targetOrigin) <= 100.0)){
					ZPlayer victim = ZPlayer(i);
					if (victim.iTeamNum == CS_TEAM_T)
						IgniteEntity(i, victim.isBoss() ? 1.0 : BURN_DURATION);
				}
				
				delete trace;
			}
		}
	}
	
	TE_SetupBeamRingPoint(origin, 10.0, BURN_DISTANCE, g_beamsprite, g_halosprite, 1, 1, 0.2, 100.0, 1.0, BurnColor, 0, 0);
	TE_SendToAll();
	LightCreate(FLASH, origin);
}

// Cancel explosion sound
public Action NormalSHook(int clients[64], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags){
	if (!strcmp(sample, "^weapons/smokegrenade/sg_explode.wav"))
		return Plugin_Handled;
	
	return Plugin_Continue;
}
public Action DoFlashLight(Handle timer, any hPack){
	ResetPack(hPack);
	
	int entref = ReadPackCell(hPack);
	bool bAura = ReadPackCell(hPack);
	
	int entity = EntRefToEntIndex(entref);
	if (!IsValidEdict(entity)){
		return Plugin_Stop;
	}
		
	char g_szClassname[64];
	GetEdictClassname(entity, g_szClassname, sizeof(g_szClassname));
	if (!strcmp(g_szClassname, "smokegrenade_projectile", false)){
		float origin[3];
		GetEntPropVector(entity, Prop_Send, "m_vecOrigin", origin);
		origin[2] += 50.0;
		LightCreate(SMOKE, origin);
		AcceptEntityInput(entity, "kill");
		
		// Spawn aura shield when the grenade pack contains one
		if (bAura){
			origin[2] -= 50.0;
			CreateAuraShield(origin);
		}
	}
	delete view_as<DataPack>(hPack);
	return Plugin_Stop;
}

//=====================================================
//					ZOMBIE MADNESS
//=====================================================
bool ZombieMadnessBegin(int client, float fSeconds = 6.0){
	ZPlayer player = ZPlayer(client);
	
	if (player.bInvulnerable || !ActualMode.bZombieMadnessAvailable){
		PrintToChat(player.id, "%s Acción no permitida", SERVERSTRING);
		return false;
	}
	
	player.bInvulnerable = true;
	
	float ori[3];
	GetClientAbsOrigin(client, ori);
	EmitAmbientSound(ZOMBIE_MADNESS_SOUND, ori, client, SNDLEVEL_NORMAL, _, 0.5);
	
	Unfreeze(player.hFreezeTimer, player.id);
	
	CreateTimer(fSeconds, ZombieMadnessEnd, player.id, TIMER_FLAG_NO_MAPCHANGE);
	
	return true;
}
public Action ZombieMadnessEnd(Handle hTimer, any client){
	ZPlayer player = ZPlayer(client);
	
	if (!IsPlayerExist(player.id))
		return Plugin_Stop;
		
	player.bInvulnerable = false;
	
	return Plugin_Stop;
}

// Get ammount of active users
void CheckQuantityPlaying(){
	
	if (fnGetPlaying() < PLAYERS_TO_GAIN){
		//hServerInfo = CreateTimer(2.0, printServerInfo, INVALID_HANDLE, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
		PrintToChatAll("%s Falta%s \x09%d\x01 usuario%s para activar las ganancias!", SERVERSTRING, PLAYERS_TO_GAIN-fnGetPlaying() > 1 ? "n":"", PLAYERS_TO_GAIN-fnGetPlaying(), PLAYERS_TO_GAIN-fnGetPlaying() > 1 ? "s":"");
		bAllowGain = false;
		
		if (fnGetPlaying() == 1){
			CS_TerminateRound(1.0, CSRoundEnd_CTWin, false);
		}
		else if (fnGetPlaying() == PLAYERS_TO_GAIN-1){
			PrintToChatAll("%s GANANCIAS \x09DESHABILITADAS\x01.", SERVERSTRING);
		}
		return;
	}
	else if (fnGetPlaying() == PLAYERS_TO_GAIN){
		CS_TerminateRound(0.2, CSRoundEnd_CTWin, false);
		PrintToChatAll("%s Reiniciando ronda.", SERVERSTRING);
		PrintToChatAll("%s GANANCIAS \x09HABILITADAS\x01!", SERVERSTRING);
	}
	
	bAllowGain = true;
}
/*public void CheckToTerminate(){

	if (fnGetAlive() == 0){
		CS_TerminateRound(5.0, CSRoundEnd_Draw, false);
	}
	else if (!fnGetAliveInTeam(CS_TEAM_T) && fnGetAliveInTeam(CS_TEAM_CT)){
		CS_TerminateRound(5.0, CSRoundEnd_CTWin, false);
	}
	else if (fnGetAliveInTeam(CS_TEAM_T) && !fnGetAliveInTeam(CS_TEAM_CT)){
		CS_TerminateRound(5.0, CSRoundEnd_TerroristWin, false);
	}
}*/

//=====================================================
//					FREE VIP TEST
//=====================================================
public Action showPruebaVipMenu(int client, int args){
	ZPlayer player = ZPlayer(client);
	Menu menu = new Menu(menuVipPruebaHandler, MenuAction_Start|MenuAction_Select|MenuAction_End);

	menu.SetTitle("Vip de Prueba");
	menu.AddItem("a", "Activar vip de prueba", player.flExpBoost == 1 ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	
	menu.ExitButton = true;
	
	
	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}
public int menuVipPruebaHandler(Menu menu, MenuAction action, int client, int selection){
	switch(action){
		case MenuAction_Start:{
		}
		
		case MenuAction_Select:{
			registerVipPrueba(client);
		}
		
		case MenuAction_End:{
			delete menu;
		}
	}
	return 0;
}
public Action registerVipPrueba(int client){
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	bool executed = false;
	ZPlayer p = ZPlayer(client);
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	PrintToChatAll("client: %d | pj: %d", client, p.iPjSeleccionado);
	char query[128];
	Format(query, sizeof(query), "CALL registerVipPrueba(%d)", p.iPjSeleccionado);
	executed = SQL_FastQuery(db, query);
	SQL_GetError(db, error, sizeof(error));
	PrintToChatAll(error);
	if(executed){
		PrintToChat(client, "%s Vip de prueba creado con exito.", SERVERSTRING);
		PrintToChat(client, "%s Para activarlo debes salir y volver a entrar.", SERVERSTRING);
		checkFechaPrueba(client, p.iPjSeleccionado);
	}
	else{
		PrintToChat(client, "%s Ya has usado tu vip de prueba.", SERVERSTRING);
	}
	
	delete db;
	return Plugin_Handled;
}
public Action checkFechaPrueba(int client, int charId){
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hQuery = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	char query[128];
	Format(query, sizeof(query), "SELECT fechaFin FROM VipPrueba WHERE idChar=%d", charId);
	if (hQuery == null){
		char error2[255];
		hQuery = SQL_PrepareQuery(db, query, error2, sizeof(error2));
		if (hQuery == null){
			PrintToServer(error2);
			
			delete hQuery;
			delete db;
			return Plugin_Handled;
		}
	}
	
	if (!SQL_Execute(hQuery)) {
		PrintToServer("Didn't execute query");
		
		delete hQuery;
		delete db;
		
		return Plugin_Handled;
	}
	int rows = SQL_GetRowCount(hQuery);
	for(int i; i < rows; i++){
		if(SQL_FetchRow(hQuery)){
			char res[32], fyh[2][16], fecha[3][8];
			SQL_FetchString(hQuery, 0, res, sizeof(res));
			
			ExplodeString(res, " ", fyh, 2, 16);
			
			ExplodeString(fyh[0], "-", fecha, 3, 8);
			
			PrintToChat(client, "%s Tu vip de prueba se vence el %s/%s/%s a las %s", SERVERSTRING, fecha[2], fecha[1], fecha[0], fyh[1]);
		}
	}
	
	// ACÁ
	delete db;
	delete hQuery;
	
	return Plugin_Handled;
}


public Action rpoints(int client, int args){
	for(int i = 1; i <= 2458; i++){
		getPointsData(i);
	}
	
	return Plugin_Handled;
}
public Action getPointsData(int charId){
	
	char error[255];
	Database db = SQL_DefConnect(error, sizeof(error));
	DBStatement hUserStmt = null;
	
	if (db == null){
		PrintToServer("Could not connect: %s", error);
	}
	
	if (hUserStmt == null){
		char error2[255];
		hUserStmt = SQL_PrepareQuery(db, "SELECT hDamageLevel, hPenetrationLevel, hDexterityLevel, hResistanceLevel, zDamageLevel, zResistanceLevel, zDexterityLevel, zHealthLevel, HPoints, ZPoints FROM Characters WHERE id = ?", error2, sizeof(error2));
		if (hUserStmt == null){
			PrintToServer(error2);
			
			delete hUserStmt;
			delete db;
			
			return Plugin_Handled;
		}
	}
	SQL_BindParamInt(hUserStmt, 0, charId, false);
	
	if (!SQL_Execute(hUserStmt)) {
		PrintToServer("Didn't executed query");
		
		delete hUserStmt;
		delete db;
		
		return Plugin_Handled;
	}
	
	int rows = SQL_GetRowCount(hUserStmt);
	
	int hPoints, hDmg, hPenetration, hDexterity, hResistance;
	int zPoints, zDmg, zResistance, zDexterity, zHealth;
	
	if(rows <= 0 ){
		PrintToServer("Personaje no encontrado");
		return Plugin_Handled;
	}
	
	// Fetch SQL data
	for(int i; i < rows; i++) {
		if(SQL_FetchRow(hUserStmt)) {
			hDmg = SQL_FetchInt(hUserStmt, 0);
			hPenetration = SQL_FetchInt(hUserStmt, 1);
			hDexterity = SQL_FetchInt(hUserStmt, 2);
			hResistance = SQL_FetchInt(hUserStmt, 3);
			zDmg = SQL_FetchInt(hUserStmt, 4);
			zResistance = SQL_FetchInt(hUserStmt, 5);
			zDexterity = SQL_FetchInt(hUserStmt, 6);
			zHealth = SQL_FetchInt(hUserStmt, 7);
			hPoints = SQL_FetchInt(hUserStmt, 8);
			zPoints = SQL_FetchInt(hUserStmt, 9);
		}
		else break;
	}
	
	int ret = 0;
	for(int i; i < zDmg; 	i++) ret += (ZOMBIE_DAMAGE_COST);
	for(int i; i < zDexterity; 	i++) ret += (ZOMBIE_DEXTERITY_COST);
	for(int i; i < zResistance; 	i++) ret += (ZOMBIE_RESISTANCE_COST);
	for(int i; i < zHealth; 	i++) ret += (ZOMBIE_HEALTH_COST);
	zPoints += ret;
	zDmg = 0;
	zDexterity = 0;
	zResistance = 0;
	zHealth = 0;
	
	ret = 0;
	for(int i; i < hDmg; 	i++) ret += (HUMAN_DAMAGE_COST);
	for(int i; i < hPenetration; 	i++) ret += (HUMAN_PENETRATION_COST);
	for(int i; i < hDexterity; 	i++) ret += (HUMAN_DEXTERITY_COST);
	for(int i; i < hResistance; 	i++) ret += (HUMAN_RESISTANCE_COST);
	hPoints += ret;
	hDmg = 0;
	hPenetration = 0;
	hDexterity = 0;
	hResistance = 0;
	
	char query[256];
	Format(query, sizeof(query), "UPDATE Characters SET hDamageLevel=%d, hPenetrationLevel=%d, hDexterityLevel=%d, hResistanceLevel=%d, zDamageLevel=%d, zResistanceLevel=%d, zDexterityLevel=%d, zHealthLevel=%d, HPoints=%d, ZPoints=%d WHERE id=%d",
														hDmg, hPenetration, hDexterity, hResistance, zDmg, zResistance, zDexterity, zHealth, hPoints, zPoints, charId);
	SQL_FastQuery(db, query);
	
	delete hUserStmt;
	delete db;
	return Plugin_Handled;
}

//=====================================================
//					AMBIENCE EFFECTS
//=====================================================
void VAmbienceApplySunDisable(bool bDisable = false){
	// Find sun entity
	int iSun = FindEntityByClassname(-1, "env_sun");
	
	// If sun is invalid, then stop
	if(iSun == -1){
	    return;
	}
	
	// If default, then re-enable sun rendering
	if(bDisable){
	    // Turn on sun rendering
	    AcceptEntityInput(iSun, "TurnOn");
	    return;
	}
	
	// Turn off sun rendering
	AcceptEntityInput(iSun, "TurnOff");
}
void VAmbienceApplyLightStyle(){
	// Initialize entity index
	int iLight = -1;

	// Searching fog lights entities
	while ((iLight = FindEntityByClassname(iLight, "env_cascade_light")) != -1){ 
		AcceptEntityInput(iLight, "Kill");
	}
}
public Action VAmbienceTest(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	SetLightStyle(0, "b");
	VAmbienceApplySunDisable();
	VAmbienceApplyLightStyle();
	
	return Plugin_Handled;
}

//=====================================================
//					SCREENFADE
//=====================================================
#pragma unused StartSmoothScreenFadeAll
void StartSmoothScreenFadeAll(float time){
	ScreenFadeAll(RoundToNearest(time * 1000.0), RoundToNearest(time * 1000.0), FFADE_OUT, { 0, 0, 0, 255 });
	CreateTimer((time*2.0)+0.1, ScreenFadeIn);
}
public Action screenfade(int client,  int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
	
	/*
	ScreenFadeAll(RoundToNearest(1.2 * 1000.0), RoundToNearest(1.2 * 1000.0), FFADE_OUT, { 0, 0, 0, 255 });
	
	CreateTimer(2.5, ScreenFadeIn);*/
	
	StartArmageddonIntro();
	
	return Plugin_Handled;
}
public Action ScreenFadeIn(Handle hTimer){
	ScreenFadeAll(RoundToNearest(1.0 * 1000.0), RoundToNearest(1.0 * 1000.0), FFADE_IN|FFADE_PURGE, { 0, 0, 0, 255 });
}

//=====================================================
//				PLAYER VISUAL EFFECTS
//=====================================================
public Action testEffects(int client, int args){
	ZPlayer id = ZPlayer(client);
	
	if (!id.bStaff)
		return Plugin_Handled;
		
	for (int i = 1; i <= MaxClients; i++){
		if (!IsPlayerExist(i, true))
			continue;
		
		VEffectSpawnEffect(i);
		
		// Initialize vector variable
		static float flOrigin[3];
		
		// Get client's position
		GetClientAbsOrigin(i, flOrigin);
		
		VEffectBloodDecalFunction(flOrigin);
		VEffectSmokeFunction(flOrigin);
		VEffectDustFunction(flOrigin);
		VEffectEnergySplashFunction(flOrigin);
	}
	
	return Plugin_Handled;
}
void VEffectSpawnEffect(int clientIndex){
	// Initialize vector variable
	static float flOrigin[3];
	
	// Get client's position
	GetClientAbsOrigin(clientIndex, flOrigin);
	
	// Create an fire entity
	int nEntity = CreateEntityByName("info_particle_system");
	
	// If entity isn't valid, then skip
	if(nEntity){
		// Give name to the entity 
		DispatchKeyValue(nEntity, "effect_name", "env_fire_large");
		
		// Sets the origin of the explosion
		DispatchKeyValueVector(nEntity, "origin", flOrigin);
		
		// Spawn the entity into the world
		DispatchSpawn(nEntity);
		
		// Get and modify flags on fired
		SetVariantString("!activator");
		
		// Sets parent to the entity
		AcceptEntityInput(nEntity, "SetParent", clientIndex);
		
		// Activate the enity
		ActivateEntity(nEntity);
		AcceptEntityInput(nEntity, "Start");
		
		// Sets modified flags on entity
		SetVariantString("OnUser1 !self:kill::1000000.0:1");
		AcceptEntityInput(nEntity, "AddOutput");
		AcceptEntityInput(nEntity, "FireUser1");
	}
}
void VEffectBloodDecalFunction(float flOrigin[3]){
	TE_Start("World Decal");
	TE_WriteVector("m_vecOrigin", flOrigin);
	TE_WriteNum("m_nIndex", decalBloodDecal);
	TE_SendToAll();
}
void VEffectSmokeFunction(float flOrigin[3]){
	TE_SetupSmoke(flOrigin, decalSmoke, 130.0, 10);
	TE_SendToAll();
}
void VEffectDustFunction(float flOrigin[3]){
	TE_SetupDust(flOrigin, NULL_VECTOR, 10.0, 1.0);
	TE_SendToAll();
}
void VEffectEnergySplashFunction(float flOrigin[3]){
	TE_SetupEnergySplash(flOrigin, NULL_VECTOR, true);
	TE_SendToAll();
}

// Damage knockback
stock void DamageOnClientKnockBack(int victim, int attacker, float knockbackAmount){
	ZPlayer Victim = ZPlayer(victim);
	ZPlayer Attacker = ZPlayer(attacker);
	
	if (!IsPlayerExist(Victim.id, true) || !IsPlayerExist(Attacker.id, true))
		return;
	
	if (knockbackAmount == 0.0)
		return;

	if(Victim.iTeamNum != CS_TEAM_T){
		return;
	}
	
	if (fnGetZombies() == 1){
		return;
	}
	
	// Initialize vectors
	static float vClientLoc[3];
	static float vEyeAngle[3];
	static float vAttackerLoc[3];
	static float vVelocity[3];
	
	// Get victim's and attacker's position
	Victim.flGetOrigin(vClientLoc);
	Attacker.flGetEyeAngles(vEyeAngle);
	Attacker.flGetEyePosition(vAttackerLoc);
	
	static float vVec[3];
	GetEntPropVector(Victim.id, Prop_Data, "m_vecVelocity", vVec);
	
	float vCache = vVec[2];
	
	// Calculate knockback end-vector
	TR_TraceRayFilter(vAttackerLoc, vEyeAngle, MASK_ALL, RayType_Infinite, FilterPlayers);
	TR_GetEndPosition(vClientLoc);
	
	// Get vector from the given starting and ending points
	MakeVectorFromPoints(vAttackerLoc, vClientLoc, vVelocity);
	
	// Normalize the vector (equal magnitude at varying distances)
	NormalizeVector(vVelocity, vVelocity);
	
	// Apply the magnitude by scaling the vector
	ScaleVector(vVelocity, Victim.isBoss() ? knockbackAmount/4.0 : knockbackAmount);
	
	if(!(GetEntityFlags(Victim.id) & FL_ONGROUND)){
		vVelocity[2] = vCache;
	}

	// Push the player
	Victim.TeleportPlayer(NULL_VECTOR, NULL_VECTOR, vVelocity);
}

//=====================================================
//					AURA SHIELD
//=====================================================
public Action CreateAuraShield(float coords[3]){
	int entity = CreateEntityByName("prop_dynamic_glow");
	
	if (IsValidEntity(entity)){
		// Move entity to origins
		TeleportEntity(entity, coords, NULL_VECTOR, NULL_VECTOR);
		
		DispatchKeyValue(entity, "Classname", AURA_CLASSNAME);
		DispatchKeyValue(entity, "model", AURA_WORLDMODEL);
		DispatchKeyValue(entity, "Solid", "6");
		DispatchKeyValue(entity, "SetGlowEnabled", "1");
		
		// SOLID TEST
		//DispatchKeyValue(entity, "Collisions", "1");
		
		
		SetEntProp(entity, Prop_Send, "m_usSolidFlags", 12); // FSOLID_NOT_SOLID|FSOLID_TRIGGER 
		SetEntProp(entity, Prop_Data, "m_nSolidType", 6); // SOLID_VPHYSICS 
		SetEntProp(entity, Prop_Send, "m_CollisionGroup", 5); // COLLISION_GROUP_DEBRIS
		
		// GLOW THIS SH*T
		SetEntProp(entity, Prop_Send, "m_bShouldGlow", true, true);
		SetEntProp(entity, Prop_Send, "m_nGlowStyle", 1); 
		SetEntPropFloat(entity, Prop_Send, "m_flModelScale", 1.2);
		SetEntPropFloat(entity, Prop_Send, "m_flGlowMaxDist", 100000000.0);
		
		int iColors[4];
		
		iColors[0] = GetRandomInt(35, 255);
		iColors[1] = GetRandomInt(35, 255);
		iColors[2] = GetRandomInt(35, 255);
		iColors[3] = 255;
		
		SetVariantColor(iColors);
		AcceptEntityInput(entity, "SetGlowColor");
		
		SetEntityRenderMode(entity, RENDER_TRANSCOLOR);
		SetEntityRenderColor(entity, 255, 255, 255, 110);
		
		DispatchSpawn(entity);
		ActivateEntity(entity);
		
		RequestFrame(FrameCallback, entity);
		
		//SDKHook(entity, SDKHook_Touch, AuraOnStartTouch);
	}
	return Plugin_Handled;
}
void PushPlayersAwayFromForceField(int client, int iForcefield){
	
	if(IsValidEntity(iForcefield)){
	
		ZPlayer player = ZPlayer(client);
		
		if (!IsPlayerExist(player.id, true))
			return;
			
		if (!player.isType(PT_ZOMBIE) || player.bInvulnerable)
			return;
		
		float clientPos[3], forcefieldPos[3];
		GetClientAbsOrigin(client, clientPos);
		GetEntPropVector(iForcefield, Prop_Send, "m_vecOrigin", forcefieldPos);
		
		float distance = GetVectorDistance(clientPos, forcefieldPos);

		if(distance < 170.0){
		
			SetEntPropEnt(client, Prop_Data, "m_hGroundEntity", -1);
			
			float direction[3];
			SubtractVectors(forcefieldPos, clientPos, direction);
			
			float gravityForce = FindConVar("sv_gravity").FloatValue * (((800.0 * 180.0 / 50) * 20.0) / GetVectorLength(direction,true));
			gravityForce = gravityForce / 20.0;
			
			NormalizeVector(direction, direction);
			ScaleVector(direction, gravityForce);
			
			float playerVel[3];
			GetEntPropVector(client, Prop_Data, "m_vecVelocity", playerVel);

			ScaleVector(direction, distance / 200);
			SubtractVectors(playerVel, direction, direction);
			TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, direction);
		}
	}
}
void UpdateForceFields(){

	for (int index = 0; index < AuraShields.Length; index++){
	
		int iForcefield = EntRefToEntIndex(AuraShields.Get(index));
		if(IsValidEntity(iForcefield)){
		
			for (int client = 1; client <= MaxClients; client++){
			
				if (!IsPlayerExist(client, true))
					continue;
				
				ZPlayer player = ZPlayer(client);
				
				if (!player.isType(PT_ZOMBIE) || player.bInvulnerable)
					continue;
					
				PushPlayersAwayFromForceField(client, iForcefield);
			}
			
			PushAwayFromForceField(iForcefield, "prop_physics*");
			//PushAwayFromForceField(iForcefield, "flashbang_projectile");
		}
		else
			AuraShields.Erase(index);
	}
}
void PushAwayFromForceField(int iForcefield, const char[] classname){

	int iEnt = MaxClients + 1;
	while((iEnt = FindEntityByClassname(iEnt, classname)) != -1){
		/*
		if (StrEqual(classname, "flashbang_projectile")){
		
			char sName[32];
			GetEntPropString(iEnt, Prop_Data, "m_iName", sName, sizeof(sName));
			
			if (!StrEqual(sName, "infectgrenade_projectile"))
				return;
		}*/
	
		float propPos[3], forcefieldPos[3];
		GetEntPropVector(iEnt, Prop_Send, "m_vecOrigin", propPos);
		GetEntPropVector(iForcefield, Prop_Send, "m_vecOrigin", forcefieldPos);
		
		float distance = GetVectorDistance(propPos, forcefieldPos);
		if(distance < 180.0){
			SetEntPropEnt(iEnt, Prop_Data, "m_hGroundEntity", -1);
			
			float direction[3];
			SubtractVectors(forcefieldPos, propPos, direction);
			
			float gravityForce = FindConVar("sv_gravity").FloatValue * (((800.0 * 180.0 / 50) * 20.0) / GetVectorLength(direction,true));
			gravityForce = gravityForce / 20.0;
			
			NormalizeVector(direction, direction);
			ScaleVector(direction, gravityForce);
			
			float playerVel[3];
			GetEntPropVector(iEnt, Prop_Data, "m_vecVelocity", playerVel);

			ScaleVector(direction, distance / 200);
			SubtractVectors(playerVel, direction, direction);
			TeleportEntity(iEnt, NULL_VECTOR, NULL_VECTOR, direction);
		}
	}
}
public void OnGameFrame(){
	UpdateForceFields();
}
public void FrameCallback(any entity){

	if(!IsValidEntity(entity))
		return;
	
	int ref = EntIndexToEntRef(entity);
	
	AuraShields.Push(ref);
	
	CreateTimer(AURA_DURATION, AuraTimer, ref, TIMER_FLAG_NO_MAPCHANGE);
}
public Action AuraTimer(Handle hTimer, any data){
	
	int entity = EntRefToEntIndex(data);
	
	if (!IsValidEntity(entity))
		return;
	
	int index = AuraShields.FindValue(data);
	
	if(index != -1)
		AuraShields.Erase(index);
	
	AcceptEntityInput(entity, "Kill");

}

//=====================================================
//			ZOMBIE CLAWS ANIMATION FIX
//=====================================================
#define	WeaponsValidateKnife(%0) (%0 == CSWeapon_KNIFE || %0 == CSWeapon_KNIFE_GG)
public Action WeaponsOnAnimationFix(int client){
	
	// Get real player index from event key 
	ZPlayer player = ZPlayer(client);

	// Validate client
	if(!IsPlayerExist(player.id)){
		return;
	}
	
	if (!player.isType(PT_ZOMBIE))
		return;

	// Convert weapon index to ID
	CSWeaponID weaponID = WeaponsGetID(GetEntPropEnt(player.id, Prop_Data, "m_hActiveWeapon"));
	
	// If weapon isn't valid, then stop
	if(!CS_IsValidWeaponID(weaponID)){
		return;
	}
	
	// Get view index
	int viewIndex = GetEntPropEnt(player.id, Prop_Send, "m_hViewModel");
	
	// If weapon isn't valid, then stop
	if(IsValidEdict(viewIndex)){
	
		// Initialize variable
		static int nOldSequence[MAXPLAYERS+1]; static float flOldCycle[MAXPLAYERS+1];
		
		// Get the sequence number and it's playing time
		int nSequence = GetEntProp(viewIndex, Prop_Send, "m_nSequence");
		float flCycle = GetEntPropFloat(viewIndex, Prop_Data, "m_flCycle");
		
		// Validate a knife
		if(WeaponsValidateKnife(weaponID) && player.isType(PT_ZOMBIE)){
		
			// Validate animation delay
			if(nSequence == nOldSequence[player.id] && flCycle < flOldCycle[player.id]){
				switch (nSequence){
					case 3 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 4);
					case 4 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 3);
					case 5 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 6);
					case 6 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 5);
					case 7 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 8);
					case 8 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 7);
					case 9 : SetEntProp(viewIndex, Prop_Send, "m_nSequence", 10);
					case 10: SetEntProp(viewIndex, Prop_Send, "m_nSequence", 11); 
					case 11: SetEntProp(viewIndex, Prop_Send, "m_nSequence", 10);
				}
			}
		}
		else{
			// Initialize variable
			static int nPrevSequence[MAXPLAYERS+1]; static float flDelay[MAXPLAYERS+1];

			// Returns the game time based on the game tick
			float flCurrentTime = GetEngineTime();
			
			// Play previous animation
			if(nPrevSequence[player.id] != 0 && flDelay[player.id] < flCurrentTime){
			
				SetEntProp(viewIndex, Prop_Send, "m_nSequence", nPrevSequence[player.id]);
				nPrevSequence[player.id] = 0;
			}
			
			// Validate animation delay
			if(flCycle < flOldCycle[player.id]){
			
				// Validate animation
				if(nSequence == nOldSequence[player.id]){
					SetEntProp(viewIndex, Prop_Send, "m_nSequence", 0);
					nPrevSequence[player.id] = nSequence;
					flDelay[player.id] = flCurrentTime + 0.03;
				}
			}
		}
		
		// Update the animation interval delay
		nOldSequence[player.id] = nSequence;
		flOldCycle[player.id] = flCycle;
	}
}
stock CSWeaponID WeaponsGetID(int weaponIndex){

	// If weapon isn't valid, then stop
	if(!IsValidEdict(weaponIndex)){
		return CSWeapon_NONE;
	}
	
	// Initialize char
	static char sClassname[16];
	
	// Get weapon classname and convert it to alias
	GetEntityClassname(weaponIndex, sClassname, sizeof(sClassname));
	ReplaceString(sClassname, sizeof(sClassname), "weapon_", "");
	
	// Convert weapon alias to ID
	return CS_AliasToWeaponID(sClassname);
}

//=====================================================
//					WARM UP
//=====================================================
public void StartWarmUp(int mode){

	if (!newRound)
		return;
	
	int nAlive = fnGetAlive();
	
	// Check if the quantity of users reaches the minimum
	if (!nAlive){
		PrintToServer("[StartMode] No hay usuarios suficientes para iniciar modos.");
		PrintToServer("[StartMode] Iniciando modo de espera de usuarios.");
		//LogError("[StartMode] No hay usuarios suficientes para iniciar algún modo.");
		
		// Initialize mode vars
		ActualMode = ZGameMode(view_as<int>(IN_WAIT));
		lastMode = view_as<int>(IN_WAIT);
		iCounter = ROUND_START_COUNTDOWN;
		
		// Initialize booleans
		newRound = false;
		endRound = false;
		startedRound = true;
		return;
	}
	
	// Initialize mode vars
	ActualMode = ZGameMode(mode);
	lastMode = mode;
	iCounter = ROUND_START_COUNTDOWN;
	
	// Initialize booleans
	newRound = false;
	endRound = false;
	startedRound = true;
	
	PrintHintTextToAll("<font size='22' color='#XXXXXX'>Ronda de</font>\n<font size='37' color='#FF0000'>calentamiento</font>");

	//EmitSoundToAll(AMBIENT_SOUND, SOUND_FROM_PLAYER,_,_,_,0.7);
	//RoundEndOnValidate();
}


void StartArmageddonIntro(){
	float fTime = 4.0;
	
	EmitSoundToAll("*/MassiveInfection/siren.mp3", SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
	ServerCommand("sv_maxvelocity 50");
	StartSmoothScreenFadeAll(fTime);
	SendHudAnnouncementAll(INVALID_HANDLE, 0);
	CreateTimer(fTime*2.5, SendHudAnnouncementAll, 1);
}

public Action SendHudAnnouncementAll(Handle hTimer, any data){
	for (int i = 1; i < MaxClients; i++){
		SendModeAnnouncement(i, data);
	}
	
	if (data){
		FinishArmageddonIntro();
	}
}

stock void SendModeAnnouncement(int client, int value){
	
	if (!IsPlayerExist(client) || IsFakeClient(client))
		return;
	
	char sBuffer[64];
	
	if (value == 0){
		SetHudTextParams(0.40, 0.20, 5.0, GetRandomInt(55, 255), GetRandomInt(55, 255), GetRandomInt(55, 255), 255, 1, 3.0, 0.4, 0.4);
		FormatEx(sBuffer, sizeof(sBuffer), "PREPARADOS PARA LA BATALLA");
	}
	else{
		SetHudTextParams(0.43, 0.20, 5.0, GetRandomInt(55, 255), GetRandomInt(55, 255), GetRandomInt(55, 255), 255, 1, 3.0, 0.4, 0.4);
		FormatEx(sBuffer, sizeof(sBuffer), "MODO ARMAGEDDON!");
	}
	
	// Show hud message	
	ShowSyncHudText(client, hAnnouncerSynchronizer, sBuffer);
}

void FinishArmageddonIntro(){
	
	// Play sound
	EmitSoundToAll("*/MassiveInfection/nemesis1.mp3", SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL);
	
	// Normalize velocity
	ServerCommand("sv_maxvelocity 3500");
	
	// Initialize vars
	int nAlive = fnGetAlive();
	int nMaxZombies = RoundToCeil(nAlive / 2.0);
	float volume = 0.2;
	int nHumans = nAlive - nMaxZombies;
	
	// Turn into the corresponding class
	GameModesTurnIntoZombie(nMaxZombies, nAlive, NEMESIS);
	GameModesTurnIntoHuman(nHumans, nAlive, SURVIVOR);
	
	// Ensure everyone is the class that should be
	for (int i = 1; i <= MaxClients; i++){
		// Get real player index from event key
		ZPlayer player = ZPlayer(i);
		
		// Verify that the client is exist
		if(!IsPlayerExist(player.id, true))
			continue;

		// Verify that the client is human
		if(player.iTeamNum != CS_TEAM_CT)
			continue;
			
		if(player.isType(PT_HUMAN))	player.Zombiefy(NEMESIS);
	}
	
	// Apply to mode counter
	modeCount += 5;
	
	// Play ambience sound
	EmitSoundToAll(AMBIENT_SOUND_ARMAGEDON, SOUND_FROM_PLAYER,SNDCHAN_STATIC,_,_, volume);
}