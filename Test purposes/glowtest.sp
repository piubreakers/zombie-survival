#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <CustomPlayerSkins>

#define DATA "1.1"

bool glowing = false;

public Plugin myinfo =
{
	name = "ZR Glowing",
	author = "Franc1sco franug",
	description = "",
	version = DATA,
	url = "http://steamcommunity.com/id/franug"
}

public void OnPluginStart()
{
	CreateConVar("zr_glowing_version", DATA, "plugin info", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	
	RegConsoleCmd("glow", glow);
	RegConsoleCmd("noglow", noglow);
}

public Action glow(int client, int args){
	if (!glowing){
		for( int i = 1; i <= MaxClients; i++ )
			if (IsClientInGame(i) && IsPlayerAlive(i))SetupGlowSkin(i);
		glowing = true;
	}
	else{
		for( int i = 1; i <= MaxClients; i++ )
			if (IsClientInGame(i) && IsPlayerAlive(i))
			{
				CPS_RemoveSkin(client);
				SetupGlowSkin(i);
			}
	}
	return Plugin_Handled;
}

public Action noglow(int client, int args){
	for( int i = 1; i <= MaxClients; i++ )
		if (IsClientInGame(i) && IsPlayerAlive(i)){
			UnhookGlow(i);
			glowing = false;
		}
	return Plugin_Handled;
}

//Perpare client for glow
void SetupGlowSkin(int client)
{
	char sModel[PLATFORM_MAX_PATH];
	GetClientModel(client, sModel, sizeof(sModel));
	int iSkin = CPS_SetSkin(client, sModel, CPS_RENDER);
	
	if (iSkin == -1)
		return;
		
	if (SDKHookEx(iSkin, SDKHook_SetTransmit, OnSetTransmit_GlowSkin))
		SetupGlow(iSkin);
}

//set client glow
void SetupGlow(int iSkin)
{
	int iOffset;
	
	if (!iOffset && (iOffset = GetEntSendPropOffs(iSkin, "m_clrGlow")) == -1)
		return;
	
	SetEntProp(iSkin, Prop_Send, "m_bShouldGlow", true, true);
	SetEntProp(iSkin, Prop_Send, "m_nGlowStyle", 1);
	SetEntPropFloat(iSkin, Prop_Send, "m_flGlowMaxDist", 10000000.0);
	
	int iRed = 155;
	int iGreen = 0;
	int iBlue = 10;

	SetEntData(iSkin, iOffset, iRed, _, true);
	SetEntData(iSkin, iOffset + 1, iGreen, _, true);
	SetEntData(iSkin, iOffset + 2, iBlue, _, true);
	SetEntData(iSkin, iOffset + 3, 255, _, true);
}


//Who can see the glow if vaild
public Action OnSetTransmit_GlowSkin(int iSkin, int client)
{
/*	if(CPS_HasSkin(client) && EntRefToEntIndex(CPS_GetSkin(client)) == iSkin)
	{
		return Plugin_Handled;
	}*/
	
	if (!IsPlayerAlive(client))
		return Plugin_Handled;
	
	return Plugin_Continue;
}


//remove glow
void UnhookGlow(int client)
{
	if (!CPS_HasSkin(client))
		return;
		
	int iSkin = EntRefToEntIndex(CPS_GetSkin(client));
	
	SDKUnhook(iSkin, SDKHook_SetTransmit, OnSetTransmit_GlowSkin);
	
	CPS_RemoveSkin(client);
}